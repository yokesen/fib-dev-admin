<!--begin::Step 3-->
<div data-kt-stepper-element="content">
  <!--begin::Wrapper-->
  <div class="w-100">
    <!--begin::Heading-->
    <div class="pb-10 pb-lg-12">
      <!--begin::Title-->
      <h2 class="fw-bolder text-dark">Open Real Account</h2>
      <!--end::Title-->
      <!--begin::Notice-->
      <div class="text-gray-400 fw-bold fs-6">If you need more info, please check out
        <a href="#" class="link-danger fw-bolder">Help Page</a>.
      </div>
      <!--end::Notice-->
    </div>
    <!--end::Heading-->

    <!--begin::Alert-->
    <!--begin::Notice-->
    <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed p-6 mb-6">
      <!--begin::Icon-->
      <!--begin::Svg Icon | path: icons/duotone/Code/Warning-1-circle.svg-->
      <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
        <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
          <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
          <rect fill="#000000" x="11" y="7" width="2" height="8" rx="1" />
          <rect fill="#000000" x="11" y="16" width="2" height="2" rx="1" />
        </svg>
      </span>
      <!--end::Svg Icon-->
      <!--end::Icon-->
      <!--begin::Wrapper-->
      <div class="d-flex flex-stack flex-grow-1">
        <!--begin::Content-->
        <div class="fw-bold">
          <h4 class="text-gray-800 fw-bolder">We need your attention!</h4>
          <div class="fs-6 text-gray-600">DOWNLOAD YOUR PLATFORM
            <a href="{{route('viewDownloadMt5')}}" target="_blank" class="fw-bolder link-danger">go to Download MT5</a>
          </div>
        </div>
        <!--end::Content-->
      </div>
      <!--end::Wrapper-->
    </div>
    <!--end::Notice-->
    <!--end::Alert-->
    <!--begin::Row-->
    <div class="row g-10">
      <!--begin::Col-->
      <div class="col-xl-6">
        <div class="d-flex h-100 align-items-center">
          <!--begin::Option-->
          <div class="w-100 d-flex flex-column flex-center rounded-3 bg-light bg-opacity-75 py-15 px-10">
            <!--begin::Heading-->
            <div class="mb-7 text-center">
              <!--begin::Title-->
              <h1 class="text-dark mb-5 fw-boldest">Startup</h1>
              <!--end::Title-->
              <!--begin::Description-->
              <div class="text-gray-400 fw-bold mb-5">Micro Account</div>
              <!--end::Description-->
              <!--begin::Price-->
              <div class="text-center">
                <span class="fs-7 fw-bold opacity-50">Start from</span>
                <span class="mb-2 text-danger">$</span>
                <span class="fs-3x fw-bolder text-danger" data-kt-plan-price-month="39" data-kt-plan-price-annual="399">10</span>

              </div>
              <!--end::Price-->
            </div>
            <!--end::Heading-->
            <!--begin::Features-->
            <div class="w-100 mb-10">
              <!--begin::Item-->
              <div class="d-flex align-items-center mb-5">
                <span class="fw-bold fs-6 text-gray-800 flex-grow-1 pe-3">Min click 0.01</span>
                <!--begin::Svg Icon | path: icons/duotone/Code/Done-circle.svg-->
                <span class="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                    <path
                      d="M16.7689447,7.81768175 C17.1457787,7.41393107 17.7785676,7.39211077 18.1823183,7.76894473 C18.5860689,8.1457787 18.6078892,8.77856757 18.2310553,9.18231825 L11.2310553,16.6823183 C10.8654446,17.0740439 10.2560456,17.107974 9.84920863,16.7592566 L6.34920863,13.7592566 C5.92988278,13.3998345 5.88132125,12.7685345 6.2407434,12.3492086 C6.60016555,11.9298828 7.23146553,11.8813212 7.65079137,12.2407434 L10.4229928,14.616916 L16.7689447,7.81768175 Z"
                      fill="#000000" fill-rule="nonzero" />
                  </svg>
                </span>
                <!--end::Svg Icon-->
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="d-flex align-items-center mb-5">
                <span class="fw-bold fs-6 text-gray-800 flex-grow-1 pe-3">Max Click 10</span>
                <!--begin::Svg Icon | path: icons/duotone/Code/Done-circle.svg-->
                <span class="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                    <path
                      d="M16.7689447,7.81768175 C17.1457787,7.41393107 17.7785676,7.39211077 18.1823183,7.76894473 C18.5860689,8.1457787 18.6078892,8.77856757 18.2310553,9.18231825 L11.2310553,16.6823183 C10.8654446,17.0740439 10.2560456,17.107974 9.84920863,16.7592566 L6.34920863,13.7592566 C5.92988278,13.3998345 5.88132125,12.7685345 6.2407434,12.3492086 C6.60016555,11.9298828 7.23146553,11.8813212 7.65079137,12.2407434 L10.4229928,14.616916 L16.7689447,7.81768175 Z"
                      fill="#000000" fill-rule="nonzero" />
                  </svg>
                </span>
                <!--end::Svg Icon-->
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="d-flex align-items-center mb-5">
                <span class="fw-bold fs-6 text-gray-800 flex-grow-1 pe-3">Spread from 2</span>
                <!--begin::Svg Icon | path: icons/duotone/Code/Done-circle.svg-->
                <span class="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                    <path
                      d="M16.7689447,7.81768175 C17.1457787,7.41393107 17.7785676,7.39211077 18.1823183,7.76894473 C18.5860689,8.1457787 18.6078892,8.77856757 18.2310553,9.18231825 L11.2310553,16.6823183 C10.8654446,17.0740439 10.2560456,17.107974 9.84920863,16.7592566 L6.34920863,13.7592566 C5.92988278,13.3998345 5.88132125,12.7685345 6.2407434,12.3492086 C6.60016555,11.9298828 7.23146553,11.8813212 7.65079137,12.2407434 L10.4229928,14.616916 L16.7689447,7.81768175 Z"
                      fill="#000000" fill-rule="nonzero" />
                  </svg>
                </span>
                <!--end::Svg Icon-->
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="d-flex align-items-center mb-5">
                <span class="fw-bold fs-6 text-gray-400 flex-grow-1">Free Commision</span>
                <!--begin::Svg Icon | path: icons/duotone/Code/Error-circle.svg-->
                <span class="svg-icon svg-icon-1">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                    <path
                      d="M12.0355339,10.6213203 L14.863961,7.79289322 C15.2544853,7.40236893 15.8876503,7.40236893 16.2781746,7.79289322 C16.6686989,8.18341751 16.6686989,8.81658249 16.2781746,9.20710678 L13.4497475,12.0355339 L16.2781746,14.863961 C16.6686989,15.2544853 16.6686989,15.8876503 16.2781746,16.2781746 C15.8876503,16.6686989 15.2544853,16.6686989 14.863961,16.2781746 L12.0355339,13.4497475 L9.20710678,16.2781746 C8.81658249,16.6686989 8.18341751,16.6686989 7.79289322,16.2781746 C7.40236893,15.8876503 7.40236893,15.2544853 7.79289322,14.863961 L10.6213203,12.0355339 L7.79289322,9.20710678 C7.40236893,8.81658249 7.40236893,8.18341751 7.79289322,7.79289322 C8.18341751,7.40236893 8.81658249,7.40236893 9.20710678,7.79289322 L12.0355339,10.6213203 Z"
                      fill="#000000" />
                  </svg>
                </span>
                <!--end::Svg Icon-->
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="d-flex align-items-center mb-5">
                <span class="fw-bold fs-6 text-gray-400 flex-grow-1">Leverage 1:200</span>
                <!--begin::Svg Icon | path: icons/duotone/Code/Error-circle.svg-->
                <span class="svg-icon svg-icon-1">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                    <path
                      d="M12.0355339,10.6213203 L14.863961,7.79289322 C15.2544853,7.40236893 15.8876503,7.40236893 16.2781746,7.79289322 C16.6686989,8.18341751 16.6686989,8.81658249 16.2781746,9.20710678 L13.4497475,12.0355339 L16.2781746,14.863961 C16.6686989,15.2544853 16.6686989,15.8876503 16.2781746,16.2781746 C15.8876503,16.6686989 15.2544853,16.6686989 14.863961,16.2781746 L12.0355339,13.4497475 L9.20710678,16.2781746 C8.81658249,16.6686989 8.18341751,16.6686989 7.79289322,16.2781746 C7.40236893,15.8876503 7.40236893,15.2544853 7.79289322,14.863961 L10.6213203,12.0355339 L7.79289322,9.20710678 C7.40236893,8.81658249 7.40236893,8.18341751 7.79289322,7.79289322 C8.18341751,7.40236893 8.81658249,7.40236893 9.20710678,7.79289322 L12.0355339,10.6213203 Z"
                      fill="#000000" />
                  </svg>
                </span>
                <!--end::Svg Icon-->
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="d-flex align-items-center mb-5">
                <span class="fw-bold fs-6 text-gray-400 flex-grow-1">PO,TP,SL 2 poin</span>
                <!--begin::Svg Icon | path: icons/duotone/Code/Error-circle.svg-->
                <span class="svg-icon svg-icon-1">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                    <path
                      d="M12.0355339,10.6213203 L14.863961,7.79289322 C15.2544853,7.40236893 15.8876503,7.40236893 16.2781746,7.79289322 C16.6686989,8.18341751 16.6686989,8.81658249 16.2781746,9.20710678 L13.4497475,12.0355339 L16.2781746,14.863961 C16.6686989,15.2544853 16.6686989,15.8876503 16.2781746,16.2781746 C15.8876503,16.6686989 15.2544853,16.6686989 14.863961,16.2781746 L12.0355339,13.4497475 L9.20710678,16.2781746 C8.81658249,16.6686989 8.18341751,16.6686989 7.79289322,16.2781746 C7.40236893,15.8876503 7.40236893,15.2544853 7.79289322,14.863961 L10.6213203,12.0355339 L7.79289322,9.20710678 C7.40236893,8.81658249 7.40236893,8.18341751 7.79289322,7.79289322 C8.18341751,7.40236893 8.81658249,7.40236893 9.20710678,7.79289322 L12.0355339,10.6213203 Z"
                      fill="#000000" />
                  </svg>
                </span>
                <!--end::Svg Icon-->
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="d-flex align-items-center">
                <span class="fw-bold fs-6 text-gray-400 flex-grow-1">No Rebate</span>
                <!--begin::Svg Icon | path: icons/duotone/Code/Error-circle.svg-->
                <span class="svg-icon svg-icon-1">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                    <path
                      d="M12.0355339,10.6213203 L14.863961,7.79289322 C15.2544853,7.40236893 15.8876503,7.40236893 16.2781746,7.79289322 C16.6686989,8.18341751 16.6686989,8.81658249 16.2781746,9.20710678 L13.4497475,12.0355339 L16.2781746,14.863961 C16.6686989,15.2544853 16.6686989,15.8876503 16.2781746,16.2781746 C15.8876503,16.6686989 15.2544853,16.6686989 14.863961,16.2781746 L12.0355339,13.4497475 L9.20710678,16.2781746 C8.81658249,16.6686989 8.18341751,16.6686989 7.79289322,16.2781746 C7.40236893,15.8876503 7.40236893,15.2544853 7.79289322,14.863961 L10.6213203,12.0355339 L7.79289322,9.20710678 C7.40236893,8.81658249 7.40236893,8.18341751 7.79289322,7.79289322 C8.18341751,7.40236893 8.81658249,7.40236893 9.20710678,7.79289322 L12.0355339,10.6213203 Z"
                      fill="#000000" />
                  </svg>
                </span>
                <!--end::Svg Icon-->
              </div>
              <!--end::Item-->
            </div>
            <!--end::Features-->
            <!--begin::Select-->
            <a href="#" class="btn btn-lg btn-danger">Select</a>
            <!--end::Select-->
          </div>
          <!--end::Option-->
        </div>
      </div>
      <!--end::Col-->
      <!--begin::Col-->
      <div class="col-xl-6">
        <div class="d-flex h-100 align-items-center">
          <!--begin::Option-->
          <div class="w-100 d-flex flex-column flex-center rounded-3 bg-light bg-opacity-75 py-20 px-10">
            <!--begin::Heading-->
            <div class="mb-7 text-center">
              <!--begin::Title-->
              <h1 class="text-dark mb-5 fw-boldest">Advanced</h1>
              <!--end::Title-->
              <!--begin::Description-->
              <div class="text-gray-400 fw-bold mb-5">Standard Account</div>
              <!--end::Description-->
              <!--begin::Price-->
              <div class="text-center">
                <span class="fs-7 fw-bold opacity-50">Start from</span>
                <span class="mb-2 text-danger">$</span>
                <span class="fs-3x fw-bolder text-danger" data-kt-plan-price-month="339" data-kt-plan-price-annual="3399">100</span>

              </div>
              <!--end::Price-->
            </div>
            <!--end::Heading-->
            <!--begin::Features-->
            <div class="w-100 mb-10">
              <!--begin::Item-->
              <div class="d-flex align-items-center mb-5">
                <span class="fw-bold fs-6 text-gray-800 flex-grow-1 pe-3">Min Clik 0.1</span>
                <!--begin::Svg Icon | path: icons/duotone/Code/Done-circle.svg-->
                <span class="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                    <path
                      d="M16.7689447,7.81768175 C17.1457787,7.41393107 17.7785676,7.39211077 18.1823183,7.76894473 C18.5860689,8.1457787 18.6078892,8.77856757 18.2310553,9.18231825 L11.2310553,16.6823183 C10.8654446,17.0740439 10.2560456,17.107974 9.84920863,16.7592566 L6.34920863,13.7592566 C5.92988278,13.3998345 5.88132125,12.7685345 6.2407434,12.3492086 C6.60016555,11.9298828 7.23146553,11.8813212 7.65079137,12.2407434 L10.4229928,14.616916 L16.7689447,7.81768175 Z"
                      fill="#000000" fill-rule="nonzero" />
                  </svg>
                </span>
                <!--end::Svg Icon-->
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="d-flex align-items-center mb-5">
                <span class="fw-bold fs-6 text-gray-800 flex-grow-1 pe-3">Max Click 500</span>
                <!--begin::Svg Icon | path: icons/duotone/Code/Done-circle.svg-->
                <span class="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                    <path
                      d="M16.7689447,7.81768175 C17.1457787,7.41393107 17.7785676,7.39211077 18.1823183,7.76894473 C18.5860689,8.1457787 18.6078892,8.77856757 18.2310553,9.18231825 L11.2310553,16.6823183 C10.8654446,17.0740439 10.2560456,17.107974 9.84920863,16.7592566 L6.34920863,13.7592566 C5.92988278,13.3998345 5.88132125,12.7685345 6.2407434,12.3492086 C6.60016555,11.9298828 7.23146553,11.8813212 7.65079137,12.2407434 L10.4229928,14.616916 L16.7689447,7.81768175 Z"
                      fill="#000000" fill-rule="nonzero" />
                  </svg>
                </span>
                <!--end::Svg Icon-->
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="d-flex align-items-center mb-5">
                <span class="fw-bold fs-6 text-gray-800 flex-grow-1 pe-3">Spread start from 1.1</span>
                <!--begin::Svg Icon | path: icons/duotone/Code/Done-circle.svg-->
                <span class="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                    <path
                      d="M16.7689447,7.81768175 C17.1457787,7.41393107 17.7785676,7.39211077 18.1823183,7.76894473 C18.5860689,8.1457787 18.6078892,8.77856757 18.2310553,9.18231825 L11.2310553,16.6823183 C10.8654446,17.0740439 10.2560456,17.107974 9.84920863,16.7592566 L6.34920863,13.7592566 C5.92988278,13.3998345 5.88132125,12.7685345 6.2407434,12.3492086 C6.60016555,11.9298828 7.23146553,11.8813212 7.65079137,12.2407434 L10.4229928,14.616916 L16.7689447,7.81768175 Z"
                      fill="#000000" fill-rule="nonzero" />
                  </svg>
                </span>
                <!--end::Svg Icon-->
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="d-flex align-items-center mb-5">
                <span class="fw-bold fs-6 text-gray-800 flex-grow-1 pe-3">Leverage 1:500</span>
                <!--begin::Svg Icon | path: icons/duotone/Code/Done-circle.svg-->
                <span class="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                    <path
                      d="M16.7689447,7.81768175 C17.1457787,7.41393107 17.7785676,7.39211077 18.1823183,7.76894473 C18.5860689,8.1457787 18.6078892,8.77856757 18.2310553,9.18231825 L11.2310553,16.6823183 C10.8654446,17.0740439 10.2560456,17.107974 9.84920863,16.7592566 L6.34920863,13.7592566 C5.92988278,13.3998345 5.88132125,12.7685345 6.2407434,12.3492086 C6.60016555,11.9298828 7.23146553,11.8813212 7.65079137,12.2407434 L10.4229928,14.616916 L16.7689447,7.81768175 Z"
                      fill="#000000" fill-rule="nonzero" />
                  </svg>
                </span>
                <!--end::Svg Icon-->
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="d-flex align-items-center mb-5">
                <span class="fw-bold fs-6 text-gray-800 flex-grow-1 pe-3">Commission 2.5$/lot</span>
                <!--begin::Svg Icon | path: icons/duotone/Code/Done-circle.svg-->
                <span class="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                    <path
                      d="M16.7689447,7.81768175 C17.1457787,7.41393107 17.7785676,7.39211077 18.1823183,7.76894473 C18.5860689,8.1457787 18.6078892,8.77856757 18.2310553,9.18231825 L11.2310553,16.6823183 C10.8654446,17.0740439 10.2560456,17.107974 9.84920863,16.7592566 L6.34920863,13.7592566 C5.92988278,13.3998345 5.88132125,12.7685345 6.2407434,12.3492086 C6.60016555,11.9298828 7.23146553,11.8813212 7.65079137,12.2407434 L10.4229928,14.616916 L16.7689447,7.81768175 Z"
                      fill="#000000" fill-rule="nonzero" />
                  </svg>
                </span>
                <!--end::Svg Icon-->
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="d-flex align-items-center mb-5">
                <span class="fw-bold fs-6 text-gray-400 flex-grow-1">PO,TP,SL 1 Poin</span>
                <!--begin::Svg Icon | path: icons/duotone/Code/Done-circle.svg-->
                <span class="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                    <path
                      d="M16.7689447,7.81768175 C17.1457787,7.41393107 17.7785676,7.39211077 18.1823183,7.76894473 C18.5860689,8.1457787 18.6078892,8.77856757 18.2310553,9.18231825 L11.2310553,16.6823183 C10.8654446,17.0740439 10.2560456,17.107974 9.84920863,16.7592566 L6.34920863,13.7592566 C5.92988278,13.3998345 5.88132125,12.7685345 6.2407434,12.3492086 C6.60016555,11.9298828 7.23146553,11.8813212 7.65079137,12.2407434 L10.4229928,14.616916 L16.7689447,7.81768175 Z"
                      fill="#000000" fill-rule="nonzero" />
                  </svg>
                </span>
                <!--end::Svg Icon-->
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="d-flex align-items-center">
                <span class="fw-bold fs-6 text-gray-400 flex-grow-1">Rebate 0.4$/lot</span>
                <!--begin::Svg Icon | path: icons/duotone/Code/Done-circle.svg-->
                <span class="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                    <path
                      d="M16.7689447,7.81768175 C17.1457787,7.41393107 17.7785676,7.39211077 18.1823183,7.76894473 C18.5860689,8.1457787 18.6078892,8.77856757 18.2310553,9.18231825 L11.2310553,16.6823183 C10.8654446,17.0740439 10.2560456,17.107974 9.84920863,16.7592566 L6.34920863,13.7592566 C5.92988278,13.3998345 5.88132125,12.7685345 6.2407434,12.3492086 C6.60016555,11.9298828 7.23146553,11.8813212 7.65079137,12.2407434 L10.4229928,14.616916 L16.7689447,7.81768175 Z"
                      fill="#000000" fill-rule="nonzero" />
                  </svg>
                </span>
                <!--end::Svg Icon-->
              </div>
              <!--end::Item-->
            </div>
            <!--end::Features-->
            <!--begin::Select-->
            <a href="#" class="btn btn-lg btn-danger">Select</a>
            <!--end::Select-->
          </div>
          <!--end::Option-->
        </div>
      </div>
      <!--end::Col-->
    </div>
    <!--end::Row-->
  </div>
  <!--end::Wrapper-->
</div>
<!--end::Step 3-->
