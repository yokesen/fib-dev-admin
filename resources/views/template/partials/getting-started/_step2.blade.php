<!--begin::Step 2-->
<div data-kt-stepper-element="content">
  <!--begin::Wrapper-->
  <div class="w-100">
    <!--begin::Heading-->
    <div class="pb-10 pb-lg-15">
      <!--begin::Title-->
      <h2 class="fw-bolder text-dark">Trading Academy</h2>
      <!--end::Title-->
      <!--begin::Notice-->
      <div class="text-gray-400 fw-bold fs-6">If you need more info, please check out
        <a href="#" class="link-danger fw-bolder">Help Page</a>.
      </div>
      <!--end::Notice-->
    </div>
    <!--end::Heading-->

    <!--begin::Section-->
    <div class="mb-17">
      <!--begin::Content-->
      <div class="d-flex flex-stack mb-5">
        <!--begin::Title-->
        <h3 class="text-black">Watch this video :</h3>
        <!--end::Title-->
      </div>
      <!--end::Content-->
      <!--begin::Separator-->
      <div class="separator separator-dashed mb-9"></div>
      <!--end::Separator-->
      <!--begin::Row-->
      <div class="row g-10">
        <!--begin::Col-->
        <div class="col-md-6">
          <!--begin::Feature post-->
          <div class="card-xl-stretch me-md-6">
            <!--begin::Image-->
            <a target="_blank" class="d-block bgi-no-repeat bgi-size-cover bgi-position-center card-rounded position-relative min-h-175px mb-5" style="background-image:url('http://i3.ytimg.com/vi/RJoNVVJyXWA/hqdefault.jpg')"
              data-fslightbox="lightbox-video-tutorials" href="https://www.youtube.com/watch?v=RJoNVVJyXWA">
              <img src="{{url('/')}}/assets/media/svg/misc/video-play.svg" class="position-absolute top-50 start-50 translate-middle" alt="" />
            </a>
            <!--end::Image-->
            <!--begin::Body-->
            <div class="m-0">
              <!--begin::Title-->
              <a href="#" class="fs-4 text-dark fw-bolder text-hover-primary text-dark lh-base">Trading 101 - How to start trading</a>
              <!--end::Title-->
              <!--begin::Text-->
              <div class="fw-bold fs-5 text-gray-600 text-dark my-4">We’ve been focused on making shortcut for you to understand your trading journey</div>
              <!--end::Text-->
              <!--begin::Content-->
              <div class="fs-6 fw-bolder">
                <!--begin::Author-->
                <a href="#" class="text-gray-700 text-hover-primary">Gema Goeyardi</a>
                <!--end::Author-->
                <!--begin::Date-->
                <span class="text-muted">on Mar 21 2021</span>
                <!--end::Date-->
              </div>
              <!--end::Content-->
            </div>
            <!--end::Body-->
          </div>
          <!--end::Feature post-->
        </div>
        <!--end::Col-->
        <!--begin::Col-->
        <div class="col-md-6">
          <!--begin::Feature post-->
          <div class="card-xl-stretch mx-md-3">
            <!--begin::Image-->
            <a target="_blank" class="d-block bgi-no-repeat bgi-size-cover bgi-position-center card-rounded position-relative min-h-175px mb-5" style="background-image:url('http://i3.ytimg.com/vi/_eHaJX9dNvY/hqdefault.jpg')"
              data-fslightbox="lightbox-video-tutorials" href="https://www.youtube.com/watch?v=_eHaJX9dNvY">
              <img src="{{url('/')}}/assets/media/svg/misc/video-play.svg" class="position-absolute top-50 start-50 translate-middle" alt="" />
            </a>
            <!--end::Image-->
            <!--begin::Body-->
            <div class="m-0">
              <!--begin::Title-->
              <a href="#" class="fs-4 text-dark fw-bolder text-hover-primary text-dark lh-base">Trading 101 - Everything You Need to Know</a>
              <!--end::Title-->
              <!--begin::Text-->
              <div class="fw-bold fs-5 text-gray-600 text-dark my-4">Before you make any decision, here are stuffs you need to know.</div>
              <!--end::Text-->
              <!--begin::Content-->
              <div class="fs-6 fw-bolder">
                <!--begin::Author-->
                <a href="#" class="text-gray-700 text-hover-primary">Gema Goeyardi</a>
                <!--end::Author-->
                <!--begin::Date-->
                <span class="text-muted">on Apr 14 2021</span>
                <!--end::Date-->
              </div>
              <!--end::Content-->
            </div>
            <!--end::Body-->
          </div>
          <!--end::Feature post-->
        </div>
        <!--end::Col-->
      </div>
      <!--end::Row-->
    </div>
    <!--end::Section-->
  </div>
  <!--end::Wrapper-->
</div>
<!--end::Step 2-->
