
							<!--begin::Container-->
							<div id="kt_content_container" class="container">

								<!--begin::Row-->
								<div class="row gy-5 g-xl-8">
									<!--begin::Col-->
									<div class="col-xxl-4">

										@include('template.partials.widgets.mixed._widget-2')

									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xxl-4">

										@include('template.partials.widgets.mixed._widget-5')

									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xxl-4">

										@include('template.partials.widgets.mixed._widget-7')

										@include('template.partials.widgets.mixed._widget-10')

									</div>
									<!--end::Col-->
								</div>
								<!--end::Row-->
								<!--begin::Row-->
								<div class="row gy-5 gx-xl-8">
									<!--begin::Col-->
									<div class="col-xxl-4">

										@include('template.partials.widgets.lists._widget-3')

									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xl-8">

										@include('template.partials.widgets.tables._widget-9')

									</div>
									<!--end::Col-->
								</div>
								<!--end::Row-->
								<!--begin::Row-->
								<div class="row gy-5 g-xl-8">
									<!--begin::Col-->
									<div class="col-xl-4">

										@include('template.partials.widgets.lists._widget-2')

									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xl-4">

										@include('template.partials.widgets.lists._widget-6')

									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xl-4">

										@include('template.partials.widgets.lists._widget-4')

									</div>
									<!--end::Col-->
								</div>
								<!--end::Row-->
								<!--begin::Row-->
								<div class="row g-5 gx-xxl-8">
									<!--begin::Col-->
									<div class="col-xxl-4">

										@include('template.partials.widgets.mixed._widget-5')

									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xxl-8">

										@include('template.partials.widgets.tables._widget-5')

									</div>
									<!--end::Col-->
								</div>
								<!--end::Row-->

							</div>
							<!--end::Container-->
