<!--begin::Aside Menu-->
<div class="hover-scroll-overlay-y my-2 py-5 py-lg-8" id="kt_aside_menu_wrapper" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer"
	data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">
	<!--begin::Menu-->
	<div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="#kt_aside_menu" data-kt-menu="true">
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'dashboard' ? 'active' : ''}}" href="{{route('viewDashboard')}}">
				<span class="menu-icon">
					<i class="bi bi-house fs-3"></i>
				</span>
				<span class="menu-title">Dashboard</span>
			</a>
		</div>


		<div class="menu-item">
			<div class="menu-content pt-8 pb-2">
				<span class="menu-section text-muted text-uppercase fs-8 ls-1">ADMIN : PARTNER</span>
			</div>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'addNew' ? 'active' : ''}}" href="{{route('viewPartnerAddNew')}}">
				<span class="menu-icon">
					<i class="bi bi-people fs-3"></i>
				</span>
				<span class="menu-title">Add New Partner</span>
			</a>
		</div>

		{{-- <div class="menu-item">
			<a class="menu-link {{ $menu == 'registerFromWeb' ? 'active' : ''}}" href="{{route('viewPartnerRegisterFromWeb')}}">
				<span class="menu-icon">
					<i class="bi bi-people fs-3"></i>
				</span>
				<span class="menu-title">Register from web</span>
			</a>
		</div> --}}

		<div class="menu-item">
			<a class="menu-link {{ $menu == 'partnerList' ? 'active' : ''}}" href="{{route('viewPartnerList')}}">
				<span class="menu-icon">
					<i class="bi bi-person-plus fs-3"></i>
				</span>
				<span class="menu-title">Partner List</span>
			</a>
		</div>
		{{-- <div class="menu-item">
			<a class="menu-link {{ $menu == 'activePartner' ? 'active' : ''}}" href="{{route('viewPartnerActive')}}">
				<span class="menu-icon">
					<i class="bi bi-telephone-outbound fs-3"></i>
				</span>
				<span class="menu-title">Active Partner</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'suspendedPartner' ? 'active' : ''}}" href="{{route('viewPartnerSuspended')}}">
				<span class="menu-icon">
					<i class="bi bi-person-check fs-3"></i>
				</span>
				<span class="menu-title">Suspended Partner</span>
			</a>
		</div> --}}
		{{-- <div class="menu-item">
			<a class="menu-link {{ $menu == 'defaultSetting' ? 'active' : ''}}" href="{{route('viewPartnerdefaultSetting')}}">
				<span class="menu-icon">
					<i class="bi bi-trash fs-3"></i>
				</span>
				<span class="menu-title">Default Setting</span>
			</a>
		</div> --}}

		{{-- <div class="menu-item">
			<div class="menu-content pt-8 pb-2">
				<span class="menu-section text-muted text-uppercase fs-8 ls-1">BUSINESS MANAGEMENT</span>
			</div>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'dailySettlement' ? 'active' : ''}}" href="{{route('viewDailySettlement')}}">
				<span class="menu-icon">
					<i class="bi bi-bar-chart-line fs-3"></i>
				</span>
				<span class="menu-title">Daily Settlement</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewStatement' ? 'active' : ''}}" href="{{route('viewStatement')}}">
				<span class="menu-icon">
					<i class="bi bi-pie-chart fs-3"></i>
				</span>
				<span class="menu-title">Statement</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewCommission' ? 'active' : ''}}" href="{{route('viewCommission')}}">
				<span class="menu-icon">
					<i class="bi bi-award fs-3"></i>
				</span>
				<span class="menu-title">Commission</span>
			</a>
		</div> --}}


		<div class="menu-item">
			<div class="menu-content pt-8 pb-2">
				<span class="menu-section text-muted text-uppercase fs-8 ls-1">ADMIN : RULES</span>
			</div>
		</div>

		<div class="menu-item">
			<a class="menu-link {{ $menu == 'typeList' ? 'active' : ''}}" href="{{route('viewAccountTypeList')}}">
				<span class="menu-icon">
					<i class="bi bi-pie-chart fs-3"></i>
				</span>
				<span class="menu-title">Account Type List</span>
			</a>
		</div>
		{{-- <div class="menu-item">
			<a class="menu-link {{ $menu == 'viewCommission' ? 'active' : ''}}" href="{{route('viewCommission')}}">
				<span class="menu-icon">
					<i class="bi bi-award fs-3"></i>
				</span>
				<span class="menu-title">Account Group List</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewProfitLoss' ? 'active' : ''}}" href="{{route('viewProfitLoss')}}">
				<span class="menu-icon">
					<i class="bi bi-bar-chart-line fs-3"></i>
				</span>
				<span class="menu-title">Account Description</span>
			</a>
		</div> --}}

		{{-- <div class="menu-item">
			<div class="menu-content pt-8 pb-2">
				<span class="menu-section text-muted text-uppercase fs-8 ls-1">TRADER MANAGEMENT</span>
			</div>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewMarginIn' ? 'active' : ''}}" href="{{route('viewMarginIn')}}">
				<span class="menu-icon">
					<i class="bi bi-journal-plus fs-3"></i>
				</span>
				<span class="menu-title">Trader List</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewTradingAccount' ? 'active' : ''}}" href="{{route('viewTradingAccount')}}">
				<span class="menu-icon">
					<i class="bi bi-hdd-network fs-3"></i>
				</span>
				<span class="menu-title">Trading Account List</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewMarginIn' ? 'active' : ''}}" href="{{route('viewMarginIn')}}">
				<span class="menu-icon">
					<i class="bi bi-journal-plus fs-3"></i>
				</span>
				<span class="menu-title">Margin In List</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewMarginOut' ? 'active' : ''}}" href="{{route('viewMarginOut')}}">
				<span class="menu-icon">
					<i class="bi bi-journal-minus fs-3"></i>
				</span>
				<span class="menu-title">Margin Out List</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'viewInternalTransfer' ? 'active' : ''}}" href="{{route('viewInternalTransfer')}}">
				<span class="menu-icon">
					<i class="bi bi-journal-check fs-3"></i>
				</span>
				<span class="menu-title">Internal Transfer List</span>
			</a>
		</div> --}}
@if (profile()->id_cms_privileges == '2')


		<!--DEALING-->
		<div class="menu-item">
			<div class="menu-content pt-8 pb-2">
				<span class="menu-section text-muted text-uppercase fs-8 ls-1">DEALING : GROUP</span>
			</div>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'listRequest' ? 'active' : ''}}" href="{{route('groupRequest')}}">
				<span class="menu-icon">
					<i class="bi bi-people fs-3"></i>
				</span>
				<span class="menu-title">Group Request</span>
			</a>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'listActivation' ? 'active' : ''}}" href="{{route('groupActivation')}}">
				<span class="menu-icon">
					<i class="bi bi-people fs-3"></i>
				</span>
				<span class="menu-title">Group Activation</span>
			</a>
		</div>

		<!--FINANCE-->
		<div class="menu-item">
			<div class="menu-content pt-8 pb-2">
				<span class="menu-section text-muted text-uppercase fs-8 ls-1">FINANCE : RECONSILE</span>
			</div>
		</div>
		<div class="menu-item">
			<a class="menu-link {{ $menu == 'listReconsile' ? 'active' : ''}}" href="{{route('cocokan')}}">
				<span class="menu-icon">
					<i class="bi bi-people fs-3"></i>
				</span>
				<span class="menu-title">Reconsile</span>
			</a>
		</div>

		<div class="menu-item">
			<a class="menu-link {{ $menu == 'partnerDeposit' ? 'active' : ''}}" href="{{route('viewDepositList')}}">
				<span class="menu-icon">
					<i class="bi bi-people fs-3"></i>
				</span>
				<span class="menu-title">In - Out Request</span>
			</a>
		</div>

		<div class="menu-item">
			<a class="menu-link {{ $menu == 'balancesheet' ? 'active' : ''}}" href="{{route('viewReconsileList')}}">
				<span class="menu-icon">
					<i class="bi bi-people fs-3"></i>
				</span>
				<span class="menu-title">Balance Sheet</span>
			</a>
		</div>
@endif
	</div>
	<!--end::Menu-->
</div>
