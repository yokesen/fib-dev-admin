
<!--begin::Menu wrapper-->
<div class="header-menu align-items-stretch" data-kt-drawer="true" data-kt-drawer-name="header-menu" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_header_menu_mobile_toggle" data-kt-place="true" data-kt-place-mode="prepend" data-kt-place-parent="{default: '#kt_body', lg: '#kt_header_nav'}">
	<!--begin::Menu-->
	<div class="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold my-5 my-lg-0 align-items-stretch" id="#kt_header_menu" data-kt-menu="true">
		{{--
		<div class="menu-item me-lg-1">
			<a class="menu-link py-3" href="{{route('viewAccountmt4')}}">
				<span class="menu-title text-warning">Request Account MT4</span>
			</a>
		</div>

		<div class="menu-item me-lg-1">
			<a class="menu-link py-3" href="{{route('viewAccountmt5')}}">
				<span class="menu-title">Request MT5</span>
			</a>
		</div>

		<div class="menu-item me-lg-1">
			<a class="menu-link py-3" href="{{route('viewDeposit')}}">
				<span class="menu-title text-warning">Request Deposit</span>
			</a>
		</div>

		<div class="menu-item me-lg-1">
			<a class="menu-link py-3" href="{{route('viewWithdrawal')}}">
				<span class="menu-title text-warning">Request Withdraw</span>
			</a>
		</div>
		@if (count(mt4accounts())>1)
			<div class="menu-item me-lg-1">
				<a class="menu-link py-3" href="{{route('createInternalTransfer')}}">
					<span class="menu-title text-warning">Request Internal Transfer</span>
				</a>
			</div>
    @endif
		--}}

	</div>
	<!--end::Menu-->
</div>
<!--end::Menu wrapper-->
