
											<!--begin::Menu-->
											<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold py-4 fs-6 w-275px" data-kt-menu="true">
												<!--begin::Menu item-->
												<div class="menu-item px-3">
													<div class="menu-content d-flex align-items-center px-3">
														<!--begin::Avatar-->
														<div class="symbol symbol-50px me-5">
															<img alt="Logo" src="{{env('CB').profile()->photo}}" />
														</div>
														<!--end::Avatar-->
														<!--begin::Username-->
														<div class="d-flex flex-column">
															<div class="fw-bolder d-flex align-items-center fs-5">{{profile()->name}}
															<span class="badge badge-light-warning fw-bolder fs-8 px-2 py-1 ms-2">New</span></div>
															<a href="#" class="fw-bold text-muted text-hover-primary fs-7">{{Session::get('priv')->name}}</a>
														</div>
														<!--end::Username-->
													</div>
												</div>
												<!--end::Menu item-->
												<!--begin::Menu separator-->
												<div class="separator my-2"></div>
												<!--end::Menu separator-->
												<!--begin::Menu item-->
												<div class="menu-item px-5">
													<a href="{{route('viewProfile')}}" class="menu-link px-5">My Profile</a>
												</div>
												<!--end::Menu item-->
												<!--begin::Menu item-->
												<div class="menu-item px-5" data-kt-menu-trigger="hover" data-kt-menu-placement="left-start" data-kt-menu-flip="center, top">
													<a href="#" class="menu-link px-5">
														<span class="menu-title">Developer Area</span>
														<span class="menu-arrow"></span>
													</a>
													<!--begin::Menu sub-->
													<div class="menu-sub menu-sub-dropdown w-175px py-4">
														<!--begin::Menu item-->
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-5">Widget</a>
														</div>
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-5">API Key</a>
														</div>
														<div class="menu-item px-3">
															<a href="#" class="menu-link px-5">Documentation</a>
														</div>
														<!--end::Menu item-->
														{{--
														<!--begin::Menu item-->
														<div class="menu-item px-3">
															<a href="#" class="menu-link d-flex flex-stack px-5">Statements
															<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="View all your statements"></i></a>
														</div>
														<!--end::Menu item-->
														--}}
													</div>
													<!--end::Menu sub-->
												</div>
												<!--end::Menu item-->
												{{--
												<!--begin::Menu item-->
												<div class="menu-item px-5">
													<a href="?page=account/statements" class="menu-link px-5">My Documents</a>
												</div>
												<!--end::Menu item-->
												<!--begin::Menu separator-->
												<div class="separator my-2"></div>
												<!--end::Menu separator-->

												<!--begin::Menu item-->
												<div class="menu-item px-5 my-1">
													<a href="#" class="menu-link px-5">Account Settings</a>
												</div>
												<!--end::Menu item-->
												--}}
												<!--begin::Menu item-->
												<div class="menu-item px-5">
													<a href="{{route('logout')}}" class="menu-link px-5">Log Out</a>
												</div>
												<!--end::Menu item-->
											</div>
											<!--end::Menu-->
