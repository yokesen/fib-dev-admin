
											<!--begin::Search-->
											<div id="kt_header_search" class="d-flex align-items-stretch" data-kt-search-keypress="true" data-kt-search-min-length="2" data-kt-search-enter="enter" data-kt-search-layout="menu" data-kt-menu-trigger="auto" data-kt-menu-overflow="false" data-kt-menu-permanent="true" data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
												<!--begin::Search toggle-->
												<div class="d-flex align-items-stretch" data-kt-search-element="toggle" id="kt_header_search_toggle">
													<div class="topbar-item px-3 px-lg-5">
														<i class="bi bi-search fs-3"></i>
													</div>
												</div>
												<!--end::Search toggle-->
												<!--begin::Menu-->
												<div data-kt-search-element="content" class="menu menu-sub menu-sub-dropdown p-7 w-325px w-md-375px">
													<!--begin::Wrapper-->
													<div data-kt-search-element="wrapper">

														@include('template.layout.search.partials._form')

														@include('template.layout.search.partials._results')

														@include('template.layout.search.partials._main')

														@include('template.layout.search.partials._empty')

													</div>
													<!--end::Wrapper-->

												@include('template.layout.search.partials._advanced-options')

												@include('template.layout.search.partials._preferences')

												</div>
												<!--end::Menu-->
											</div>
											<!--end::Search-->
