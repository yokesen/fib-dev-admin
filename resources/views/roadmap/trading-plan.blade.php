@extends('template.master')

@section('title','Trading Plan')
@section('bc-1','Roadmap')
@section('bc-2','Trading Plan')

@section('container')

  <div id="kt_content_container" class="container">
    @include('app.partials._user-card')
    <div class="row">
      <div class="col-md-6">

          <div class="card mb-5 mb-xl-8">
  					<!--begin::Body-->
  					<div class="card-body pb-0">
  						<!--begin::Header-->
  						<div class="d-flex align-items-center mb-5">
  							<!--begin::User-->
  							<div class="d-flex align-items-center flex-grow-1">
  								<!--begin::Avatar-->
  								<div class="symbol symbol-45px me-5">
  									<img src="https://cb.1129-1891.cyou/uploads/18/2021-06/icon_512x512.png" alt="" />
  								</div>
  								<!--end::Avatar-->
  								<!--begin::Info-->
  								<div class="d-flex flex-column">
  									<a href="#" class="text-gray-800 text-hover-primary fs-6 fw-bolder">Trading Plan</a>
  									<span class="text-gray-400 fw-bold">MRG Premiere Roadmap</span>
  								</div>
  								<!--end::Info-->
  							</div>
  							<!--end::User-->
  						</div>
  						<!--end::Header-->
  						<!--begin::Post-->
  						<div class="mb-5">
  							<!--begin::Image-->
  							<div class="bgi-no-repeat bgi-size-cover rounded min-h-250px mb-5" style="background-image:url('{{ENV('IMG_STATIC')}}peta-harga/ph{{$i}}.png');"></div>
  							<!--end::Image-->
  						</div>
  						<!--end::Post-->
  					</div>
  					<!--end::Body-->
  				</div>



      </div>
    </div>
  </div>
@endsection
