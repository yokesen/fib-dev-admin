@extends('template.master')

@section('title','Trading Academy')
@section('bc-1','Trading Academy')
@section('bc-2','assessment')

@section('container')
  <div id="kt_content_container" class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="card mb-5 mb-xl-8">
          <!--begin::Card header-->
          <div class="card-header border-0">
            <div class="card-title">
              <div class="d-flex align-items-center flex-grow-1">
                <!--begin::Avatar-->
                <div class="symbol symbol-45px me-5">
                  <img src="{{env('CB').$video->photo}}" />
                </div>
                <!--end::Avatar-->
                <!--begin::Info-->
                <div class="d-flex flex-column">
                  <a href="#" class="text-gray-800 text-hover-primary fs-6 fw-bolder">{{$video->videoTitle}}</a>
                  <span class="text-gray-400 fw-bold">{{$video->name}}</span>
                </div>
                <!--end::Info-->
              </div>
            </div>
          </div>
          <!--end::Card header-->
          <!--begin::Card body-->
          <div class="card-body pt-2">
            <div  class="mb-20">
              <iframe  src="{{$video->videoUrl}}" width="640" height="360" allow="autoplay"></iframe>
            </div>
            <p class="fs-6 fw-bold text-gray-600 py-2">{{$video->videoDesc}}</p>
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="card mb-5 mb-xl-8">
          <!--begin::Card header-->
          <div class="card-header border-0">
            <div class="card-title">
              <h3 class="fw-bolder m-0">Trading Academy Quiz</h3>
            </div>
          </div>
          <!--end::Card header-->
          <!--begin::Card body-->
          <div class="card-body pt-2">
            <form class="form" action="{{route('academySubmitQuiz')}}" method="post">
              @foreach ($questions as $x => $q)

                <div class="fw-bolder mt-5">{{$x+1}}. {{$q->qQuestion}}</div>
                <div class="row mv-5">
                  <div class="col-md-12">
                    <div class="row mb-3 mt-3 ms-3">
                      <div class="col-md-1 form-check form-check-solid">
                        <input type="radio" class="form-check-input" name="qJawaban[{{$x }}]" value="a" required>
                      </div>
                      <div class="col-md-11">
                        <span class="text-gray-600 "> a. {{$q->qAnswer_a}}</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="row mb-3 mt-3 ms-3">
                      <div class="col-md-1 form-check form-check-solid">
                        <input type="radio" class="form-check-input" name="qJawaban[{{$x }}]" value="b" required>
                      </div>
                      <div class="col-md-11">
                        <span class="text-gray-600 "> b. {{$q->qAnswer_b}}</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="row mb-3 mt-3 ms-3">
                      <div class="col-md-1 form-check form-check-solid">
                        <input type="radio" class="form-check-input" name="qJawaban[{{$x }}]" value="c" required>
                      </div>
                      <div class="col-md-11">
                        <span class="text-gray-600 "> c. {{$q->qAnswer_c}}</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="row mb-3 mt-3 ms-3">
                      <div class="col-md-1 form-check form-check-solid">
                        <input type="radio" class="form-check-input" name="qJawaban[{{$x }}]" value="d" required>
                      </div>
                      <div class="col-md-11">
                        <span class="text-gray-600 "> d. {{$q->qAnswer_d}}</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="separator separator-dashed my-5"></div>
              @endforeach
              @csrf
              <!--begin::Actions-->
              <input type="hidden" name="uavid" value="{{$video->uavid}}">
              <div class="row py-6 px-9">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              <!--end::Actions-->
            </form>


          </div>

        </div>
      </div>
    </div>
  </div>
@endsection
