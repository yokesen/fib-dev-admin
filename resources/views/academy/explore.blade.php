@extends('template.master')

@section('title','Trading Academy')
@section('bc-1','Trading Academy')
@section('bc-2','Explore')

@section('container')
  <div id="kt_content_container" class="container">
    @include('app.partials._user-card')
    <div class="row">
      <div class="col-md-4">
        <div class="card rounded card-xl-stretch mb-xl-8">
          <!--begin::Beader-->
          <div class="card-header border-0 py-5">
            <h3 class="card-title align-items-start flex-column">
              <span class="card-label fw-bolder fs-3 mb-1">My Trading Academy</span>
              <span class="text-muted fw-bold fs-7">Training Completion</span>
            </h3>
          </div>
          <!--end::Header-->
          <!--begin::Body-->
          @php

            $q = DB::table('academyRespond')->where('uuid',profile()->uuid)->get();

            $score = 0;
            foreach ($q as $c => $s) {
              $score += $s->poinGain;
            }
          @endphp
          <div class="card-body d-flex flex-column">
            <div class="flex-grow-1 text-center">
              {{--  <div class="mixed-widget-4-chart" data-kt-chart-color="success" style="height: 200px"></div>--}}
              <h1 class="text-dark display-3">{{$score}}</h1>
              <h4>Score Trading Academy</h4>
            </div>
            <div class="pt-5">
              <p class="text-center fs-6 pb-5">
                <span class="badge badge-light-success fs-8">Notes:</span>
                &#160; Kumpulkan Scorenya dan dapatkan
                <br />akses Pro Trader.
              </p>
                <a href="{{route('level10info')}}" class="btn btn-info w-100 py-3">Lihat Akun Pro Trader</a>
              </div>
            </div>
            <!--end::Body-->
          </div>
      </div>
        <div class="col-md-8">
          <!--begin::Tables Widget 9-->
          <div class="card rounded card-xxl-stretch mb-5 mb-xl-8">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
              <h4>List Video</h4>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-3">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="fw-bolder text-muted">
                      <th>Video By</th>
                      <th>Video Title</th>
                      <th>Video Poin</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody id="table-body">
                    @foreach ($videos as $x => $value)
                      <tr>
                        <td>
                          <a class="text-dark fw-bolder text-hover-primary d-block fs-6">
                            <img src="{{env('CB').$value->photo}}" height="40">
                          </a>
                        </td>
                        <td>
                          <a class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->videoTitle}}</a>
                        </td>
                        <td>
                          <a class="text-dark fw-bolder text-hover-primary d-block fs-6">{{number_format($value->videoPoin)}}</a>
                        </td>
                        <td>

                        </td>
                        <td>
                          <a href="{{route('showAcademy',$value->uavid)}}" class="btn btn-bg-primary btn-active-color-primary text-white" style="white-space:nowrap;">
                            <i class="bi bi-play-btn-fill text-white fs-2x"></i> Watch Now
                          </a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                  <!--end::Table body-->
                </table>
                <!--end::Table-->
                {{$videos->links()}}
              </div>
              <!--end::Table container-->
            </div>
            <!--begin::Body-->
          </div>
          <!--end::Tables Widget 9-->
        </div>
      </div>
  </div>
@endsection
