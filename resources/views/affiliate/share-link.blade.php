@extends('template.master')

@section('title','Affiliate')
@section('bc-1','Affiliate')
@section('bc-2','Referral')

@section('container')

  <div id="kt_content_container" class="container">
    <div class="row">
      <div class="col-md-6">
        <!--begin::Statistics Widget 3-->
          <div class="card card-xl-stretch mb-xl-8">
            <div class="card-header p-10">
              <h4>Share Link</h4>
              <p class="text-muted fw-bold mt-1">Simple Click Referral System</p>
            </div>
            <!--begin::Body-->
            <div class="card-body d-flex flex-column p-10">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="row mb-6">
                        <!--begin::Label-->
                        <label class="col-lg-2 col-form-label fw-bold fs-6">Link </label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-10 fv-row">
                          <input type="text" id="copyIB" class="form-control form-control-lg form-control-solid" value="{{env('APP_URL')}}/?ref={{profile()->id}}&utm_source=copylink&utm_campaign=Affiliate&utm_medium=super&utm_content=master">
                        </div>
                      </div>

                      <div class="card-footer d-flex justify-content-end py-6 mt-5">
                        <button style="background: #0088cc; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-5" onclick="copyClipboard()">Copy Link</button>
                      </div>
                    </div>
                  </div>
              <!-- share row -->

                  <div class="row">

                    <div class="col-md-6 col-xs-12">
                      <a id="whatsapp" style="background: #4FCE5D; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=whatsapp&wid=94kf&url={{urlencode(env("APP_URL").'/?ref='.profile()->id.'&utm_source=whatsapp&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=whatsapp&wid=94kf&url={{urlencode(env("APP_URL").'/?ref='.profile()->id.'&utm_source=whatsapp&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}, 'newwindow', 'width=600,height=500'); return false;"><i class="fab fa-whatsapp" aria-hidden="true"></i> Whatsapp</a>
                    </div>

                      <div class="col-md-6 col-xs-12">
                        <a id="facebook" style="background: #3c5b9b; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=facebook&wid=94kf&url={{urlencode(env("APP_URL").'/?ref='.profile()->id.'&utm_source=facebook&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=facebook&wid=94kf&url={{urlencode(env("APP_URL").'/?ref='.profile()->id.'&utm_source=facebook&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}, 'newwindow', 'width=600,height=500'); return false;"><i class="fab fa-facebook-f" aria-hidden="true"></i> Facebook</a>
                      </div>

                      <div class="col-md-6 col-xs-12">
                        <a id="twitter" style="background: #00acee; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=twitter&wid=94kf&url={{urlencode(env("APP_URL").'/?ref='.profile()->id.'&utm_source=twitter&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=twitter&wid=94kf&url={{urlencode(env("APP_URL").'/?ref='.profile()->id.'&utm_source=twitter&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}, 'newwindow', 'width=600,height=500'); return false;"><i class="fab fa-twitter" aria-hidden="true"></i> Twitter</a>
                      </div>

                      <div class="col-md-6 col-xs-12">
                        <a id="telegram" style="background: #0088cc; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=telegram&wid=94kf&url={{urlencode(env("APP_URL").'/?ref='.profile()->id.'&utm_source=telegram&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=telegram&wid=94kf&url={{urlencode(env("APP_URL").'/?ref='.profile()->id.'&utm_source=telegram&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}, 'newwindow', 'width=600,height=500'); return false;"><i class="fab fa-telegram" aria-hidden="true"></i> Telegram</a>
                      </div>

                      <div class="col-md-6 col-xs-12">
                        <a id="line" style="background: #00B900; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=lineme&wid=94kf&url={{urlencode(env("APP_URL").'/?ref='.profile()->id.'&utm_source=line&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=lineme&wid=94kf&url={{urlencode(env("APP_URL").'/?ref='.profile()->id.'&utm_source=line&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}, 'newwindow', 'width=600,height=500'); return false;"><i class="fab fa-line" aria-hidden="true"></i> Line</a>
                      </div>

                      <div class="col-md-6 col-xs-12">
                        <a id="linkedin" style="background: #0e76a8; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3"
                      href="window.open('https://www.linkedin.com/shareArticle?mini=true&url={{urlencode(env("APP_URL").'/?ref='.profile()->id.'&utm_source=line&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}&ro=false&summary=&source="
                      onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url={{urlencode(env("APP_URL").'/?ref='.profile()->id.'&utm_source=line&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}&ro=false&summary=&source=', 'newwindow', 'width=600,height=500'); return false;">
                        <i class="fab fa-linkedin-in"></i> LinkedIn</a>
                      </div>
                  </div>
                  <!-- /share row -->
            </div>
          </div>

          <!--begin::Tables Widget 9-->
          <div class="card card-xxl-stretch mb-5">

            <!--begin::Body-->
            <div class="card-body py-3">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="fw-bolder text-muted">
                      <th>Account ID</th>
                      <th>Full Name</th>
                      <th>Deposit</th>
                      <th>Volume</th>
                      <th>Update Time</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody id="table-body">

                  </tbody>

                  <!--end::Table body-->
                </table>
                <!--end::Table-->
              </div>
              <!--end::Table container-->
            </div>
            <!--begin::Body-->
          </div>
          <!--end::Tables Widget 9-->


        </div>
        <div class="col-md-6">
          <!--begin::Tables Widget 9-->
          <div class="card card-xxl-stretch mb-5">

            <!--begin::Body-->
            <div class="card-body py-3">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="fw-bolder text-muted">
                      <th>Nama</th>
                      <th>Detail</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody id="table-body">
                    @foreach ($users as $x => $value)
                      @php
                        $role = DB::table('cms_privileges')->where('id',$value->id_cms_privileges)->first();
                        $parent = DB::table('users_cabinet')->where('id',$value->parent)->select('username','uuid')->first();
                      @endphp
                      <tr>
                        <td>
                          <div class="d-flex align-items-center">
                            <div class="symbol symbol-45px me-5">
                              <img src="{{$value->photo}}" alt="" />
                            </div>
                            <div class="d-flex justify-content-start flex-column">
                              <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary fs-6">{{$value->name}}</a>
                              <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->username}}</span>
                            </div>
                          </div>
                        </td>
                        <td>
                          <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->email}}</a>
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->phone}}</span>
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->whatsapp}}</span>
                        </td>
                        <td>
                          <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$role->name}}</a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                  <!--end::Table body-->
                </table>
                <!--end::Table-->
                {{$users->links()}}
              </div>
              <!--end::Table container-->
            </div>
            <!--begin::Body-->
          </div>
          <!--end::Tables Widget 9-->
        </div>
      </div>
  </div>
@endsection

@section('jsonpage')
  <script type="text/javascript">
  function copyClipboard() {
      /* Get the text field */
      var copyText = document.getElementById("copyIB");

      /* Select the text field */
      copyText.select();

      /* Copy the text inside the text field */
      document.execCommand("copy");

      /* Alert the copied text */
      alert("Copied the text: " + copyText.value);
    }
  </script>
@endsection
