@extends('template.master')

@section('title','Getting Started')
@section('metadescription','Onboarding Client')
@section('metakeyword','Onboarding')
@section('bc-1','Dashboard')
@section('bc-2','Onboarding')

@section('container')
<!--begin::Container-->
<div id="kt_content_container" class="container">
  <!--begin::Stepper-->
  <div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_create_account_stepper">
    <!--begin::Aside-->
    <div class="d-flex justify-content-center bg-white rounded justify-content-xl-start flex-row-auto w-100 w-xl-300px w-xxl-400px me-9 mb-6">
      <!--begin::Wrapper-->
      <div class="px-6 px-lg-10 px-xxl-15 py-20">
        <!--begin::Nav-->
        <div class="stepper-nav">
          <!--begin::Step 1-->
          <div class="stepper-item current" data-kt-stepper-element="nav">
            <!--begin::Line-->
            <div class="stepper-line w-40px"></div>
            <!--end::Line-->
            <!--begin::Icon-->
            <div class="stepper-icon w-40px h-40px">
              @if ($user->id_cms_privileges == 7)
                <span class="stepper-number">1</span>
              @else
                <i class="fas fa-check text-white"></i>
              @endif
            </div>
            <!--end::Icon-->
            <!--begin::Label-->
            <div class="stepper-label">
              <h3 class="stepper-title">Credential Verification</h3>
              <div class="stepper-desc fw-bold">Setup Your Account Details</div>
            </div>
            <!--end::Label-->
          </div>
          <!--end::Step 1-->
          <!--begin::Step 2-->
          <div class="stepper-item" data-kt-stepper-element="nav">
            <!--begin::Line-->
            <div class="stepper-line w-40px"></div>
            <!--end::Line-->
            <!--begin::Icon-->
            <div class="stepper-icon w-40px h-40px">
              <i class="stepper-check fas fa-check"></i>
              <span class="stepper-number">2</span>
            </div>
            <!--end::Icon-->
            <!--begin::Label-->
            <div class="stepper-label">
              <h3 class="stepper-title">Trading Academic</h3>
              <div class="stepper-desc fw-bold">Prepare Your Trading Journey</div>
            </div>
            <!--end::Label-->
          </div>
          <!--end::Step 2-->
          <!--begin::Step 3-->
          <div class="stepper-item" data-kt-stepper-element="nav">
            <!--begin::Line-->
            <div class="stepper-line w-40px"></div>
            <!--end::Line-->
            <!--begin::Icon-->
            <div class="stepper-icon w-40px h-40px">
              <i class="stepper-check fas fa-check"></i>
              <span class="stepper-number">3</span>
            </div>
            <!--end::Icon-->
            <!--begin::Label-->
            <div class="stepper-label">
              <h3 class="stepper-title">Open Real Account</h3>
              <div class="stepper-desc fw-bold">Setup Your Real Account</div>
            </div>
            <!--end::Label-->
          </div>
          <!--end::Step 3-->
          <!--begin::Step 4-->
          <div class="stepper-item" data-kt-stepper-element="nav">
            <!--begin::Line-->
            <div class="stepper-line w-40px"></div>
            <!--end::Line-->
            <!--begin::Icon-->
            <div class="stepper-icon w-40px h-40px">
              <i class="stepper-check fas fa-check"></i>
              <span class="stepper-number">4</span>
            </div>
            <!--end::Icon-->
            <!--begin::Label-->
            <div class="stepper-label">
              <h3 class="stepper-title">Deposit</h3>
              <div class="stepper-desc fw-bold">Funding Your Trading Account</div>
            </div>
            <!--end::Label-->
          </div>
          <!--end::Step 4-->
          <!--begin::Step 5-->
          <div class="stepper-item" data-kt-stepper-element="nav">
            <!--begin::Line-->
            <div class="stepper-line w-40px"></div>
            <!--end::Line-->
            <!--begin::Icon-->
            <div class="stepper-icon w-40px h-40px">
              <i class="stepper-check fas fa-check"></i>
              <span class="stepper-number">5</span>
            </div>
            <!--end::Icon-->
            <!--begin::Label-->
            <div class="stepper-label">
              <h3 class="stepper-title">Completed</h3>
              <div class="stepper-desc fw-bold">Ready to Trade</div>
            </div>
            <!--end::Label-->
          </div>
          <!--end::Step 5-->
        </div>
        <!--end::Nav-->
      </div>
      <!--end::Wrapper-->
    </div>
    <!--begin::Aside-->
    <!--begin::Content-->
    <div class="d-flex flex-row-fluid flex-center bg-white rounded">
      <!--begin::Form-->
      <form class="py-20 w-100 w-xl-700px px-9" novalidate="novalidate" id="kt_create_account_form">

        @include('template.partials.getting-started._step1')
        @include('template.partials.getting-started._step2')
        @include('template.partials.getting-started._step3')
        @include('template.partials.getting-started._step4')
        @include('template.partials.getting-started._step5')
        <!--begin::Actions-->
        <div class="d-flex flex-stack pt-10">
          <!--begin::Wrapper-->
          <div class="mr-2">
            <button type="button" class="btn btn-lg btn-light-danger me-3" data-kt-stepper-action="previous">
              <!--begin::Svg Icon | path: icons/duotone/Navigation/Left-2.svg-->
              <span class="svg-icon svg-icon-4 me-1">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000)" x="14" y="7" width="2" height="10" rx="1" />
                    <path
                      d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z"
                      fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997)" />
                  </g>
                </svg>
              </span>
              <!--end::Svg Icon-->Back
            </button>
          </div>
          <!--end::Wrapper-->
          <!--begin::Wrapper-->
          <div>
            <button type="button" class="btn btn-lg btn-danger me-3" data-kt-stepper-action="submit">
              <span class="indicator-label">Next To Dashboard
                <!--begin::Svg Icon | path: icons/duotone/Navigation/Right-2.svg-->
                <span class="svg-icon svg-icon-3 ms-2 me-0">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <polygon points="0 0 24 0 24 24 0 24" />
                      <rect fill="#000000" opacity="0.5" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1" />
                      <path
                        d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z"
                        fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
                    </g>
                  </svg>
                </span>
                <!--end::Svg Icon-->
              </span>
              <span class="indicator-progress">Please wait...
                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
            </button>
            <button type="button" class="btn btn-lg btn-danger" data-kt-stepper-action="next">Continue
              <!--begin::Svg Icon | path: icons/duotone/Navigation/Right-2.svg-->
              <span class="svg-icon svg-icon-4 ms-1 me-0">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <rect fill="#000000" opacity="0.5" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1" />
                    <path
                      d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z"
                      fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)" />
                  </g>
                </svg>
              </span>
              <!--end::Svg Icon-->
            </button>
          </div>
          <!--end::Wrapper-->
        </div>
        <!--end::Actions-->
      </form>
      <!--end::Form-->
    </div>
    <!--end::Content-->
  </div>
  <!--end::Stepper-->
</div>
<!--end::Container-->
@endsection
