@extends('template.master')

@section('title','Getting Started')
@section('metadescription','Onboarding Client')
@section('metakeyword','Onboarding')
@section('bc-1','Dashboard')
@section('bc-2','Onboarding')

@section('container')
  <!--begin::Container-->
  <div id="kt_content_container" class="container">
    <!--begin::Stepper-->
    <div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_create_account_stepper">
      <!--begin::Aside-->
      <div class="d-flex justify-content-center bg-white rounded justify-content-xl-start flex-row-auto w-100 w-xl-300px w-xxl-400px me-9 mb-6">
        <!--begin::Wrapper-->
        <div class="px-6 px-lg-10 px-xxl-15 py-20">
          <!--begin::Nav-->
          <div class="stepper-nav" id="nav-step">
            @include('pages.onboard.js.nav-step')
          </div>
          <!--end::Nav-->
        </div>
        <!--end::Wrapper-->
      </div>
      <!--begin::Aside-->
      <!--begin::Content-->
      <div class="d-flex flex-row-fluid flex-center bg-white rounded" >
        <div class="py-20 w-100 w-xl-700px px-9" id="content-step">
          @include('pages.onboard.js.content-step-1')
        </div>
      </div>
      <!--end::Content-->
    </div>
    <!--end::Stepper-->
  </div>
  <!--end::Container-->
@endsection

@section('jsinline')
@endsection
