@extends('template.layout.auth.master')

@section('container')
  <!--begin::Body-->
  	<body id="kt_body" class="bg-gold header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed toolbar-tablet-and-mobile-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
  		<!--begin::Main-->
  		<div class="d-flex flex-column flex-root">
  			<!--begin::Authentication - Password reset -->
  			<div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
  				<!--begin::Content-->
  				<div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
  					@include('template.layout.auth._logo')
  					<!--begin::Wrapper-->
  					<div class="w-lg-500px bg-white rounded shadow-sm p-10 p-lg-15 mx-auto">
  						<!--begin::Form-->
  						<form class="form w-100" action="{{route('passwordCheckingProcess')}}" method="POST">
  							<!--begin::Heading-->
  							<div class="text-center mb-10">
  								<!--begin::Title-->
  								<h1 class="text-dark mb-3">Forgot Password ?</h1>
  								<!--end::Title-->
  								<!--begin::Link-->
  								<div class="text-gray-400 fw-bold fs-4">Enter your email to reset your password.</div>
  								<!--end::Link-->
  							</div>
  							<!--begin::Heading-->
  							<!--begin::Input group-->
  							<div class="fv-row mb-10">
  								<label class="form-label fw-bolder text-gray-900 fs-6">Email</label>
  								<input class="form-control form-control-solid" type="email" placeholder="masukkan email address" name="email" autocomplete="off" />
  							</div>
  							<!--end::Input group-->
                @csrf
                <div class="g-recaptcha" data-sitekey="{{ENV('NOCAPTCHA_SITEKEY')}}"></div>

                <h4 class="text-danger mt-6" hidden id="recaptcha-error"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> Wajib buktikan kamu bukan robot!</h4>
  							<!--begin::Actions-->
  							<div class="d-flex flex-wrap justify-content-center pb-lg-0 mb-6 mt-6">
                  <div class="d-grid gap-2">
    								<button type="submit" name="submit" value="submit" class="btn btn-lg btn-danger fw-bolder me-4" id="ot_button">
    									<span class="indicator-label">Submit</span>
    									<span class="indicator-progress">
                          Please wait...
    									     <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                      </span>
    								</button>
                  </div>
    								<a href="{{route('viewLogin')}}" class="btn btn-lg btn-light-danger fw-bolder">Cancel</a>
  							</div>
  							<!--end::Actions-->
                <script src="https://www.google.com/recaptcha/api.js?hl=id"></script>
  						</form>
  						<!--end::Form-->
  					</div>
  					<!--end::Wrapper-->
  				</div>
  				<!--end::Content-->
@endsection

@section('jsinline')
	<script src="{{url('/')}}/assets/js/orbitrade/auth/recaptcha.js"></script>
@endsection
