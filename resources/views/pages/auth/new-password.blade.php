@extends('template.layout.auth.master')

@section('container')
  <!--begin::Body-->
	<body id="kt_body" class="bg-gold header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed toolbar-tablet-and-mobile-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Authentication - New password -->
			<div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
				<!--begin::Content-->
				<div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
					@include('template.layout.auth._logo')
					<!--begin::Wrapper-->
					<div class="w-lg-550px bg-white rounded shadow-sm p-10 p-lg-15 mx-auto">
						<!--begin::Form-->
						<form class="form w-100" method="POST" action="{{route('changePassword')}}">
							<!--begin::Heading-->
							<div class="text-center mb-10">
								<!--begin::Title-->
								<h1 class="text-dark mb-3">Setup New Password</h1>
								<!--end::Title-->
								<!--begin::Link-->
								<div class="text-gray-400 fw-bold fs-4">Already have reset your password ?
								<a href="{{route('viewLogin')}}" class="link-danger fw-bolder">Sign in here</a></div>
								<!--end::Link-->
							</div>
							<!--begin::Heading-->
							<!--begin::Input group-->
	            <div class="mb-10 fv-row" data-kt-password-meter="true">
	              <!--begin::Wrapper-->
	              <div class="mb-1">
	                <!--begin::Label-->
	                <label class="form-label fw-bolder text-dark fs-6">Password</label>
	                <!--end::Label-->
	                <!--begin::Input wrapper-->
	                <div class="position-relative mb-3">
	                  <input class="form-control form-control-lg form-control-solid {{old('password') ? 'input-error' : ''}}" type="password" placeholder="" minlength="8"  name="password" autocomplete="off" required/>
	                  <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
	                    <i class="bi bi-eye-slash fs-2"></i>
	                    <i class="bi bi-eye fs-2 d-none"></i>
	                  </span>
	                </div>
	                <!--end::Input wrapper-->
	                <!--begin::Meter-->
	                <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
	                  <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
	                  <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
	                  <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
	                  <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
	                </div>
	                <!--end::Meter-->
	              </div>
	              <!--end::Wrapper-->
	              <!--begin::Hint-->
	              <div class="text-muted">Use 8 or more characters with a mix of letters, numbers &amp; symbols.</div>
	              <!--end::Hint-->
	              @if ($errors->has('password'))
	                  <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('password') }}</h4>
	              @endif
	            </div>
	            <!--end::Input group=-->
	            <!--begin::Input group-->
	            <div class="fv-row mb-5">
	              <label class="form-label fw-bolder text-dark fs-6">Confirm Password</label>
	              <input class="form-control form-control-lg form-control-solid {{old('password') ? 'input-error' : ''}}" minlength="8" type="password" placeholder="" name="password_confirmation" autocomplete="off" required />
	            </div>
	            <!--end::Input group-->


	            @csrf
	            <div class="g-recaptcha" data-sitekey="{{ENV('NOCAPTCHA_SITEKEY')}}"></div>
							<input type="hidden" name="email" value="{{$email}}" required readonly/>
							<input type="hidden" name="uuid" value="{{$uuid}}" required readonly/>
	            <h4 class="text-danger mt-6" hidden id="recaptcha-error"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> Wajib buktikan kamu bukan robot!</h4>

	            <!--begin::Actions-->
	            <div class="text-center mt-6">
	              <div class="d-grid gap-2">
	                <button type="submit" class="btn btn-lg btn-danger" id="ot_button">
	                    <span class="indicator-label">
	                        Submit
	                    </span>
	                    <span class="indicator-progress">
	                        Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
	                    </span>
	                </button>

	              </div>
	            </div>
	            <!--end::Actions-->
	            <script src="https://www.google.com/recaptcha/api.js?hl=id"></script>
						</form>
						<!--end::Form-->
					</div>
					<!--end::Wrapper-->
				</div>
				<!--end::Content-->
@endsection

@section('jsinline')
	<script src="{{url('/')}}/assets/js/orbitrade/auth/recaptcha.js"></script>
@endsection
