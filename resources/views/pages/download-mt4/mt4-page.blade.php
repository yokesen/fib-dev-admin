@extends('template.master')

@section('title','Download Metatrader 5')
@section('bc-1','Trading Platform')
@section('bc-2','Metatrader 5')

@section('container')
  <!--begin::Container-->
  <div id="kt_content_container" class="container">
    <!--begin::About card-->
    <div class="card">
      <div class="card-body">
        <div id="webterminal" style="width:100%;height:700px;"></div>
        <script type="text/javascript" src="https://trade.mql5.com/trade/widget.js"></script>
        <script type="text/javascript">
            new MetaTraderWebTerminal( "webterminal", {
                version: 5,
                server: "MetaQuotes-Demo",
                startMode: "create_demo",
                language: "en",
                colorScheme: "black_on_white"
            } );
        </script>
      </div>
    </div>
  </div>
@endsection

@section('jsonpage')
@endsection

@section('jsinline')

@endsection
