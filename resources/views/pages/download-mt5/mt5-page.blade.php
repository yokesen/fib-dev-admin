@extends('template.master')

@section('title','Web Trader Metatrader 4')
@section('bc-1','Trading Platform')
@section('bc-2','Metatrader 4')

@section('container')
  <!--begin::Container-->
  <div id="kt_content_container" class="container">
    <!--begin::About card-->
    <div class="card">
      <div class="card-body">
        <div id="webterminal" style="width:100%;height:700px;"></div>
        <script type="text/javascript" src="https://trade.mql5.com/trade/widget.js"></script>
        <script type="text/javascript">
        new MetaTraderWebTerminal( "webterminal", {
          version: 4,
          server: "MaxrichGroup-Real",
          startMode: "login",
          language: "id",
          colorScheme: "green_on_black"
        } );
        </script>
      </div>
    </div>
  </div>
@endsection

@section('jsonpage')
@endsection

@section('jsinline')

@endsection
