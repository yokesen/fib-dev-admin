@extends('template.master')

@section('title','Download Metatrader 5')
@section('bc-1','Trading Platform')
@section('bc-2','Metatrader 5')

@section('container')

    <!--begin::Container-->
    <div id="kt_content_container" class="container">
      <!--begin::About card-->
      <div class="card">
        <!--begin::Body-->
        <div class="card-body p-lg-17">
          <!--begin::About-->
          <div class="mb-18">
            <!--begin::Wrapper-->
            <div class="mb-10">
              <!--begin::Top-->
              <div class="text-center mb-15">
                <!--begin::Title-->
                <h3 class="fs-2hx text-dark mb-5">MetaTrader 5 Trading Platform</h3>
                <!--end::Title-->
                <!--begin::Text-->
                <div class="fs-5 text-muted fw-bold">The best application for trading Currencies, Stocks and Futures</div>
                <!--end::Text-->
              </div>
              <!--end::Top-->
              <!--begin::Overlay-->
              <div class="overlay">
                <!--begin::Image-->
                <img class="w-100 card-rounded" src="{{url('/')}}/images/orbitrade-mt5-hr.jpg" alt="" />
                <!--end::Image-->
                <!--begin::Links-->
                <div class="overlay-layer card-rounded bg-dark bg-opacity-25">
                  <a href="{{ENV('DOWNLOAD_MT5_IOS')}}" target="_blank" class="btn btn-primary"><i class="fas fa-download"></i> Install MT5 IOS</a>
                </div>
                <!--end::Links-->
              </div>
              <!--end::Container-->
            </div>
            <!--end::Wrapper-->

            <!--begin::Description-->
            <div class="fs-5 fw-bold text-gray-600">

              <p class="mb-8">MetaTrader 5 is a multi-asset platform that allows trading Forex, stocks and futures. It offers superior tools for comprehensive price analysis, use of algorithmic trading applications (trading robots, Expert Advisor) and copy trading.</p>

              <p class="mb-8">The MetaTrader 5 multi-asset platform supports the hedging method, which allows opening multiple positions of the same financial instrument, of opposite or same direction. This feature is widely used in Forex trading</p>

              <p class="mb-8">MetaTrader 5 delivers a powerful trading system with the Market Depth and a system of separate accounting of orders and trades. It supports both order accounting systems: the traditional netting system and the hedging option system. Four order execution modes are available to meet various trading objectives: Instant, Request, Market and Exchange execution. The platform supports all types of trade orders, including market, pending and stop orders, as well as trailing stop.</p>

              <p>With such a diversity of order types and available execution modes, traders can use any trading strategy for successful work on the financial markets.</p>

            </div>
            <!--end::Description-->
          </div>
          <!--end::About-->

          <!--begin::Info-->
            <div class="row mb-10">
              <!--begin::Stats-->
              <div class="col-md-4 py-5 text-center">
                <div class="fs-6 fw-bolder text-gray-700 mb-3"><i class="fab fa-apple fa-4x"></i></div>
                <div class="fw-bold text-gray-400 mb-3">Install for IOS</div>
                <a href="{{ENV('DOWNLOAD_MT5_IOS')}}" target="_blank" class="btn btn-primary btn-lg"><i class="fas fa-download"></i> Install MT5 IOS</a>
              </div>
              <!--end::Stats-->
            </div>
          <!--end::Info-->

          <!--begin::Section-->
          <div class="mb-16">
            <!--begin::Top-->
            <div class="text-center mb-12">

              <h3 class="fs-2hx text-dark mb-5">A powerful platform for Forex and Exchange markets</h3>

              <div class="fs-5 text-muted fw-bold">Successful trading starts with convenient and functional trading.<br>MetaTrader 5 is the best choice for the modern trader.</div>

            </div>
            <!--end::Top-->
            <!--begin::Row-->
            <div class="row g-10">
              <!--begin::Col-->
              <div class="col-md-4">
                <!--begin::Publications post-->
                <div class="card-xl-stretch me-md-6">
                  <!--begin::Overlay-->
                  <a class="d-block overlay mb-4" data-fslightbox="lightbox-hot-sales" href="/images/mt5-technical.jpg">
                    <!--begin::Image-->
                    <div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('/images/mt5-technical.jpg')"></div>
                    <!--end::Image-->
                    <!--begin::Action-->
                    <div class="overlay-layer bg-dark card-rounded bg-opacity-25">
                      <i class="bi bi-eye-fill fs-2x text-white"></i>
                    </div>
                    <!--end::Action-->
                  </a>
                  <!--end::Overlay-->
                  <!--begin::Body-->
                  <div class="m-0">
                    <!--begin::Title-->
                    <a href="#" class="fs-4 text-dark fw-bolder text-hover-primary text-dark lh-base">Professional technical analysis</a>
                    <!--end::Title-->
                    <!--begin::Text-->
                    <div class="fw-bold fs-5 text-gray-600 text-dark mt-3 mb-5">All these features enhance the analytical capacity of your platform and your ability to achieve the most detailed and accurate technical analysis of quotes.</div>
                    <!--end::Text-->

                  </div>
                  <!--end::Body-->
                </div>
                <!--end::Publications post-->
              </div>
              <!--end::Col-->
              <!--begin::Col-->
              <div class="col-md-4">
                <!--begin::Publications post-->
                <div class="card-xl-stretch mx-md-3">
                  <!--begin::Overlay-->
                  <a class="d-block overlay mb-4" data-fslightbox="lightbox-hot-sales" href="/images/mt5-fundamental.jpg">
                    <!--begin::Image-->
                    <div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('/images/mt5-fundamental.jpg')"></div>
                    <!--end::Image-->
                    <!--begin::Action-->
                    <div class="overlay-layer bg-dark card-rounded bg-opacity-25">
                      <i class="bi bi-eye-fill fs-2x text-white"></i>
                    </div>
                    <!--end::Action-->
                  </a>
                  <!--end::Overlay-->
                  <!--begin::Body-->
                  <div class="m-0">
                    <!--begin::Title-->
                    <a href="#" class="fs-4 text-dark fw-bolder text-hover-primary text-dark lh-base">Fundamental analysis</a>
                    <!--end::Title-->
                    <!--begin::Text-->
                    <div class="fw-bold fs-5 text-gray-600 text-dark mt-3 mb-5">Stay tuned and make more weighted trading decisions from the benefit of having access to financial news and economic calendar straight from your platform!</div>
                    <!--end::Text-->

                  </div>
                  <!--end::Body-->
                </div>
                <!--end::Publications post-->
              </div>
              <!--end::Col-->
              <!--begin::Col-->
              <div class="col-md-4">
                <!--begin::Publications post-->
                <div class="card-xl-stretch ms-md-6">
                  <!--begin::Overlay-->
                  <a class="d-block overlay mb-4" data-fslightbox="lightbox-hot-sales" href="/images/mt5-mobile.jpg">
                    <!--begin::Image-->
                    <div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('/images/mt5-mobile.jpg')"></div>
                    <!--end::Image-->
                    <!--begin::Action-->
                    <div class="overlay-layer bg-dark card-rounded bg-opacity-25">
                      <i class="bi bi-eye-fill fs-2x text-white"></i>
                    </div>
                    <!--end::Action-->
                  </a>
                  <!--end::Overlay-->
                  <!--begin::Body-->
                  <div class="m-0">
                    <!--begin::Title-->
                    <a href="#" class="fs-4 text-dark fw-bolder text-hover-primary text-dark lh-base">Mobile Trading</a>
                    <!--end::Title-->
                    <!--begin::Text-->
                    <div class="fw-bold fs-5 text-gray-600 text-dark mt-3 mb-5">That's it! Financial markets are at your finger tips with the MetaTrader 5 mobile platform!</div>
                    <!--end::Text-->

                  </div>
                  <!--end::Body-->
                </div>
                <!--end::Publications post-->
              </div>
              <!--end::Col-->
            </div>
            <!--end::Row-->
          </div>
          <!--end::Section-->

        </div>
        <!--end::Body-->
      </div>
      <!--end::About card-->
    </div>
    <!--end::Container-->

@endsection

@section('jsonpage')
  <script src="{{url('/')}}/assets/plugins/custom/fslightbox/fslightbox.bundle.js"></script>
@endsection
