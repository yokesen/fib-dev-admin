@extends('template.master')

@section('title', $menu)
@section('bc-1','Signal')
@section('bc-2',$menu)

@section('container')

  <div id="kt_content_container" class="container">
    @include('app.partials._user-card')
    <div class="row">
      {{--
      <div class="col-md-5">
        <!--begin::Statistics Widget 3-->
					<div class="card card-xl-stretch mb-xl-8">
						<!--begin::Body-->
						<div class="card-body d-flex flex-column p-0">
							<div class="d-flex flex-stack flex-grow-1 card-p">
								<div class="d-flex flex-column me-2">
									<a href="#" class="text-dark text-hover-primary fw-bolder fs-3">Signal Progress</a>
									<span class="text-muted fw-bold mt-1">{{$category}} | {{$menu}} Chart</span>
								</div>
								<span class="symbol symbol-50px">
									<span class="symbol-label fs-5 fw-bolder bg-light-danger text-danger">-260</span>
								</span>
							</div>
							<div class="statistics-widget-3-chart card-rounded-bottom" data-kt-chart-color="danger" style="height: 150px"></div>


            </div>
						<!--end::Body-->
            <div class="card-footer">
              <div class="pt-5">
                <p class="text-center fs-6 pb-5">
                  <span class="badge badge-light-success fs-8">Notes:</span>
                  &#160; History signal tidak menjadi jaminan
                  <br />Selalu terapkan Manajemen Risiko
                </p>
                <a href="#" class="btn btn-success w-100 py-3"><i class="fab fa-whatsapp fs-2x"></i> Subscribe By Whatsapp</a>
              </div>
            </div>
					</div>
					<!--end::Statistics Widget 3-->

      </div>
      --}}
      <div class="col-md-7">
        <!--begin::Tables Widget 4-->
        <div class="card rounded card-xl-stretch mb-5 mb-xl-8">
          <!--begin::Header-->
          <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
              <span class="card-label fw-bolder fs-3 mb-1">{{$menu}}</span>
              <span class="text-muted mt-1 fw-bold fs-7">{{count($signals)}} Signal</span>
            </h3>
            <div class="card-toolbar">
              <ul class="nav">
                <li class="nav-item">
                  <a class="nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary active fw-bolder px-4 me-1" data-bs-toggle="tab" href="#kt_table_widget_4_tab_1">Week</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary fw-bolder px-4 me-1" data-bs-toggle="tab" href="#kt_table_widget_4_tab_2">Month</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link btn btn-sm btn-color-muted btn-active btn-active-light-primary fw-bolder px-4" data-bs-toggle="tab" href="#kt_table_widget_4_tab_3">Year</a>
                </li>
              </ul>
            </div>
          </div>
          <!--end::Header-->
          <!--begin::Body-->
          <div class="card-body py-3">
            <div class="tab-content">
              <!--begin::Tap pane-->
              <div class="tab-pane fade show active" id="kt_table_widget_4_tab_1">
                <!--begin::Table container-->
                <div class="table-responsive">
                  <!--begin::Table-->
                  <table class="table align-middle gs-0 gy-3">
                    <!--begin::Table head-->
                    <thead>
                      <tr>
                        <th class="text-center p-0 w-50px"></th>
                        <th class="text-center p-0 min-w-70px">Action</th>
                        <th class="text-center p-0 min-w-70px">Price</th>
                        <th class="text-center p-0 min-w-70px">Limit</th>
                        <th class="text-center p-0 min-w-70px">Open Time</th>
                      </tr>
                    </thead>
                    <!--end::Table head-->
                    <!--begin::Table body-->
                    <tbody>
                      @foreach ($signals as $key => $value)
                        <tr>
                          <td class="text-center">
                            <div class="symbol symbol-50px">
                              <img src="https://cb.1129-1891.cyou/{{$signalName->signalLogo}}" alt="" />
                            </div>
                          </td>
                          <td class="text-center">
                            <a href="#" class="text-dark fw-bolder text-hover-primary mb-1 fs-6">{{$value->type}}</a>
                            <span class="text-muted fw-bold d-block fs-7">{{$value->pair}}</span>
                          </td>
                          <td class="text-center">
                            <span class="text-dark fw-bold d-block fs-7">{{$value->price}}</span>
                          </td>
                          <td class="text-center">
                            <span class="text-dark fw-bold d-block fs-7">TP {{$value->takeprofit}}</span>
                            <span class="text-dark fw-bold d-block fs-7">SL {{$value->stoploss}}</span>
                          </td>
                          <td class="text-center">
                            <span class="text-muted fw-bold d-block fs-7">{{$value->signal_date}}</span>
                            <span class="text-dark fw-bold d-block fs-7">{{$value->signal_note}}</span>
                          </td>

                        </tr>
                      @endforeach
                    </tbody>
                    <!--end::Table body-->
                  </table>
                  {{$signals->links()}}
                </div>
                <!--end::Table-->
              </div>
              <!--end::Tap pane-->
              <!--begin::Tap pane-->
              <div class="tab-pane fade" id="kt_table_widget_4_tab_2">
                <!--begin::Table container-->
                <div class="table-responsive">
                  <!--begin::Table-->
                  <table class="table align-middle gs-0 gy-3">
                    <!--begin::Table head-->
                    <thead>
                      <tr>
                        <th class="p-0 w-50px"></th>
                        <th class="p-0 min-w-150px"></th>
                        <th class="p-0 min-w-140px"></th>
                        <th class="p-0 min-w-120px"></th>
                      </tr>
                    </thead>
                    <!--end::Table head-->
                    <!--begin::Table body-->
                    <tbody>

                    </tbody>
                    <!--end::Table body-->
                  </table>
                </div>
                <!--end::Table-->
              </div>
              <!--end::Tap pane-->
              <!--begin::Tap pane-->
              <div class="tab-pane fade" id="kt_table_widget_4_tab_3">
                <!--begin::Table container-->
                <div class="table-responsive">
                  <!--begin::Table-->
                  <table class="table align-middle gs-0 gy-3">
                    <!--begin::Table head-->
                    <thead>
                      <tr>
                        <th class="p-0 w-50px"></th>
                        <th class="p-0 min-w-150px"></th>
                        <th class="p-0 min-w-140px"></th>
                        <th class="p-0 min-w-120px"></th>
                      </tr>
                    </thead>
                    <!--end::Table head-->
                    <!--begin::Table body-->
                    <tbody>


                    </tbody>
                    <!--end::Table body-->
                  </table>
                </div>
                <!--end::Table-->
              </div>
              <!--end::Tap pane-->
            </div>
          </div>
          <!--end::Body-->
        </div>
        <!--end::Tables Widget 4-->
      </div>
      <!--end::Col-->
    </div>
  </div>
@endsection
