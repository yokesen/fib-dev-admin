@extends('template.master')

@section('title','Menunggu Approval Account')
@section('bc-1','Account Trading')
@section('bc-2','Menunggu Approval')

@section('container')
  <div class="d-flex flex-column flex-root">
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <!--begin::Engage Widget 1-->
        <div class="card rounded mb-5 mb-xl-10">
          <!--begin::Body-->
          <div class="card-body pb-0">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column justify-content-between h-100">
              <!--begin::Section-->
              <div class="pt-15 mb-10">
                <!--begin::Title-->
                <h3 class="text-grey-800 text-center fs-1 fw-bolder lh-lg">Hi {{profile()->name}}!</h3>
                <p class="text-grey-800 text-center">Untuk bisa melakukan Deposit, minimum harus punya 1 account trading</p>
                <p class="text-grey-800 text-center">Tunggu sebentar ya, atau Kamu bisa juga hubungi Admin Kami.</p>

                <div class="text-center py-7">
                  <a href="https://wa.me/6287773093999/?text=Halo admin, saya mau nanya akun trading saya belum aktif" class="btn btn-success btn-lg"><i class="bi bi-whatsapp fs-2"></i> Hubungi Admin via Whatsapp</a>
                </div>

                  <!--begin::Image-->
                  <div class="flex-grow-1 bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-bottom card-rounded-bottom h-200px" style="background-image:url('/assets/media/illustrations/winner.png')"></div>
                  <!--end::Image-->
                </div>
                <!--end::Wrapper-->
              </div>
              <!--end::Body-->
            </div>
            <!--end::Engage Widget 1-->
      </div>
    </div>
  </div>
</div>
@endsection
