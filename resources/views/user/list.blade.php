@extends('template.master')

@section('title','User')
@section('bc-1','User')
@section('bc-2','List')

@section('cssinline')
    <link href="{{url('/')}}/assets-1/css/select2.min.css" rel="stylesheet">
@endsection

@section('cssonpage')
<style media="screen">

</style>
@endsection

@section('container')

  <div id="kt_content_container" class="container">
      <div class="row">
        <div class="col-md-12">
          <!--begin::Tables Widget 9-->
          <div class="card card-xxl-stretch mb-5 mb-xl-8">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
              <div class="card-title align-items-start flex-column">
                <select class="form-control w-250px select2-show-search pb-9" id="js-data-user"  name="search"></select>
              </div>

              <div class="card-toolbar">
  							<a href="{{route('createUser')}}" class="btn btn-sm btn-light-primary" >
  							<i class="fas fa-user-alt"></i> Add New User</a>
  						</div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-3">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="fw-bolder text-muted">
                      <th>Created Date</th>
                      <th>Nama</th>
                      <th>Komunikasi</th>
                      <th>Provider</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody id="table-body">
                    @foreach ($users as $x => $value)
                      @php
                        $role = DB::table('cms_privileges')->where('id',$value->id_cms_privileges)->first();
                        $parent = DB::table('users_cabinet')->where('id',$value->parent)->select('username','uuid')->first();
                      @endphp
                      <tr>
                        <td>
                          <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->created_at}}</a>
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{ \Carbon\Carbon::parse(strtotime($value->created_at.'+7hours'))->diffForHumans()}}</span>
                        </td>
                        <td>
                          <div class="d-flex align-items-center">
                            <div class="symbol symbol-45px me-5">
                              <img src="{{$value->photo}}" alt="" />
                            </div>
                            <div class="d-flex justify-content-start flex-column">
                              <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary fs-6">{{$value->name}}</a>
                              <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->username}}</span>
                            </div>
                          </div>
                        </td>
                        <td>
                          <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->email}}</a>
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->phone}}</span>
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->whatsapp}}</span>
                        </td>
                        <td>
                          @if ($parent)
                            <a href="{{route('showUser',$parent->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$parent->username}}</a>
                          @endif
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->providerOrigin}}</span>
                        </td>
                        <td>
                          <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$role->name}}</a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                  <!--end::Table body-->
                </table>
                <!--end::Table-->
                {{$users->links()}}
              </div>
              <!--end::Table container-->
            </div>
            <!--begin::Body-->
          </div>
          <!--end::Tables Widget 9-->
        </div>
      </div>

    </div>

@endsection

@section('jsinline')
  <script src="{{url('/')}}/assets-1/js/select2.min.js"></script>
@endsection

@section('jsonpage')
  <script type="text/javascript">
  $('#js-data-user').select2({
      placeholder: 'Search User',
      ajax: {
        url: '/search/user',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
          $('#table-body').html(data.html);
        },
        cache: true
      }
    });

  </script>
@endsection
