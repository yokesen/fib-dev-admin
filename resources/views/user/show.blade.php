@extends('template.master')

@section('title','User')
@section('bc-1','User')
@section('bc-2','Show')

@section('cssinline')
    <link href="{{url('/')}}/assets-1/css/select2.min.css" rel="stylesheet">
@endsection

@section('container')
  <div id="kt_content_container" class="container">
    <!--begin::Layout-->
    <div class="d-flex flex-column flex-xl-row">
      <!--begin::Sidebar-->
      <div class="flex-column flex-lg-row-auto w-100 w-xl-400px mb-10">
        <!--begin::Card-->
        @include('user.partials._user-profile')
        <!--end::Card-->
        <!--begin::Connected Accounts-->
        @include('user.partials._user-analytic')
        <!--end::Connected Accounts-->
      </div>
      <!--end::Sidebar-->

      <div class="flex-lg-row-fluid ms-lg-15">
        <!--begin:::Tabs-->
        <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-bold mb-8">
          <!--begin:::Tab item-->
          <li class="nav-item">
            <a class="nav-link text-dark pb-4 active" data-bs-toggle="tab" href="#kt_customer_view_overview_tab">Profile</a>
          </li>
          <!--end:::Tab item-->
          <!--begin:::Tab item-->
          <li class="nav-item">
            <a class="nav-link text-dark pb-4" data-bs-toggle="tab" href="#kt_customer_view_document_tab">Document</a>
          </li>
          <!--end:::Tab item-->
          <!--begin:::Tab item-->
          <li class="nav-item">
            <a class="nav-link text-dark pb-4" data-bs-toggle="tab" href="#kt_customer_view_overview_events_and_logs_tab">Finances</a>
          </li>
          <!--end:::Tab item-->
          <!--begin:::Tab item-->
          <li class="nav-item">
            <a class="nav-link text-dark pb-4" data-kt-countup-tabs="true" data-bs-toggle="tab" href="#kt_customer_view_overview_statements">Referral</a>
          </li>
          <!--end:::Tab item-->

        </ul>
        <!--end:::Tabs-->
        <!--begin:::Tab content-->
        <div class="tab-content" id="myTabContent">
          <!--begin:::Tab pane-->
          <div class="tab-pane fade show active" id="kt_customer_view_overview_tab" role="tabpanel">

            @include('user.partials._profile-profile')

            @include('user.partials._profile-analytic')

            @include('user.partials._user-metatrader-list')

          </div>
          <!--end:::Tab pane-->
          <!--begin:::Tab pane-->
          <div class="tab-pane fade" id="kt_customer_view_document_tab" role="tabpanel">

            @include('user.partials._document_ktp')

            @include('user.partials._document_tabungan')

          </div>
          <!--end:::Tab pane-->
          <!--begin:::Tab pane-->
          <div class="tab-pane fade" id="kt_customer_view_overview_events_and_logs_tab" role="tabpanel">

            @include('user.partials._user-deposit-method')

            @include('user.partials._user-deposit-list')

            @include('user.partials._user-withdrawal-list')

          </div>
          <!--end:::Tab pane-->
          <!--begin:::Tab pane-->
          <div class="tab-pane fade" id="kt_customer_view_overview_statements" role="tabpanel">
            <!--begin::Earnings-->
            @include('user.partials._referral-link')
            <!--end::Earnings-->
            @include('user.partials._referral-list')
            <!--end::Statements-->
          </div>
          <!--end:::Tab pane-->
        </div>
      </div>
      <!--MODAL-->
      @include('user.partials._modal-withdrawal')
      @include('user.partials._modal-deposit')
      @include('user.partials._modal-add-metatrader-4')
    </div>

  </div>
@endsection

@section('jsinline')
  <script src="{{url('/')}}/assets-1/js/select2.min.js"></script>
@endsection

@section('jsonpage')
  <script type="text/javascript">

  $( document ).ready(function() {
    $('#ibchange').hide();
  });

  $('#changeib').click(function(){
    $('#ibchange').show();
  });

  $('#js-data-user').select2({
      placeholder: 'Search IB',
      ajax: {
        url: '/search/ib',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
          return {
            results:  $.map(data, function (item) {
              return {
                text: item.username+' | '+item.email,
                id: item.id
              }
            })
          };
        },
        cache: true
      }
    });


  $('#paternRupiah').change(function(){
    var num = $('#paternRupiah').val()
    num = addPeriod(num);
    $('#paternRupiah').val(num)
  });

  $('#paternRupiah').click(function(){

    $('#paternRupiah').val("")
  });

  function addPeriod(nStr)
  {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1;
  }

  $('#akunchange').change(function(){
    $.ajax({
      type: "POST",
      url: "{{route('ajaxSearchAccountMt4')}}",
      data: {
        mt4: this.value
      },
      headers: {
        "X-CSRF-TOKEN": "{{ csrf_token() }}"
      },
      success: function(e) {

        $("#tipeDeposit").html(e.html);
      }
    })
  });

  function copyClipboard() {
      /* Get the text field */
      var copyText = document.getElementById("copyPassword");

      /* Select the text field */
      copyText.select();

      /* Copy the text inside the text field */
      document.execCommand("copy");

      /* Alert the copied text */
      alert("Copied the text: " + copyText.value);
    }

    function copyClipboard() {
        /* Get the text field */
        var copyText = document.getElementById("copyIB");

        /* Select the text field */
        copyText.select();

        /* Copy the text inside the text field */
        document.execCommand("copy");

        /* Alert the copied text */
        alert("Copied the text: " + copyText.value);
      }


    $("#username").keyup(function(){var e=this.value;console.log(e.length),e.length>3&&$.ajax({type:"POST",url:"{{route('ajaxSearchUsername')}}",data:{username:e,uuid:'{{$user->uuid}}'},headers:{"X-CSRF-TOKEN":"{{ csrf_token() }}"},success:function(e){0==e?$("#chooseUsername").html(""):($("#chooseUsername").html(e.html),document.getElementById("username").value="")}})});

    function fixDecimal(e) {
      return e.value = e.value.toString().match(/^\d+(?:\.\d{0,2})?/);
    }

    var myDropzone=new Dropzone("#ot_buku_rekening",{url:"{{route('updateUseruploadRekening',$user->uuid)}}",paramName:"gambar",method:"post",data: {
      uuid: '{{$user->uuid}}'
    },headers:{"X-CSRF-TOKEN":"{{ csrf_token() }}"},maxFiles:1,maxFilesize:10,addRemoveLinks:!0,accept:function(e,a){"wow.jpg"==e.name?a("Naha, you don't."):a()}});

    myDropzone=new Dropzone("#ot_ktp",{url:"{{route('updateUseruploadKTP',$user->uuid)}}",paramName:"gambar",method:"post",data: {
      uuid: '{{$user->uuid}}'
    },headers:{"X-CSRF-TOKEN":"{{ csrf_token() }}"},maxFiles:1,maxFilesize:10,addRemoveLinks:!0,accept:function(e,a){"wow.jpg"==e.name?a("Naha, you don't."):a()}});
  </script>
@endsection
