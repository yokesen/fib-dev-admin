<div class="card mb-5 mb-xl-10">
  <!--begin::Card header-->
  <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
    <!--begin::Card title-->
    <div class="card-title m-0">
      <h3 class="fw-bolder m-0">Document KTP </h3>
    </div>
    <!--end::Card title-->
  </div>
  <!--begin::Card header-->
  <!--begin::Content-->
  <div class="collapse show">
    <div class="card-body border-top p-9">
      <div class="row pb-8">
        <div class="col-md-6">
          @if ($user->photoKTP != "/images/upload-default.jpg")
            <img src="{{env('IMG_USER').$user->photoKTP}}" height="200">
          @else
            <img src="{{url('/')}}/images/upload-default.jpg" height="200">
          @endif
          <p>Refresh halaman ini setelah upload photo baru</p>
        </div>
      </div>

      <form class="form" action="#" method="post">


          <!--begin::Input group-->
          <div class="fv-row">
            <!--begin::Dropzone-->
            <div class="dropzone" id="ot_ktp">
              <!--begin::Message-->
              <div class="dz-message needsclick">
                <!--begin::Icon-->
                <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                <!--end::Icon-->

                <!--begin::Info-->
                <div class="ms-4">
                  <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Klik di sini untuk upload.</h3>
                  <span class="fs-7 fw-bold text-gray-400">Upload foto KTP/SIM/Paspor</span>
                </div>
                <!--end::Info-->
              </div>
            </div>
            <!--end::Dropzone-->
          </div>
          <!--end::Input group-->
          <div class="form-text">Format yang bisa diupload: png, jpg, jpeg.</div>
          <div class="form-text">Maksimal 5Mb.</div>

      </form>
      <!--end::Form-->
    </div>
  </div>
</div>
