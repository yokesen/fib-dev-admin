<div class="card pt-4 mb-6 mb-xl-9">
  <!--begin::Card header-->
  <div class="card-header border-0">
    <!--begin::Card title-->
    <div class="card-title">
      <h2>Reset Password</h2>
    </div>
    <!--end::Card title-->

  </div>
  <!--end::Card header-->
  <!--begin::Card body-->
  <div class="card-body py-6">
    <div class="row mb-6">
      <!--begin::Label-->
      <label class="col-lg-2 col-form-label fw-bold fs-6">Link </label>
      <!--end::Label-->
      <!--begin::Col-->
      <div class="col-lg-10 fv-row">
        <input type="text" id="copyPassword" class="form-control form-control-lg form-control-solid" value="{{env('IMS_ACTIVE')}}/new-password/{{$user->email}}/verify/{{$user->uuid}}">
      </div>
    </div>

    <div class="card-footer d-flex justify-content-end py-6">
      <button class="btn btn-info" onclick="copyClipboard()">Copy Link</button>
    </div>
  </div>
  <!--end::Card body-->
</div>
