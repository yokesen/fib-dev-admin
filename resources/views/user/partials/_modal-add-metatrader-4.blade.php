<div class="modal fade" id="kt_modal_mt4" tabindex="-1" aria-hidden="true">
  <!--begin::Modal dialog-->
  <div class="modal-dialog mw-650px">
    <!--begin::Modal content-->
    <div class="modal-content">
      <!--begin::Modal header-->
      <div class="modal-header">
        <!--begin::Modal title-->
        <h2 class="fw-bolder">Add Metatrader 5 for {{$user->username ? $user->username : $user->name}}</h2>
        <!--end::Modal title-->
      </div>
      <!--end::Modal header-->
      <!--begin::Modal body-->
      <div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
        <!--begin::Form-->
        @foreach ($listAccounts as $x => $account)
          <!--begin::Col-->
          <div class="col-md-6 mb-6">
            <form action="{{route('createAccountMt4byAdmin')}}" method="post">
              @csrf
              <input type="hidden" name="accountType" value="{{$account->namaAccount}}">
              <input type="hidden" name="uuid" value="{{$user->uuid}}">
              <input type="submit" class="btn btn-lg btn-info btn-block" style="width:300px!important;" name="create" value=" + MT5 {{$account->namaAccount}}">
            </form>
          </div>
          <!--end::Col-->
        @endforeach
      </div>
      <!--end::Modal content-->
    </div>
    <!--end::Modal dialog-->
  </div>
</div>
