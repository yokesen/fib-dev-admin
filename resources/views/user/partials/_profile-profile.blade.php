<div class="card mb-5 mb-xl-10">
      <!--begin::Card header-->
      <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
        <!--begin::Card title-->
        <div class="card-title m-0">
          <h3 class="fw-bolder m-0">Profil </h3>
        </div>
        <!--end::Card title-->
      </div>
      <!--begin::Card header-->
      <!--begin::Content-->
      <div class="collapse show">
        <!--begin::Form-->
        <form  method="POST" action="{{route('updateUser',$user->uuid)}}" class="form" enctype="multipart/form-data">
          <!--begin::Card body-->
          <div class="card-body border-top p-9">
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label fw-bold fs-6">Avatar</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8">
                <!--begin::Image input-->
                <div class="image-input image-input-outline" data-kt-image-input="true" style="background-image: url(assets/media/avatars/blank.png)">
                  <!--begin::Preview existing avatar-->
                  <div class="image-input-wrapper w-125px h-125px" style="background-image: url({{$user->photo}})"></div>
                  <!--end::Preview existing avatar-->
                  <!--begin::Label-->
                  <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-white shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                    <i class="bi bi-pencil-fill fs-7"></i>
                    <!--begin::Inputs-->
                    <input type="file" name="photo" max-size="3000" accept=".png, .jpg, .jpeg" />
                    <input type="hidden" name="avatar_remove" />
                    <!--end::Inputs-->
                  </label>
                  <!--end::Label-->

                  <!--end::Remove-->
                </div>
                <!--end::Image input-->
                <!--begin::Hint-->
                <div class="form-text">Format yang bisa diupload: png, jpg, jpeg.</div>
                <div class="form-text">Maksimal 5Mb.</div>
                @if ($errors->has('photo'))
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('photo') }}</h4>
                @endif
                <!--end::Hint-->
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Username</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">

                  <input class="form-control form-control-lg form-control-solid {{ old('username') && !$errors->has('username') ? 'input-valid' : '' }} {{$errors->has('username') ? 'input-error' : ''}}" id="username" type="text" placeholder="" name="username" autocomplete="off" value="{{ old('username') ? old('username') : $user->username }}" required {{$errors->has('username') ? 'autofocus' : ''}}/>

                  <div id="chooseUsername">

                  </div>

                  @if ($errors->has('username'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('username') }}</h4>
                  @endif


              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid {{ old('name') && !$errors->has('name') ? 'input-valid' : '' }} {{$errors->has('name') ? 'input-error' : ''}}" type="text" placeholder="" name="name" autocomplete="off" value="{{ old('name') ? old('name') : $user->name }}" required {{$errors->has('name') ? 'autofocus' : ''}}/>
                @if ($errors->has('name'))
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('name') }}</h4>
                @endif
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Email</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">

                  <input class="form-control form-control-lg form-control-solid {{ old('email') && !$errors->has('email') ? 'input-valid' : '' }} {{$errors->has('email') ? 'input-error' : ''}}" type="email" name="email"  autocomplete="off" value="{{ old('email') ? old('email') : $user->email }}" required {{$errors->has('email') ? 'autofocus' : ''}}/>
                  @if ($errors->has('email'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('email') }}</h4>
                  @endif


              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Whatsapp</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid {{ old('whatsapp') && !$errors->has('whatsapp') ? 'input-valid' : '' }} {{$errors->has('whatsapp') ? 'input-error' : ''}}" type="text" name="whatsapp"  autocomplete="off" value="{{ old('whatsapp') ? old('whatsapp') : $user->whatsapp }}" required {{$errors->has('whatsapp') ? 'autofocus' : ''}} required/>
                @if ($errors->has('whatsapp'))
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('whatsapp') }}</h4>
                @endif
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Alamat</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <textarea class="form-control form-control-lg form-control-solid {{ old('address') && !$errors->has('address') ? 'input-valid' : '' }} {{ $errors->has('address') ? 'input-error' : '' }}" name="address" rows="5" cols="100%" {{ $errors->has('address') ? 'autofocus' : '' }} required>{{ old('address') ? old('address') : $user->address }}</textarea>
                @if ($errors->has('address'))
                  <h4 class="text-danger mt-6"> <i
                    class="bi bi-exclamation-triangle text-danger fs-2 blink"></i>
                    {{ $errors->first('address') }}</h4>
                  @endif
                </div>
                <!--end::Col-->
              </div>
              <!--end::Input group-->
              <!--begin::Input group-->
              <div class="row mb-6">
                <!--begin::Label-->
                <label class="col-lg-4 col-form-label required fw-bold fs-6">City</label>
                <!--end::Label-->
                <!--begin::Col-->
                <div class="col-lg-8 fv-row">
                  @if ($user->city == '')
                    <input
                    class="form-control form-control-lg form-control-solid {{ old('city') && !$errors->has('city') ? 'input-valid' : '' }} {{ $errors->has('city') ? 'input-error' : '' }}"
                    type="city" value="{{ $user->city }}" name="city" autocomplete="off"
                    value="{{ old('city') }}" required
                    {{ $errors->has('city') ? 'autofocus' : '' }} />
                    @if ($errors->has('city'))
                      <h4 class="text-danger mt-6"> <i
                        class="bi bi-exclamation-triangle text-danger fs-2 blink"></i>
                        {{ $errors->first('city') }}</h4>
                      @endif
                    @else
                      <input class="form-control form-control-lg form-control-solid input-valid"
                      type="city" name="city" value="{{ $user->city }}" required />
                    @endif

                  </div>
                  <!--end::Col-->
                </div>
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Tanggal Lahir</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">

                <input class="form-control form-control-lg form-control-solid {{ old('dob') && !$errors->has('dob') ? 'input-valid' : '' }} {{$errors->has('dob') ? 'input-error' : ''}}" type="date"  placeholder="" name="dob" value="{{ old('dob') ? old('dob') : $user->dob }}" max="{{date('Y-m-d', strtotime('- 17 years'))}}" autocomplete="off" id="ot_datepicker_2" required/>
                @if ($errors->has('dob'))
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('dob') }}</h4>
                @endif
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->

          </div>
          <!--end::Card body-->
          @csrf
          <!--begin::Actions-->
          <div class="card-footer d-flex justify-content-end py-6 px-9">

            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
          <!--end::Actions-->
        </form>
        <!--end::Form-->
      </div>
      <!--end::Content-->
    </div>
