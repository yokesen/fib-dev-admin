<div class="modal fade" id="kt_modal_withdrawal" tabindex="-1" aria-hidden="true">
  <!--begin::Modal dialog-->
  <div class="modal-dialog mw-650px">
    <!--begin::Modal content-->
    <div class="modal-content">
      <!--begin::Modal header-->
      <div class="modal-header">
        <!--begin::Modal title-->
        <h2 class="fw-bolder">Add Withdrawal for {{$user->username ? $user->username : $user->name}}</h2>
        <!--end::Modal title-->
      </div>
      <!--end::Modal header-->
      <!--begin::Modal body-->
      <div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
        <!--begin::Form-->
        <form  method="POST" action="{{route('submitWithdrawalbyAdmin')}}" class="form">
              <!--begin::Card body-->
              <div class="card-body border-top p-9">
                <h4 class="text-dark text-muted mb-6 ">Detail Withdrawal</h4>
                <!--begin::Input group-->
                <div class="row mb-6">
                  <!--begin::Label-->
                  <label class="col-lg-4 col-form-label required fw-bold fs-6">Nilai Withdrawal </label>
                  <!--end::Label-->
                  <!--begin::Col-->
                  <div class="col-lg-8 fv-row">
                    <div class="input-group">
                      <span class="input-group-text" style="font-size:1.5em!important">
                        US$
                      </span>
                      <input class="form-control form-control-lg form-control-solid" type="number" name="amount" min="1" step="0.01" onkeyup="fixDecimal(this)" style="font-size:1.5em!important" required/>

                    </div>

                    @if ($errors->has('ammount'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('ammount') }}</h4>
                    @endif
                  </div>

                  <!--end::Col-->

                </div>
                <!--end::Input group-->
                <!--begin::Input group-->
                <div class="row mb-6">
                  <!--begin::Label-->
                  <label class="col-lg-4 col-form-label required fw-bold fs-6">Dari Account Trading</label>
                  <!--end::Label-->
                  <!--begin::Col-->
                  <div class="col-lg-8 fv-row">
                    <select class="form-select form-select-solid" name="accountmt5" required>
                      <option value="">Pilih Account</option>

                      @foreach ($mt4 as $x => $acc)
                        <option value="{{$acc->mt4_id}}">{{$acc->mt4_id}}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('accountmt5'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('accountmt5') }}</h4>
                    @endif
                  </div>
                  <!--end::Col-->
                </div>
                <!--end::Input group-->


              </div>
              <!--end::Card body-->
              <div class="card-footer border-0">
                @csrf
                <!--begin::Actions-->
                <input type="hidden" name="uuid" value="{{$user->uuid}}"/>
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <!--end::Actions-->
              </form>
        <!--end::Form-->
      </div>
      <!--end::Modal body-->
    </div>
    <!--end::Modal content-->
  </div>
  <!--end::Modal dialog-->
</div>
</div>
