<div class="card pt-4 mb-6 mb-xl-9">
  <!--begin::Card header-->

  <!--begin::Card body-->
  <div class="card-body pt-0">
    <!--begin::Option-->
    <div class="py-6" data-kt-customer-payment-method="row">

      <!--begin::Body-->
      <div id="kt_customer_view_payment_method_1" class="collapse show fs-6 ps-10" data-bs-parent="#kt_customer_view_payment_method">
        <!--begin::Row-->
        <div class="row py-5">
          <!--begin::Row-->
          <div class="col-md-12">
            <!--begin::Item-->
            <div class="d-flex mb-3">
              <h4 class="text-gray-800 fw-bold">DEPOSIT METHOD </h4>

            </div>
            <!--end::Item-->
            <form method="POST" action="{{route('updateUserBank',$user->uuid)}}" class="form">
          <!--begin::Card body-->
          <div class="card-body border-top p-9">

            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama Bank</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <select class="form-select form-select-solid" name="bank_name" required>
                  <option value="">Pilih Bank</option>

                  @foreach ($listAllow as $x => $allow)
                    <option value="{{$allow->bank_name}}" {{$bank->bank_name == $allow->bank_name ? "selected" : ""}} >{{$allow->bank_name}}</option>
                  @endforeach

                </select>
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nomor Rekening</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid {{ old('account_number') && !$errors->has('account_number') ? 'input-valid' : '' }} {{$errors->has('account_number') ? 'input-error' : ''}}" type="text" placeholder="" name="account_number" autocomplete="off" value="{{ old('account_number') ? old('account_number') : $bank->account_number }}" required {{$errors->has('account_number') ? 'autofocus' : ''}}/>
                @if ($errors->has('account_number'))
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('account_number') }}</h4>
                @endif
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama di Rekening</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid {{ old('account_name') && !$errors->has('account_name') ? 'input-valid' : '' }} {{$errors->has('account_name') ? 'input-error' : ''}}" type="text" placeholder="" name="account_name" autocomplete="off" value="{{ old('account_name') ? old('account_name') : $bank->account_name }}" required {{$errors->has('account_name') ? 'autofocus' : ''}}/>
                @if ($errors->has('account_name'))
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('account_name') }}</h4>
                @endif
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
          </div>
          <!--end::Card body-->
          @csrf
          <!--begin::Actions-->
          <div class="card-footer d-flex justify-content-end py-6 px-9">
            <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Submit</button>
          </div>
          <!--end::Actions-->
        </form>

          </div>
          <!--end::Row-->
        </div>
        <!--end::Row-->
      </div>
      <!--end::Body-->
    </div>
    <!--end::Option-->
  </div>
</div>
