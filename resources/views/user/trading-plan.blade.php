@extends('template.master')

@section('title','Trading Plan')
@section('bc-1','Trading Plan')
@section('bc-2','Upload')

@section('container')

  <div id="kt_content_container" class="container">
    <div class="row">
      <div class="col-md-6">
        <!--begin::Basic info-->
        <div class="card mb-5 mb-xl-10">
          <!--begin::Card header-->
          <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
            <!--begin::Card title-->
            <div class="card-title m-0">
              <h3 class="fw-bolder m-0">Import Trading Plan </h3>
            </div>
            <!--end::Card title-->
          </div>
          <!--begin::Card header-->
          <!--begin::Content-->
          <div class="card-body">
            <form class="form" action="{{route('ImportPdfTradingPlan')}}" enctype="multipart/form-data" method="post">
              <input class="form-control form-control-lg mb-6" type="file" name="pdf">
              @csrf
              <input class="btn btn-primary" type="submit" name="submit" value="submit">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

