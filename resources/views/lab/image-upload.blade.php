@extends('template.master')

@section('title','Getting Started')
@section('metadescription','Onboarding Client')
@section('metakeyword','Onboarding')
@section('bc-1','Nonono')
@section('bc-2','Onboarding')

@section('container')

  <div id="kt_content_container" class="container">


    <!--begin::Form-->
    <form class="form" action="#" method="post">
        <!--begin::Input group-->
        <div class="fv-row">
            <!--begin::Dropzone-->
            <div class="dropzone" id="kt_dropzonejs_example_1">
                <!--begin::Message-->
                <div class="dz-message needsclick">
                    <!--begin::Icon-->
                    <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                    <!--end::Icon-->

                    <!--begin::Info-->
                    <div class="ms-4">
                        <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Drop files here or click to upload.</h3>
                        <span class="fs-7 fw-bold text-gray-400">Upload 1 file</span>
                    </div>
                    <!--end::Info-->
                </div>
            </div>
            <!--end::Dropzone-->
        </div>
        <!--end::Input group-->
    </form>
    <!--end::Form-->



  </div>

@endsection


@section('jsonpage')
  <script type="text/javascript">

  var myDropzone = new Dropzone("#kt_dropzonejs_example_1", {
      url: "{{route('process-Upload-Image')}}", // Set the url for your upload script location
      paramName: "gambar", // The name that will be used to transfer the file
      method: "post",
      headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
      maxFiles: 1,
      maxFilesize: 10, // MB
      addRemoveLinks: true,
      accept: function(file, done) {
        if (file.name == "wow.jpg") {
          done("Naha, you don't.");
          } else {
              done();
          }
      }
    });
  </script>
@endsection
