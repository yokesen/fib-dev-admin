<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head>
    <meta charset="utf-8" />
		<title>Ga boleh Akses</title>
		<meta name="robots" content="noindex">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="shortcut icon" href="{{url('/')}}/images/favicon.png" />

		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="{{url('/')}}/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/assets-1/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="{{url('/')}}/assets/css/custom.css" rel="stylesheet" type="text/css" />

	</head>

  <body id="kt_body" class="bg-gold header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed toolbar-tablet-and-mobile-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
    <!--begin::Main-->
    <div class="d-flex flex-column flex-root">
      <!--begin::Authentication - Sign-up -->
      <div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
        <!--begin::Content-->
        <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
          @include('template.layout.auth._logo')
	<!--end::Head-->

          <!--begin::Card-->
          <div class="card rounded">
            <!--begin::Card body-->
            <div class="card-body pt-12 p-9 text-center">
              <h4>Hey! ngapain ke sini?</h4>

              <a href="{{route('viewDashboard')}}" class="btn btn-primary btn-lg">Klik di sini untuk ke halaman utama</a>
            </div>
						<!--begin::Image-->
						<div class="flex-grow-1 bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-bottom card-rounded-bottom h-200px" style="background-image:url('/assets/media/illustrations/alert-2.png')"></div>
						<!--end::Image-->
          </div>
					<!--begin::Footer-->
					<div class="d-flex flex-center flex-wrap fs-6 p-5 pb-0">
						<!--begin::Links-->
						<div class="d-flex flex-center fw-bold fs-6">

						</div>
						<!--end::Links-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Body-->
			</div>
			<!--end::Authentication - Sign-up-->
		</div>

	</body>
	<!--end::Body-->
</html>
