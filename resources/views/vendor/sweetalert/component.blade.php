<?php
Swal.fire({"title":"Kamu Udah logout!!","text":"Cepat balik ke sini lagi ya!!","width":"32rem","heightAuto":true,"padding":"1.25rem","showConfirmButton":true,"showCloseButton":false,"customClass":{"container":null,"popup":"rounded","header":null,"title":null,"closeButton":"rounded-1","icon":null,"image":null,"content":null,"input":null,"actions":null,"confirmButton":"rounded-1","cancelButton":"rounded-1","footer":null},"icon":"success","confirmButtonText":"Okei dokei!","confirmButtonColor":"#DB1430","allowOutsideClick":false});

Alert::alert('Title', 'Message', 'Type');

Alert::success('Success Title', 'Success Message');

Alert::info('Info Title', 'Info Message');

Alert::warning('Warning Title', 'Warning Message');

Alert::error('Error Title', 'Error Message');

Alert::question('Question Title', 'Question Message');

Alert::image('Image Title!','Image Description','Image URL','Image Width','Image Height');

Alert::html('Html Title', 'Html Code', 'Type');

Alert::toast('Toast Message', 'Toast Type');

alert('Title','Lorem Lorem Lorem', 'success');

alert()->success('Title','Lorem Lorem Lorem');

alert()->info('Title','Lorem Lorem Lorem');

alert()->warning('Title','Lorem Lorem Lorem');

alert()->error('Title','Lorem Lorem Lorem');

alert()->question('Title','Lorem Lorem Lorem');

alert()->image('Image Title!','Image Description','Image URL','Image Width','Image Height');

alert()->html('<i>HTML</i> <u>example</u>'," You can use <b>bold text</b>, <a href='//github.com'>links</a> and other HTML tags ",'success');

toast('Your Post as been submited!','success');

// example:persistent($showConfirmBtn = true, $showCloseBtn = false)
alert()->success('SuccessAlert','Lorem ipsum dolor sit amet.')->persistent(true,false);
// example:autoClose($milliseconds = 5000)
toast('Success Toast','success')->autoClose(5000);
// example:position($position = 'top-end')
alert('Title','Lorem Lorem Lorem', 'success')->position('top-end');
// example:showConfirmButton($btnText = 'Ok', $btnColor = '#3085d6')
alert()->success('SuccessAlert','Lorem ipsum dolor sit amet.')->showConfirmButton('Confirm', '#DB1430');
// example:showCancelButton($btnText = 'Cancel', $btnColor = '#aaa')
alert()->question('Are you sure?','You won\'t be able to revert this!')->showCancelButton('Cancel', '#aaa');
// example:showCloseButton($closeButtonAriaLabel = 'aria-label')
toast('Post Updated','success','top-right')->showCloseButton();
// example:hideCloseButton()
toast('Post Updated','success','top-right')->hideCloseButton();
// example:reverseButtons()
alert()->question('Are you sure?','You won\'t be able to revert this!')
->showConfirmButton('Yes! Delete it', '#3085d6')
->showCancelButton('Cancel', '#aaa')->reverseButtons();
// example:footer($HTMLcode)
alert()->error('Oops...', 'Something went wrong!')->footer('<a href="#">Why do I have this issue?</a>');
// example:toToast($position = 'top-right')
alert()->success('Post Created', 'Successfully')->toToast();
// example:toHtml()
alert()->success('Post Created', '<strong>Successfully</strong>')->toHtml();
// example:addImage($imageUrl)
alert('Title','Lorem Lorem Lorem', 'success')->addImage('https://unsplash.it/400/200');
// example:width('32rem')
alert('Title','Lorem Lorem Lorem', 'success')->width('720px');
// example:padding('1.25rem')
alert('Title','Lorem Lorem Lorem', 'success')->padding('50px');
// example:background('#fff')
alert('Title','Lorem Lorem Lorem', 'success')->background('#fff');
// example:animation($showAnimation, $hideAnimation)
alert()->info('InfoAlert','Lorem ipsum dolor sit amet.')
->animation('tada faster','fadeInUp faster');
// example:buttonsStyling($buttonsStyling)
alert()->success('Post Created', 'Successfully')->buttonsStyling(false);
// example:iconHtml($iconHtml)
alert()->success('Post Created', 'Successfully')->iconHtml('<i class="far fa-thumbs-up"></i>);
// example:focusConfirm(true)
alert()->question('Are you sure?','You won\'t be able to revert this!')->showCancelButton()->showConfirmButton()->focusConfirm(true);
// example:focusCancel(false)
alert()->question('Are you sure?','You won\'t be able to revert this!')->showCancelButton()->showConfirmButton()->focusCancel(true);
// example:timerProgressBar(
toast('Signed in successfully','success')->timerProgressBar();

config/sweetalert.php
SWEET_ALERT_MIDDLEWARE_AUTO_CLOSE=false
SWEET_ALERT_MIDDLEWARE_TOAST_POSITION='top-end'
SWEET_ALERT_MIDDLEWARE_TOAST_CLOSE_BUTTON=true
SWEET_ALERT_MIDDLEWARE_ALERT_CLOSE_TIME=5000
SWEET_ALERT_AUTO_DISPLAY_ERROR_MESSAGES=true
