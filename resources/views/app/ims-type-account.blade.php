@extends('template.master')

@section('title','Getting Started')
@section('metadescription','Metatrader')
@section('metakeyword','metatrader')
@section('bc-1','Account')
@section('bc-2','MT5')

@section('container')

  <div id="kt_content_container" class="container">
    @include('app.partials._account-type')
  </div>
@endsection

@section('jsinline')
<script type="text/javascript">
  $( document ).ready(function() {
    @foreach ($listCategories as $x => $category)
      @if ($x>0)
        $(".{{$category->namaCategory}}").hide();
      @endif
      $("#{{$category->namaCategory}}").click(function(){
        @foreach ($listCategories as $cat)
          @if ($category->namaCategory == $cat->namaCategory)
            $(".{{$cat->namaCategory}}").show();
          @else
            $(".{{$cat->namaCategory}}").hide();
          @endif
        @endforeach
      });
    @endforeach
  });


</script>
@endsection
