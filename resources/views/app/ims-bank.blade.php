@extends('template.master')

@section('title','Getting Started')
@section('metadescription','Onboarding Client')
@section('metakeyword','Onboarding')
@section('bc-1','Dashboard')
@section('bc-2','Onboarding')

@section('container')

  <div id="kt_content_container" class="container">
    @include('app.partials._user-card')


    @include('app.partials._bank-edit')
  </div>
@endsection

@section('jsinline')
  @if($user->photoTabungan == '/images/upload-default.jpg')
    <script type="text/javascript">
    var myDropzone=new Dropzone("#ot_buku_rekening",{url:"{{route('process-Upload-Tabungan')}}",paramName:"gambar",method:"post",headers:{"X-CSRF-TOKEN":"{{ csrf_token() }}"},maxFiles:1,maxFilesize:10,addRemoveLinks:!0,accept:function(e,a){"wow.jpg"==e.name?a("Naha, you don't."):a()}});
    </script>
  @endif
@endsection
