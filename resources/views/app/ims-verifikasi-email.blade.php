@extends('template.master')

@section('title','Getting Started')
@section('bc-1','Dashboard')
@section('bc-2','Onboarding')

@section('container')
  <div id="kt_content_container" class="container">
    @include('app.partials._verifikasi-email')
  </div>
@endsection
