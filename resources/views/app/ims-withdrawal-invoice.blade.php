@extends('template.master')

@section('title','Getting Started')
@section('bc-1','Withdrawal')
@section('bc-2','Detail')

@section('container')

  <div id="kt_content_container" class="container">
    @include('app.partials._withdrawal-invoice')
  </div>
@endsection
