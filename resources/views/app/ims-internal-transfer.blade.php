@extends('template.master')

@section('title','Cashier | Internal Transfer')
@section('bc-1','Internal Transfer')
@section('bc-2','Create')

@section('container')

  <div id="kt_content_container" class="container">
    @include('app.partials._internal-transfer')
  </div>
@endsection

@section('jsinline')
  <script type="text/javascript">
  function fixDecimal(e) {
    return e.value = e.value.toString().match(/^\d+(?:\.\d{0,2})?/);
  }
  </script>
@endsection
