@extends('template.master')

@section('title','Getting Started')
@section('bc-1','Dashboard')
@section('bc-2','Onboarding')

@section('container')

  <div id="kt_content_container" class="container">
    @include('app.partials._user-card')


    @include('app.partials._profil-edit')
  </div>
@endsection

@section('jsinline')
  @if($user->photoKTP == '/images/upload-default.jpg')
    <script type="text/javascript">
      myDropzone=new Dropzone("#ot_ktp",{url:"{{route('process-Upload-Ktp')}}",paramName:"gambar",method:"post",headers:{"X-CSRF-TOKEN":"{{ csrf_token() }}"},maxFiles:1,maxFilesize:10,addRemoveLinks:!0,accept:function(e,a){"wow.jpg"==e.name?a("Naha, you don't."):a()}});
    </script>
  @endif
  <script type="text/javascript">
    $("#username").keyup(function(){var e=this.value;console.log(e.length),e.length>3&&$.ajax({type:"POST",url:"{{route('ajaxSearchUsername')}}",data:{username:e},headers:{"X-CSRF-TOKEN":"{{ csrf_token() }}"},success:function(e){0==e?$("#chooseUsername").html(""):($("#chooseUsername").html(e.html),document.getElementById("username").value="")}})});
    $("#ot_dob").flatpickr();

    
  </script>
@endsection
