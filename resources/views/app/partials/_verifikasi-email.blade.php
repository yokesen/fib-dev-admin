<div class="row">
  <div class="col-md-6 offset-md-3">
    <!--begin::Engage Widget 1-->
    <div class="card rounded mb-5 mb-xl-10">
      <!--begin::Body-->
      <div class="card-body pb-0">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column justify-content-between h-100">
          <!--begin::Section-->
          <div class="pt-15 mb-10">
            <!--begin::Title-->
            <h3 class="text-success text-center fs-1 fw-bolder lh-lg">Selamat!!
              <br />Whatsapp Kamu udah terverifikasi.</h3>
              <!--end::Title-->

              @if (profile()->email_verification != 'verified')
                <!--begin::Text-->
                <div class="text-center text-gray-600 fs-6 fw-bold pt-4 pb-1">Sekarang saatnya verifikasi email ya.
                  <br />Kalau belum menerima, silahkan klik tombol di bawah ini.</div>
                  <!--end::Text-->
                  <!--begin::Action-->
                  <div class="text-center py-7">
                    <a href="{{route('askToVerifyMail')}}" class="btn btn-primary fs-6 px-6">Kirim Ulang Link Verifikasi</a>
                  </div>
                  <!--end::Action-->
                  <div class="text-center fs-6 pt-4 pb-1">
                    <p>Kami sudah mengirimkan link verifikasi ke
                    <br />{{profile()->email}}.</p>
                    <p> <small>(Bisa jadi ga masuk di inbox, mohon bantuan check di folder promotion atau spam/junk ya)</small> </p>
                  </div>
                </div>
                <!--end::Section-->

              @else

                <div class="text-center py-7">
                  <a href="{{route('viewDashboard')}}" class="btn btn-primary fs-6 px-6">Klik untuk melanjutkan</a>
                </div>
              @endif



              <!--begin::Image-->
              <div class="flex-grow-1 bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-bottom card-rounded-bottom h-200px" style="background-image:url('assets/media/illustrations/work.png')"></div>
              <!--end::Image-->
            </div>
            <!--end::Wrapper-->
          </div>
          <!--end::Body-->
        </div>
        <!--end::Engage Widget 1-->
  </div>
</div>
