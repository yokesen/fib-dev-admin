<div class="row">
  <div class="col-md-12 text-end mb-6">
    @if (count(mt4accounts())>1)
      <a href="{{route('createInternalTransfer')}}" class="btn btn-primary"><i class="fas fa-plus"></i> Request Internal Transfer</a>
    @endif
  </div>
</div>
<div class="row">
  @if (count(mt4accounts())>1)
    @foreach ($withdrawals as $wd)
      @php
        if ($wd->status == "pending") {
          $warna = 'info';
          $icon = 'fa-clock';
        }elseif($wd->status == "approved"){
          $warna = 'success';
          $icon = 'fa-check-circle';
        }elseif($wd->status == "rejected"){
          $warna = 'danger';
          $icon = 'fa-times-circle';
        }
      @endphp
      <div class="col-md-4 mb-6">
      	<!--begin::Statistics Widget 5-->
      	<a href="#" class="card bg-{{$warna}} bgi-no-repeat rounded hoverable card-xl-stretch mb-xl-8" style="background-position: right top; background-position: 90% 20px; background-size: 30% auto; background-image: url({{env('IMG_LOGO')}});">
      		<!--begin::Body-->
      		<div class="card-body text-start">
            <i class="far {{$icon}} text-white ms-n1 fa-3x"></i> <span class="text-white">{{ \Carbon\Carbon::parse(strtotime($wd->created_at.'+7hours'))->diffForHumans()}}</span>
            <div class="text-inverse-warning fw-bolder fs-1 mb-2 mt-5">US$ {{number_format($wd->amount,2,'.',',')}}</div>
      			<div class="fw-bold text-inverse-warning fs-7 mb-5">Internal Transfer</div>
            <span class="float-end text-inverse-warning fw-bolder fs-1">{{$wd->metatrader}} <i class="far fa-arrow-alt-circle-right fs-2x text-white"></i> {{$wd->internalToAccountMeta}}</span>
      		</div>
      		<!--end::Body-->
      	</a>
      	<!--end::Statistics Widget 5-->
      </div>
    @endforeach

  @else
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <!--begin::Engage Widget 1-->
        <div class="card rounded mb-5 mb-xl-10">
          <!--begin::Body-->
          <div class="card-body pb-0">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column justify-content-between h-100">
              <!--begin::Section-->
              <div class="pt-15 mb-10">
                <!--begin::Title-->
                <h3 class="text-grey-800 text-center fs-1 fw-bolder lh-lg">Hi {{profile()->name}}!</h3>
                <p class="text-grey-800 text-center">Untuk bisa melakukan Internal Transfer, minimum harus punya 2 account trading</p>
                <p class="text-grey-800 text-center">Yuk bikin akun satu lagi!</p>

                <div class="text-center py-7">
                  <a href="{{route('viewAccountmt4')}}" class="btn btn-primary fs-6 px-6">Klik untuk Request Account MT4</a>
                </div>


                  <!--begin::Image-->
                  <div class="flex-grow-1 bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-bottom card-rounded-bottom h-200px" style="background-image:url('/assets/media/illustrations/process-2.png')"></div>
                  <!--end::Image-->
                </div>
                <!--end::Wrapper-->
              </div>
              <!--end::Body-->
            </div>
            <!--end::Engage Widget 1-->
      </div>
    </div>
  </div>

  @endif

</div>
