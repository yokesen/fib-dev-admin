
<div class="card rounded ">
  <!--begin::Body-->
  <div class="card-body p-lg-20">
    <!--begin::Layout-->
    <div class="d-flex flex-column flex-xl-row">
      <!--begin::Content-->
      <div class="flex-lg-row-fluid me-xl-18 mb-10 mb-xl-0">
        <!--begin::Invoice 2 content-->
        <div class="mt-n1">
          <!--begin::Top-->
          <div class="d-flex flex-stack pb-10">
            <!--begin::Logo-->
            <a href="#">
              <img alt="Logo" src="{{ENV('IMG_LOGO')}}" width="200"/>
            </a>
            <!--end::Logo-->
            <!--begin::Action-->
						<a href="{{route('viewWithdrawalList')}}" class="btn btn-sm btn-primary">Back</a>
						<!--end::Action-->
          </div>
          <!--end::Top-->
          <!--begin::Wrapper-->
          <div class="m-0">
            <!--begin::Label-->
            <div class="fw-bolder fs-3 text-gray-800 mb-8">WITHDRAWAL #{{$withdrawal->id}}</div>
            <!--end::Label-->
            <!--begin::Row-->
            <div class="row g-5 mb-11">
              <!--end::Col-->
              <div class="col-sm-6">
                <!--end::Label-->
                <div class="fw-bold fs-7 text-gray-600 mb-1">Create Date:</div>
                <!--end::Label-->
                <!--end::Col-->
                <div class="fw-bolder fs-6 text-gray-800">{{date('d-F-y H:i:s',strtotime($withdrawal->created_at.'+7hours'))}} WIB</div>
                <!--end::Col-->
              </div>
              <!--end::Col-->
              <!--end::Col-->
              <div class="col-sm-6">
                <!--end::Label-->
                <div class="fw-bold fs-7 text-gray-600 mb-1">Respond Date:</div>
                <!--end::Label-->
                <!--end::Info-->
                <div class="fw-bolder fs-6 text-gray-800 d-flex align-items-center flex-wrap">
                  <span class="pe-2">{{date('d-F-y H:i:s',strtotime($withdrawal->updated_at.'+7hours'))}} WIB</span>
                  <span class="fs-7 text-danger d-flex align-items-center">
                    @php
                    $dateCreate = Carbon\Carbon::parse(strtotime($withdrawal->created_at.'+7hours'));
                    $dateUpdate = Carbon\Carbon::parse(strtotime($withdrawal->updated_at.'+7hours'));
                    @endphp
                    <span class="bullet bullet-dot bg-danger me-2"></span>{{$dateUpdate->diffForHumans($dateCreate)}}</span>
                  </div>
                  <!--end::Info-->
                </div>
                <!--end::Col-->
              </div>
              <!--end::Row-->
              <!--begin::Row-->
              <div class="row g-5 mb-12">
                <!--end::Col-->
                <div class="col-sm-6">
                  <!--end::Label-->
                  <div class="fw-bold fs-7 text-gray-600 mb-1">Withdrawal From:</div>
                  <!--end::Label-->
                  <!--end::Text-->
                  <div class="fw-bolder fs-6 text-gray-800">{{$withdrawal->metatrader}}</div>
                  <!--end::Text-->
                  <!--end::Description-->
                  <div class="fw-bold fs-7 text-gray-600">account type - {{account4($withdrawal->metatrader)->typeAccount}}</div>
                  <!--end::Description-->
                </div>
                <!--end::Col-->
                <!--end::Col-->
                <div class="col-sm-6">
                  <!--end::Label-->
                  <div class="fw-bold fs-7 text-gray-600 mb-1">Withdrawal By:</div>
                  <!--end::Label-->
                  <!--end::Text-->
                  <div class="fw-bolder fs-6 text-gray-800">{{profile()->username}}</div>
                  <!--end::Text-->
                  <!--end::Description-->
                  <div class="fw-bold fs-7 text-gray-600">{{profile()->name}}</div>
                  <!--end::Description-->
                </div>
                <!--end::Col-->
              </div>
              <!--end::Row-->
              <!--begin::Content-->
              <div class="flex-grow-1">
                @if (agent()->platform() == 'Windows')
                  <!--begin::Table-->
                  <div class="table-responsive border-bottom mb-9">
                    <table class="table mb-3">
                      <thead>
                        <tr class="border-bottom fs-6 fw-bolder text-gray-400">
                          <th class="min-w-175px pb-2">Description</th>
                          <th class="min-w-70px text-end pb-2">Transfer Amount</th>
                          <th class="min-w-80px text-end pb-2">Rate</th>
                          <th class="min-w-100px text-end pb-2">Approved Amount</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="fw-bolder text-gray-700 fs-5 text-end">
                          @if ($withdrawal->status == "pending")
                            <td class="d-flex align-items-center pt-6"><i class="fa fa-genderless text-warning fs-2 me-2"></i>Withdrawal Account</td>

                          @elseif($withdrawal->status == "approved")
                            <td class="d-flex align-items-center pt-6"><i class="fa fa-genderless text-success fs-2 me-2"></i>Withdrawal Account</td>

                          @elseif($withdrawal->status == "rejected")
                            <td class="d-flex align-items-center pt-6"><i class="fa fa-genderless text-danger fs-2 me-2"></i>Withdrawal Account</td>

                          @endif
                          <td class="pt-6">US$ {{number_format($withdrawal->amount,2,'.',',')}}</td>

                          @if ($withdrawal->process_by == "import from TF")
                            <td class="pt-6">Historical Rate</td>
                            <td class="pt-6 text-dark fw-boldest">Check Bank Account</td>
                          @else
                            @if ($withdrawal->status == "approved")
                              <td class="pt-6">{{number_format($withdrawal->currency_rate,'0','.','.')}}</td>
                              <td class="pt-6 text-dark fw-boldest">IDR {{number_format($withdrawal->approved_amount,0,'.',',')}}</td>
                            @else
                              <td class="pt-6">waiting</td>
                              <td class="pt-6 text-dark fw-boldest">waiting</td>
                            @endif
                          @endif


                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <!--end::Table-->
                @else
                  <!--begin::Table-->
                  <div class="table-responsive border-bottom mb-9">
                    <table class="table mb-3">
                      <tbody>
                          <tr class="fw-bolder text-gray-700 fs-5">
                            <td class="pt-6">Description</td>
                            @if ($withdrawal->status == "pending")
                              <td class="pt-6 text-end"><i class="fa fa-genderless text-warning fs-2 me-2"></i>Withdrawal Account</td>
                            @elseif($withdrawal->status == "approved")
                              <td class="pt-6 text-end"><i class="fa fa-genderless text-success fs-2 me-2"></i>Withdrawal Account</td>
                            @elseif($withdrawal->status == "rejected")
                              <td class="pt-6 text-end"><i class="fa fa-genderless text-danger fs-2 me-2"></i>Withdrawal Account</td>
                            @endif
                          </tr>
                          <tr class="fw-bolder text-gray-700 fs-5">
                            <td class="pt-6">Transfer Amount</td>
                            <td class="pt-6 text-end">US$ {{number_format($withdrawal->amount,2,'.',',')}}</td>
                          </tr>
                          @if ($withdrawal->process_by == "import from TF")
                            <td class="pt-6">Historical Rate</td>
                            <td class="pt-6 text-dark fw-boldest">Check Bank Account</td>
                          @else
                            @if ($withdrawal->status == "approved")
                              <tr class="fw-bolder text-gray-700 fs-5">
                                <td class="pt-6">Rate</td>
                                <td class="pt-6 text-end">{{number_format($withdrawal->currency_rate,'0','.','.')}}</td>
                              </tr>
                              <tr class="fw-bolder text-gray-700 fs-5">
                                <td class="pt-6">Approved Amount</td>
                                <td class="pt-6 text-dark fw-boldest text-end">IDR {{number_format($withdrawal->approved_amount,0,'.',',')}}</td>
                              </tr>
                            @else
                              <tr class="fw-bolder text-gray-700 fs-5">
                                <td class="pt-6">Rate</td>
                                <td class="pt-6 text-end">Waiting</td>
                              </tr>
                              <tr class="fw-bolder text-gray-700 fs-5">
                                <td class="pt-6">Approved Amount</td>
                                <td class="pt-6 text-dark fw-boldest text-end">Waiting</td>
                              </tr>
                            @endif
                          @endif

                        </tbody>
                      </table>
                    </div>
                    <!--end::Table-->
                @endif

                  <!--begin::Container-->
                  <div class="d-flex justify-content-end">
                    <!--begin::Section-->
                    <div class="mw-300px">

                      <!--begin::Item-->
                      <div class="d-flex flex-stack">
                        <!--begin::Code-->
                        <div class="fw-bold pe-10 text-gray-600 fs-2">Total</div>
                        <!--end::Code-->
                        @if ($withdrawal->process_by == "import from TF")

                          <div class="text-end fw-bolder fs-2 text-gray-800">Check Bank Account</div>
                        @else
                          @if ($withdrawal->status == "approved")
                            <div class="text-end fw-bolder fs-2 text-gray-800">IDR {{number_format($withdrawal->approved_amount,0,'.',',')}}</div>
                          @else
                            <div class="text-end fw-bolder fs-2 text-gray-800">Waiting</div>
                          @endif
                        @endif

                        <!--begin::Label-->

                        <!--end::Label-->
                      </div>
                      <!--end::Item-->
                    </div>
                    <!--end::Section-->
                  </div>
                  <!--end::Container-->
                </div>
                <!--end::Content-->
              </div>
              <!--end::Wrapper-->
            </div>
            <!--end::Invoice 2 content-->
          </div>
          <!--end::Content-->
          <!--begin::Sidebar-->
          <div class="m-0">
            <!--begin::Invoice 2 sidebar-->
            <div class="border border-dashed border-gray-300 card-rounded h-lg-100 min-w-md-350px p-9 bg-lighten">
              <!--begin::Labels-->
              <div class="mb-8">
                @if ($withdrawal->status == "pending")
                  <span class="badge badge-light-warning">Pending</span>
                @elseif($withdrawal->status == "approved")
                  <span class="badge badge-light-success">Approved</span>
                @elseif($withdrawal->status == "rejected")
                  <span class="badge badge-light-danger">Rejected</span> <span class="text-danger">{{$withdrawal->reason}}</span>
                @endif
              </div>

              <!--end::Labels-->
              <!--begin::Title-->
              <h6 class="mb-8 fw-boldest text-gray-600 text-hover-primary">PAYMENT DETAILS</h6>
              <!--end::Title-->
              <!--begin::Item-->
              <div class="mb-6">
                <div class="fw-bold text-gray-600 fs-7">To Bank:</div>
                <div class="fw-bolder text-gray-800 fs-6">{{$withdrawal->bank_name}}</div>
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="mb-6">
                <div class="fw-bold text-gray-600 fs-7">To Name:</div>
                <div class="fw-bolder text-gray-800 fs-6">{{$withdrawal->account_name}}</div>
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="mb-15">
                <div class="fw-bold text-gray-600 fs-7">To Account:</div>
                <div class="fw-bolder fs-6 text-gray-800 d-flex align-items-center">{{$withdrawal->account_number}}</div>
              </div>
              <!--end::Item-->
            </div>
            <!--end::Invoice 2 sidebar-->
          </div>
          <!--end::Sidebar-->
        </div>
        <!--end::Layout-->
      </div>
      <!--end::Body-->
    </div>
    <!--end::Invoice 2 main-->
