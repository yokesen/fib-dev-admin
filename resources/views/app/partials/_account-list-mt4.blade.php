<div class="row">
  <div class="col-md-12 text-end mb-6">
    <a href="{{route('viewAccountmt4')}}" class="btn btn-primary"><i class="fas fa-plus"></i> Request Account MT4</a>
  </div>
</div>
<!--begin::Row-->
<div class="row g-6 g-xl-9">
  @foreach ($myAccounts as $x => $acc)
    <!--begin::Col-->
    <div class="col-md-6 col-xxl-4">
      <!--begin::Card-->
      <div class="card rounded">
        <!--begin::Card body-->
        <div class="card-body pt-12 p-9">

          <div style="display:flex;">
              <!--begin::Menu-->
              <div style="margin-left: auto;order: 2;">
                <button type="button" class="btn btn-sm btn-icon btn-bg-light btn-color-primary btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-flip="top-end">
                  <i class="bi bi-three-dots fs-3"></i>
                </button>
                <!--begin::Menu 3-->
                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3" data-kt-menu="true">
                  <!--begin::Heading-->
                  <div class="menu-item px-3">
                    <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Action</div>
                  </div>
                  <!--end::Heading-->
                  <!--begin::Menu item-->
                  <div class="menu-item px-3">
                    <a href="{{route('viewDeposit')}}" class="menu-link px-3">Deposit</a>
                  </div>
                  <!--end::Menu item-->
                  <!--begin::Menu item-->
                  <div class="menu-item px-3">
                    <a href="{{route('viewWithdrawal')}}" class="menu-link px-3">Withdrawal</a>
                  </div>
                  <!--end::Menu item-->
                  <!--begin::Menu item-->
                  <div class="menu-item px-3">
                    <a href="#" class="menu-link px-3">Reset Password</a>
                  </div>
                  <!--end::Menu item-->

                </div>
                <!--end::Menu 3-->
              </div>
              <!--end::Menu-->
            <div class="d-flex flex-center flex-wrap">
              <!--begin::Avatar-->
              <div class="symbol symbol-65px mb-5">
                <img src="{{url('/')}}/images/mt4-icon.png" alt="image" />
              </div>
              <!--end::Avatar-->
              <div class="ms-6">
                <!--begin::Name-->
                <a href="#"><span class="fw-bold text-gray-400 mb-6">Tipe Akun : </span><span class="fs-4 text-gray-800 text-hover-primary fw-bolder mb-0">{{$acc->typeAccount}}</span></a>
                <!--end::Name-->
                <!--begin::status-->
                @if ($acc->status == 'waiting')
                  <div class="fw-bold text-gray-400 mb-6"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Menunggu verifikasi dari admin"></i> {{$acc->status}}</div>
                @else
                  <div class="fw-bold text-gray-400 mb-6"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> {{$acc->status}}</div>
                @endif
                <!--end::status-->
              </div>
            </div>
          </div>


          <!--begin::Info-->
          <div class="row">
            <!--begin::Stats-->
            <div class="col-md-12 border border-gray-300 border-dashed rounded py-3 px-6 mb-3 text-center">
              <div class="fs-6 fw-bolder text-gray-700">MaxrichGroup-Real</div>
              <div class="fw-bold text-gray-400">Server</div>
            </div>
            <!--end::Stats-->
            <!--begin::Stats-->
            <div class="col-md-12 border border-gray-300 border-dashed rounded py-3 px-6 mb-3 text-center">
              @if ($acc->status == 'waiting')
                <div class="fs-6 fw-bolder text-gray-700">Waiting</div>
              @else
                <div class="fs-6 fw-bolder text-gray-700">{{$acc->mt4_id}}</div>
              @endif

              <div class="fw-bold text-gray-400">Login</div>
            </div>
            <!--end::Stats-->
          </div>
          <!--end::Info-->
        </div>
        <!--end::Card body-->
      </div>
      <!--end::Card-->
    </div>
    <!--end::Col-->
  @endforeach
</div>
<!--end::Row-->
