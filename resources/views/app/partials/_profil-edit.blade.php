<div class="row">
  <div class="col-md-6">
    <!--begin::Basic info-->
    <div class="card rounded mb-5 mb-xl-10">
      <!--begin::Card header-->
      <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
        <!--begin::Card title-->
        <div class="card-title m-0">
          <h3 class="fw-bolder m-0">Profil </h3>
        </div>
        <!--end::Card title-->
      </div>
      <!--begin::Card header-->
      <!--begin::Content-->
      <div id="kt_account_profile_details" class="collapse show">
        <!--begin::Form-->
        <form id="kt_account_profile_details_form" method="POST" action="{{route('process-Edit-Profile')}}" class="form" enctype="multipart/form-data">
          <!--begin::Card body-->
          <div class="card-body border-top p-9">
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label fw-bold fs-6">Avatar</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8">
                <!--begin::Image input-->
                <div class="image-input image-input-outline" data-kt-image-input="true" style="background-image: url(assets/media/avatars/blank.png)">
                  <!--begin::Preview existing avatar-->
                  <div class="image-input-wrapper w-125px h-125px" style="background-image: url({{Session::get('user')->photo}})"></div>
                  <!--end::Preview existing avatar-->
                  <!--begin::Label-->
                  <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-white shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                    <i class="bi bi-pencil-fill fs-7"></i>
                    <!--begin::Inputs-->
                    <input type="file" name="photo" max-size="3000" accept=".png, .jpg, .jpeg" />
                    <input type="hidden" name="avatar_remove" />
                    <!--end::Inputs-->
                  </label>
                  <!--end::Label-->
                  <!--begin::Cancel-->
                  <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-white shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                    <i class="bi bi-x fs-2"></i>
                  </span>
                  <!--end::Cancel-->
                  <!--begin::Remove-->
                  <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-white shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                    <i class="bi bi-x fs-2"></i>
                  </span>
                  <!--end::Remove-->
                </div>
                <!--end::Image input-->
                <!--begin::Hint-->
                <div class="form-text">Format yang bisa diupload: png, jpg, jpeg.</div>
                <div class="form-text">Maksimal 5Mb.</div>
                @if ($errors->has('photo'))
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('photo') }}</h4>
                @endif
                <!--end::Hint-->
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Username</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                @if ($user->username)
                  <input class="form-control form-control-lg form-control-solid input-valid" name="username" value="{{$user->username}}" required readonly/>
                @elseif (empty($user->username))
                  <input class="form-control form-control-lg form-control-solid {{ old('username') && !$errors->has('username') ? 'input-valid' : '' }} {{$errors->has('username') ? 'input-error' : ''}}" id="username" type="text" placeholder="" name="username" autocomplete="off" value="{{ old('username') ? old('username') : $user->username }}" required {{$errors->has('username') ? 'autofocus' : ''}}/>

                  <div id="chooseUsername">

                  </div>

                  @if ($errors->has('username'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('username') }}</h4>
                  @endif
                @endif

              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid {{ old('name') && !$errors->has('name') ? 'input-valid' : '' }} {{$errors->has('name') ? 'input-error' : ''}}" type="text" placeholder="" name="name" autocomplete="off" value="{{ old('name') ? old('name') : $user->name }}" required {{$errors->has('name') ? 'autofocus' : ''}}/>
                @if ($errors->has('name'))
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('name') }}</h4>
                @endif
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Email</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                @if ($user->email_verification == "new")
                  <input class="form-control form-control-lg form-control-solid {{ old('email') && !$errors->has('email') ? 'input-valid' : '' }} {{$errors->has('email') ? 'input-error' : ''}}" type="email" placeholder="{{$user->email}}" name="email"  autocomplete="off" value="{{ old('email') }}" required {{$errors->has('email') ? 'autofocus' : ''}}/>
                  @if ($errors->has('email'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('email') }}</h4>
                  @endif
                @else
                  <input class="form-control form-control-lg form-control-solid input-valid" type="email" name="email" value="{{$user->email}}" required/>
                @endif

              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Whatsapp</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                @if ($user->whatsapp)
                  <input class="form-control form-control-lg form-control-solid input-valid" value="{{$user->whatsapp}}" required readonly/>
                @elseif (empty($user->whatsapp))
                  <a href="https://wa.me/6281287608190/?text=verify {{$user->uuid}}" class="btn btn-success btn-lg" target="_blank"><i class="bi bi-whatsapp fs-2"></i> Verifikasi Whatsapp di sini</a>
                @endif

              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Tanggal Lahir</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">

                <input class="form-control form-control-lg form-control-solid {{ old('dob') && !$errors->has('dob') ? 'input-valid' : '' }} {{$errors->has('dob') ? 'input-error' : ''}}" type="date"  placeholder="" name="dob" value="{{ old('dob') ? old('dob') : $user->dob }}" max="{{date('Y-m-d', strtotime('- 17 years'))}}" autocomplete="off" id="ot_datepicker_2" required/>
                @if ($errors->has('dob'))
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('dob') }}</h4>
                @endif
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->

            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Communication</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <!--begin::Options-->
                <div class="d-flex align-items-center mt-3">
                  <!--begin::Option-->
                  <label class="form-check form-check-inline form-check-solid me-5">
                    <input class="form-check-input" name="consent_email" type="checkbox" value="1" {{profile()->consent_email == 1 ? 'checked' : ''}} />
                    <span class="fw-bold ps-2 fs-6">Email</span>
                  </label>
                  <!--end::Option-->
                  <!--begin::Option-->
                  <label class="form-check form-check-inline form-check-solid">
                    <input class="form-check-input" name="consent_wa" type="checkbox" value="1" {{profile()->consent_wa == 1 ? 'checked' : ''}} />
                    <span class="fw-bold ps-2 fs-6">Whatsapp</span>
                  </label>
                  <!--end::Option-->
                </div>
                <!--end::Options-->
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-0">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label fw-bold fs-6">Allow Marketing</label>
              <!--begin::Label-->
              <!--begin::Label-->
              <div class="col-lg-8 d-flex align-items-center">
                <div class="form-check form-check-solid form-switch fv-row">
                  <input class="form-check-input w-45px h-30px" type="checkbox" name="consent_marketing" id="consent_marketing" value="1" {{profile()->consent_marketing == 1 ? 'checked' : ''}} />
                  <label class="form-check-label" for="allowmarketing"></label>
                </div>
              </div>
              <!--begin::Label-->
            </div>
            <!--end::Input group-->
          </div>
          <!--end::Card body-->
          @csrf
          <!--begin::Actions-->
          <div class="card-footer d-flex justify-content-end py-6 px-9">
            <a href="{{route('viewGettingStarted')}}" type="reset" class="btn btn-white btn-active-light-primary me-2">Batal</a>
            <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Submit</button>
          </div>
          <!--end::Actions-->
        </form>
        <!--end::Form-->
      </div>
      <!--end::Content-->
    </div>
  </div>
  <div class="col-md-6">
    @if($user->photoKTP == '/images/upload-default.jpg')
      <!--begin::Basic info-->
      <div class="card rounded mb-5 mb-xl-10">
        <!--begin::Card header-->
        <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
          <!--begin::Card title-->
          <div class="card-title m-0">
            <h3 class="fw-bolder m-0">Dokumen KTP </h3>
          </div>
          <!--end::Card title-->
        </div>
        <!--begin::Card header-->
        <!--begin::Content-->
        <div class="collapse show">
          <!--begin::Form-->
          <form class="form" action="#" method="post">
            <!--begin::Card body-->
            <div class="card-body border-top p-9">

              <!--begin::Input group-->
              <div class="fv-row">
                <!--begin::Dropzone-->
                <div class="dropzone" id="ot_ktp">
                  <!--begin::Message-->
                  <div class="dz-message needsclick">
                    <!--begin::Icon-->
                    <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                    <!--end::Icon-->

                    <!--begin::Info-->
                    <div class="ms-4">
                      <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Klik di sini untuk upload.</h3>
                      <span class="fs-7 fw-bold text-gray-400">Upload foto KTP/SIM/Paspor</span>
                    </div>
                    <!--end::Info-->
                  </div>
                </div>
                <!--end::Dropzone-->
              </div>
              <!--end::Input group-->
              <div class="form-text">Format yang bisa diupload: png, jpg, jpeg.</div>
              <div class="form-text">Maksimal 5Mb.</div>
            </div>
            <!--end::Card body-->
          </form>
          <!--end::Form-->

        </div>
      </div>
    @endif
  </div>
</div>
