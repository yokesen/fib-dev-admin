<div class="row">
  <div class="col-md-12 text-end mb-6">
    <a href="{{route('viewDeposit')}}" class="btn btn-primary"><i class="fas fa-plus"></i> Request Deposit</a>
  </div>
</div>
<div class="row">
  @foreach ($deposits as $depo)
    @php
      if ($depo->status == "pending") {
        $warna = 'info';
        $icon = 'fa-clock';
      }elseif($depo->status == "approved"){
        $warna = 'success';
        $icon = 'fa-check-circle';
      }elseif($depo->status == "rejected"){
        $warna = 'danger';
        $icon = 'fa-times-circle';
      }elseif($depo->status == "expired"){
        $warna = 'danger';
        $icon = 'fa-times-circle';
      }
    @endphp
    <div class="col-md-4 mb-6">
    	<!--begin::Statistics Widget 5-->
      @if(!$depo->metatrader)
        <a href="{{route('viewDepositInvoice',[$depo->id,0,uuid()])}}" class="card bg-{{$warna}} bgi-no-repeat rounded hoverable card-xl-stretch mb-xl-8" style="background-position: right top; background-position: 90% 20px; background-size: 30% auto; background-image: url({{env('IMG_LOGO')}})">
      @else
        <a href="{{route('viewDepositInvoice',[$depo->id,$depo->metatrader,uuid()])}}" class="card bg-{{$warna}} bgi-no-repeat rounded hoverable card-xl-stretch mb-xl-8" style="background-position: right top; background-position: 90% 20px; background-size: 30% auto; background-image: url({{env('IMG_LOGO')}})">
      @endif

    		<!--begin::Body-->
    		<div class="card-body">
          <i class="far {{$icon}} text-white ms-n1 fa-3x"></i> <span class="text-white">{{ \Carbon\Carbon::parse(strtotime($depo->created_at.'+7hours'))->diffForHumans()}}</span>
          <div class="text-inverse-warning fw-bolder fs-1 mb-2 mt-5">IDR {{number_format($depo->amount)}}</div>
    			<div class="fw-bold text-inverse-warning fs-7 mb-5"><span class="text-white">Deposit</span> </div>
          <span class="float-end text-inverse-warning fw-bolder fs-1">Account ID : {{$depo->metatrader}}</span>
    		</div>
    		<!--end::Body-->
    	</a>
    	<!--end::Statistics Widget 5-->
    </div>
  @endforeach
</div>
