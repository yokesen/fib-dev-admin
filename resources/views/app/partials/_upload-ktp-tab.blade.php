<div class="row">
  <div class="col-md-6">
    @if($user->photoKTP == '/images/upload-default.jpg')
      <!--begin::Basic info-->
      <div class="card rounded mb-5 mb-xl-10">
        <!--begin::Card header-->
        <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
          <!--begin::Card title-->
          <div class="card-title m-0">
            <h3 class="fw-bolder m-0">Dokumen KTP </h3>
          </div>
          <!--end::Card title-->
        </div>
        <!--begin::Card header-->
        <!--begin::Content-->
        <div class="collapse show">
          <!--begin::Form-->
          <form class="form" action="#" method="post">
            <!--begin::Card body-->
            <div class="card-body border-top p-9">

              <!--begin::Input group-->
              <div class="fv-row">
                <!--begin::Dropzone-->
                <div class="dropzone" id="ot_ktp">
                  <!--begin::Message-->
                  <div class="dz-message needsclick">
                    <!--begin::Icon-->
                    <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                    <!--end::Icon-->

                    <!--begin::Info-->
                    <div class="ms-4">
                      <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Klik di sini untuk upload.</h3>
                      <span class="fs-7 fw-bold text-gray-400">Upload foto KTP/SIM/Paspor</span>
                    </div>
                    <!--end::Info-->
                  </div>
                </div>
                <!--end::Dropzone-->
              </div>
              <!--end::Input group-->
              <div class="form-text">Format yang bisa diupload: png, jpg, jpeg.</div>
              <div class="form-text">Maksimal 5Mb.</div>
            </div>
            <!--end::Card body-->
          </form>
          <!--end::Form-->

        </div>
      </div>
    @endif
    @if($user->photoTabungan == '/images/upload-default.jpg')
      <!--begin::Basic info-->
      <div class="card mb-5 mb-xl-10">
        <!--begin::Card header-->
        <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
          <!--begin::Card title-->
          <div class="card-title m-0">
            <h3 class="fw-bolder m-0">Dokumen Buku Tabungan </h3>
          </div>
          <!--end::Card title-->
        </div>
        <!--begin::Card header-->
        <!--begin::Content-->
        <div class="collapse show">
          <!--begin::Form-->
          <form class="form" action="#" method="post">
            <!--begin::Card body-->
            <div class="card-body border-top p-9">

              <!--begin::Input group-->
              <div class="fv-row">
                <!--begin::Dropzone-->
                <div class="dropzone" id="ot_buku_rekening">
                  <!--begin::Message-->
                  <div class="dz-message needsclick">
                    <!--begin::Icon-->
                    <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                    <!--end::Icon-->

                    <!--begin::Info-->
                    <div class="ms-4">
                      <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Klik di sini untuk upload.</h3>
                      <span class="fs-7 fw-bold text-gray-400">Upload foto cover buku tabungan</span>
                    </div>
                    <!--end::Info-->
                  </div>
                </div>
                <!--end::Dropzone-->
              </div>
              <!--end::Input group-->

              <!--begin::Info-->
              <div class="form-text">Format yang bisa diupload: png, jpg, jpeg.</div>
              <div class="form-text">Maksimal 5Mb.</div>
              <!--end::Info-->
            </div>
            <!--end::Card body-->
          </form>
          <!--end::Form-->
        </div>
      </div>
    @endif
    <div class="card mb-5 mb-xl-10">
      <div class="card-body">
        <form method="POST" action="{{route('process-submit-bukti')}}" class="form">
          @csrf
          <!--begin::Actions-->
          <div class="card-footer d-flex justify-content-end py-6 px-9">
            <a href="{{route('viewGettingStarted')}}" type="reset" class="btn btn-white btn-active-light-primary me-2">Batal</a>
            <button type="submit" class="btn btn-primary" >Submit</button>
          </div>
          <!--end::Actions-->
        </form>
      </div>
    </div>
  </div>
</div>
