<!--begin::Stats-->
<div class="d-flex flex-wrap flex-stack">
  @if (Session::get('user')->id_cms_privileges > 7)
    <!--begin::Wrapper-->
    <div class="d-flex flex-column flex-grow-1 pe-8">
      <!--begin::Stats-->
      <div class="d-flex flex-wrap">


        
      </div>
      <!--end::Stats-->
    </div>
    <!--end::Wrapper-->
  @endif



  <!--begin::Progress-->
  <div class="d-flex align-items-center w-200px w-sm-300px flex-column mt-3">
    <div class="d-flex justify-content-between w-100 mt-auto mb-2">
      <span class="fw-bold fs-6 text-gray-400">Profile Completion</span>
      <span class="fw-bolder fs-6">{{profileCompletion()}}%</span>
    </div>
    <div class="h-5px mx-3 w-100 bg-light mb-3">
      <div class="bg-{{profileCompletion() > 92 ? 'success' : 'warning'}} rounded h-5px" role="progressbar" style="width: {{profileCompletion()}}%;" aria-valuenow="{{profileCompletion()}}" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
  </div>
  <!--end::Progress-->
</div>
<!--end::Stats-->
