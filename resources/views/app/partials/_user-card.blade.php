<!--begin::Navbar-->
<div class="card mb-5 rounded mb-xl-10">
  <div class="card-body pt-9 pb-0">
    <!--begin::Details-->
    <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
      <!--begin: Pic-->
      <div class="d-inline-flex me-7 mb-4">
        <div class="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative">
          <img src="{{Session::get('user')->photo}}" alt="{{Session::get('user')->name}}" />
          <div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-20px w-20px"></div>
        </div>

        <!--begin::User-->
        <div class="d-flex ms-6 flex-column">
          <!--begin::Name-->
          <div class="d-flex align-items-center mb-2">
            <a href="#" class="text-gray-800 text-hover-primary fs-2 fw-bolder me-1">{{Session::get('user')->username ? Session::get('user')->username : Session::get('user')->name}}</a>
          @if (Session::get('user')->id_cms_privileges > 9)
            <a href="#">
              <!--begin::Svg Icon | path: icons/duotone/Design/Verified.svg-->
              <span class="svg-icon svg-icon-1 svg-icon-primary">
                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <path d="M10.0813 3.7242C10.8849 2.16438 13.1151 2.16438 13.9187 3.7242V3.7242C14.4016 4.66147 15.4909 5.1127 16.4951 4.79139V4.79139C18.1663 4.25668 19.7433 5.83365 19.2086 7.50485V7.50485C18.8873 8.50905 19.3385 9.59842 20.2758 10.0813V10.0813C21.8356 10.8849 21.8356 13.1151 20.2758 13.9187V13.9187C19.3385 14.4016 18.8873 15.491 19.2086 16.4951V16.4951C19.7433 18.1663 18.1663 19.7433 16.4951 19.2086V19.2086C15.491 18.8873 14.4016 19.3385 13.9187 20.2758V20.2758C13.1151 21.8356 10.8849 21.8356 10.0813 20.2758V20.2758C9.59842 19.3385 8.50905 18.8873 7.50485 19.2086V19.2086C5.83365 19.7433 4.25668 18.1663 4.79139 16.4951V16.4951C5.1127 15.491 4.66147 14.4016 3.7242 13.9187V13.9187C2.16438 13.1151 2.16438 10.8849 3.7242 10.0813V10.0813C4.66147 9.59842 5.1127 8.50905 4.79139 7.50485V7.50485C4.25668 5.83365 5.83365 4.25668 7.50485 4.79139V4.79139C8.50905 5.1127 9.59842 4.66147 10.0813 3.7242V3.7242Z" fill="#00A3FF" />
                  <path class="permanent" d="M14.8563 9.1903C15.0606 8.94984 15.3771 8.9385 15.6175 9.14289C15.858 9.34728 15.8229 9.66433 15.6185 9.9048L11.863 14.6558C11.6554 14.9001 11.2876 14.9258 11.048 14.7128L8.47656 12.4271C8.24068 12.2174 8.21944 11.8563 8.42911 11.6204C8.63877 11.3845 8.99996 11.3633 9.23583 11.5729L11.3706 13.4705L14.8563 9.1903Z" fill="white" />
                </svg>
              </span>
              <!--end::Svg Icon-->
            </a>
          @else
            <a href="{{route('viewProfile')}}" class="btn btn-sm btn-light-success fw-bolder ms-2 fs-8 py-1 px-3" >Upgrade to Pro</a>
          @endif
          </div>
          <!--end::Name-->
          <!--begin::Info-->
          <div class="d-flex flex-wrap fw-bold fs-6 mb-4 pe-2">
            <a href="#" class="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
              <!--begin::Svg Icon | path: icons/duotone/General/User.svg-->
              <span class="svg-icon svg-icon-4 me-1">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
                </svg>
              </span>
              <!--end::Svg Icon-->{{Session::get('priv')->name}}</a>
                </div>
                <!--end::Info-->
              </div>
              <!--end::User-->

      </div>
      <!--end::Pic-->
      <!--begin::Info-->
      <div class="flex-grow-1">
        <!--begin::Title-->
        <div class="d-flex justify-content-between align-items-start flex-wrap mb-2">

                <div class="d-inline-flex flex-column">
                  @include('app.partials._statistic')
                </div>
                <!--begin::Actions-->
                <div class="d-inline-flex my-4">


                  <!--begin::Menu-->
                  <div class="me-0">
                    <button class="btn btn-sm btn-icon btn-bg-light btn-active-color-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-flip="top-end">
                      <i class="bi bi-three-dots fs-3"></i>
                    </button>
                    <!--begin::Menu 3-->
                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3" data-kt-menu="true">
                      <!--begin::Heading-->
                      <div class="menu-item px-3">
                        <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Tasks</div>
                      </div>
                      <!--end::Heading-->

                      <!--begin::Menu item-->
                      <div class="menu-item px-3">
                        <a href="{{route('verifyEmail')}}" class="menu-link flex-stack px-3">Verifikasi Email
                          @if (profile()->email_verification == "new")
                            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Verifikasi email untuk pengiriman akun trading dan statemen transaksi"></i></a>
                          @else
                            <i class="fas fa-check-circle ms-2 fs-7 text-success" data-bs-toggle="tooltip" title="Verifikasi email untuk pengiriman akun trading dan statemen transaksi"></i></a>
                          @endif
                        </div>
                        <!--end::Menu item-->

                        <!--begin::Menu item-->
                        <div class="menu-item px-3">
                          <a href="{{route('verifyWhatsapp')}}" class="menu-link flex-stack px-3">Verifikasi Whatsapp
                            @if (profile()->whatsapp == "")
                              <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Verifikasi whatsapp untuk pengiriman sinyal trading"></i></a>
                            @else
                              <i class="fas fa-check-circle ms-2 fs-7 text-success" data-bs-toggle="tooltip" title="Verifikasi whatsapp untuk pengiriman sinyal trading"></i></a>
                            @endif

                          </div>
                          <!--end::Menu item-->

                          <!--begin::Menu item-->
                          <div class="menu-item px-3">
                            <a href="{{route('viewAppDoEditProfile')}}" class="menu-link flex-stack px-3">Lengkapi Profil
                              @if (profile()->id_cms_privileges == 7)
                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Lengkapi profil untuk unlock fitur-fitur trading tools"></i></a>
                              @else
                                <i class="fas fa-check-circle ms-2 fs-7 text-success" data-bs-toggle="tooltip" title="Lengkapi profil untuk unlock fitur-fitur trading tools"></i></a>
                              @endif
                            </div>
                            <!--end::Menu item-->

                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                              <a href="{{route('viewAcademyExplore')}}" class="menu-link flex-stack px-3">Trading Academy
                                @if (profile()->id_cms_privileges < 10)
                                  <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Belajar secara lengkap dengan metode terbaik dari Mentor berpengalaman"></i></a>
                                @else
                                  <i class="fas fa-check-circle ms-2 fs-7 text-success" data-bs-toggle="tooltip" title="Belajar secara lengkap dengan metode terbaik dari Mentor berpengalaman"></i></a>
                                @endif

                              </div>
                              <!--end::Menu item-->

                            </div>
                            <!--end::Menu 3-->
                          </div>
                          <!--end::Menu-->
                        </div>
                        <!--end::Actions-->

                      </div>
                      <!--end::Title-->

                    </div>
                    <!--end::Info-->
                  </div>
                  <!--end::Details-->
                  <!--begin::Navs-->
                  <div class="d-flex overflow-auto h-55px">
                    <ul class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap">
                      <!--begin::Nav item-->
                      <li class="nav-item">
                        <a class="nav-link text-active-primary me-6 {{isset($usermenu) && $usermenu == 'profile' ? 'active' : ''}}" href="{{route('viewProfile')}}">Profile</a>
                      </li>
                      <!--end::Nav item-->
                      <!--begin::Nav item-->
                      <li class="nav-item">
                        <a class="nav-link text-active-primary me-6 {{isset($usermenu) && $usermenu == 'account-mt4' ? 'active' : ''}}" href="{{route('viewAccountmt4')}}">MT4</a>
                      </li>
                      <!--end::Nav item-->
                      {{--
                      <!--begin::Nav item-->
                      <li class="nav-item">
                        <a class="nav-link text-active-primary me-6 {{isset($usermenu) && $usermenu == 'account-mt5' ? 'active' : ''}}" href="{{route('viewAccountmt5')}}">MT5</a>
                      </li>
                      <!--end::Nav item-->
                      --}}
                      <!--begin::Nav item-->
                      <li class="nav-item">
                        <a class="nav-link text-active-primary me-6 {{isset($usermenu) && $usermenu == 'deposit' ? 'active' : ''}}" href="{{route('viewDeposit')}}">Deposit</a>
                      </li>
                      <!--end::Nav item-->
                      <!--begin::Nav item-->
                      <li class="nav-item">
                        <a class="nav-link text-active-primary me-6 {{isset($usermenu) && $usermenu == 'withdrawal' ? 'active' : ''}}" href="{{route('viewWithdrawal')}}">Withdrawal</a>
                      </li>
                      <!--end::Nav item-->
                    </ul>
                  </div>
                  <!--begin::Navs-->
                </div>
              </div>
              <!--end::Navbar-->
