<div class="row">
  <div class="col-md-12">
    <!--begin::Basic info-->
    <div class="card rounded mb-5 mb-xl-10">
      <!--begin::Card header-->
      <div class="card-header border-0 cursor-pointer">
        <!--begin::Card title-->
        <div class="card-title m-0">
          <h3 class="fw-bolder m-0">Deposit Request </h3>
        </div>
        <!--end::Card title-->
      </div>
      <!--begin::Card header-->

      <div class="row">
        <div class="col-md-7">
          <!--begin::Content-->
          <div class="collapse show">
            <!--begin::Form-->
            <form  method="POST" action="{{route('process-Submit-Deposit')}}" class="form">
              <!--begin::Card body-->
              <div class="card-body border-top p-9">
                <h4 class="text-dark text-muted mb-6 ">Detail Pengirim</h4>
                <!--begin::Input group-->
                <div class="row mb-6">
                  <!--begin::Label-->
                  <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama Bank </label>
                  <!--end::Label-->
                  <!--begin::Col-->
                  <div class="col-lg-8 fv-row">
                    <input class="form-control form-control-lg form-control-solid input-valid" name="bank_name" value="{{bank()->bank_name}}" required/>
                    @if ($errors->has('bank_name'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('bank_name') }}</h4>
                    @endif
                  </div>
                  <!--end::Col-->
                </div>
                <!--end::Input group-->
                <!--begin::Input group-->
                <div class="row mb-6">
                  <!--begin::Label-->
                  <label class="col-lg-4 col-form-label required fw-bold fs-6">Nomor Rekening </label>
                  <!--end::Label-->
                  <!--begin::Col-->
                  <div class="col-lg-8 fv-row">
                    <input class="form-control form-control-lg form-control-solid input-valid" name="account_number" value="{{bank()->account_number}}" required/>
                    @if ($errors->has('account_number'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('account_number') }}</h4>
                    @endif
                  </div>
                  <!--end::Col-->
                </div>
                <!--end::Input group-->
                <!--begin::Input group-->
                <div class="row mb-6">
                  <!--begin::Label-->
                  <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama Rekening </label>
                  <!--end::Label-->
                  <!--begin::Col-->
                  <div class="col-lg-8 fv-row">
                    <input class="form-control form-control-lg form-control-solid input-valid" name="account_name" value="{{bank()->account_name}}" required/>
                    @if ($errors->has('account_name'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('account_name') }}</h4>
                    @endif
                  </div>
                  <!--end::Col-->
                </div>
                <!--end::Input group-->

                <hr>
                <h4 class="text-dark text-muted mb-6 ">Detail Deposit</h4>
                <!--begin::Input group-->
                <div class="row mb-6">
                  <!--begin::Label-->
                  <label class="col-lg-4 col-form-label required fw-bold fs-6">Untuk Account Trading</label>
                  <!--end::Label-->
                  <!--begin::Col-->
                  <div class="col-lg-8 fv-row">
                    <select class="form-select form-select-solid" name="accountmt5" id="akunchange" required>
                      <option value="">Pilih Account</option>

                      @foreach (mt4accounts() as $x => $acc)
                        <option value="{{$acc->mt4_id}}" {{$account == $acc->mt4_id ? 'selected' : ''}}>{{$acc->mt4_id}}</option>
                      @endforeach

                      @foreach (mt5accounts() as $x => $acc)
                        <option value="{{$acc->mt5_id}}" {{$account == $acc->mt5_id ? 'selected' : ''}}>{{$acc->mt5_id}}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('accountmt5'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('accountmt5') }}</h4>
                    @endif
                  </div>
                  <!--end::Col-->
                </div>
                <!--end::Input group-->
                <!--begin::Input group-->
                <div class="row mb-6" id="tipeDeposit">

                </div>
                <!--end::Input group-->
                <!--begin::Input group-->
                <div class="row mb-6">
                  <!--begin::Label-->
                  <label class="col-lg-4 col-form-label required fw-bold fs-6">Nilai Transfer </label>
                  <!--end::Label-->
                  <!--begin::Col-->
                  <div class="col-lg-8 fv-row">
                    <div class="input-group">
                      <span class="input-group-text">
                        IDR
                      </span>
                      <input class="form-control form-control-lg form-control-solid" name="amount" id="paternRupiah"  value="" min="10000" style="font-size:1.5em!important" required/>

                    </div>

                    @if ($errors->has('ammount'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('ammount') }}</h4>
                    @endif
                  </div>
                  <!--end::Col-->

                </div>
                <!--end::Input group-->

                <!--begin::Input group-->
                <div class="mb-10 fv-row">
                  <!--begin::Label-->
                  <label class="d-flex align-items-center required form-label mb-3">Pilihan Bank Tujuan Transfer</label>
                  <!--end::Label-->
                  <!--begin::Row-->
                  <div class="row mb-2" data-kt-buttons="true">
                    <!--begin::Col-->
                    <div class="col">
                      <!--begin::Option-->
                      <label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5 active">
                        <input type="radio" class="btn-check" name="transferTo" checked="checked" id="transferTo" value="BCA" />
                        <span class="fw-bolder fs-3">BCA</span>
                      </label>
                      <!--end::Option-->
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col">
                      <!--begin::Option-->
                      <label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5">
                        <input type="radio" class="btn-check" name="transferTo" id="transferTo" value="BRI" />
                        <span class="fw-bolder fs-3">BRI</span>
                      </label>
                      <!--end::Option-->
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col">
                      <!--begin::Option-->
                      <label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5">
                        <input type="radio" class="btn-check" name="transferTo" id="transferTo" value="BNI" />
                        <span class="fw-bolder fs-3">BNI</span>
                      </label>
                      <!--end::Option-->
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col">
                      <!--begin::Option-->
                      <label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5">
                        <input type="radio" class="btn-check" name="transferTo" id="transferTo" value="Mandiri" />
                        <span class="fw-bolder fs-3">Mandiri</span>
                      </label>
                      <!--end::Option-->
                    </div>
                    <!--end::Col-->
                  </div>
                  <!--end::Row-->
                  <!--begin::Hint-->
                  <div class="form-text">Pilih salah satu untuk menampilkan detil keterangan transfer</div>
                  <!--end::Hint-->
                </div>

              </div>
              <!--end::Card body-->
              <div class="card-footer border-0">
                @csrf
                <!--begin::Actions-->
                <input type="hidden" name="photo" id="buktiTransfer" value=""/>
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <!--end::Actions-->
              </form>
              <!--end::Form-->
              </div>
          </div>
          <!--end::Content-->

        </div>
        <div class="col-md-5 px-10 pb-20">
          <!--begin::Invoice 2 sidebar-->
          <div class="border border-dashed border-gray-300 card-rounded h-lg-100 min-w-md-350px p-9 bg-lighten">
            <!--begin::Labels-->
            <div class="mb-8">
              <span class="badge badge-light-warning">Menunggu Transferan</span>
            </div>
            <!--end::Labels-->
            <!--begin::Title-->
            <h6 class="mb-8 fw-boldest text-gray-600 text-hover-primary">DETIL KETERANGAN TRANSFER</h6>
            <!--end::Title-->
            <!--begin::Item-->
            <div class="mb-6">
              <div class="fw-bold text-gray-600 fs-7">Metode:</div>
              <div class="fw-bolder text-gray-800 fs-6">Transfer Antar Bank</div>
            </div>
            <!--end::Item-->
            <!--begin::Item-->
            <div class="mb-6">
              <div class="fw-bold text-gray-600 fs-7">Bank:</div>
              <div class="fw-bolder text-gray-800 fs-6 namaBank">
                <br />Cabang : <span class="namaCabang"></span></div>
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="mb-6">
                <div class="fw-bold text-gray-600 fs-7">Nomor Rekening:</div>
                <div class="fw-bolder text-gray-800 fs-6 nomorRekening"></div>
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="mb-6">
                <div class="fw-bold text-gray-600 fs-7">Nama Rekening:</div>
                <div class="fw-bolder text-gray-800 fs-6 namaRekening"></div>
              </div>
              <!--end::Item-->
              <!--begin::Item-->
              <div class="mb-15">
                <div class="fw-bold text-gray-600 fs-7">Batas Kadaluarsa :</div>
                <div class="fw-bolder fs-6 text-gray-800 d-flex align-items-center">
                  <span class="fs-7 text-danger d-flex align-items-center">
                    <span class="bullet bullet-dot bg-danger mx-2"></span>{{date('d-m-Y 12:00',strtotime('+1 day'))}} WIB</span></div>
                  </div>
                  <!--end::Item-->
                  <hr>
                  <form class="form" action="#" method="post">
                    <!--begin::Input group-->
                    <div class="fv-row">
                      <!--begin::Dropzone-->
                      <div class="dropzone" id="ot_bukti_transfer">
                        <!--begin::Message-->
                        <div class="dz-message needsclick">
                          <!--begin::Icon-->
                          <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                          <!--end::Icon-->

                          <!--begin::Info-->
                          <div class="ms-4">
                            <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Klik di sini untuk upload bukti transfer.</h3>
                            <span class="fs-7 fw-bold text-gray-400">Foto struk atm atau screenshot ibanking</span>
                          </div>
                          <!--end::Info-->
                        </div>
                      </div>
                      <!--end::Dropzone-->
                    </div>
                    <!--end::Input group-->
                    <div class="form-text">Format yang bisa diupload: png, jpg, jpeg.</div>
                    <div class="form-text">Maksimal 5Mb.</div>

                    </form>
                  <!--end::Form-->
                </div>
                <!--end::Invoice 2 sidebar-->



              </div>
            </div>



          </div>
        </div>
      </div>
