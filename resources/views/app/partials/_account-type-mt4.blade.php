<div class="row">
  <div class="col-md-12">
  <!--begin::Pricing-->
		<div class="text-center" id="kt_pricing">
			<!--begin::Nav group-->
			<div class="nav-group rounded landing-bg d-inline-flex mb-15 px-10" data-kt-buttons="true" style="border: 1px dashed #2B4666;background-color:#fff!important">
        @foreach ($listCategories as $x => $category)
          <a class="btn btn-color-gray-800 btn-active btn-active-danger px-3 py-3 me-1 {{$x == 0 ? 'active' : ''}}" id="{{$category->namaCategory}}" >{{$category->namaCategory}}</a>
        @endforeach

			</div>
			<!--end::Nav group-->
			<!--begin::Row-->
			<div class="row g-1" id="typeAccounts">

        @foreach ($listAccounts as $x => $account)
          <!--begin::Col-->
          <div class="col-xl-3 {{$account->namaCategory}}">
            <div class="d-flex h-100 align-items-center">
              <!--begin::Option-->
              <div class="w-100 d-flex flex-column flex-center rounded-3 {{$account->bestChoice == 'yes' ? 'bg-primary py-20' : 'bg-white py-15'}} px-10">
                <!--begin::Heading-->
                <div class="mb-7 text-center">
                  <!--begin::Title-->
                  <h1 class="{{$account->bestChoice == 'yes' ? 'text-white' : 'text-dark'}} mb-5 fw-boldest">{{$account->namaAccount}}</h1>
                  <!--end::Title-->
                  <!--begin::Description-->
                  <div class="{{$account->bestChoice == 'yes' ? 'text-white opacity-75' : 'text-gray-400'}} fw-bold mb-5">{{$account->deskripsiAccount}}</div>
                  <!--end::Description-->
                  <!--begin::Price-->
                  <div class="text-center">
                    <span class="mb-2 {{$account->bestChoice == 'yes' ? 'text-white' : 'text-primary'}}">$</span>
                    <span class="fs-3x fw-bolder {{$account->bestChoice == 'yes' ? 'text-white' : 'text-primary'}}">{{number_format($account->minimumDepo,0,',','.')}}</span>
                  </div>
                  <!--end::Price-->
                </div>
                <!--end::Heading-->
                <!--begin::Features-->
                <div class="w-100 mb-10">
                  @php
                    $descs = DB::table('typeMT4Description')->where('typeAccount',$account->id)->where('status','active')->get();
                  @endphp
                  @foreach ($descs as $y => $desc)
                    <!--begin::Item-->
                    <div class="d-flex flex-stack mb-5">
                      <span class="fw-bold fs-6 {{$account->bestChoice == 'yes' ? 'text-white' : 'text-gray-800'}} text-start pe-3">{{$desc->descOffer}}</span>
                      @if ($desc->descMark == 'benefit')
                        <!--begin::Svg Icon | path: icons/duotone/Code/Done-circle.svg-->
                        <span class="svg-icon svg-icon-1 {{$account->bestChoice == 'yes' ? 'svg-icon-white opacity-1' : 'svg-icon-success'}}">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                            <path d="M16.7689447,7.81768175 C17.1457787,7.41393107 17.7785676,7.39211077 18.1823183,7.76894473 C18.5860689,8.1457787 18.6078892,8.77856757 18.2310553,9.18231825 L11.2310553,16.6823183 C10.8654446,17.0740439 10.2560456,17.107974 9.84920863,16.7592566 L6.34920863,13.7592566 C5.92988278,13.3998345 5.88132125,12.7685345 6.2407434,12.3492086 C6.60016555,11.9298828 7.23146553,11.8813212 7.65079137,12.2407434 L10.4229928,14.616916 L16.7689447,7.81768175 Z" fill="#000000" fill-rule="nonzero" />
                          </svg>
                        </span>
                        <!--end::Svg Icon-->
                      @else
                        <!--begin::Svg Icon | path: icons/duotone/Code/Error-circle.svg-->
                        <span class="svg-icon svg-icon-1 {{$account->bestChoice == 'yes' ? 'svg-icon-white' : ''}}">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                            <path d="M12.0355339,10.6213203 L14.863961,7.79289322 C15.2544853,7.40236893 15.8876503,7.40236893 16.2781746,7.79289322 C16.6686989,8.18341751 16.6686989,8.81658249 16.2781746,9.20710678 L13.4497475,12.0355339 L16.2781746,14.863961 C16.6686989,15.2544853 16.6686989,15.8876503 16.2781746,16.2781746 C15.8876503,16.6686989 15.2544853,16.6686989 14.863961,16.2781746 L12.0355339,13.4497475 L9.20710678,16.2781746 C8.81658249,16.6686989 8.18341751,16.6686989 7.79289322,16.2781746 C7.40236893,15.8876503 7.40236893,15.2544853 7.79289322,14.863961 L10.6213203,12.0355339 L7.79289322,9.20710678 C7.40236893,8.81658249 7.40236893,8.18341751 7.79289322,7.79289322 C8.18341751,7.40236893 8.81658249,7.40236893 9.20710678,7.79289322 L12.0355339,10.6213203 Z" fill="#000000" />
                          </svg>
                        </span>
                        <!--end::Svg Icon-->
                      @endif

                    </div>
                    <!--end::Item-->
                  @endforeach

                </div>
                <!--end::Features-->
                <!--begin::Select-->
                <form action="{{route('process-create-mt4')}}" method="post">
                  @csrf

                  <input type="hidden" name="accountType" value="{{$account->namaAccount}}">
                  <input type="submit" class="btn btn-lg {{$account->bestChoice == 'yes' ? 'btn-color-primary btn-active-light-primary btn-white' : 'btn-primary'}}" name="create" value="Create Account MT4">
                </form>
                <!--end::Select-->
              </div>
              <!--end::Option-->
            </div>
          </div>
          <!--end::Col-->
        @endforeach

			</div>
			<!--end::Row-->
		</div>
	<!--end::Pricing-->
  </div>
</div>
