<div class="row">
  <div class="col-md-12 text-end mb-6">
    <a href="{{route('viewWithdrawal')}}" class="btn btn-primary"><i class="fas fa-plus"></i> Request Withdrawal</a>
  </div>
</div>
<div class="row">
  @foreach ($withdrawals as $wd)
    @php
      if ($wd->status == "pending") {
        $warna = 'info';
        $icon = 'fa-clock';
      }elseif($wd->status == "approved"){
        $warna = 'success';
        $icon = 'fa-check-circle';
      }elseif($wd->status == "rejected"){
        $warna = 'danger';
        $icon = 'fa-times-circle';
      }
    @endphp
    <div class="col-md-4 mb-6">
    	<!--begin::Statistics Widget 5-->
    	<a href="{{route('viewWithdrawalInvoice',[$wd->id,$wd->metatrader,uuid()])}}" class="card bg-{{$warna}} bgi-no-repeat rounded hoverable card-xl-stretch mb-xl-8" style="background-position: right top; background-position: 90% 20px; background-size: 30% auto; background-image: url({{env('IMG_LOGO')}});">
    		<!--begin::Body-->
    		<div class="card-body text-start">
          <i class="far {{$icon}} text-white ms-n1 fa-3x"></i> <span class="text-white">{{ \Carbon\Carbon::parse(strtotime($wd->created_at.'+7hours'))->diffForHumans()}}</span>
          <div class="text-inverse-warning fw-bolder fs-1 mb-2 mt-5">US$ {{number_format($wd->amount,2,'.',',')}}</div>
    			<div class="fw-bold text-inverse-warning fs-7 mb-5"><span class="text-white">Withdrawal</span></div>
          <span class="float-end text-inverse-warning fw-bolder fs-1">Account ID : {{$wd->metatrader}}</span>
    		</div>
    		<!--end::Body-->
    	</a>
    	<!--end::Statistics Widget 5-->
    </div>
  @endforeach
</div>
