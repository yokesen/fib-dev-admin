<div class="row">
  <div class="col-md-12">
    <!--begin::Basic info-->
    <div class="card rounded mb-5 mb-xl-10">
      <!--begin::Card header-->
      <div class="card-header border-0 cursor-pointer">
        <!--begin::Card title-->
        <div class="card-title m-0">
          <h3 class="fw-bolder m-0">Withdrawal Request </h3>
        </div>
        <!--end::Card title-->
      </div>
      <!--begin::Card header-->

      <div class="row">
        <div class="col-md-5 px-10 pb-20">
          <!--begin::Invoice 2 sidebar-->
          <div class="border border-dashed border-gray-300 card-rounded h-lg-100 min-w-md-350px p-9 bg-lighten">

            <!--begin::Title-->
            <h6 class="mb-8 fw-boldest text-gray-600 text-hover-primary">DETIL KETERANGAN PENERIMA</h6>
            <!--end::Title-->
            <!--begin::Item-->
            <div class="mb-6">
              <div class="fw-bold text-gray-600 fs-7">Metode:</div>
              <div class="fw-bolder text-gray-800 fs-6">Transfer Bank</div>
            </div>
            <!--end::Item-->
            <!--begin::Item-->
            <div class="mb-6">
              <div class="fw-bold text-gray-600 fs-7">Bank:</div>
              <div class="fw-bolder text-gray-800 fs-6 namaBank">{{bank()->bank_name}}</div>
            </div>
            <!--end::Item-->
            <!--begin::Item-->
            <div class="mb-6">
              <div class="fw-bold text-gray-600 fs-7">Nomor Rekening:</div>
              <div class="fw-bolder text-gray-800 fs-6 nomorRekening">{{bank()->account_number}}</div>
            </div>
            <!--end::Item-->
            <!--begin::Item-->
            <div class="mb-6">
              <div class="fw-bold text-gray-600 fs-7">Nama Rekening:</div>
              <div class="fw-bolder text-gray-800 fs-6 namaRekening">{{bank()->account_name}}</div>
            </div>
            <!--end::Item-->
          </div>
          <!--end::Invoice 2 sidebar-->
        </div>
        <div class="col-md-7">
          <!--begin::Content-->
          <div class="collapse show">
            <!--begin::Form-->
            <form  method="POST" action="{{route('process-Submit-Withdrawal')}}" class="form">
              <!--begin::Card body-->
              <div class="card-body border-top p-9">
                <h4 class="text-dark text-muted mb-6 ">Detail Withdrawal</h4>
                <!--begin::Input group-->
                <div class="row mb-6">
                  <!--begin::Label-->
                  <label class="col-lg-4 col-form-label required fw-bold fs-6">Nilai Withdrawal </label>
                  <!--end::Label-->
                  <!--begin::Col-->
                  <div class="col-lg-8 fv-row">
                    <div class="input-group">
                      <span class="input-group-text" style="font-size:1.5em!important">
                        US$
                      </span>
                      <input class="form-control form-control-lg form-control-solid" type="number" name="amount" min="1" step="0.01" onkeyup="fixDecimal(this)" style="font-size:1.5em!important" required/>

                    </div>

                    @if ($errors->has('ammount'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('ammount') }}</h4>
                    @endif
                  </div>

                  <!--end::Col-->

                </div>
                <!--end::Input group-->
                <!--begin::Input group-->
                <div class="row mb-6">
                  <!--begin::Label-->
                  <label class="col-lg-4 col-form-label required fw-bold fs-6">Dari Account Trading</label>
                  <!--end::Label-->
                  <!--begin::Col-->
                  <div class="col-lg-8 fv-row">
                    <select class="form-select form-select-solid" name="accountmt5" required>
                      <option value="">Pilih Account</option>

                      @foreach (mt4accounts() as $x => $acc)
                        <option value="{{$acc->mt4_id}}" {{$account == $acc->mt4_id ? 'selected' : ''}}>{{$acc->mt4_id}}</option>
                      @endforeach

                      @foreach (mt5accounts() as $x => $acc)
                        <option value="{{$acc->mt5_id}}" {{$account == $acc->mt5_id ? 'selected' : ''}}>{{$acc->mt5_id}}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('accountmt5'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('accountmt5') }}</h4>
                    @endif
                  </div>
                  <!--end::Col-->
                </div>
                <!--end::Input group-->


              </div>
              <!--end::Card body-->
              <div class="card-footer border-0">
                @csrf
                <!--begin::Actions-->
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <!--end::Actions-->
              </form>
              <!--end::Form-->
            </div>
          </div>
          <!--end::Content-->

        </div>

      </div>



    </div>
  </div>
</div>
