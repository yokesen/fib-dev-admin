<!--begin::Label-->
<label class="col-lg-4 col-form-label required fw-bold fs-6">Tipe Deposit </label>
<!--end::Label-->
<!--begin::Col-->
<div class="col-lg-8 fv-row">
  <select class="form-select form-select-solid" name="tipeDeposit">
    @foreach (currency() as $cur)
      <option value="{{$cur->id}}">{{$cur->cur_one}}</option>
    @endforeach
  </select>
</div>
<!--end::Col-->
