<h4 class="text-danger mt-6"><i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> Username sudah dipakai</h4>
<p class="text-danger mt-6">Saran Username :</p>
@foreach ($suggests as $saran)
  <span class="text-danger mt-6 mb-6 me-6" onclick="document.getElementById('username').value='{{$saran}}'">{{$saran}}</span>
@endforeach
