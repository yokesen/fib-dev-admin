<div class="row">
  <div class="col-md-6 offset-md-3">
    <!--begin::Engage Widget 1-->
    <div class="card rounded mb-5 mb-xl-10">
      <!--begin::Body-->
      <div class="card-body pb-0">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column justify-content-between h-100">
          <!--begin::Section-->
          <div class="pt-15 mb-10">
            <!--begin::Title-->
            <h3 class="text-dark text-center fs-1 fw-bolder lh-lg">Verifikasi Komunikasi Whatsapp,
              </h3>
              <!--end::Title-->
              <!--begin::Text-->
              <div class="text-center text-gray-600 fs-6 fw-bold pt-4 pb-1">Sebelum mulai, kami membutuhkan verifikasi untuk komunikasi dengan Kamu
                <br />Silahkan klik tombol di bawah ini.</div>
                <!--end::Text-->
                <!--begin::Action-->
                <div class="text-center py-7">
                  <a href="https://wa.me/6281287608190/?text=verify {{$user->uuid}}" class="btn btn-success btn-lg" target="_blank" id="submitWhatsapp">
                    <span class="ot-label"><i class="bi bi-whatsapp fs-2"></i> Verifikasi Whatsapp di sini</span>
                    <span class="ot-progress">Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                  </a>
                </div>
                <!--end::Action-->
              </div>
              <!--end::Section-->
              <!--begin::Image-->
              <div class="flex-grow-1 bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-bottom card-rounded-bottom h-200px" style="background-image:url('assets/media/illustrations/work.png')"></div>
              <!--end::Image-->
            </div>
            <!--end::Wrapper-->
          </div>
          <!--end::Body-->
        </div>
        <!--end::Engage Widget 1-->
  </div>
</div>
