<div class="row">

  <div class="col-md-6">
    <!--begin::Basic info-->
    <div class="card rounded mb-5 mb-xl-10">
      <!--begin::Card header-->
      <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
        <!--begin::Card title-->
        <div class="card-title m-0">
          <h3 class="fw-bolder m-0">Bank </h3>
        </div>
        <!--end::Card title-->
      </div>
      <!--begin::Card header-->
      <!--begin::Content-->
      <div class="collapse show">
        <!--begin::Form-->
        <form method="POST" action="{{route('process-Edit-Bank')}}" class="form">
          <!--begin::Card body-->
          <div class="card-body border-top p-9">

            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama Bank</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <select class="form-select form-select-solid" name="bank_name" required>
                  <option value="">Pilih Bank</option>

                  @foreach ($listAllow as $x => $allow)
                    <option value="{{$allow->bank_name}}" {{$bank->bank_name == $allow->bank_name ? "selected" : ""}} >{{$allow->bank_name}}</option>
                  @endforeach

                </select>
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nomor Rekening</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid {{ old('account_number') && !$errors->has('account_number') ? 'input-valid' : '' }} {{$errors->has('account_number') ? 'input-error' : ''}}" type="text" placeholder="" name="account_number" autocomplete="off" value="{{ old('account_number') ? old('account_number') : $bank->account_number }}" required {{$errors->has('account_number') ? 'autofocus' : ''}}/>
                @if ($errors->has('account_number'))
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('account_number') }}</h4>
                @endif
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama di Rekening</label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid {{ old('account_name') && !$errors->has('account_name') ? 'input-valid' : '' }} {{$errors->has('account_name') ? 'input-error' : ''}}" type="text" placeholder="" name="account_name" autocomplete="off" value="{{ old('account_name') ? old('account_name') : $bank->account_name }}" required {{$errors->has('account_name') ? 'autofocus' : ''}}/>
                @if ($errors->has('account_name'))
                    <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('account_name') }}</h4>
                @endif
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
          </div>
          <!--end::Card body-->
          @csrf
          <!--begin::Actions-->
          <div class="card-footer d-flex justify-content-end py-6 px-9">
            <a href="{{route('viewGettingStarted')}}" type="reset" class="btn btn-white btn-active-light-primary me-2">Batal</a>
            <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Submit</button>
          </div>
          <!--end::Actions-->
        </form>
        <!--end::Form-->
      </div>
      <!--end::Content-->
    </div>
  </div>
  <div class="col-md-6">
    @if($user->photoTabungan == '/images/upload-default.jpg')
      <!--begin::Basic info-->
      <div class="card rounded mb-5 mb-xl-10">
        <!--begin::Card header-->
        <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
          <!--begin::Card title-->
          <div class="card-title m-0">
            <h3 class="fw-bolder m-0">Dokumen Buku Tabungan </h3>
          </div>
          <!--end::Card title-->
        </div>
        <!--begin::Card header-->
        <!--begin::Content-->
        <div class="collapse show">
          <!--begin::Form-->
          <form class="form" action="#" method="post">
            <!--begin::Card body-->
            <div class="card-body border-top p-9">

              <!--begin::Input group-->
              <div class="fv-row">
                <!--begin::Dropzone-->
                <div class="dropzone" id="ot_buku_rekening">
                  <!--begin::Message-->
                  <div class="dz-message needsclick">
                    <!--begin::Icon-->
                    <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                    <!--end::Icon-->

                    <!--begin::Info-->
                    <div class="ms-4">
                      <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Klik di sini untuk upload.</h3>
                      <span class="fs-7 fw-bold text-gray-400">Upload foto cover buku tabungan</span>
                    </div>
                    <!--end::Info-->
                  </div>
                </div>
                <!--end::Dropzone-->
              </div>
              <!--end::Input group-->

              <!--begin::Info-->
              <div class="form-text">Format yang bisa diupload: png, jpg, jpeg.</div>
              <div class="form-text">Maksimal 5Mb.</div>
              <!--end::Info-->
            </div>
            <!--end::Card body-->
          </form>
          <!--end::Form-->
        </div>
      </div>
    @endif
  </div>
</div>
