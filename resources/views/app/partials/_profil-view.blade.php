<div class="row">
  <div class="col-md-6">
    @if (profile()->id_cms_privileges == 7)
    <div class="card mb rounded mb-xl-10 mb-10">
      <div class="card-body p-9">
        <!--begin::Notice-->
        <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed p-6">
          <!--begin::Icon-->
          <!--begin::Svg Icon | path: icons/duotone/Code/Warning-1-circle.svg-->
          <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
              <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
              <rect fill="#000000" x="11" y="7" width="2" height="8" rx="1" />
              <rect fill="#000000" x="11" y="16" width="2" height="2" rx="1" />
            </svg>
          </span>
          <!--end::Svg Icon-->
          <!--end::Icon-->
          <!--begin::Wrapper-->
          <div class="d-flex flex-stack flex-grow-1">
            <!--begin::Content-->
            <div class="fw-bold">
              <h4 class="text-gray-800 fw-bolder">Hi {{profile()->username ? profile()->username : profile()->name}}!</h4>
              <div class="fs-6 text-gray-600">Tolong bantu lengkapi profil-mu ya, karena dibutuhkan beberapa verifikasi untuk Kamu bisa mulai trading.
              </div>
            </div>
            <!--end::Content-->
          </div>
          <!--end::Wrapper-->
          </div>
          <!--end::Notice-->
      </div>
    </div>
  @endif
    <!--begin::details View-->
    <div class="card mb-5 rounded mb-xl-10" id="kt_profile_details_view">
      <!--begin::Card header-->
      <div class="card-header cursor-pointer">
        <!--begin::Card title-->
        <div class="card-title m-0">
          <h3 class="fw-bolder m-0">Profile Details</h3>
        </div>
        <!--end::Card title-->
        <!--begin::Action-->
        <a href="{{route('viewAppDoEditProfile')}}" class="btn btn-primary align-self-center">Edit Profile</a>
        <!--end::Action-->
      </div>
      <!--begin::Card header-->
      <!--begin::Card body-->
      <div class="card-body p-9">
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Username</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            @if (profile()->username)
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> {{profile()->username}}</span>
            @else
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Pilih username-mu."></i></span>
            @endif
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Full Name</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            @if (profile()->name)
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> {{profile()->name}}</span>
            @else
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Nama lengkap sesuai KTP."></i></span>
            @endif
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Email</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            @if (profile()->email && profile()->email_verification == 'verified')
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> {{profile()->email}}</span>
            @elseif (profile()->email && profile()->email_verification != 'verified')
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Verifikasikan Email-mu, karena semua akun trading dan statementnya akan dikirimkan ke sini."></i> {{profile()->email}}</span>
            @else
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Email tidak boleh kosong."></i></span>
            @endif
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Whatsapp</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            @if (!empty(profile()->whatsapp))
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> {{profile()->whatsapp}}</span>
            @elseif (profile()->phone && empty(profile()->whatsapp))
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Verifikasi whatsapp-mu untuk mendapatkan sinyal trading dan berbagai macam manfaat lainnya."></i> {{profile()->phone}}</span>
            @else
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Verifikasi whatsapp-mu untuk mendapatkan sinyal trading dan berbagai macam manfaat lainnya."></i></span>
            @endif
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Tanggal Lahir</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            @if (profile()->dob)
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> {{date('d-F-Y',strtotime(profile()->dob))}}</span>
            @else
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Minimal 18 tahun ke atas."></i></span>
            @endif
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
        </div>
        <!--end::Card body-->
      </div>
      <!--end::details View-->
  </div>
  <div class="col-md-6">
    <!--begin::details View-->
    <div class="card mb-5 rounded mb-xl-10">
      <!--begin::Card header-->
      <div class="card-header cursor-pointer">
        <!--begin::Card title-->
        <div class="card-title m-0">
          <h3 class="fw-bolder m-0">Bank</h3>
        </div>
        <!--end::Card title-->
        <!--begin::Action-->
        @if (empty(bank()->bank_name))
          <a href="{{route('viewAppDoEditBank')}}" class="btn btn-primary align-self-center">Edit Bank</a>
        @endif
        <!--end::Action-->
      </div>
      <!--begin::Card header-->
      <!--begin::Card body-->
      <div class="card-body p-9">
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Nama Bank</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            @if (bank()->bank_name)
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> {{bank()->bank_name}}</span>
            @else
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Isikan nama bank."></i></span>
            @endif
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Nomor Rekening</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            @if (bank()->account_number)
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> {{bank()->account_number}}</span>
            @else
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Isikan nomor rekening bank."></i></span>
            @endif
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row mb-7">
          <!--begin::Label-->
          <label class="col-lg-4 fw-bold text-muted">Nama di Rekening</label>
          <!--end::Label-->
          <!--begin::Col-->
          <div class="col-lg-8">
            @if (bank()->account_name)
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> {{bank()->account_name}}</span>
            @else
              <span class="fw-bolder fs-6 text-dark"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="Isikan nama sesuai di buku tabungan."></i></span>
            @endif
          </div>
          <!--end::Col-->
        </div>
        <!--end::Row-->
      </div>
      <!--end::Card body-->
    </div>
    <!--end::details View-->

    <!--begin::List Widget 7-->
		<div class="card rounded card-xl-stretch mb-xl-8">
			<!--begin::Header-->
			<div class="card-header align-items-center border-0 mt-4">
				<h3 class="card-title align-items-start flex-column">
					<span class="fw-bolder text-dark">Dokumen Syarat</span>
					<span class="text-muted mt-1 fw-bold fs-7">verifikasi identitas dan bank</span>
				</h3>
			</div>
			<!--end::Header-->
			<!--begin::Body-->
			<div class="card-body pt-3">
				<!--begin::Item-->
				<div class="d-flex align-items-sm-center mb-7">
					<!--begin::Symbol-->
					<div class="symbol symbol-60px symbol-2by3 me-4">
            @if ($user->photoKTP != '/images/upload-default.jpg')
              <div class="symbol-label" style="background-image: url('{{env('IMG_USER').$user->photoKTP}}')"></div>
            @else
              <div class="symbol-label" style="background-image: url('/images/upload-default.jpg')"></div>
            @endif
					</div>
					<!--end::Symbol-->
					<!--begin::Title-->
					<div class="d-flex flex-row-fluid flex-wrap align-items-center">
						<div class="flex-grow-1 me-2">
							<span class="text-gray-800 fw-bolder fs-6">KTP/SIM/Paspor</span>
              @if ($user->photoKTP != '/images/upload-default.jpg')
                <span class="text-muted fw-bold d-block pt-1"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> Valid</span>
              @else
                <a href="{{route('viewUploadDocument')}}" class="text-muted fw-bold text-hover-primary d-block pt-1"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="upload foto KTP."></i> Klik di sini untuk upload</a>
              @endif
						</div>
            @if ($user->photoKTP == '/images/upload-default.jpg')
              <span class="badge badge-light-warning fs-8 fw-bolder my-2">Waiting</span>

            @else
              <span class="badge badge-light-success fs-8 fw-bolder my-2">Approved</span>

            @endif
					</div>
					<!--end::Title-->
				</div>
				<!--end::Item-->
				<!--begin::Item-->
				<div class="d-flex align-items-sm-center mb-7">
					<!--begin::Symbol-->
					<div class="symbol symbol-60px symbol-2by3 me-4">
            @if ($user->photoTabungan != '/images/upload-default.jpg')
              <div class="symbol-label" style="background-image: url('{{env('IMG_USER').$user->photoTabungan}}')"></div>
            @else
              <div class="symbol-label" style="background-image: url('/images/upload-default.jpg')"></div>
            @endif
					</div>
					<!--end::Symbol-->
					<!--begin::Title-->
					<div class="d-flex flex-row-fluid flex-wrap align-items-center">
						<div class="flex-grow-1 me-2">
							<span class="text-gray-800 fw-bolder fs-6">Cover Buku Tabungan</span>

                @if ($user->photoTabungan != '/images/upload-default.jpg')
                  <span class="text-muted fw-bold d-block pt-1"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> Valid</span>
                @else
                  <a href="{{route('viewUploadDocument')}}" class="text-muted fw-bold text-hover-primary d-block pt-1"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5" data-bs-toggle="tooltip" title="upload foto cover buku tabungan."></i> Klik di sini untuk upload</a>
                @endif
						</div>
            @if ($user->photoTabungan == '/images/upload-default.jpg')
              <span class="badge badge-light-warning fs-8 fw-bolder my-2">Waiting</span>

            @else
              <span class="badge badge-light-success fs-8 fw-bolder my-2">Approved</span>

            @endif
					</div>
					<!--end::Title-->
				</div>
				<!--end::Item-->
			</div>
			<!--end::Body-->
		</div>
		<!--end::List Widget 7-->
  </div>
</div>
