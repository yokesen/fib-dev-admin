@extends('template.master')

@section('title','Getting Started')
@section('metadescription','Metatrader')
@section('metakeyword','metatrader')
@section('bc-1','Account')
@section('bc-2','MT4')

@section('container')
  <div id="kt_content_container" class="container">
    @include('app.partials._user-card')
    @include('app.partials._account-list-mt4')
  </div>
@endsection
