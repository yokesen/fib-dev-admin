@extends('template.master')

@section('title','Getting Started')
@section('bc-1','Deposit')
@section('bc-2','List')

@section('container')

  <div id="kt_content_container" class="container">
    @include('app.partials._user-card')
    @include('app.partials._deposit-list')
  </div>
@endsection
