@extends('template.master')

@section('title','Getting Started')
@section('bc-1','Dashboard')
@section('bc-2','Onboarding')

@section('container')
  <div id="kt_content_container" class="container">
      <div id="verifikasiWhatsapp">
        @include('app.partials._verifikasi-whatsapp')
      </div>
      <div id="verifikasiEmail">
        @include('app.partials._verifikasi-email')
      </div>
  </div>
@endsection

@section('jsinline')
  <script type="text/javascript">
  $( document ).ready(function() {
    $("#verifikasiEmail").hide();
    $(".ot-progress").hide();
    setInterval(function() {
      $.ajax({
        type: "POST",
        url: "{{route('ajaxSearchWhatsapp')}}",
        data: {
          uuid: '{{profile()->uuid}}'
        },
        headers: {
          "X-CSRF-TOKEN": "{{ csrf_token() }}"
        },
        success: function(e) {
          if(e == ''){
          }else{
            $("#verifikasiWhatsapp").hide();
            $("#verifikasiEmail").show();
          }
        }
      })
    }, 5000);

    $("#submitWhatsapp").click(function(){
      $(".ot-progress").show();
      $(".ot-label").hide();
    });

  });
  </script>
@endsection
