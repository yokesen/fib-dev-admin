@extends('template.master')

@section('title','Cashier | Deposit')
@section('bc-1','Deposit')
@section('bc-2','Create')

@section('container')

  <div id="kt_content_container" class="container">
    @include('app.partials._deposit')
  </div>
@endsection


@section('jsinline')

  <script type="text/javascript">

  var myDropzone = new Dropzone("#ot_bukti_transfer", {
      url: "{{route('process-Upload-Deposit')}}",
      paramName: "gambar",
      method: "post",
      headers: {
          "X-CSRF-TOKEN": "{{ csrf_token() }}"
      },
      maxFiles: 1,
      maxFilesize: 10,
      addRemoveLinks: !0,
      accept: function(e,a) {
        "wow.jpg" == e.name ? a("Naha, you don't.") : a();
      },
      success: function(file, response){

        $("#buktiTransfer").val(response);
      }
  });

    $(document).ready(function(){
      var pilihan = $("input[type=radio][name=transferTo]").val();
      $.ajax({
        type: "POST",
        url: "{{route('ajaxSearchBankCustodian')}}",
        data: {
          namaBank: pilihan
        },
        headers: {
          "X-CSRF-TOKEN": "{{ csrf_token() }}"
        },
        success: function(e) {
          $(".namaBank").html(e[0].namaBank);
          $(".namaCabang").html(e[0].cabangBank);
          $(".nomorRekening").html(e[0].nomorRekening);
          $(".namaRekening").html(e[0].namaRekening);
        }
      })
    });

    $('input[type=radio][name=transferTo]').change(function() {
      $.ajax({
        type: "POST",
        url: "{{route('ajaxSearchBankCustodian')}}",
        data: {
          namaBank: this.value
        },
        headers: {
          "X-CSRF-TOKEN": "{{ csrf_token() }}"
        },
        success: function(e) {
          $(".namaBank").html(e[0].namaBank);
          $(".namaCabang").html(e[0].cabangBank);
          $(".nomorRekening").html(e[0].nomorRekening);
          $(".namaRekening").html(e[0].namaRekening);
        }
      })
    });

    Inputmask("9.999.999.999", {
        "numericInput": true,
        greedy: false
    }).mask("#ot_amount");

    $(document).ready(function() {

    });

    $('#paternRupiah').change(function(){
      var num = $('#paternRupiah').val()
      num = addPeriod(num);
      $('#paternRupiah').val(num)
    });

    $('#paternRupiah').click(function(){

      $('#paternRupiah').val("")
    });

    function addPeriod(nStr)
    {
      nStr += '';
      x = nStr.split('.');
      x1 = x[0];
      x2 = x.length > 1 ? '.' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      return x1 + x2;
    }

    $('#akunchange').change(function(){
      $.ajax({
        type: "POST",
        url: "{{route('ajaxSearchAccountMt4')}}",
        data: {
          mt4: this.value
        },
        headers: {
          "X-CSRF-TOKEN": "{{ csrf_token() }}"
        },
        success: function(e) {
          
          $("#tipeDeposit").html(e.html);
        }
      })
    });

  </script>
@endsection
