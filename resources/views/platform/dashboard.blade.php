@extends('template.master')

@section('title','Dashboard')
@section('metadescription','FIB Partner the best business partner for IB')
@section('metakeyword','FIB Partner')
@section('bc-1','My Business')
@section('bc-2','Dashboard')

@section('container')
  <div class="post d-flex flex-column-fluid" id="kt_post">
    <!--begin::Container-->
    <div id="kt_content_container" class="container">
      <!--begin::Row-->
			{{-- <div class="row g-5 g-xl-8">
        <div class="col-xl-4">
					<!--begin::List Widget 1-->
					<div class="card card-xl-stretch mb-xl-8">
						<!--begin::Body-->
						<div class="card-body p-0">
							<!--begin::Header-->
							<div class="px-9 pt-7 card-rounded h-275px w-100 bg-primary">
								<!--begin::Heading-->
								<div class="d-flex flex-stack">
									<h3 class="m-0 text-white fw-bolder fs-3">{{date('F Y')}} Summary</h3>

								</div>
								<!--end::Heading-->
								<!--begin::Balance-->
								<div class="d-flex text-center flex-column text-white pt-8">
									<span class="fw-bold fs-7">Net Margin In</span>
									<span class="fw-bolder fs-2x pt-1">IDR {{number_format($netmarginThismonth,0,',','.')}}</span>
								</div>
								<!--end::Balance-->
							</div>
							<!--end::Header-->
							<!--begin::Items-->
							<div class="shadow-xs card-rounded mx-9 mb-9 px-6 py-9 position-relative z-index-1 bg-white" style="margin-top: -100px">
								<!--begin::Item-->
								<div class="d-flex align-items-center mb-6">
									<!--begin::Symbol-->
									<div class="symbol symbol-45px w-40px me-5">
										<span class="symbol-label bg-lighten">
											<i class="bi bi-journal-plus fs-3"></i>
										</span>
									</div>
									<!--end::Symbol-->
									<!--begin::Description-->
									<div class="d-flex align-items-center flex-wrap w-100">
										<!--begin::Title-->
										<div class="mb-1 pe-3 flex-grow-1">
											<a href="#" class="fs-5 text-gray-800 text-hover-primary fw-bolder">Margin In</a>
											<div class="text-gray-400 fw-bold fs-7">Target > IDR {{number_format($depositLastmonth/1000000000,1,',','.')}}B</div>
										</div>
										<!--end::Title-->
										<!--begin::Label-->
										<div class="d-flex align-items-center">
											<div class="fw-bolder fs-5 text-gray-800 pe-1">IDR {{number_format($depositThismonth/1000000000,1,',','.')}}B</div>
                      @if ($depositThismonth > $depositLastmonth)
                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Arrow-up.svg-->
  											<span class="svg-icon svg-icon-5 svg-icon-success ms-1">
  												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
  													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
  														<polygon points="0 0 24 0 24 24 0 24" />
  														<rect fill="#000000" opacity="0.5" x="11" y="5" width="2" height="14" rx="1" />
  														<path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero" />
  													</g>
  												</svg>
  											</span>
  											<!--end::Svg Icon-->
                      @else
                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Arrow-down.svg-->
  											<span class="svg-icon svg-icon-5 svg-icon-danger ms-1">
  												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
  													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
  														<polygon points="0 0 24 0 24 24 0 24" />
  														<rect fill="#000000" opacity="0.5" x="11" y="5" width="2" height="14" rx="1" />
  														<path d="M6.70710678,18.7071068 C6.31658249,19.0976311 5.68341751,19.0976311 5.29289322,18.7071068 C4.90236893,18.3165825 4.90236893,17.6834175 5.29289322,17.2928932 L11.2928932,11.2928932 C11.6714722,10.9143143 12.2810586,10.9010687 12.6757246,11.2628459 L18.6757246,16.7628459 C19.0828436,17.1360383 19.1103465,17.7686056 18.7371541,18.1757246 C18.3639617,18.5828436 17.7313944,18.6103465 17.3242754,18.2371541 L12.0300757,13.3841378 L6.70710678,18.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 14.999999) scale(1, -1) translate(-12.000003, -14.999999)" />
  													</g>
  												</svg>
  											</span>
  											<!--end::Svg Icon-->
                      @endif

										</div>
										<!--end::Label-->
									</div>
									<!--end::Description-->
								</div>
								<!--end::Item-->

                <!--begin::Item-->
								<div class="d-flex align-items-center mb-6">
									<!--begin::Symbol-->
									<div class="symbol symbol-45px w-40px me-5">
										<span class="symbol-label bg-lighten">
											<i class="bi bi-journal-minus fs-3"></i>
										</span>
									</div>
									<!--end::Symbol-->
									<!--begin::Description-->
									<div class="d-flex align-items-center flex-wrap w-100">
										<!--begin::Title-->
										<div class="mb-1 pe-3 flex-grow-1">
											<a href="#" class="fs-5 text-gray-800 text-hover-primary fw-bolder">Margin Out</a>
											<div class="text-gray-400 fw-bold fs-7">Target < IDR {{number_format($withdrawalLastmonth/1000000000,1,',','.')}}B</div>
										</div>
										<!--end::Title-->
										<!--begin::Label-->
										<div class="d-flex align-items-center">
											<div class="fw-bolder fs-5 text-gray-800 pe-1">IDR {{number_format($withdrawalThismonth/1000000000,1,',','.')}}B</div>
                      @if ($withdrawalLastmonth > $withdrawalThismonth)
                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Arrow-up.svg-->
  											<span class="svg-icon svg-icon-5 svg-icon-success ms-1">
  												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
  													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
  														<polygon points="0 0 24 0 24 24 0 24" />
  														<rect fill="#000000" opacity="0.5" x="11" y="5" width="2" height="14" rx="1" />
  														<path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero" />
  													</g>
  												</svg>
  											</span>
  											<!--end::Svg Icon-->
                      @else
                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Arrow-down.svg-->
  											<span class="svg-icon svg-icon-5 svg-icon-danger ms-1">
  												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
  													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
  														<polygon points="0 0 24 0 24 24 0 24" />
  														<rect fill="#000000" opacity="0.5" x="11" y="5" width="2" height="14" rx="1" />
  														<path d="M6.70710678,18.7071068 C6.31658249,19.0976311 5.68341751,19.0976311 5.29289322,18.7071068 C4.90236893,18.3165825 4.90236893,17.6834175 5.29289322,17.2928932 L11.2928932,11.2928932 C11.6714722,10.9143143 12.2810586,10.9010687 12.6757246,11.2628459 L18.6757246,16.7628459 C19.0828436,17.1360383 19.1103465,17.7686056 18.7371541,18.1757246 C18.3639617,18.5828436 17.7313944,18.6103465 17.3242754,18.2371541 L12.0300757,13.3841378 L6.70710678,18.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 14.999999) scale(1, -1) translate(-12.000003, -14.999999)" />
  													</g>
  												</svg>
  											</span>
  											<!--end::Svg Icon-->
                      @endif

										</div>
										<!--end::Label-->
									</div>
									<!--end::Description-->
								</div>
								<!--end::Item-->

                <!--begin::Item-->
								<div class="d-flex align-items-center mb-6">
									<!--begin::Symbol-->
									<div class="symbol symbol-45px w-40px me-5">
										<span class="symbol-label bg-lighten">
											<i class="bi bi-journal-minus fs-3"></i>
										</span>
									</div>
									<!--end::Symbol-->
									<!--begin::Description-->
									<div class="d-flex align-items-center flex-wrap w-100">
										<!--begin::Title-->
										<div class="mb-1 pe-3 flex-grow-1">
											<a href="#" class="fs-5 text-gray-800 text-hover-primary fw-bolder">P/L</a>
											<div class="text-gray-400 fw-bold fs-7">Last Month IDR {{number_format($profitLastmonth/1000000000,1,',','.')}}B</div>
										</div>
										<!--end::Title-->
										<!--begin::Label-->
										<div class="d-flex align-items-center">
											<div class="fw-bolder fs-5 text-gray-800 pe-1">IDR {{number_format($profitThismonth/1000000000,1,',','.')}}B</div>
                      @if ($profitThismonth > $profitLastmonth)
                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Arrow-up.svg-->
  											<span class="svg-icon svg-icon-5 svg-icon-success ms-1">
  												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
  													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
  														<polygon points="0 0 24 0 24 24 0 24" />
  														<rect fill="#000000" opacity="0.5" x="11" y="5" width="2" height="14" rx="1" />
  														<path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero" />
  													</g>
  												</svg>
  											</span>
  											<!--end::Svg Icon-->
                      @else
                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Arrow-down.svg-->
  											<span class="svg-icon svg-icon-5 svg-icon-danger ms-1">
  												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
  													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
  														<polygon points="0 0 24 0 24 24 0 24" />
  														<rect fill="#000000" opacity="0.5" x="11" y="5" width="2" height="14" rx="1" />
  														<path d="M6.70710678,18.7071068 C6.31658249,19.0976311 5.68341751,19.0976311 5.29289322,18.7071068 C4.90236893,18.3165825 4.90236893,17.6834175 5.29289322,17.2928932 L11.2928932,11.2928932 C11.6714722,10.9143143 12.2810586,10.9010687 12.6757246,11.2628459 L18.6757246,16.7628459 C19.0828436,17.1360383 19.1103465,17.7686056 18.7371541,18.1757246 C18.3639617,18.5828436 17.7313944,18.6103465 17.3242754,18.2371541 L12.0300757,13.3841378 L6.70710678,18.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 14.999999) scale(1, -1) translate(-12.000003, -14.999999)" />
  													</g>
  												</svg>
  											</span>
  											<!--end::Svg Icon-->
                      @endif

										</div>
										<!--end::Label-->
									</div>
									<!--end::Description-->
								</div>
								<!--end::Item-->

						</div>
						<!--end::Body-->
					</div>
					<!--end::List Widget 1-->
				</div>
      </div>

      <div class="col-xl-4">
				<!--begin::Mixed Widget 4-->
				<div class="card card-xl-stretch mb-xl-8">
					<!--begin::Beader-->
					<div class="card-header border-0 py-5">
						<h3 class="card-title align-items-start flex-column">
							<span class="card-label fw-bolder fs-3 mb-1">Net Margin In Target</span>
							<span class="text-muted fw-bold fs-7">To Achieve {{$shareThisMonth*100}}% Share</span>
						</h3>
					</div>
					<!--end::Header-->
					<!--begin::Body-->
					<div class="card-body d-flex flex-column">
						<div class="flex-grow-1">
							<div class="achievement-radial" data-kt-chart-color="success" style="height: 200px"></div>
						</div>
						<div class="pt-5">
							<p class="text-center fs-6 pb-5">
							<span class="badge badge-light-danger fs-8">Notes:</span>&#160; Current sprint is temporary
							<br />until settled end of month</p>
							<a href="{{route('viewMarginIn')}}" class="btn btn-success w-100 py-3">Check on Detail</a>
						</div>
					</div>
					<!--end::Body-->
				</div>
				<!--end::Mixed Widget 4-->
			</div>

      <div class="col-xl-4">
        <!--begin::Budget-->
        <div class="card card-xl-stretch mb-xl-8">
          <div class="card-body p-9">
            <div class="fs-2hx fw-bolder">{{$counters ? number_format($counters->volume,'2',',','.') : 0}} Lot</div>
            <div class="fs-4 fw-bold text-gray-400 mb-7">On Going Volume</div>
            <div class="fs-6 d-flex justify-content-between mb-4">
              <div class="fw-bold">In Rupiah</div>
              <div class="d-flex fw-bolder">IDR {{number_format($comThisMonth,0,',','.')}}</div>
            </div>
          <div class="separator separator-dashed"></div>
          <div class="fs-6 d-flex justify-content-between my-4">
            <div class="fw-bold">Today Volume</div>
            <div class="d-flex fw-bolder">{{$vtoday ? number_format($vtoday->volume,'2',',','.') : 0}}</div>
          </div>
          <div class="separator separator-dashed"></div>
          <div class="fs-6 d-flex justify-content-between mt-4">
            <div class="fw-bold">Daily Average</div>
            <div class="d-flex fw-bolder">{{$avgDaily ? number_format($avgDaily,'2',',','.') : 0}}</div>
          </div>
        </div>
      </div>
      <!--end::Budget-->
    </div>

    </div> --}}

    {{-- <div class="row">
      <div class="col-md-12 mb-5">
        <!--begin::Statistics Widget 3-->
        <div class="card card-xl-stretch mb-xl-8">
          <div class="card-header p-10">
            <h4>Invite New Trader</h4>
            <p class="text-muted fw-bold mt-1">Simple Click Referral System</p>
          </div>
          <!--begin::Body-->
          <div class="card-body d-flex flex-column p-10">
            <div class="row">
              <div class="col-md-12">
                <div class="row mb-6">
                  <!--begin::Label-->
                  <label class="col-lg-2 col-form-label fw-bold fs-6">Link </label>
                  <!--end::Label-->
                  <!--begin::Col-->
                  <div class="col-lg-10 fv-row">
                    <input type="text" id="copyIB" class="form-control form-control-lg form-control-solid" value="{{env('IMS_ACTIVE')}}/?ref={{profile()->id}}&utm_source=copylink&utm_campaign=Affiliate&utm_medium=super&utm_content=master">
                  </div>
                </div>

                <div class="card-footer d-flex justify-content-end py-6 mt-5">
                  <button style="background: #0088cc; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-5" onclick="copyClipboard()">Copy Link</button>
                </div>
              </div>
            </div>
            <!-- share row -->

            <div class="row">

              <div class="col-md-6 col-xs-12">
                <a id="whatsapp" style="background: #4FCE5D; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=whatsapp&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=whatsapp&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=whatsapp&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=whatsapp&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}, 'newwindow', 'width=600,height=500'); return false;"><i class="fab fa-whatsapp" aria-hidden="true"></i> Whatsapp</a>
              </div>

              <div class="col-md-6 col-xs-12">
                <a id="facebook" style="background: #3c5b9b; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=facebook&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=facebook&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=facebook&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=facebook&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}, 'newwindow', 'width=600,height=500'); return false;"><i class="fab fa-facebook-f" aria-hidden="true"></i> Facebook</a>
              </div>

              <div class="col-md-6 col-xs-12">
                <a id="twitter" style="background: #00acee; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=twitter&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=twitter&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=twitter&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=twitter&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}, 'newwindow', 'width=600,height=500'); return false;"><i class="fab fa-twitter" aria-hidden="true"></i> Twitter</a>
              </div>

              <div class="col-md-6 col-xs-12">
                <a id="telegram" style="background: #0088cc; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=telegram&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=telegram&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=telegram&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=telegram&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}, 'newwindow', 'width=600,height=500'); return false;"><i class="fab fa-telegram" aria-hidden="true"></i> Telegram</a>
              </div>

              <div class="col-md-6 col-xs-12">
                <a id="line" style="background: #00B900; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3" href="http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=lineme&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=line&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}" onclick="window.open('http://www.addthis.com/bookmark.php?v=300&winname=addthis&pub=ra-59563471e59e31ba&source=csmlsh-1.0&lng=en&s=lineme&wid=94kf&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=line&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}, 'newwindow', 'width=600,height=500'); return false;"><i class="fab fa-line" aria-hidden="true"></i> Line</a>
              </div>

              <div class="col-md-6 col-xs-12">
                <a id="linkedin" style="background: #0e76a8; border: transparent;margin-bottom:20px;" target="_blank" class="btn btn-success w-100 py-3"
                href="window.open('https://www.linkedin.com/shareArticle?mini=true&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=line&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}&ro=false&summary=&source="
                onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url={{urlencode(env("IMS_ACTIVE").'/?ref='.profile()->id.'&utm_source=line&utm_campaign=Affiliate System&utm_medium=share button&utm_content=Cabinet')}}&ro=false&summary=&source=', 'newwindow', 'width=600,height=500'); return false;">
                <i class="fab fa-linkedin-in"></i> LinkedIn</a>
              </div>
            </div>
            <!-- /share row -->
          </div>
        </div>
      </div>
    </div> --}}

  </div>

@endsection

@section('jsonpage')
  <script type="text/javascript">
  var KTWidgets = {
    init: function () {
      var e = document.querySelectorAll(".achievement-radial");
      [].slice.call(e).map((function (e) {
        var t = parseInt(KTUtil.css(e, "height"));
        if (e) {
          var a = e.getAttribute("data-kt-chart-color"),
          o = KTUtil.getCssVariableValue("--bs-" + a),
          s = KTUtil.getCssVariableValue("--bs-light-" + a),
          r = KTUtil.getCssVariableValue("--bs-gray-700");
          new ApexCharts(e, {
            series: [{{number_format($netmarginThismonth/$toAchieve*100,'0')}}],
            chart: {
              fontFamily: "inherit",
              height: t,
              type: "radialBar"
            },
            plotOptions: {
              radialBar: {
                hollow: {
                  margin: 0,
                  size: "65%"
                },
                dataLabels: {
                  showOn: "always",
                  name: {
                    show: !1,
                    fontWeight: "700"
                  },
                  value: {
                    color: r,
                    fontSize: "30px",
                    fontWeight: "700",
                    offsetY: 12,
                    show: !0,
                    formatter: function (e) {
                      return e + "%"
                    }
                  }
                },
                track: {
                  background: s,
                  strokeWidth: "100%"
                }
              }
            },
            colors: [o],
            stroke: {
              lineCap: "round"
            },
            labels: ["Progress"]
          }).render()
        }
      }))
    }()
  }
  </script>
@endsection
