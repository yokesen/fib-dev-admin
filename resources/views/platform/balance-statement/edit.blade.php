@extends('template.master')

@section('title','Deposit')
@section('bc-1','Deposit')
@section('bc-2','Process')

@section('container')
  <div id="kt_content_container" class="container">
    <div class="row">
      <div class="col-md-12">
        <!--begin::Tables Widget 9-->
        <div class="card card-xxl-stretch mb-5 mb-xl-8">
          <!--begin::Header-->
          <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
              <span class="card-label fw-bolder fs-3 mb-1">Process {{$deposit->tipe_deposit}}</span>
            </h3>
            <div class="card-toolbar">
							<a href="{{route('viewDepositList')}}" class="btn btn-sm btn-light-primary" >
							<!--begin::Svg Icon | path: icons/duotone/Communication/Add-user.svg-->
							<span class="svg-icon svg-icon-3">
								<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
									<path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
								</svg>
							</span>
							<!--end::Svg Icon-->Back</a>
						</div>
          </div>
          <!--end::Header-->
          <!--begin::Body-->
          <div class="card-body py-3">

            <div class="row py-5">
              <!--begin::Row-->
              <div class="col-md-12">
                <!--begin::Item-->
                <div class="modal-header" id="kt_modal_new_address_header">
                  <!--begin::Modal title-->
                  <h2>Profile</h2>
                  <!--end::Modal title-->

                </div>

                <!--begin::Card body-->
                <div class="card-body border-top p-9">
                  <div class="row">

                    <div class="col-md-6">
                      <div class="row mb-6">
                        <!--begin::Label-->
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">Username</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-8 fv-row">
                          <input class="form-control form-control-lg form-control-solid" value="{{$client->name }}">
                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group-->
                      <!--begin::Input group-->
                      <div class="row mb-6">
                        <!--begin::Label-->
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-8 fv-row">
                          <input class="form-control form-control-lg form-control-solid" value="{{$client->name }}">
                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group-->
                      <!--begin::Input group-->
                      <div class="row mb-6">
                        <!--begin::Label-->
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">Email</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-8 fv-row">
                          <input class="form-control form-control-lg form-control-solid" value="{{$client->email }}">
                        </div>
                        <!--end::Col-->
                      </div>

                      <!--begin::Input group-->
                      <div class="row mb-6">
                        <!--begin::Label-->
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama Bank</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-8 fv-row">
                          <input class="form-control form-control-lg form-control-solid" value="{{$bank->bank_name }}">
                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group-->
                      <!--begin::Input group-->
                      <div class="row mb-6">
                        <!--begin::Label-->
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">Nomor Rekening</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-8 fv-row">
                          <input class="form-control form-control-lg form-control-solid" value="{{$bank->account_number }}">
                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group-->
                      <!--begin::Input group-->
                      <div class="row mb-6">
                        <!--begin::Label-->
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama di Rekening</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-8 fv-row">
                          <input class="form-control form-control-lg form-control-solid" value="{{$bank->account_name }}">
                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group-->
                    </div>
                    <div class="col-md-6">
                      <div class="col-md-6">
                        <div class="row pb-8">
                          @if ($client->photoTabungan != "/images/upload-default.jpg")
                            <img src="{{env('IMG_USER').$client->photoTabungan}}" width="100%">
                          @else
                            <img src="{{url('/')}}/images/upload-default.jpg" width="100%">
                          @endif
                        </div>
                        <div class="row pb-8">
                          @if ($client->photoKTP == '/images/upload-default.jpg')
                            <img src="{{url('/')}}/images/upload-default.jpg" width="100%">
                          @else
                            @php
                              $str = strpos($client->photoKTP,'orbitrade.email');
                            @endphp
                            @if ($str>0)
                              <img src="https://orbitrade.email/storages/home/imsgema/www/uploads/{{$client->id}}.jpg" width="100%">
                            @else
                              <img src="{{env('IMG_USER')}}{{$client->photoKTP}}" width="100%">
                            @endif
                          @endif
                        </div>

                      </div>
                    </div>
                  </div>

                </div>

              </div>
              <!--end::Row-->
            </div>


            <form class="form" action="{{route('depositProcess')}}" method="post">
              <!--begin::Modal header-->
              <div class="modal-header" id="kt_modal_new_address_header">
                <!--begin::Modal title-->
                <h2>Process {{$deposit->tipe_deposit}}</h2>
                <!--end::Modal title-->

              </div>
              <!--end::Modal header-->
              <!--begin::Modal body-->
              <div class="modal-body py-10 px-lg-17">
                <!--begin::Scroll-->
                <div class="scroll-y me-n7 pe-7">
                  <!--begin::Notice-->
                  <!--begin::Notice-->
                  <div class="notice d-flex bg-light-info rounded border-info border border-dashed mb-9 p-6">
                    <!--begin::Icon-->
                    <!--begin::Svg Icon | path: icons/duotone/Code/Warning-1-circle.svg-->
                    <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                        <rect fill="#000000" x="11" y="7" width="2" height="8" rx="1" />
                        <rect fill="#000000" x="11" y="16" width="2" height="2" rx="1" />
                      </svg>
                    </span>
                    <!--end::Svg Icon-->
                    <!--end::Icon-->
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-stack flex-grow-1">
                      <!--begin::Content-->
                      <div class="fw-bold">
                        <h4 class="text-gray-800 fw-bolder">Warning</h4>
                        <div class="fs-6 text-gray-600">Pastikan uangnya sudah masuk di rekening</div>
                      </div>
                      <!--end::Content-->
                    </div>
                    <!--end::Wrapper-->
                  </div>
                  <!--end::Notice-->
                  <!--end::Notice-->
                  <div class="row mb-5">
                    <div class="col-md-6">
                      <!--begin::Input group-->
                      <div class="row mb-5">
                        <!--begin::Col-->
                        <div class="col-md-12 fv-row">
                          <!--begin::Label-->
                          <label class="required fs-5 fw-bold mb-2">Bank Partner</label>
                          <!--end::Label-->
                          <!--begin::Input-->
                          <input type="text" name="from_rekening" class="form-control form-control-solid" value="{{$deposit->from_bank}}" readonly/>
                          <!--end::Input-->
                        </div>
                        <!--end::Col-->
                      </div>
                      <div class="row mb-5">
                        <!--begin::Col-->
                        <div class="col-md-12 fv-row">
                          <!--end::Label-->
                          <label class="required fs-5 fw-bold mb-2">Rekening Partner</label>
                          <!--end::Label-->
                          <!--end::Input-->
                          <input type="text" name="from_bank" class="form-control form-control-solid" value="{{$deposit->from_rekening}}" readonly/>
                          <!--end::Input-->
                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group-->
                      <div class="row mb-5">
                        <!--begin::Col-->
                        <div class="col-md-12 fv-row">
                          <!--end::Label-->
                          <label class="required fs-5 fw-bold mb-2">Nama Partner</label>
                          <!--end::Label-->
                          <!--end::Input-->
                          <input type="text" name="from_name" class="form-control form-control-solid" value="{{$deposit->from_name}}" readonly/>
                          <!--end::Input-->
                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group-->
                    </div>
                    <div class="col-md-6">
                      <!--begin::Input group-->
                      <div class="row mb-5">
                        <!--begin::Col-->
                        <div class="col-md-12 fv-row">
                          <!--begin::Label-->
                          <label class="required fs-5 fw-bold mb-2">Bank Company</label>
                          <!--end::Label-->
                          <!--begin::Input-->
                          <input type="text" name="to_rekening" class="form-control form-control-solid" value="{{$deposit->to_bank}}" readonly/>
                          <!--end::Input-->
                        </div>
                        <!--end::Col-->
                      </div>
                      <div class="row mb-5">
                        <!--begin::Col-->
                        <div class="col-md-12 fv-row">
                          <!--end::Label-->
                          <label class="required fs-5 fw-bold mb-2">Rekening Company</label>
                          <!--end::Label-->
                          <!--end::Input-->
                          <input type="text" name="to_bank" class="form-control form-control-solid" value="{{$deposit->to_rekening}}" readonly/>
                          <!--end::Input-->
                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group-->
                      <div class="row mb-5">
                        <!--begin::Col-->
                        <div class="col-md-12 fv-row">
                          <!--end::Label-->
                          <label class="required fs-5 fw-bold mb-2">Nama Account Company</label>
                          <!--end::Label-->
                          <!--end::Input-->
                          <input type="text" name="to_name" class="form-control form-control-solid" value="{{$deposit->to_name}}" readonly/>
                          <!--end::Input-->
                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group-->
                    </div>
                  </div>

                  <hr>

                  <div class="row mb-5">
                    <div class="col-md-6">
                      <img src="{{$deposit->bukti == '/images/upload-default.jpg' ? '/images/upload-default.jpg' : env('IMG_USER').$deposit->bukti}}">
                    </div>
                    <div class="col-md-6">
                      {{-- <!--end::Input group-->
                      <div class="row mb-5">
                        <!--begin::Col-->
                        <div class="col-md-12 fv-row">
                          <!--end::Label-->
                          <label class="required fs-5 fw-bold mb-2">To Account</label>
                          <!--end::Label-->
                          <!--end::Input-->
                          <input type="text" name="to_name" class="form-control form-control-solid" value="{{$deposit->metatrader}}" readonly/>
                          <!--end::Input-->
                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group--> --}}
                      <!--end::Input group-->
                      <div class="row mb-5">
                        <!--begin::Col-->
                        <div class="col-md-12 fv-row">
                          <!--end::Label-->
                          <label class="required fs-5 fw-bold mb-2">Transfer Amount</label>
                          <!--end::Label-->
                          <!--end::Input-->
                          <input type="text" name="to_name" class="form-control form-control-solid" value="Rp {{number_format($deposit->amount,0,'.','.')}},-" readonly/>
                          <!--end::Input-->
                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group-->
                      {{-- <!--end::Input group-->
                      <div class="row mb-5">
                        <!--begin::Col-->
                        <div class="col-md-12 fv-row">
                          <!--end::Label-->
                          <label class="required fs-5 fw-bold mb-2"> <b>RATE</b> </label>
                          <!--end::Label-->
                          <!--end::Input-->

                            <input type="number" name="rate" class="form-control form-control-solid input-valid" value="10000" readonly/>


                          <!--end::Input-->
                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group-->
                      <!--end::Input group-->
                      <div class="row mb-5">
                        <!--begin::Col-->
                        <div class="col-md-12 fv-row">
                          <!--end::Label-->
                          <label class="required fs-5 fw-bold mb-2">Approved amount</label>
                          <!--end::Label-->
                          <!--end::Input-->

                            <input type="text" name="approved_amount" class="form-control form-control-solid input-valid" value="{{number_format($deposit->amount/10000,2,',','.')}}" style="font-size:2em!important" readonly/>


                          <!--end::Input-->
                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group--> --}}
                      <!--begin::Notice-->
                      <div class="notice d-flex bg-light-primary rounded border-info border border-dashed mb-9 p-6">
                        <!--begin::Icon-->
                        <!--begin::Svg Icon | path: icons/duotone/Code/Warning-1-circle.svg-->
                        <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                            <rect fill="#000000" x="11" y="7" width="2" height="8" rx="1" />
                            <rect fill="#000000" x="11" y="16" width="2" height="2" rx="1" />
                          </svg>
                        </span>
                        <!--end::Svg Icon-->
                        <!--end::Icon-->
                        <!--begin::Wrapper-->
                        <div class="d-flex flex-stack flex-grow-1">
                          <!--begin::Content-->
                          <div class="fw-bold">
                            <h4 class="text-gray-800 fw-bolder">Warning</h4>
                            <div class="fs-6 text-gray-600">Pastikan input ke Metatrader sesuai angka diatas</div>
                          </div>
                          <!--end::Content-->
                        </div>
                        <!--end::Wrapper-->
                      </div>
                      <!--end::Notice-->
                    </div>
                  </div>
                </div>
                <!--end::Scroll-->
              </div>
              <!--end::Modal body-->


              <!--begin::Modal footer-->
              <div class="modal-footer flex-center">
                <input type="hidden" name="uuid" value="{{$deposit->uuid}}">
                <input type="hidden" name="id" value="{{$deposit->id}}">
                @csrf
                <!--begin::Button-->
                <div class="row">
                  <div class="col-md-6">
                    <button type="submit" class="btn btn-success me-10">
                      <span class="indicator-label">Approve</span>
                      <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                      </span>
                    </button>
                  </div>
                  <div class="col-md-6">
                    <a href="#" class="btn btn-primary btn-sm ms-10" data-bs-toggle="modal" data-bs-target="#ot_reject_{{$deposit->id}}">
                      <!--begin::Svg Icon | path: icons/duotone/General/Trash.svg-->
                      <span class="svg-icon svg-icon-3">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                          <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24" />
                            <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero" />
                            <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3" />
                          </g>
                        </svg>
                      </span> Reject
                      <!--end::Svg Icon-->
                    </a>
                  </div>
                </div>

                <!--end::Button-->
              </div>
              <!--end::Modal footer-->
              </form>


          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="ot_reject_{{$deposit->id}}" tabindex="-1" aria-hidden="true">
    <!--begin::Modal dialog-->
    <div class="modal-dialog modal-dialog-centered mw-650px">
      <!--begin::Modal content-->
      <div class="modal-content">
        <!--begin::Form-->
        <form class="form" action="{{route('depositReject')}}" method="post">
          <!--begin::Modal header-->
          <div class="modal-header" id="kt_modal_new_address_header">
            <!--begin::Modal title-->
            <h2>Reject Deposit</h2>
            <!--end::Modal title-->
            <!--begin::Close-->
            <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
              <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
              <span class="svg-icon svg-icon-1">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                  </g>
                </svg>
              </span>
              <!--end::Svg Icon-->
            </div>
            <!--end::Close-->
          </div>
          <!--end::Modal header-->
          <!--begin::Modal body-->
          <div class="modal-body py-10 px-lg-17">
            <!--begin::Scroll-->
            <div class="scroll-y me-n7 pe-7">
              <!--begin::Notice-->
              <!--begin::Notice-->
              <div class="notice d-flex bg-light-danger rounded border-danger border border-dashed mb-9 p-6">
                <!--begin::Icon-->
                <!--begin::Svg Icon | path: icons/duotone/Code/Warning-1-circle.svg-->
                <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />
                    <rect fill="#000000" x="11" y="7" width="2" height="8" rx="1" />
                    <rect fill="#000000" x="11" y="16" width="2" height="2" rx="1" />
                  </svg>
                </span>
                <!--end::Svg Icon-->
                <!--end::Icon-->
                <!--begin::Wrapper-->
                <div class="d-flex flex-stack flex-grow-1">
                  <!--begin::Content-->
                  <div class="fw-bold">
                    <h4 class="text-gray-800 fw-bolder">Warning</h4>
                    <div class="fs-6 text-gray-600">Permintaan deposit akan direject, mohon masukkan alasannya!</div>
                  </div>
                  <!--end::Content-->
                </div>
                <!--end::Wrapper-->
              </div>
              <!--end::Notice-->
              <!--end::Notice-->
              <!--begin::Input group-->
              <div class="row mb-5">
                <!--begin::Col-->
                <div class="col-md-12 fv-row">
                  <!--begin::Label-->
                  <label class="required fs-5 fw-bold mb-2">Alasan</label>
                  <!--end::Label-->
                  <!--begin::Input-->
                  <input type="text" name="reason" class="form-control form-control-solid" placeholder="masukkan alasan reject"/>
                  <!--end::Input-->
                </div>
                <!--end::Col-->

              </div>
              <!--end::Input group-->


            </div>
            <!--end::Scroll-->
          </div>
          <!--end::Modal body-->
          <!--begin::Modal footer-->
          <div class="modal-footer flex-center">
            <input type="hidden" name="uuid" value="{{$deposit->uuid}}">
            <input type="hidden" name="id" value="{{$deposit->id}}">
            <!--begin::Button-->
            @csrf
            <button type="submit" class="btn btn-primary">
              <span class="indicator-label">Reject!</span>
              <span class="indicator-progress">Please wait...
                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
              </button>
              <!--end::Button-->
            </div>
            <!--end::Modal footer-->
          </form>
          <!--end::Form-->
        </div>
      </div>
    </div>
@endsection

@section('jsonpage')
  {{-- <script type="text/javascript">
    $('#rate').change(function(){
      $.ajax({
        type: "POST",
        url: "{{route('ajaxDepositCalculation')}}",
        data: {
          depoId:{{$deposit->id}},
          rate: this.value
        },
        headers: {
          "X-CSRF-TOKEN": "{{ csrf_token() }}"
        },
        success: function(e) {
          var converted = parseFloat(e).toFixed(2);
          $('#converted').val(converted);
        }
      })
    });
  </script> --}}
@endsection
