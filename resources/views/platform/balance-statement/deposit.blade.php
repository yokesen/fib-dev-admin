@extends('template.master')

@section('title','Request Balance')
@section('bc-1','Request Balance')
@section('bc-2','Request')

@section('container')

  <div id="kt_content_container" class="container">
    <div class="row">
      <div class="col-md-12">
        <!--begin::Tables Widget 9-->
        <div class="card card-xxl-stretch mb-5 mb-xl-8">
          <!--begin::Header-->
          <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
              <span class="card-label fw-bolder fs-3 mb-1">Request Balance</span>
              <span class="text-muted mt-1 fw-bold fs-7">{{$count}} new request</span>
            </h3>
            <div class="card-toolbar">
              <div class="input-group mb-3">
                <select class="form-select form-select-solid w-200px" name="pilihan" id="pilihan">
                  <option value="mt4">Search by MT5 ID</option>
                  <option value="username">Search by username</option>
                  <option value="rek">Search by Nomor Rekening</option>
                  <option value="nama">Search by Nama Rekening</option>
                  <option value="amount">Search by Amount</option>
                </select>
                <input type="text" id="searchDepositRequests" class="form-control form-control-solid w-250px ps-5 " placeholder="minimal 4 huruf" />
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              {{-- <div id="clearSearch"><a href="{{route('depositRequest')}}">clear search</a></div> --}}
            </div>
          </div>
          <!--end::Header-->
          <!--begin::Body-->
          <div class="card-body py-3">
            <!--begin::Table container-->
            <div class="table-responsive" id="listnew">
              <!--begin::Table-->
              <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                <!--begin::Table head-->
                <thead>
                  <tr class="fw-bolder text-muted">
                    <th>Request Date</th>
                    <th>Trader Account</th>
                    <th>from Bank</th>
                    <th>to Bank</th>
                    <th>Amount</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <!--end::Table head-->
                <!--begin::Table body-->
                <tbody>
                  @foreach ($requests as $x => $value)
                    <tr>
                      <td>
                        <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{date('d-m-Y H:i:s',strtotime($value->created_at.'+7hours'))}}</a>
                        <span class="text-muted fw-bold text-muted d-block fs-7">{{ \Carbon\Carbon::parse(strtotime($value->created_at.'+7hours'))->diffForHumans()}}</span>
                      </td>
                      <td>
                        <div class="d-flex align-items-center">
                          <div class="symbol symbol-45px me-5">
                            <img src="{{$value->photo}}" alt="" />
                          </div>
                          <div class="d-flex justify-content-start flex-column">
                            <a href="#" class="text-dark fw-bolder text-hover-primary fs-6">{{$value->metatrader}}</a>
                            <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->username}}</span>
                          </div>
                        </div>
                      </td>
                      <td>
                        <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->from_rekening}}</a>
                        <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->from_bank}}</span>
                        <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->from_name}}</span>
                      </td>
                      <td>
                        <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->to_bank}}</a>
                        <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->to_rekening}}</span>
                        <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->to_name}}</span>
                      </td>
                      <td>
                        <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->tipe_deposit}}</span>
                        <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">Rp {{number_format($value->amount,0,'.',',')}}</a>
                      </td>
                      <td class="text-end">
                        <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->status}}</span>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <!--end::Table body-->
              </table>
              <!--end::Table-->
              {{$requests->links()}}
            </div>
            <!--end::Table container-->
          </div>
          <!--begin::Body-->
        </div>
        <!--end::Tables Widget 9-->
      </div>
    </div>
  </div>
@endsection

@section('jsonpage')
<script type="text/javascript">
$(document).ready(function(){
  $("#clearSearch").hide();
});

</script>
@endsection
