@extends('template.master')

@section('title','Trading Account')
@section('metadescription','FIB Partner the best business partner for IB')
@section('metakeyword','FIB Partner')
@section('bc-1','My Business')
@section('bc-2','Trading Account')

@section('container')

  <div id="kt_content_container" class="container">
      <div class="row">
        <div class="col-md-12">
          <!--begin::Tables Widget 9-->
          <div class="card card-xxl-stretch mb-5 mb-xl-8">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">

            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-3">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="fw-bolder text-muted">
                      <th>Username</th>
                      <th>Account ID</th>
                      <th>Type Account</th>
                      <th>Group Account</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody id="table-body">
                    @foreach ($accounts as $x => $value)
                      <tr>
                        <td>
                          <span class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->username}}</a>
                        </td>
                        <td>
                          <span class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->mt4_id}}</a>
                        </td>
                        <td>
                          <span class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->typeAccount}}</a>
                        </td>
                        <td>
                          <span class="text-dark fw-bolder text-hover-primary d-block fs-6"></a>
                        </td>
                      </tr>

                    @endforeach
                  </tbody>
                  <!--end::Table body-->
                </table>
                <!--end::Table-->
                {{$accounts->links()}}
              </div>
              <!--end::Table container-->
            </div>
            <!--begin::Body-->
          </div>
          <!--end::Tables Widget 9-->
        </div>
      </div>

    </div>

@endsection
