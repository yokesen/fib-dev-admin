@extends('template.master')

@section('title','Margin Out')
@section('metadescription','FIB Partner the best business partner for IB')
@section('metakeyword','FIB Partner')
@section('bc-1','Margin In Out')
@section('bc-2','Margin Out')

@section('container')

  <div id="kt_content_container" class="container">
      <div class="row">
        <div class="col-md-12">
          <!--begin::Tables Widget 9-->
          <div class="card card-xxl-stretch mb-5 mb-xl-8">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">

            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-3">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="fw-bolder text-muted">
                      <th>Username</th>
                      <th>Time</th>
                      <th>Account ID</th>
                      <th>Amount</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody id="table-body">
                    @foreach ($margin as $x => $value)
                      <tr>
                        <td>
                          <span class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->username}}</span>
                        </td>
                        <td>
                          <span class="text-dark fw-bolder text-hover-primary d-block fs-6">{{date('d-m-Y H:i:s',strtotime($value->created_at))}}</span>
                        </td>
                        <td>
                          <span class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->metatrader}}</span>
                        </td>
                        <td class="text-center">
                          <span class="text-dark fw-bolder text-hover-primary d-block fs-6">{{number_format($value->approved_amount,'2',',','.')}}
                          @if ($value->reason != "")
                            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="{{$value->reason}}"></i></a>
                          @endif
                          </span>
                        </td>
                        <td>
                          <span class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->status}}
                            @if ($value->status=="approved")
                              <i class="fas fa-check-circle fs-7 text-success ls-5"></i>
                            @elseif ($value->status=="pending")
                              <i class="fas fa-question-circle fs-7 text-success ls-5"></i>
                            @elseif ($value->status=="rejected")
                              <i class="fas fa-times-circle fs-7 text-danger ls-5"></i>
                            @elseif ($value->status=="expired")
                              <i class="fas fa-clock fs-7 text-danger ls-5"></i>
                            @endif
                          </span>

                        </td>
                      </tr>

                    @endforeach
                  </tbody>
                  <!--end::Table body-->
                </table>
                <!--end::Table-->
                {{$margin->links()}}
              </div>
              <!--end::Table container-->
            </div>
            <!--begin::Body-->
          </div>
          <!--end::Tables Widget 9-->
        </div>
      </div>

    </div>

@endsection
