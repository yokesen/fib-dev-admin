@extends('template.master')

@section('title','Approval Group')
@section('metadescription','Invest to rich')
@section('metakeyword','Invest to rich')
@section('bc-1','Group')
@section('bc-2','Approval')

@section('container')
  <div id="kt_content_container" class="container">
      <div class="row">
        <form method="POST" action="{{route('groupProcessApproval')}}" class="form" enctype="multipart/form-data">
          <div class="col-md-12">
            <!--begin::Basic info-->
            <div class="card mb-5 mb-xl-10">
              <!--begin::Card header-->
              <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
                <!--begin::Card title-->
                <div class="card-title m-0">
                  <h3 class="fw-bolder m-0">Approval Group {{$grup->accountGroup}}</h3>
                </div>
                <!--end::Card title-->
              </div>
              <!--begin::Card header-->
              <!--begin::Content-->
              <div class="card-body border-top p-9">
                <div class="row">
                  <div class="col-md-6">

                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Group Code</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">
                        <input class="form-control form-control-lg form-control-solid" type="text" placeholder="" name="accountGroup" autocomplete="off" value="{{$grup->accountGroup}}" required readonly/>
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Group Name</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">
                        <input class="form-control form-control-lg form-control-solid" type="text" placeholder="" name="namaAccount" autocomplete="off" value="{{$grup->namaAccount}}" required readonly/>
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Number Grouping</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">
                        <input class="form-control form-control-lg form-control-solid" type="text" placeholder="" name="startAccNumber" autocomplete="off" value="{{$grup->startAccNumber}}00001" required readonly/>
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Client Name</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">
                        <input class="form-control form-control-lg form-control-solid" type="text" placeholder="" name="name" autocomplete="off" value="{{$grup->name}}" required readonly/>
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Client Username</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">
                        <input class="form-control form-control-lg form-control-solid" type="text" placeholder="" name="username" autocomplete="off" value="{{$grup->username}}" required readonly/>
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->






                  </div>

                  <div class="col-md-6">
                    <!--begin::Input group-->
                    <div class="mb-10 fv-row">
                        <!--begin::Label-->
                        <label class="d-flex align-items-center required form-label mb-3">Leverage</label>
                        <!--end::Label-->
                        <!--begin::Row-->
                        <div class="row mb-2" data-kt-buttons="true">
                          <!--begin::Col-->
                          <div class="col">
                              <!--begin::Option-->
                              <label
                                  class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5">
                                  <input type="radio" class="btn-check" name="accountLeverage"
                                      value="50" />
                                  <span class="fw-bolder fs-3">1 : 50</span>
                              </label>
                              <!--end::Option-->
                          </div>
                          <!--end::Col-->
                            <!--begin::Col-->
                            <div class="col">
                                <!--begin::Option-->
                                <label
                                    class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5">
                                    <input type="radio" class="btn-check" name="accountLeverage"
                                        checked="checked" value="100" />
                                    <span class="fw-bolder fs-3">1 : 100</span>
                                </label>
                                <!--end::Option-->
                            </div>
                            <!--end::Col-->
                            <!--begin::Col-->
                            <div class="col">
                                <!--begin::Option-->
                                <label
                                    class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5 active">
                                    <input type="radio" class="btn-check" name="accountLeverage"
                                        value="200" />
                                    <span class="fw-bolder fs-3">1 : 200</span>
                                </label>
                                <!--end::Option-->
                            </div>
                            <!--end::Col-->

                            <!--begin::Col-->
                            <div class="col">
                                <!--begin::Option-->
                                <label
                                    class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5">
                                    <input type="radio" class="btn-check" name="accountLeverage"
                                        value="500" />
                                    <span class="fw-bolder fs-3">1 : 500</span>
                                </label>
                                <!--end::Option-->
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Row-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="mb-10 fv-row">
                        <!--begin::Label-->
                        <label class="d-flex align-items-center required form-label mb-3">Minimum Lot</label>
                        <!--end::Label-->
                        <!--begin::Row-->
                        <div class="row mb-2" data-kt-buttons="true">
                          <!--begin::Col-->
                          <div class="col">
                              <!--begin::Option-->
                              <label
                                  class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5 active">
                                  <input type="radio" class="btn-check" name="accountMinLot"
                                      value="0.01" />
                                  <span class="fw-bolder fs-3">0.01</span>
                              </label>
                              <!--end::Option-->
                          </div>
                          <!--end::Col-->
                            <!--begin::Col-->
                            <div class="col">
                                <!--begin::Option-->
                                <label
                                    class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5">
                                    <input type="radio" class="btn-check" name="accountMinLot"
                                        checked="checked" value="0.1" />
                                    <span class="fw-bolder fs-3">0.1</span>
                                </label>
                                <!--end::Option-->
                            </div>
                            <!--end::Col-->
                            <!--begin::Col-->
                            <div class="col">
                                <!--begin::Option-->
                                <label
                                    class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5">
                                    <input type="radio" class="btn-check" name="accountMinLot"
                                        value="1.0" />
                                    <span class="fw-bolder fs-3">1.0</span>
                                </label>
                                <!--end::Option-->
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Row-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="mb-10 fv-row">
                        <!--begin::Label-->
                        <label class="d-flex align-items-center required form-label mb-3">Lot Step</label>
                        <!--end::Label-->
                        <!--begin::Row-->
                        <div class="row mb-2" data-kt-buttons="true">
                          <!--begin::Col-->
                          <div class="col">
                              <!--begin::Option-->
                              <label
                                  class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5 active">
                                  <input type="radio" class="btn-check" name="accountMinStep"
                                      value="0.01" />
                                  <span class="fw-bolder fs-3">0.01</span>
                              </label>
                              <!--end::Option-->
                          </div>
                          <!--end::Col-->
                            <!--begin::Col-->
                            <div class="col">
                                <!--begin::Option-->
                                <label
                                    class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5">
                                    <input type="radio" class="btn-check" name="accountMinStep"
                                        checked="checked" value="0.1" />
                                    <span class="fw-bolder fs-3">0.1</span>
                                </label>
                                <!--end::Option-->
                            </div>
                            <!--end::Col-->
                            <!--begin::Col-->
                            <div class="col">
                                <!--begin::Option-->
                                <label
                                    class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5">
                                    <input type="radio" class="btn-check" name="accountMinStep"
                                        value="1.0" />
                                    <span class="fw-bolder fs-3">1.0</span>
                                </label>
                                <!--end::Option-->
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Row-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-6 col-form-label required fw-bold fs-6">Default Spread</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-6 fv-row">

                        <input class="form-control form-control-lg form-control-solid" type="number"
                            name="accountSpread" min="1" step="1"
                            style="font-size:1.5em!important;text-align:center;" required />
                        @if ($errors->has('accountSpread'))
                            <h4 class="text-danger mt-6"> <i
                                    class="bi bi-exclamation-triangle text-danger fs-2 blink"></i>
                                {{ $errors->first('accountSpread') }}</h4>
                        @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-6 col-form-label required fw-bold fs-6">Commission / LOT</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-6 fv-row">

                        <input class="form-control form-control-lg form-control-solid" type="number"
                            name="accountCom" min="1" step="1"
                            style="font-size:1.5em!important;text-align:center;" required />
                        @if ($errors->has('accountCom'))
                            <h4 class="text-danger mt-6"> <i
                                    class="bi bi-exclamation-triangle text-danger fs-2 blink"></i>
                                {{ $errors->first('accountCom') }}</h4>
                        @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->


                  </div>

                </div>

                @csrf
                <!--begin::Actions-->
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                  <input type="hidden" name="user" value="{{$grup->categoryAccount}}" />
                  <button type="submit" class="btn btn-primary" id="ot_button">Submit</button>
                </div>
                <!--end::Actions-->
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
@endsection

@section('jsinline')

@endsection
