@extends('template.master')

@section('title','Groups')
@section('bc-1','Groups')
@section('bc-2','Request')

@section('container')
  <div id="kt_content_container" class="container">
      <div class="row">
        <div class="col-md-12">
          <!--begin::Tables Widget 9-->
          <div class="card card-xxl-stretch mb-5 mb-xl-8">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
              <div class="card-toolbar">
                <h1>GROUP REQUEST</h1>
  						</div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-3">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="fw-bolder text-muted">
                      <th>Request DateTime</th>
                      <th>Partner Name</th>
                      <th>Group Code</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody id="table-body">
                    @foreach ($list as $x => $value)

                      <tr>
                        <td>
                          <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->created_at}}</a>
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{ \Carbon\Carbon::parse(strtotime($value->created_at.'+7hours'))->diffForHumans()}}</span>
                        </td>
                        <td>
                          <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->name}}</a>
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->username}}</span>
                        </td>
                        <td>
                          <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->accountGroup}}</a>
                        </td>
                        <td>
                          <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->status}}</a>
                        </td>
                        <td>
                          <a href="{{route('groupApproval',$value->accountGroup)}}" class="btn btn-success">Approval</a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                  <!--end::Table body-->
                </table>
                <!--end::Table-->
                {{$list->links()}}
              </div>
              <!--end::Table container-->
            </div>
            <!--begin::Body-->
          </div>
          <!--end::Tables Widget 9-->
        </div>
      </div>

    </div>
@endsection

@section('jsonpage')

@endsection
