@extends('template.master')

@section('title','Profit Loss')
@section('metadescription','FIB Partner the best business partner for IB')
@section('metakeyword','FIB Partner')
@section('bc-1','My Business')
@section('bc-2','Profit Loss')

@section('container')

  <div id="kt_content_container" class="container">
    <div class="card rounded ">
      <!--begin::Body-->
      <div class="card-body p-lg-20">
        <!--begin::Layout-->
        <div class="d-flex flex-column flex-xl-row">
          <!--begin::Content-->
          <div class="flex-lg-row-fluid me-xl-18 mb-10 mb-xl-0">
            <!--begin::Invoice 2 content-->
            <div class="mt-n1">
              <!--begin::Top-->
              <div class="d-flex flex-stack pb-10">
                <!--begin::Logo-->
                <a href="#">
                  <img alt="Logo" src="{{ENV('IMG_LOGO')}}" width="200"/>
                </a>
                <!--end::Logo-->

              </div>
              <!--end::Top-->
              <!--begin::Wrapper-->
              <div class="m-0">
                <!--begin::Label-->
                <div class="fw-bolder fs-3 text-gray-800 mb-8">RECONSILE {{date('F-Y',strtotime('- 1 month'))}}</div>
                <!--end::Label-->
                <!--begin::Row-->
                <div class="row g-5 mb-11">
                  <!--end::Col-->
                  <div class="col-sm-6">
                    <!--end::Label-->
                    <div class="fw-bold fs-7 text-gray-600 mb-1">Issued Date:</div>
                    <!--end::Label-->
                    <!--end::Col-->
                    <div class="fw-bolder fs-6 text-gray-800">{{date('01-F-Y 14:23:01')}} WIB</div>
                    <!--end::Col-->
                  </div>
                  <!--end::Col-->
                  <!--end::Col-->
                  <div class="col-sm-6">
                    <!--end::Label-->
                    <div class="fw-bold fs-7 text-gray-600 mb-1">Cut Off Date:</div>
                    <!--end::Label-->
                    <!--end::Info-->
                    <div class="fw-bolder fs-6 text-gray-800 d-flex align-items-center flex-wrap">
                      <span class="pe-2">{{date('01-F-Y 05:15:00')}} WIB</span>

                      </div>
                      <!--end::Info-->
                    </div>
                    <!--end::Col-->
                  </div>
                  <!--end::Row-->
                  <!--begin::Row-->
                  <div class="row g-5 mb-12">
                    <!--end::Col-->
                    <div class="col-sm-6">
                      <!--end::Label-->
                      <div class="fw-bold fs-7 text-gray-600 mb-1">Issued For:</div>
                      <!--end::Label-->
                      <!--end::Text-->
                      <div class="fw-bolder fs-6 text-gray-800">{{profile()->name}}</div>
                      <!--end::Text-->
                      <!--end::Description-->
                      <div class="fw-bold fs-7 text-gray-600">account type - Master IB</div>
                      <!--end::Description-->
                    </div>
                    <!--end::Col-->
                    <!--end::Col-->
                    <div class="col-sm-6">
                      <!--end::Label-->
                      <div class="fw-bold fs-7 text-gray-600 mb-1">Achievement:</div>
                      <!--end::Label-->
                      <!--end::Text-->
                      <div class="fw-bolder fs-6 text-gray-800">NMI : IDR {{number_format($netmarginLastmonth,0,',','.')}}</div>
                      <!--end::Text-->
                      <!--end::Description-->
                      <div class="fw-bold fs-7 text-gray-600">% P/L : {{$share*100}}%</div>
                      <!--end::Description-->
                    </div>
                    <!--end::Col-->
                  </div>
                  <!--end::Row-->
                  <!--begin::Content-->
                  <div class="flex-grow-1">

                      <!--begin::Table-->
                      <div class="table-responsive border-bottom mb-9">
                        <table class="table mb-3">
                          <thead>
                            <tr class="border-bottom fs-6 fw-bolder text-gray-400">
                              <th class="min-w-175px pb-2">Description</th>
                              <th class="min-w-70px text-end pb-2">Amount</th>
                              <th class="min-w-80px text-end pb-2">% Share</th>
                              <th class="min-w-100px text-end pb-2">Value Share</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="fw-bolder text-gray-700 fs-5 text-end">
                              @if ($profit > 0)
                                <td class="d-flex align-items-center pt-6"><i class="fa fa-genderless text-success fs-2 me-2"></i>Profit</td>
                              @else
                                <td class="d-flex align-items-center pt-6"><i class="fa fa-genderless text-danger fs-2 me-2"></i>Loss</td>
                              @endif

                              <td class="pt-6">IDR {{number_format($profit,0,',','.')}}</td>
                              <td class="pt-6">{{$share*100}}%</td>
                              <td class="pt-6">IDR {{number_format($profit * $share,0,',','.')}}</td>
                            </tr>
                            <tr class="fw-bolder text-gray-700 fs-5 text-end">
                              <td class="d-flex align-items-center pt-6"><i class="fa fa-genderless text-success fs-2 me-2"></i>Commission</td>
                              <td class="pt-6">IDR {{number_format($comm,0,',','.')}}</td>
                              <td class="pt-6">100%</td>
                              <td class="pt-6">IDR {{number_format($comm,0,',','.')}}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <!--end::Table-->


                    <!--begin::Container-->
                    <div class="d-flex justify-content-end">
                      <!--begin::Section-->
                      <div class="mw-300px">

                        <!--begin::Item-->
                        <div class="d-flex flex-stack">
                          <!--begin::Code-->
                          <div class="fw-bold pe-10 text-gray-600 fs-2">Total</div>
                          <!--end::Code-->
                          <!--begin::Label-->

                              <div class="text-end fw-bolder fs-2 text-gray-800">IDR {{number_format(($profit * $share) + $comm,0,',','.')}}</div>


                          <!--end::Label-->
                        </div>
                        <!--end::Item-->
                      </div>
                      <!--end::Section-->
                    </div>
                    <!--end::Container-->
                  </div>
                  <!--end::Content-->
                </div>
                <!--end::Wrapper-->
              </div>
              <!--end::Invoice 2 content-->
            </div>
            <!--end::Content-->
            <!--begin::Sidebar-->
            <div class="m-0">
              <!--begin::Invoice 2 sidebar-->
              <div class="border border-dashed border-gray-300 card-rounded h-lg-100 min-w-md-350px p-9 bg-lighten">
                <!--begin::Labels-->
                <div class="mb-8">

                    <span class="badge badge-light-success">Paid at</span> <span class="text-primary">15-October-2021</span>

                </div>

                <!--end::Labels-->
                <!--begin::Title-->
                <h6 class="mb-8 fw-boldest text-gray-600 text-hover-primary">PAYMENT DETAILS</h6>
                <!--end::Title-->
                <!--begin::Item-->
                <div class="mb-6">
                  <div class="fw-bold text-gray-600 fs-7">From Bank:</div>
                  <div class="fw-bolder text-gray-800 fs-6">BCA</div>
                </div>
                <!--end::Item-->
                <!--begin::Item-->
                <div class="mb-6">
                  <div class="fw-bold text-gray-600 fs-7">From Name:</div>
                  <div class="fw-bolder text-gray-800 fs-6">PT. Financia IB Trade</div>
                </div>
                <!--end::Item-->
                <!--begin::Item-->
                <div class="mb-15">
                  <div class="fw-bold text-gray-600 fs-7">From Account:</div>
                  <div class="fw-bolder fs-6 text-gray-800 d-flex align-items-center">1234567891</div>
                </div>
                <!--end::Item-->
                <!--begin::Title-->
                <h6 class="mb-8 fw-boldest text-gray-600 text-hover-primary">PAYMENT OVERVIEW</h6>
                <!--end::Title-->
                <!--begin::Item-->
                <div class="mb-6">
                  <div class="fw-bold text-gray-600 fs-7">To Bank:</div>
                  <div class="fw-bolder text-gray-800 fs-6">{{bank()->bank_name}}</div>
                </div>
                <!--end::Item-->
                <!--begin::Item-->
                <div class="mb-6">
                  <div class="fw-bold text-gray-600 fs-7">To Name:</div>
                  <div class="fw-bolder text-gray-800 fs-6">{{bank()->account_name}}</div>
                </div>
                <!--end::Item-->
                <!--begin::Item-->
                <div class="mb-15">
                  <div class="fw-bold text-gray-600 fs-7">To Account:</div>
                  <div class="fw-bolder fs-6 text-gray-800 d-flex align-items-center">{{bank()->account_number}}</div>
                </div>
                <!--end::Item-->
              </div>
              <!--end::Invoice 2 sidebar-->
            </div>
            <!--end::Sidebar-->
          </div>
          <!--end::Layout-->
        </div>
        <!--end::Body-->
      </div>
      <!--end::Invoice 2 main-->
    </div>
  @endsection
