@extends('template.master')

@section('title','Add New Account Type')
@section('metadescription','Invest to rich')
@section('metakeyword','Invest to rich')
@section('bc-1','Account Management')
@section('bc-2','Add New Account Type')

@section('container')
  <div id="kt_content_container" class="container">
      <div class="row">
        <form method="POST" action="{{route('storeNewAccountType')}}" class="form" enctype="multipart/form-data">
          <div class="col-md-12">
            <!--begin::Basic info-->
            <div class="card mb-5 mb-xl-10">
              <!--begin::Card header-->
              <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
                <!--begin::Card title-->
                <div class="card-title m-0">
                  <h3 class="fw-bolder m-0">Add New Master Account Type</h3>
                </div>
                <!--end::Card title-->
              </div>
              <!--begin::Card header-->
              <!--begin::Content-->
              <div class="card-body border-top p-9">
                <div class="row">
                  <div class="col-md-6">

                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Master Account Name</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">
                        <input class="form-control form-control-lg form-control-solid {{ old('masterName') && !$errors->has('masterName') ? 'input-valid' : '' }} {{$errors->has('masterName') ? 'input-error' : ''}}" type="text" placeholder="" name="masterName" autocomplete="off" value="{{ old('masterName') ? old('masterName') : '' }}" required {{$errors->has('masterName') ? 'autofocus' : ''}}/>
                        @if ($errors->has('masterName'))
                            <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('masterName') }}</h4>
                        @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Master Rate</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">
                          <select class="form-select form-select-solid" name="masterRate" required>
                              <option value="">Available Rate</option>
                              {{-- <option value="5K" {{ old('masterRate') == '5K' ? 'selected' : '' }}>5000 (5K)/US$</option> --}}
                              <option value="10K" {{ old('masterRate') == '10K' ? 'selected' : '' }}>10000 (10K)/US$</option>
                              {{-- <option value="11K" {{ old('masterRate') == '11K' ? 'selected' : '' }}>11000 (11K)/US$</option>
                              <option value="12K" {{ old('masterRate') == '12K' ? 'selected' : '' }}>12000 (12K)/US$</option>
                              <option value="13K" {{ old('masterRate') == '13K' ? 'selected' : '' }}>13000 (13K)/US$</option>
                              <option value="14K" {{ old('masterRate') == '14K' ? 'selected' : '' }}>14000 (14K)/US$</option> --}}
                              <option value="15K" {{ old('masterRate') == '15K' ? 'selected' : '' }}>15000 (15K)/US$</option>
                              <option value="FL" {{ old('masterRate') == 'FL' ? 'selected' : '' }}>Floating/US$</option>
                          </select>
                          @if ($errors->has('masterRate'))
                              <h4 class="text-danger mt-6"> <i
                                      class="bi bi-exclamation-triangle text-danger fs-2 blink"></i>
                                  {{ $errors->first('masterRate') }}</h4>
                          @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Master Account Group</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">

                        <div class="input-group">
                          <span class="input-group-text" style="font-size:1.5em!important;border:0px!important">
                              XXX - YYY -
                          </span>
                          <input class="form-control form-control-lg form-control-solid {{ old('masterGroup') && !$errors->has('masterGroup') ? 'input-valid' : '' }} {{$errors->has('masterGroup') ? 'input-error' : ''}}" type="text" placeholder="" name="masterGroup" autocomplete="off" value="{{ old('masterGroup') ? old('masterGroup') : '' }}" required {{$errors->has('masterGroup') ? 'autofocus' : ''}}/>
                        </div>
                        <div class="text-muted">XXX : Prefix for Partner Initial</div>
                        <div class="text-muted">YYY : Prefix for rate</div>
                        <div class="text-muted">Example : GON-10K-GL</div>
                          @if ($errors->has('masterGroup'))
                              <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('masterGroup') }}</h4>
                          @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Master Start Number</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">
                        <div class="input-group">

                          <input class="form-control form-control-lg form-control-solid {{ old('masterNumber') && !$errors->has('masterNumber') ? 'input-valid' : '' }} {{$errors->has('masterNumber') ? 'input-error' : ''}}" type="number" min="0" step="1" max="9999" placeholder="" name="masterNumber" autocomplete="off" value="{{ old('masterNumber') ? old('masterNumber') : $next + 1 }}" required {{$errors->has('masterNumber') ? 'autofocus' : ''}}/>
                          <span class="input-group-text" style="font-size:1.5em!important;border:0px!important">
                              00001
                          </span>
                        </div>
                        <div class="text-muted">All Account under type will be start by this number, auto increment</div>
                        <div class="text-muted">Example : 200001</div>
                          @if ($errors->has('masterNumber'))
                              <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('masterNumber') }}</h4>
                          @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                  </div>

                  <div class="col-md-6">


                  </div>

                </div>


                @csrf
                <!--begin::Actions-->
                <div class="card-footer d-flex justify-content-end py-6 px-9">

                  <button type="submit" class="btn btn-primary" id="ot_button">Submit</button>
                </div>
                <!--end::Actions-->
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
@endsection
