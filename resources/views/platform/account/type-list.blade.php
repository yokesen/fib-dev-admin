@extends('template.master')

@section('title','Account Type List')
@section('metadescription','Invest to rich')
@section('metakeyword','Invest to rich')
@section('bc-1','Account Management')
@section('bc-2','Account Type List')

@section('container')
  <div id="kt_content_container" class="container">
      <div class="row">
        <div class="col-md-12">
          <!--begin::Tables Widget 9-->
          <div class="card card-xxl-stretch mb-5 mb-xl-8">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
              <div class="card-toolbar">
                <a href="{{route('viewAccountAddNew')}}" class="btn btn-sm btn-light-primary">
                <i class="fas fa-user-alt"></i> Add New Account Type</a>
              </div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-3">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="fw-bolder text-muted">
                      <th>Created Date</th>
                      <th>Master Name</th>
                      <th>Master Group</th>
                      <th>Rate</th>
                      <th>Start Number</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody id="table-body">
                    @foreach ($masterType as $x => $type)
                      <tr>
                        <td>
                          <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$type->masterCreated_at}}</a>
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{ \Carbon\Carbon::parse(strtotime($type->masterCreated_at.'+7hours'))->diffForHumans()}}</span>
                        </td>
                        <td>
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{$type->masterName}}</span>
                        </td>
                        <td>
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{$type->masterGroup}}</span>
                        </td>
                        <td>
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{$type->masterRate}}</span>
                        </td>
                        <td>
                          <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$type->masterNumber}}00001</a>
                        </td>
                        <td>
                          <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$type->masterStatus}}</a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                  <!--end::Table body-->
                </table>
                <!--end::Table-->
                {{$masterType->links()}}
              </div>
              <!--end::Table container-->
            </div>
            <!--begin::Body-->
          </div>
          <!--end::Tables Widget 9-->
        </div>
      </div>

    </div>
@endsection
