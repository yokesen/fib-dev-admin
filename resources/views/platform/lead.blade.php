@extends('template.master')

@section('title','Lead')
@section('metadescription','FIB Partner the best business partner for IB')
@section('metakeyword','FIB Partner')
@section('bc-1','My Client')
@section('bc-2','Lead')

@section('container')

  <div id="kt_content_container" class="container">
      <div class="row">
        <div class="col-md-12">
          <!--begin::Tables Widget 9-->
          <div class="card card-xxl-stretch mb-5 mb-xl-8">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
              <div class="card-toolbar">
                @if (profile()->allowOperate == 'yes')
                  <a href="{{env('IMS_ACTIVE')}}/register?ref={{profile()->id}}&utm_source=manual-lead&utm_campaign=no-campaign&utm_medium=web&utm_content=ib-platform" class="btn btn-sm btn-light-primary" target="_blank">
                @else
                  <a href="{{route('createUser')}}" class="btn btn-sm btn-light-primary" >
                @endif

  							<i class="fas fa-user-alt"></i> Add New User</a>
  						</div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-3">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="fw-bolder text-muted">
                      <th>Created Date</th>
                      <th>Nama</th>
                      <th>Komunikasi</th>
                      <th>Provider</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody id="table-body">
                    @foreach ($users as $x => $value)
                      @php
                        $role = DB::table('cms_privileges')->where('id',$value->id_cms_privileges)->first();
                        $parent = DB::table('users_cabinet')->where('id',$value->parent)->select('username','uuid')->first();
                      @endphp
                      <tr>
                        <td>
                          <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->created_at}}</a>
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{ \Carbon\Carbon::parse(strtotime($value->created_at.'+7hours'))->diffForHumans()}}</span>
                        </td>
                        <td>
                          <div class="d-flex align-items-center">
                            <div class="symbol symbol-45px me-5">
                              <img src="{{$value->photo}}" alt="" />
                            </div>
                            <div class="d-flex justify-content-start flex-column">
                              <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary fs-6">{{$value->name}}</a>
                              <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->username}}</span>
                            </div>
                          </div>
                        </td>
                        <td>
                          <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->email}}</a>
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->phone}}</span>
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->whatsapp}}</span>
                        </td>
                        <td>
                          @if ($parent)
                            <a href="{{route('showUser',$parent->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$parent->username}}</a>
                          @endif
                          <span class="text-muted fw-bold text-muted d-block fs-7">{{$value->providerOrigin}}</span>
                        </td>
                        <td>
                          <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$role->name}}</a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                  <!--end::Table body-->
                </table>
                <!--end::Table-->
                {{$users->links()}}
              </div>
              <!--end::Table container-->
            </div>
            <!--begin::Body-->
          </div>
          <!--end::Tables Widget 9-->
        </div>
      </div>

    </div>

@endsection
