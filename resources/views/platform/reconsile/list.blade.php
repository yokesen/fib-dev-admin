@extends('template.master')

@section('title','Balance Sheet')
@section('bc-1','Balance Sheet')
@if ($last)
  @section('bc-2',$balanceWeek." - ".$last->year)

@endif

@section('container')
  <div id="kt_content_container" class="container">
      <div class="row">
        <div class="col-md-12">
          <!--begin::Tables Widget 9-->
          <div class="card card-xxl-stretch mb-5 mb-xl-8">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
              <div class="card-title">
                <h1>BALANCE SHEET</h1>
  						</div>
              {{-- @if ($last)
                <div class="card-title">
                  WEEK : <a href="{{route('last-statement',[$lastWeek,$last->year])}}" class="btn btn-primary mx-1"><<</a> <a href="#" class="btn btn-secondary mx-1">{{$balanceWeek}}</a>
                  @if ($nextweek > 0)
                    <a href="{{route('last-statement',[$nextweek,$last->year])}}" class="btn btn-primary mx-1">>></a>
                  @endif


                </div>
              @endif --}}

            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-3">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="fw-bolder text-muted">
                      <th>Tanggal</th>
                      <th>Trx Type</th>
                      <th>Description</th>
                      <th class="text-end">Amount</th>
                      <th class="text-end">Type</th>
                      <th class="text-end">Waranty</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody id="table-body">
                    {{-- @if ($last)
                      <tr>
                        <td colspan="6" class="text-end">
                          <a href="{{route('last-statement',[$lastWeek,$last->year])}}" class="text-dark fw-bolder text-hover-primary d-block fs-6"> Saldo Awal</a>

                        </td>

                        <td>
                          <a href="{{route('last-statement',[$balanceWeek-1,$last->year])}}" class="text-dark fw-bolder text-hover-primary d-block fs-6 text-end">{{number_format($last->saldo_global,'0','.','.')}}</a>
                        </td>
                      </tr>
                    @endif --}}

                    @foreach ($list as $x => $value)
                      <tr>
                        <td>
                          <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->tanggal}}</a>

                        </td>
                        <td>
                          <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->tipe_transaksi}}</a>
                        </td>
                        <td>
                          <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->keterangan}}</a>
                        </td>
                        <td>
                          <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6 text-end">{{number_format($value->amount_partner,'0','.','.')}}</a>
                        </td>
                        <td>
                          <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6 text-end">{{$value->tipe_mutasi}}</a>
                        </td>
                        <td>
                          <a href="#" class="text-dark fw-bolder text-hover-primary d-block fs-6 text-end">{{number_format($value->saldo,'0','.','.')}}</a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                  <!--end::Table body-->
                </table>
                <!--end::Table-->

              </div>
              <!--end::Table container-->
            </div>
            <!--begin::Body-->
          </div>
          <!--end::Tables Widget 9-->
        </div>
      </div>

    </div>
@endsection

@section('jsonpage')

@endsection
