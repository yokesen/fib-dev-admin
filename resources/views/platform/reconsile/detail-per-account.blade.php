@extends('template.master')

@section('title','Reconsile')
@section('bc-1','Reconsile')
@section('bc-2','Cocokan')

@section('cssonpage')
  <style media="screen">
    .supsub {position: absolute}
    .subscript {color: grey; display:block; position:relative; left:2px; bottom: -20px}
    .superscript {color: grey; display:block; position:relative; left:2px; top: 5px}
  </style>
@endsection

@section('container')
  <div id="kt_content_container" class="container">
      <div class="row">
        <div class="col-md-12">
          <!--begin::Tables Widget 9-->
          <div class="card card-xxl-stretch mb-5 mb-xl-8">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
              <div class="card-title">
                <h1>Reconsile {{$dataDetail[0]->dateStart}} - {{$dataDetail[0]->dateEnd}}</h1>
  						</div>

              <div class="card-title">

              </div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-3">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="fw-bolder text-muted">
                      <th>Account MT5</th>
                      <th class="text-end">Company P/L</th>
                      <th class="text-end">Senior P/L</th>
                      <th class="text-end">Master P/L</th>
                      <th class="text-end">IB P/L</th>
                      <th class="text-end">Total P/L</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody id="table-body">
                    @foreach ($dataDetail as $x => $value)
                      @php
                        $sib = DB::table('users_ib')->where('id',$value->sib)->first();
                        $mib = DB::table('users_ib')->where('id',$value->mib)->first();
                        $ib = DB::table('users_ib')->where('id',$value->ib)->first();
                      @endphp
                      <tr>
                        <td>
                          <a href="{{route('statementPerAccount',$value->hashCalc)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->accountId}}</a>
                        </td>
                        <td class="text-end">
                          <a href="{{route('statementPerAccount',$value->hashCalc)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{number_format($value->calcComp,'0','.','.')}}
                            <span class='supsub'>
                                <sup class='superscript'><small>({{$value->takeComp}}%)</small></sup>
                                <sub class='subscript'>Company</sub>
                            </span>
                          </a>
                        </td>
                        <td class="text-end">
                          <a href="{{route('statementPerAccount',$value->hashCalc)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{number_format($value->calcSib,'0','.','.')}}
                            <span class='supsub'>
                                <sup class='superscript'><small>({{$value->takeSib}}%)</small></sup>
                                <sub class='subscript'>{{$sib->username}}</sub>
                            </span>
                          </a>
                        </td>
                        <td class="text-end">
                          <a href="{{route('statementPerAccount',$value->hashCalc)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6 text-end">{{number_format($value->calcMib,'0','.','.')}}
                            <span class='supsub'>
                                <sup class='superscript'><small>({{$value->takeMib}}%)</small></sup>
                                <sub class='subscript'>{{$mib->username}}</sub>
                            </span>
                          </a>
                        </td>
                        <td class="text-end">
                          <a href="{{route('statementPerAccount',$value->hashCalc)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6 text-end">{{number_format($value->calcIb,'0','.','.')}}
                            <span class='supsub'>
                                <sup class='superscript'><small>({{$value->takeIb}}%)</small></sup>
                                <sub class='subscript'>{{$ib->username}}</sub>
                            </span>
                          </a>
                        </td>
                        <td class="text-end">
                          <a href="{{route('statementPerAccount',$value->hashCalc)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6 text-end">{{number_format($value->totalCalc,'0','.','.')}}</a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                  <!--end::Table body-->
                </table>
                <!--end::Table-->

              </div>
              <!--end::Table container-->
            </div>
            <!--begin::Body-->
          </div>
          <!--end::Tables Widget 9-->
        </div>
      </div>

    </div>
@endsection

@section('jsonpage')

@endsection
