@extends('template.master')

@section('title','Reconsile')
@section('metadescription','Invest to rich')
@section('metakeyword','Invest to rich')
@section('bc-1','Reconsile Management')
@section('bc-2','Payment')

@section('container')
  <div id="kt_content_container" class="container">
    <div class="card rounded ">
      <!--begin::Body-->
      <div class="card-body p-lg-20">
        <!--begin::Layout-->
        <div class="d-flex flex-column flex-xl-row">
          <!--begin::Content-->
          <div class="flex-lg-row-fluid me-xl-18 mb-10 mb-xl-0">
            <!--begin::Invoice 2 content-->
            <div class="mt-n1">
              <!--begin::Top-->
              <div class="d-flex flex-stack pb-10">
                <!--begin::Logo-->
                <a href="#">
                  <img alt="Logo" src="{{ENV('IMG_LOGO')}}" width="200"/>
                </a>
                <!--end::Logo-->

              </div>
              <!--end::Top-->
              <!--begin::Wrapper-->
              <div class="m-0">
                <!--begin::Label-->
                <div class="fw-bolder fs-3 text-gray-800 mb-8">RECONSILE {{ strtoupper($reconsile->typeReconsile) }}</div>
                <!--end::Label-->
                <!--begin::Row-->
                <div class="row g-5 mb-11">
                  <!--end::Col-->
                  <div class="col-sm-6">
                    <!--end::Label-->
                    <div class="fw-bold fs-7 text-gray-600 mb-1">Issued Date:</div>
                    <!--end::Label-->
                    <!--end::Col-->
                    <div class="fw-bolder fs-6 text-gray-800">{{ $reconsile->created_at }} WIB</div>
                    <!--end::Col-->
                  </div>
                  <!--end::Col-->
                  <!--end::Col-->
                  <div class="col-sm-6">
                    <!--end::Label-->
                    <div class="fw-bold fs-7 text-gray-600 mb-1">Cut Off Date:</div>
                    <!--end::Label-->
                    <!--end::Info-->
                    <div class="fw-bolder fs-6 text-gray-800 d-flex align-items-center flex-wrap">
                      <span class="pe-2">{{ $reconsile->dateReconsile }} WIB</span>

                      </div>
                      <!--end::Info-->
                    </div>
                    <!--end::Col-->
                  </div>
                  <!--end::Row-->
                  <!--begin::Row-->
                  <div class="row g-5 mb-12">
                    <!--end::Col-->
                    <div class="col-sm-6">
                      <!--end::Label-->
                      <div class="fw-bold fs-7 text-gray-600 mb-1">Issued For:</div>
                      <!--end::Label-->
                      <!--end::Text-->
                      <div class="fw-bolder fs-6 text-gray-800">{{$reconsile->name}}</div>
                      <!--end::Text-->
                      <!--end::Description-->
                      <div class="fw-bold fs-7 text-gray-600">account type - IB</div>
                      <!--end::Description-->
                    </div>
                    <!--end::Col-->
                    <!--end::Col-->
                    <div class="col-sm-6">
                      <!--end::Label-->
                      <div class="fw-bold fs-7 text-gray-600 mb-1">Achievement:</div>
                      <!--end::Label-->
                      <!--end::Text-->
                      <div class="fw-bolder fs-6 text-gray-800">NMI : IDR {{number_format($reconsile->lastKnownDeposit*10000,2,',','.')}}</div>
                      <!--end::Text-->
                      <!--end::Description-->
                      <div class="fw-bold fs-7 text-gray-600">% P/L : {{$reconsile->takingPositionPercentage}}%</div>
                      <!--end::Description-->
                    </div>
                    <!--end::Col-->
                  </div>
                  <!--end::Row-->
                  <!--begin::Content-->
                  <div class="flex-grow-1">

                      <!--begin::Table-->
                      <div class="table-responsive border-bottom mb-9">
                        <table class="table mb-3">
                          <thead>
                            <tr class="border-bottom fs-6 fw-bolder text-gray-400">
                              <th class="min-w-175px pb-2">Description</th>
                              <th class="min-w-70px text-end pb-2">Amount</th>
                              <th class="min-w-80px text-end pb-2">% Taking Position</th>
                              <th class="min-w-100px text-end pb-2">Value Share</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="fw-bolder text-gray-700 fs-5 text-end">
                              @if ($reconsile->amount < 0)
                                <td class="d-flex align-items-center pt-6"><i class="fa fa-genderless text-success fs-2 me-2"></i>Profit</td>
                              @else
                                <td class="d-flex align-items-center pt-6"><i class="fa fa-genderless text-danger fs-2 me-2"></i>Loss</td>
                              @endif

                              <td class="pt-6">IDR {{ number_format($reconsile->amount * 10000,'2',',','.') }}</td>
                              <td class="pt-6">{{$reconsile->takingPositionPercentage}}%</td>
                              <td class="pt-6">IDR {{ number_format($reconsile->amount * 10000 * $reconsile->takingPositionPercentage / 100,'2',',','.') }}</td>
                            </tr>
                            <tr class="fw-bolder text-gray-700 fs-5 text-end">
                              <td class="d-flex align-items-center pt-6"><i class="fa fa-genderless text-success fs-2 me-2"></i>Commission</td>
                              <td class="pt-6">IDR 0</td>
                              <td class="pt-6">100%</td>
                              <td class="pt-6">IDR 0</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <!--end::Table-->


                    <!--begin::Container-->
                    <div class="d-flex justify-content-end">
                      <!--begin::Section-->
                      <div class="mw-300px">

                        <!--begin::Item-->
                        <div class="d-flex flex-stack">
                          <!--begin::Code-->
                          <div class="fw-bold pe-10 text-gray-600 fs-2">Total</div>
                          <!--end::Code-->
                          <!--begin::Label-->

                              <div class="text-end fw-bolder fs-2 text-gray-800">IDR {{ number_format($reconsile->amount * 10000 * $reconsile->takingPositionPercentage / 100,'2',',','.') }}</div>


                          <!--end::Label-->
                        </div>
                        <!--end::Item-->
                      </div>
                      <!--end::Section-->
                    </div>
                    <!--end::Container-->
                  </div>
                  <!--end::Content-->
                </div>
                <!--end::Wrapper-->
              </div>
              <!--end::Invoice 2 content-->
            </div>
            <!--end::Content-->
            <!--begin::Sidebar-->
            <div class="m-0">
              <!--begin::Invoice 2 sidebar-->
              <div class="border border-dashed border-gray-300 card-rounded h-lg-100 min-w-md-350px p-9 bg-lighten">
                <!--begin::Labels-->
                <div class="mb-8">

                    <span class="badge badge-light-success">Paid at</span> <span class="text-primary"></span>

                </div>

                <!--end::Labels-->
                <!--begin::Title-->
                <h6 class="mb-8 fw-boldest text-gray-600 text-hover-primary">PAYMENT DETAILS</h6>
                <!--end::Title-->
                <!--begin::Item-->
                <div class="mb-6">
                  <div class="fw-bold text-gray-600 fs-7">From Bank:</div>
                  <div class="fw-bolder text-gray-800 fs-6">BCA</div>
                </div>
                <!--end::Item-->
                <!--begin::Item-->
                <div class="mb-6">
                  <div class="fw-bold text-gray-600 fs-7">From Name:</div>
                  <div class="fw-bolder text-gray-800 fs-6">PT. </div>
                </div>
                <!--end::Item-->
                <!--begin::Item-->
                <div class="mb-15">
                  <div class="fw-bold text-gray-600 fs-7">From Account:</div>
                  <div class="fw-bolder fs-6 text-gray-800 d-flex align-items-center">1234567891</div>
                </div>
                <!--end::Item-->
                <!--begin::Title-->
                <h6 class="mb-8 fw-boldest text-gray-600 text-hover-primary">PAYMENT OVERVIEW</h6>
                <!--end::Title-->
                <!--begin::Item-->
                <div class="mb-6">
                  <div class="fw-bold text-gray-600 fs-7">To Bank:</div>
                  <div class="fw-bolder text-gray-800 fs-6">Bank Client</div>
                </div>
                <!--end::Item-->
                <!--begin::Item-->
                <div class="mb-6">
                  <div class="fw-bold text-gray-600 fs-7">To Name:</div>
                  <div class="fw-bolder text-gray-800 fs-6">{{$reconsile->name}}</div>
                </div>
                <!--end::Item-->
                <!--begin::Item-->
                <div class="mb-15">
                  <div class="fw-bold text-gray-600 fs-7">To Account:</div>
                  <div class="fw-bolder fs-6 text-gray-800 d-flex align-items-center">0</div>
                </div>
                <!--end::Item-->
              </div>
              <!--end::Invoice 2 sidebar-->
            </div>
            <!--end::Sidebar-->
          </div>
          <!--end::Layout-->
        </div>
        <!--end::Body-->
      </div>
      <!--end::Invoice 2 main-->

      @if ($reconsile->statusReconsile != 'paid')
        <div class="row mt-10">
          <form method="POST" action="{{route('processReconsileAction')}}" class="form" enctype="multipart/form-data">
            <div class="col-md-12">
              <!--begin::Basic info-->
              <div class="card mb-5 mb-xl-10">
                <!--begin::Card header-->
                <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
                  <!--begin::Card title-->
                  <div class="card-title m-0">
                    <h3 class="fw-bolder m-0">Reconsile </h3>
                  </div>
                  <!--end::Card title-->
                </div>
                <!--begin::Card header-->
                <!--begin::Content-->
                <div class="card-body border-top p-9">
                  <div class="row">
                    <div class="col-md-6">

                      <!--begin::Input group-->
                      <div class="row mb-6">
                        <!--begin::Label-->
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-8 fv-row">
                          <input class="form-control form-control-lg form-control-solid" type="text" value="{{$reconsile->name}}" disabled/>
                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group-->
                      <!--begin::Input group-->
                      <div class="row mb-6">
                        <!--begin::Label-->
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">Type Reconsile</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-8 fv-row">

                            <input class="form-control form-control-lg form-control-solid " type="text" value="{{ $reconsile->typeReconsile }}" disabled/>

                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group-->
                      <!--begin::Input group-->
                      <div class="row mb-6">
                        <!--begin::Label-->
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">Date Reconsile</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-8 fv-row">

                            <input class="form-control form-control-lg form-control-solid " type="text" value="{{ $reconsile->dateReconsile }}" disabled/>

                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group-->
                      <!--begin::Input group-->
                      <div class="row mb-6">
                        <!--begin::Label-->
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">Amount</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-8 fv-row">

                            <input class="form-control form-control-lg form-control-solid " type="text" value="Rp {{ number_format($reconsile->amount * 10000 * $reconsile->takingPositionPercentage / 100) }}" disabled/>
                            <!--begin::Hint-->
                            @if ($reconsile->amount < 0)
                              <div class="text-success">PT bayar ke {{$reconsile->name}}</div>
                            @elseif($reconsile->amount > 0)
                              <div class="text-danger">{{$reconsile->name}} bayar ke PT</div>
                            @endif
                            <!--end::Hint-->
                        </div>
                        <!--end::Col-->
                      </div>
                      <!--end::Input group-->
                    </div>

                    <div class="col-md-6">
                      <div class="row mb-6">
                        <!--begin::Label-->
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">Action</label>
                        <!--end::Label-->
                        <div class="col-lg-8 fv-row">
                          <select class="form-select" name="statusReconsile" required>
                              <option value="">---------Action---------</option>
                              <option value="paid">Paid</option>
                              <option value="pending">Pending</option>
                              <option value="overdue">Overdue</option>
                          </select>
                        </div>
                      </div>

                          <!--begin::Input group-->
                          <div class="fv-row mt-20">
                              <!--begin::Dropzone-->
                              <div class="dropzone" id="ot_bukti_transfer">
                                  <!--begin::Message-->
                                  <div class="dz-message needsclick">
                                      <!--begin::Icon-->
                                      <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                                      <!--end::Icon-->

                                      <!--begin::Info-->
                                      <div class="ms-4">
                                          <h3 class="fs-5 fw-bolder text-gray-900 mb-1">Klik di sini untuk upload bukti
                                              transfer.</h3>
                                          <span class="fs-7 fw-bold text-gray-400">Foto struk atm atau screenshot
                                              ibanking</span>
                                      </div>
                                      <!--end::Info-->
                                  </div>
                              </div>
                              <!--end::Dropzone-->
                          </div>
                          <!--end::Input group-->
                          <div class="form-text">Format yang bisa diupload: png, jpg, jpeg.</div>
                          <div class="form-text">Maksimal 5Mb.</div>

                    </div>

                  </div>

                  @csrf
                  <!--begin::Actions-->
                  <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <input type="hidden" name="id" value="{{$reconsile->id}}">
                    <button type="submit" class="btn btn-primary" id="ot_button">Submit</button>
                  </div>
                  <!--end::Actions-->
                </div>
              </div>
            </div>
          </form>
        </div>
      @endif


    </div>
@endsection

@section('jsinline')

  </script>
@endsection
