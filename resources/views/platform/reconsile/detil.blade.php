@extends('template.master')

@section('title','Reconsile')
@section('bc-1','Reconsile')
@section('bc-2','Statement')

@section('container')
  <div id="kt_content_container" class="container">
      <div class="row">
        <div class="col-md-12">
          <!--begin::Tables Widget 9-->
          <div class="card card-xxl-stretch mb-5 mb-xl-8">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
              <div class="card-title">
                <h1>STATEMENT</h1>
  						</div>

              <div class="card-title">

              </div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-3">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr>
                      <th class="min-w-100px">Account Trading</th>
                      <th>Time</th>
                      <th>Ticket</th>
                      <th>Symbol</th>
                      <th>Type</th>
                      <th class="text-end">Price Open</th>
                      <th class="text-end">Volume Open</th>
                      <th class="text-end">Price</th>
                      <th class="text-end">Swap</th>
                      <th class="text-end">Volume Closed</th>
                      <th class="text-end">Closed</th>
                      <th class="text-end">Floating</th>
                      <th class="text-end">Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php
                      $lot = 0;
                      $swap = 0;
                      $adjust = 0;
                      $plclosed = 0;
                      $plfloating = 0;
                      $margin = 0;
                    @endphp
                    @foreach ($weeklyBalanceStatements as $w)
                      @php
                      if ($w->Profit < 0) {
                        $warna = 'success';
                        $icon = 'fa-check-circle';
                      }elseif($w->Profit > 0){
                        $warna = 'danger';
                        $icon = 'fa-times-circle';
                      }else{
                        $warna = 'primary';
                        $icon = 'fa-clock';
                      }
                      $comment = strtolower($w->Comment);
                      if (str_contains($comment,'adj')) {
                        if (str_contains($comment,'ollover')) {
                          //$swap += $w->Profit;
                        }else{
                          $adjust += $w->Profit;
                        }

                      }

                      @endphp
                      <!--begin::Table row-->
                      <tr {{$w->ModifyFlags == '1' ? 'class=table-danger' : ''}}>
                        <!--begin::Invoice=-->
                        <td>
                          {{$w->Login}}
                        </td>
                        <td>
                          {{$w->humanTime}}
                        </td>
                        <td colspan="10" class="text-end">
                          {{$w->Comment}}
                        </td>
                        <!--begin::Action=-->
                        <td class="text-end">{{number_format($w->Profit,'2','.',',')}}</td>
                        <!--end::Action=-->
                      </tr>
                      <!--end::Table row-->
                    @endforeach
                    @foreach ($weeklyClosedStatements as $w)
                      @php
                      if ($w->Profit < 0) {
                        $warna = 'success';
                        $icon = 'fa-check-circle';
                      }elseif($w->Profit > 0){
                        $warna = 'danger';
                        $icon = 'fa-times-circle';
                      }else{
                        $warna = 'primary';
                        $icon = 'fa-clock';
                      }

                      $lot += $w->Volume;
                      $swap += $w->Storage;
                      $plclosed += $w->Profit;
                      @endphp
                      <!--begin::Table row-->
                      <tr {{$w->ModifyFlags == '1' ? 'class=table-danger' : ''}}>
                        <!--begin::Invoice=-->
                        <td>
                          {{$w->Login}}
                        </td>
                        <td>
                          {{$w->humanTime}}
                        </td>
                        <td>
                          {{$w->PositionID}}
                        </td>
                        <td>
                          {{$w->Symbol}}
                        </td>
                        <td>
                          {{$w->Action == 0 ? 'BUY' : 'SELL'}}
                        </td>
                        <td class="text-end">
                          {{$w->Price}}
                        </td>
                        <td class="text-end">
                          {{$w->Volume != 0 ? number_format($w->Volume/10000,'2','.',',') : $w->Volume}}
                        </td>
                        <td class="text-end">
                          {{$w->PricePosition}}
                        </td>
                        <td class="text-end">
                          {{$w->Storage}}
                        </td>
                        <td class="text-end">
                          {{$w->VolumeClosed != 0 ? number_format($w->VolumeClosed/10000,'2','.',',') : $w->VolumeClosed}}
                        </td>
                        <!--end::Invoice=-->
                        <!--begin::Status=-->
                        <td class="text-{{$warna}} text-end">
                          {{$w->Profit != 0 ? number_format($w->Profit,'2','.',',') : $w->Profit}}
                        </td>
                        <!--end::Status=-->
                        <!--begin::Amount=-->
                        <td class="text-end">
                          0
                        </td>
                        <!--end::Amount=-->
                        <!--begin::Date=-->

                        <!--end::Date=-->
                        <!--begin::Action=-->
                        <td class="text-end">{{number_format($w->Profit,'2','.',',')}}</td>
                        <!--end::Action=-->
                      </tr>
                      <!--end::Table row-->
                    @endforeach
                    @foreach ($weeklyFloatingStatements as $w)
                      @php
                        if ($w->Profit < 0) {
                          $warna = 'success';
                          $icon = 'fa-check-circle';
                        }elseif($w->Profit > 0){
                          $warna = 'danger';
                          $icon = 'fa-times-circle';
                        }else{
                          $warna = 'primary';
                          $icon = 'fa-clock';
                        }

                        $lot += $w->Volume;
                        $plfloating += $w->Profit;
                        $datelot = $w->lotdate - 1;
                        $storage = $datelot;
                        $Countswap = DB::table('raw_data_position')->where('Position',$w->Position)->where('lotdate',$datelot)->count();
                        if ($Countswap > 0) {
                          $stor = DB::table('raw_data_position')->where('Position',$w->Position)->where('lotdate',$datelot)->first();
                          $storage = $stor->Storage;
                          $plfloating += $storage;
                        }else{
                          $storage = 0;
                        }

                        // if ($swap) {
                        //   echo $swap->id;
                        // }else{
                        //   echo "anu";
                        // }
                        //echo $swap." | ";
                        //dd("okay");
                      @endphp
                      <!--begin::Table row-->
                      <tr {{$w->ModifyFlags == '1' ? 'class=table-danger' : ''}}>
                        <!--begin::Invoice=-->
                        <td>
                          {{$w->Login}}
                        </td>
                        <td>
                          {{$w->humanTime}}
                        </td>
                        <td>
                          {{$w->Position}}
                        </td>
                        <td>
                          {{$w->Symbol}}
                        </td>
                        <td>
                          {{$w->Action == 0 ? 'BUY' : 'SELL'}}
                        </td>
                        <td class="text-end">
                          {{$w->PriceOpen}}
                        </td>
                        <td class="text-end">
                          {{$w->Volume != 0 ? number_format($w->Volume/10000,'2','.',',') : $w->Volume}}
                        </td>
                        <td class="text-end">
                          {{$w->PriceCurrent}}
                        </td>
                        <td class="text-end">
                          {{$storage}}
                        </td>
                        <td class="text-end">
                          0
                        </td>
                        <!--end::Invoice=-->
                        <!--begin::Status=-->
                        <td class="text-{{$warna}} text-end">
                          0
                        </td>
                        <!--end::Status=-->
                        <!--begin::Amount=-->
                        <td class="text-end">
                          {{$w->Profit != 0 ? number_format($w->Profit,'2','.',',') : $w->Profit}}
                        </td>
                        <!--end::Amount=-->
                        <!--begin::Date=-->

                        <!--end::Date=-->
                        <!--begin::Action=-->
                        <td class="text-end">{{number_format($w->Profit,'2','.',',')}}</td>
                        <!--end::Action=-->
                      </tr>
                      <!--end::Table row-->
                    @endforeach

                  </tbody>
                  <!--end::Table body-->
                </table>
                <!--end::Table-->

              </div>
              <!--end::Table container-->
            </div>
            <!--begin::Body-->
          </div>
          <!--end::Tables Widget 9-->
        </div>
      </div>

    </div>
@endsection

@section('jsonpage')

@endsection
