@extends('template.master')

@section('title','Reconsile')
@section('bc-1','Reconsile')
@section('bc-2','Cocokan')

@section('container')
  <div id="kt_content_container" class="container">
    <div class="card rounded ">
      <!--begin::Body-->
      <div class="card-body p-lg-20">
        <!--begin::Layout-->
        <div class="d-flex flex-column flex-xl-row">
          <!--begin::Content-->
          <div class="flex-lg-row-fluid me-xl-18 mb-10 mb-xl-0">
            <!--begin::Invoice 2 content-->
            <div class="mt-n1">
              <!--begin::Top-->
              <div class="d-flex flex-stack pb-10">
                <!--begin::Logo-->
                <a href="#">
                  <img alt="Logo" src="{{ENV('IMG_LOGO')}}" width="200"/>
                </a>
                <!--end::Logo-->
                <!--begin::Action-->
                <a href="#" class="btn btn-sm btn-primary">Back</a>
                <!--end::Action-->
              </div>
              <!--end::Top-->
              <!--begin::Wrapper-->
              <div class="m-0">
                <!--begin::Label-->
                <div class="fw-bolder fs-3 text-gray-800 mb-8">RECONSILE #{{$invoice->year}}{{$invoice->weekoty}}{{$invoice->id}}</div>
                <!--end::Label-->
                <!--begin::Row-->
                <div class="row g-5 mb-11">
                  <!--end::Col-->
                  <div class="col-sm-6">
                    <!--end::Label-->
                    <div class="fw-bold fs-7 text-gray-600 mb-1">Create Date:</div>
                    <!--end::Label-->
                    <!--end::Col-->
                    <div class="fw-bolder fs-6 text-gray-800">{{date('d-F-y H:i:s',strtotime($invoice->tanggal_terbit.'+7hours'))}} WIB</div>
                    <!--end::Col-->
                  </div>
                  <!--end::Col-->
                  <!--end::Col-->
                  <div class="col-sm-6">
                    <!--end::Label-->
                    <div class="fw-bold fs-7 text-gray-600 mb-1">Due Date:</div>
                    <!--end::Label-->
                    <!--end::Info-->
                    <div class="fw-bolder fs-6 text-gray-800 d-flex align-items-center flex-wrap">
                      <span class="pe-2">{{date('d-F-y H:i:s',strtotime($invoice->tanggal_batas.'+7hours'))}} WIB</span>
                      <span class="fs-7 text-danger d-flex align-items-center">
                        @php
                        $dateCreate = Carbon\Carbon::parse(strtotime($invoice->tanggal_batas.'+7hours'));
                        $dateUpdate = Carbon\Carbon::parse(strtotime($invoice->tanggal_batas.'+7hours'));
                        @endphp
                        <span class="bullet bullet-dot bg-danger me-2"></span>{{$dateUpdate->diffForHumans($dateCreate)}}</span>
                      </div>
                      <!--end::Info-->
                    </div>
                    <!--end::Col-->
                  </div>
                  <!--end::Row-->
                  <!--begin::Row-->
                  <div class="row g-5 mb-12">
                    <!--end::Col-->
                    <div class="col-sm-6">
                      <!--end::Label-->
                      <div class="fw-bold fs-7 text-gray-600 mb-1">Reconsile For:</div>
                      <!--end::Label-->
                      <!--end::Text-->
                      <div class="fw-bolder fs-6 text-gray-800">{{$invoice->name}}</div>
                      <!--end::Text-->
                      <!--end::Description-->
                      <div class="fw-bold fs-7 text-gray-600">Username - {{$invoice->username}}</div>
                      <!--end::Description-->
                    </div>
                    <!--end::Col-->
                    <!--end::Col-->
                    <div class="col-sm-6">
                      <!--end::Label-->
                      <div class="fw-bold fs-7 text-gray-600 mb-1">Taking Position:</div>
                      <!--end::Label-->
                      <!--end::Text-->
                      <div class="fw-bolder fs-6 text-gray-800">P/L Partner {{$invoice->takingPositionPercentage}} % : {{100 - $invoice->takingPositionPercentage}} % Company</div>
                      <!--end::Text-->
                      <!--end::Description-->
                      <div class="fw-bold fs-7 text-gray-600">Com Partner {{$invoice->takingPositionPercentage}} % : {{100 - $invoice->takingPositionPercentage}} % Company</div>
                      <!--end::Description-->
                    </div>
                    <!--end::Col-->
                  </div>
                  <!--end::Row-->
                  <!--begin::Content-->
                  <div class="flex-grow-1">

                      <!--begin::Table-->
                      <div class="table-responsive border-bottom mb-9">
                        <table class="table mb-3">
                          <thead>
                            <tr class="border-bottom text-gray-400">
                              <th></th>
                              <th class="pb-2">Reference</th>
                              <th class="pb-2">Description</th>
                              <th class="pb-2 text-end">Total</th>
                              <th class="pb-2 text-end">Partner</th>
                              <th class="pb-2 text-end">Company</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach ($balance as $key => $value)

                                <tr class="text-gray-700">
                                  <td class="pt-6"><a href="{{route('trading-statement',[$value->weekoty,$value->year,$value->id])}}"> <i class="fa fa-genderless text-success fs-2 me-2"></i> </a></td>
                                  <td class="pt-6">{{$value->sumber}}</td>
                                  <td class="pt-6">{{$value->keterangan}}</td>
                                  <td class="pt-6 text-end">{{number_format($value->mutasi,0,'.',',')}}</td>
                                  <td class="pt-6 text-end">{{number_format($value->amount_partner,0,'.',',')}}</td>
                                  <td class="pt-6 text-dark fw-boldest text-end">{{number_format($value->amount_company,0,'.',',')}}</td>
                                </tr>


                            @endforeach

                          </tbody>
                        </table>
                      </div>
                      <!--end::Table-->


                      <!--begin::Container-->
                      <div class="d-flex justify-content-end">
                        <!--begin::Section-->
                        <div class="mw-300px">

                          <!--begin::Item-->
                          <div class="d-flex flex-stack">
                            <!--begin::Code-->
                            <div class="fw-bold pe-10 text-gray-600 fs-2">Total</div>
                            <!--end::Code-->
                            <!--begin::Label-->


                                <div class="text-end fw-bolder fs-2 text-gray-800">Rp {{number_format($invoice->amount,0,'.',',')}}</div>



                            <!--end::Label-->
                          </div>
                          <!--end::Item-->
                        </div>
                        <!--end::Section-->
                      </div>
                      <!--end::Container-->
                    </div>
                    <!--end::Content-->
                  </div>
                  <!--end::Wrapper-->
                </div>
                <!--end::Invoice 2 content-->
              </div>
              <!--end::Content-->
              <!--begin::Sidebar-->
              <div class="m-0">
                <!--begin::Invoice 2 sidebar-->
                <div class="border border-dashed border-gray-300 card-rounded h-lg-100 min-w-md-350px p-9 bg-lighten">
                  <!--begin::Labels-->
                  <div class="mb-8">
                    @if ($invoice->status_bayar == "waiting")
                      <span class="badge badge-light-warning">Pending</span>
                    @elseif($invoice->status_bayar == "approved")
                      <span class="badge badge-light-success">Approved</span>
                    @elseif($invoice->status_bayar == "rejected")
                      <span class="badge badge-light-danger">Rejected</span> <span class="text-danger"></span>
                    @elseif($invoice->status_bayar == "expired")
                      <span class="badge badge-light-danger">Rejected</span> <span class="text-danger"></span>
                    @endif
                  </div>

                  <!--end::Labels-->
                  <!--begin::Title-->
                  <h6 class="mb-8 fw-boldest text-gray-600 text-hover-primary">PAYMENT DETAILS</h6>
                  <!--end::Title-->
                  <!--begin::Item-->
                  <div class="mb-6">
                    <div class="fw-bold text-gray-600 fs-7">From Bank:</div>
                    <div class="fw-bolder text-gray-800 fs-6"></div>
                  </div>
                  <!--end::Item-->
                  <!--begin::Item-->
                  <div class="mb-6">
                    <div class="fw-bold text-gray-600 fs-7">From Name:</div>
                    <div class="fw-bolder text-gray-800 fs-6"></div>
                  </div>
                  <!--end::Item-->
                  <!--begin::Item-->
                  <div class="mb-15">
                    <div class="fw-bold text-gray-600 fs-7">From Account:</div>
                    <div class="fw-bolder fs-6 text-gray-800 d-flex align-items-center"></div>
                  </div>
                  <!--end::Item-->
                  <!--begin::Title-->
                  <h6 class="mb-8 fw-boldest text-gray-600 text-hover-primary">PAYMENT OVERVIEW</h6>
                  <!--end::Title-->
                  <!--begin::Item-->
                  <div class="mb-6">
                    <div class="fw-bold text-gray-600 fs-7">To Bank:</div>
                    <div class="fw-bolder text-gray-800 fs-6"></div>
                  </div>
                  <!--end::Item-->
                  <!--begin::Item-->
                  <div class="mb-6">
                    <div class="fw-bold text-gray-600 fs-7">To Name:</div>
                    <div class="fw-bolder text-gray-800 fs-6"></div>
                  </div>
                  <!--end::Item-->
                  <!--begin::Item-->
                  <div class="mb-15">
                    <div class="fw-bold text-gray-600 fs-7">To Account:</div>
                    <div class="fw-bolder fs-6 text-gray-800 d-flex align-items-center"></div>
                  </div>
                  <!--end::Item-->
                </div>
                <!--end::Invoice 2 sidebar-->
              </div>
              <!--end::Sidebar-->
            </div>
            <!--end::Layout-->
          </div>
          <!--end::Body-->
        </div>
        <!--end::Invoice 2 main-->
    </div>
@endsection

@section('jsonpage')

@endsection
