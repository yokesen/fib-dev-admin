@extends('template.master')

@section('title','Reconsile')
@section('bc-1','Reconsile')
@section('bc-2','Cocokan')

@section('container')
  <div id="kt_content_container" class="container">
      <div class="row">
        <div class="col-md-12">
          <!--begin::Tables Widget 9-->
          <div class="card card-xxl-stretch mb-5 mb-xl-8">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">
              <div class="card-title">
                <h1>Reconsile</h1>
  						</div>

              <div class="card-title">



              </div>
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-3">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="fw-bolder text-muted">
                      <th>Tanggal Terbit</th>
                      <th>Username</th>
                      <th>Name</th>
                      <th>Periode</th>
                      <th class="text-end">Hitungan</th>
                      <th class="text-end">Status Bayar</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody id="table-body">
                    @foreach ($getCocokan as $x => $value)

                      <tr>
                        <td>
                          <a href="{{route('detailPerAccount',$value->hashCalc)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{date('d-m-Y H:i:s',strtotime($value->created_at))}}</a>

                        </td>
                        <td>
                          <a href="{{route('detailPerAccount',$value->hashCalc)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->username}}</a>
                        </td>
                        <td>
                          <a href="{{route('detailPerAccount',$value->hashCalc)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->name}}</a>
                        </td>
                        <td>
                          <a href="{{route('detailPerAccount',$value->hashCalc)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->dateStart}}-{{$value->dateEnd}}</a>
                        </td>
                        <td>
                          <a href="{{route('detailPerAccount',$value->hashCalc)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6 text-end">{{number_format($value->calcComp,'0','.','.')}}</a>
                        </td>
                        <td>
                          <a href="{{route('detailPerAccount',$value->hashCalc)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6 text-end">{{$value->payment_comp == 0 ? "waiting" : "done"}}</a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                  <!--end::Table body-->
                </table>
                <!--end::Table-->

              </div>
              <!--end::Table container-->
            </div>
            <!--begin::Body-->
          </div>
          <!--end::Tables Widget 9-->
        </div>
      </div>

    </div>
@endsection

@section('jsonpage')

@endsection
