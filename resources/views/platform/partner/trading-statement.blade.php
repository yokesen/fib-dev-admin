@extends('template.master')

@section('title','Partner')
@section('bc-1','Partner')
@section('bc-2','Trading Statement')

@section('container')

  <div id="kt_content_container" class="container">
    @include('platform.partner.partial-partner._user-card')
    @include('platform.partner.partial-partner._partner-pl')
  </div>
@endsection

@section('jsinline')

@endsection
