@extends('template.master')

@section('title','Partner')
@section('bc-1','Partner')
@section('bc-2','Action')

@section('cssinline')
    <link href="{{url('/')}}/assets-1/css/select2.min.css" rel="stylesheet">
@endsection

@section('container')
  <div id="kt_content_container" class="container">
    <!--begin::Layout-->
    <div class="d-flex flex-column flex-xl-row">
      <!--begin::Sidebar-->
      <div class="flex-column flex-lg-row-auto w-100 w-xl-400px mb-10">
        <!--begin::Card-->
        @include('platform.partner.partial-partner._partner-profile')
        <!--end::Card-->
        <!--begin::Connected Accounts-->

        <!--end::Connected Accounts-->
      </div>
      <!--end::Sidebar-->

      <div class="flex-lg-row-fluid ms-lg-15">
        <!--begin:::Tabs-->
        <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-bold mb-8">
          <!--begin:::Tab item-->
          <li class="nav-item">
            <a class="nav-link text-dark pb-4 active" data-bs-toggle="tab" href="#kt_customer_view_overview_tab">Profile</a>
          </li>
          <!--end:::Tab item-->
          <!--begin:::Tab item-->
          <li class="nav-item">
            <a class="nav-link text-dark pb-4" data-bs-toggle="tab" href="#kt_customer_view_overview_events_and_logs_tab">Balance Statement</a>
          </li>
          <!--end:::Tab item-->
          <!--begin:::Tab item-->
          <li class="nav-item">
            <a class="nav-link text-dark pb-4" data-kt-countup-tabs="true" data-bs-toggle="tab" href="#kt_customer_view_overview_statements">Trading Statement</a>
          </li>
          <!--end:::Tab item-->
          <!--begin:::Tab item-->
          <li class="nav-item">
            <a class="nav-link text-dark pb-4" data-bs-toggle="tab" href="#kt_customer_view_document_tab">Margin In-Out</a>
          </li>
          <!--end:::Tab item-->
        </ul>
        <!--end:::Tabs-->
        <!--begin:::Tab content-->
        <div class="tab-content" id="myTabContent">
          <!--begin:::Tab pane-->
          <div class="tab-pane fade show active" id="kt_customer_view_overview_tab" role="tabpanel">

            @include('platform.partner.partial-partner._partner-detail')

            {{--@include('partner.partials._profile-analytic')

            @include('partner.partials._user-metatrader-list') --}}

          </div>
          <!--end:::Tab pane-->
          <!--begin:::Tab pane-->
          <div class="tab-pane fade" id="kt_customer_view_document_tab" role="tabpanel">

            {{-- @include('partner.partials._document_ktp')

            @include('partner.partials._document_tabungan') --}}

          </div>
          <!--end:::Tab pane-->
          <!--begin:::Tab pane-->
          <div class="tab-pane fade" id="kt_customer_view_overview_events_and_logs_tab" role="tabpanel">
            @include('platform.partner.partial-partner._partner-finance')
            {{-- @include('partner.partials._user-deposit-method')

            @include('partner.partials._user-deposit-list')

            @include('partner.partials._user-withdrawal-list') --}}

          </div>
          <!--end:::Tab pane-->
          <!--begin:::Tab pane-->
          <div class="tab-pane fade" id="kt_customer_view_overview_statements" role="tabpanel">
            {{-- <!--begin::Earnings-->
            @include('partner.partials._referral-link')
            <!--end::Earnings-->
            @include('partner.partials._referral-list')
            <!--end::Statements--> --}}
          </div>
          <!--end:::Tab pane-->
        </div>
      </div>
      <!--MODAL-->
      {{-- @include('partner.partials._modal-withdrawal')--}}
      @include('platform.partner.partial-partner._modal-deposit')
      {{-- @include('partner.partials._modal-add-metatrader-4') --}}
    </div>

  </div>
@endsection

@section('jsinline')
  <script src="{{url('/')}}/assets-1/js/select2.min.js"></script>
@endsection

@section('jsonpage')
  <script type="text/javascript">
    $('input[type=radio][name=freeMarginStatus]').change(function() {
      if (this.value == "yes") {
        $('input[name=depositGuarantee]').val(0);
        $('input[name=depositGuarantee]').attr({
          min : 0,
        });

      }else{
        $('input[name=depositGuarantee]').val(2500);
        $('input[name=depositGuarantee]').attr({
          min : 2500,
        });
      }
    });


  </script>
@endsection
