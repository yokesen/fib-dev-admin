@extends('template.master')

@section('title','Add New Partner')
@section('metadescription','Invest to rich')
@section('metakeyword','Invest to rich')
@section('bc-1','Partner Management')
@section('bc-2','Add New Partner')

@section('container')
  <div id="kt_content_container" class="container">
      <div class="row">
        <form method="POST" action="{{route('storeNewPartner')}}" class="form" enctype="multipart/form-data">
          <div class="col-md-12">
            <!--begin::Basic info-->
            <div class="card mb-5 mb-xl-10">
              <!--begin::Card header-->
              <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
                <!--begin::Card title-->
                <div class="card-title m-0">
                  <h3 class="fw-bolder m-0">Add New Partner </h3>
                </div>
                <!--end::Card title-->
              </div>
              <!--begin::Card header-->
              <!--begin::Content-->
              <div class="card-body border-top p-9">
                <div class="row">
                  <div class="col-md-6">

                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">
                        <input class="form-control form-control-lg form-control-solid {{ old('name') && !$errors->has('name') ? 'input-valid' : '' }} {{$errors->has('name') ? 'input-error' : ''}}" type="text" placeholder="" name="name" autocomplete="off" value="{{ old('name') ? old('name') : '' }}" required {{$errors->has('name') ? 'autofocus' : ''}}/>
                        @if ($errors->has('name'))
                            <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('name') }}</h4>
                        @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Email</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">

                          <input class="form-control form-control-lg form-control-solid {{ old('email') && !$errors->has('email') ? 'input-valid' : '' }} {{$errors->has('email') ? 'input-error' : ''}}" type="email" name="email"  autocomplete="off" value="{{ old('email') ? old('email') : '' }}" required {{$errors->has('email') ? 'autofocus' : ''}}/>
                          @if ($errors->has('email'))
                              <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('email') }}</h4>
                          @endif


                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Whatsapp</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">
                        <input class="form-control form-control-lg form-control-solid {{ old('whatsapp') && !$errors->has('whatsapp') ? 'input-valid' : '' }} {{$errors->has('whatsapp') ? 'input-error' : ''}}" type="text" name="whatsapp"  autocomplete="off" value="" required {{$errors->has('whatsapp') ? 'autofocus' : ''}} required/>
                        @if ($errors->has('whatsapp'))
                            <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('whatsapp') }}</h4>
                        @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-6" data-kt-password-meter="true">

                        <!--begin::Label-->
                        <label class="col-lg-4 col-form-label required fw-bold fs-6">Password</label>
                        <!--end::Label-->
                        <div class="col-lg-8 fv-row">
                          <!--begin::Input wrapper-->
                          <div class="position-relative mb-3">
                            <input class="form-control form-control-lg form-control-solid {{old('password') ? 'input-error' : ''}}" type="password" placeholder="" minlength="8"  name="password" autocomplete="off" required/>
                            <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                              <i class="bi bi-eye-slash fs-2"></i>
                              <i class="bi bi-eye fs-2 d-none"></i>
                            </span>
                          </div>
                          <!--end::Input wrapper-->
                          <!--begin::Meter-->
                          <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                            <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                            <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                            <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                            <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
                          </div>
                          <!--end::Meter-->
                          <!--begin::Hint-->
                          <div class="text-muted">min 8 chars with a mix of letters, numbers &amp; symbols.</div>
                          <!--end::Hint-->
                          @if ($errors->has('password'))
                              <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('password') }}</h4>
                          @endif
                        </div>

                    </div>
                    <!--end::Input group=-->


                  </div>

                  <div class="col-md-6">
                    <!--begin::Input group-->
                    <div class="mb-10 fv-row">
                        <!--begin::Label-->
                        <label class="d-flex align-items-center required form-label mb-3">Partner Type</label>
                        <!--end::Label-->
                        <!--begin::Row-->
                        <div class="row mb-2" data-kt-buttons="true">
                            <!--begin::Col-->
                            <div class="col">
                                <!--begin::Option-->
                                <label
                                    class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5 active">
                                    <input type="radio" class="btn-check" name="freeMarginStatus"
                                        checked="checked" value="no" />
                                    <span class="fw-bolder fs-3">Deposit Credit</span>
                                </label>
                                <!--end::Option-->
                            </div>
                            <!--end::Col-->
                            <!--begin::Col-->
                            <div class="col">
                                <!--begin::Option-->
                                <label
                                    class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5">
                                    <input type="radio" class="btn-check" name="freeMarginStatus"
                                        value="yes" />
                                    <span class="fw-bolder fs-3">Free Margin</span>
                                </label>
                                <!--end::Option-->
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Row-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Deposit Credit</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">

                        <div class="input-group">
                          <span class="input-group-text" style="font-size:1.5em!important;border:0px!important">
                              IDR
                          </span>
                            <input class="form-control form-control-lg form-control-solid" type="number"
                                name="depositGuarantee" min="2500" step="250"
                                style="font-size:1.5em!important;text-align:right;" required />

                        </div>
                        @if ($errors->has('depositGuarantee'))
                            <h4 class="text-danger mt-6"> <i
                                    class="bi bi-exclamation-triangle text-danger fs-2 blink"></i>
                                {{ $errors->first('depositGuarantee') }}</h4>
                        @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Credit</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">

                        <div class="input-group">
                          <span class="input-group-text" style="font-size:1.5em!important;border:0px!important">
                              IDR
                          </span>
                            <input class="form-control form-control-lg form-control-solid" type="number"
                                name="creditGiven" min="5000" step="1000"
                                style="font-size:1.5em!important;text-align:right;" required />

                        </div>
                        @if ($errors->has('creditGiven'))
                            <h4 class="text-danger mt-6"> <i
                                    class="bi bi-exclamation-triangle text-danger fs-2 blink"></i>
                                {{ $errors->first('creditGiven') }}</h4>
                        @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->

                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Taking Position</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">

                        <div class="input-group">

                            <input class="form-control form-control-lg form-control-solid" type="number"
                                name="takingPositionPercentage" min="{{$senior_minimum_taking_position}}" step="5" max="{{$senior_maximum_taking_position}}"
                                style="font-size:1.5em!important;text-align:right;" required value="{{$senior_default_taking_position}}"/>
                            <span class="input-group-text" style="font-size:1.5em!important;border:0px!important">
                                %
                            </span>
                        </div>
                        @if ($errors->has('takingPositionPercentage'))
                            <h4 class="text-danger mt-6"> <i
                                    class="bi bi-exclamation-triangle text-danger fs-2 blink"></i>
                                {{ $errors->first('takingPositionPercentage') }}</h4>
                        @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Daily Settlement Limit</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">

                        <div class="input-group">
                            <span class="input-group-text" style="font-size:1.5em!important;border:0px!important">
                                IDR
                            </span>
                            <input class="form-control form-control-lg form-control-solid" type="number"
                                name="dailySettlementLimit" min="1000" step="1000"
                                style="font-size:1.5em!important;text-align:right;" required />
                        </div>
                        @if ($errors->has('ammount'))
                            <h4 class="text-danger mt-6"> <i
                                    class="bi bi-exclamation-triangle text-danger fs-2 blink"></i>
                                {{ $errors->first('ammount') }}</h4>
                        @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                  </div>

                </div>
                <hr>
                <div class="row">
                  <div class="col-md-6">
                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Username</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">
                        <input class="form-control form-control-lg form-control-solid {{ old('username') && !$errors->has('username') ? 'input-valid' : '' }} {{$errors->has('username') ? 'input-error' : ''}}" type="text" minlength="4" maxlength="20" placeholder="" name="username" autocomplete="off" value="{{ old('username') ? old('username') : '' }}" required {{$errors->has('username') ? 'autofocus' : ''}}/>
                        @if ($errors->has('username'))
                            <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('username') }}</h4>
                        @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="row mb-6">
                      <!--begin::Label-->
                      <label class="col-lg-4 col-form-label required fw-bold fs-6">Group Prefix MT5</label>
                      <!--end::Label-->
                      <!--begin::Col-->
                      <div class="col-lg-8 fv-row">
                        <input class="form-control form-control-lg form-control-solid {{ old('prefix') && !$errors->has('prefix') ? 'input-valid' : '' }} {{$errors->has('prefix') ? 'input-error' : ''}}" type="text" minlength="2" maxlength="3" placeholder="" name="prefix" autocomplete="off" value="{{ old('prefix') ? old('prefix') : '' }}" required {{$errors->has('prefix') ? 'autofocus' : ''}}/>
                        @if ($errors->has('prefix'))
                            <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('prefix') }}</h4>
                        @endif
                      </div>
                      <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                  </div>
                  <div class="col-md-6">
                    <h4>Account Type : </h4>
                    @foreach ($accountType as $type)
                      <!--begin::Input group-->
                      <div class="row mb-0">
                        <!--begin::Label-->
                        <label class="col-lg-8 col-form-label fw-bold fs-6">{{$type->masterName}} {{$type->masterRate}} (<span class="prefix"></span>-{{$type->masterRate}}-{{$type->masterGroup}})</label>
                        <!--begin::Label-->
                        <!--begin::Label-->
                        <div class="col-lg-4 d-flex align-items-center">
                          <div class="form-check form-check-solid form-switch fv-row">
                            <input class="form-check-input w-45px h-30px" type="checkbox" name="group[]" value="{{$type->id}}" />
                          </div>
                        </div>
                        <!--begin::Label-->
                      </div>
                      <!--end::Input group-->
                    @endforeach
                    @if ($errors->has('group'))
                        <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('group') }}</h4>
                    @endif
                  </div>
                </div>

                @csrf
                <!--begin::Actions-->
                <div class="card-footer d-flex justify-content-end py-6 px-9">

                  <button type="submit" class="btn btn-primary" id="ot_button">Submit</button>
                </div>
                <!--end::Actions-->
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
@endsection

@section('jsinline')
  <script type="text/javascript">
    $('input[type=radio][name=freeMarginStatus]').change(function() {
      if (this.value == "yes") {
        $('input[name=depositGuarantee]').val(0);
        $('input[name=depositGuarantee]').attr({
          min : 0,
        });

      }else{
        $('input[name=depositGuarantee]').val(100000);
        $('input[name=depositGuarantee]').attr({
          min : 50000,
        });
      }
    });

    $('input[name=whatsapp]').change(function() {
      let text = $('input[name=name]').val();
      const newText = text.split(" ");
      var prefix = "";
      var final = "";
      $.each( newText, function( key, value ) {
        char = value.slice(0,1);
        prefix += char;
      });

      whatsapp = this.value;
      wa = whatsapp.slice(-2);
      final = prefix + wa;
      user = '{{date('M')}}' + prefix + '{{date('d')}}' + wa;
      $('input[name=prefix]').val(final);
      $('input[name=username]').val(user);
      $('.prefix').html(final);
    });
  </script>
@endsection
