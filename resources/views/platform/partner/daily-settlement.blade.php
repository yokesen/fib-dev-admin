@extends('template.master')

@section('title','My Client')
@section('metadescription','FIB Partner the best business partner for IB')
@section('metakeyword','FIB Partner')
@section('bc-1','My Client')
@section('bc-2','My Client')

@section('container')

  <div id="kt_content_container" class="container">
      <div class="row">
        <div class="col-md-12">
          <!--begin::Tables Widget 9-->
          <div class="card card-xxl-stretch mb-5 mb-xl-8">
            <!--begin::Header-->
            <div class="card-header border-0 pt-5">

            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body py-3">
              <!--begin::Table container-->
              <div class="table-responsive">
                <!--begin::Table-->
                <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                  <!--begin::Table head-->
                  <thead>
                    <tr class="fw-bolder text-muted">
                      <th>Name</th>
                      <th>Free Margin</th>
                      <th>Daily Limit</th>
                      <th>Daily Reporting</th>
                      <th>Daily Status</th>
                      <th>Allow to operate</th>
                    </tr>
                  </thead>
                  <!--end::Table head-->
                  <!--begin::Table body-->
                  <tbody id="table-body">
                    @foreach ($users as $x => $value)
                      <tr>
                        <td>
                          <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->name}}</a>
                        </td>
                        <td>
                          <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->freeMarginStatus}}</a>
                        </td>
                        <td>
                          <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->dailySettlementLimit}}</a>
                        </td>
                        <td>
                          <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->lastKnownPL}}</a>
                        </td>
                        <td>
                          <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->dailySettlementStatus}}</a>
                        </td>
                        <td>
                          <a href="{{route('showUser',$value->uuid)}}" class="text-dark fw-bolder text-hover-primary d-block fs-6">{{$value->allowOperate}}</a>
                        </td>

                      </tr>
                    @endforeach
                  </tbody>
                  <!--end::Table body-->
                </table>
                <!--end::Table-->
                {{$users->links()}}
              </div>
              <!--end::Table container-->
            </div>
            <!--begin::Body-->
          </div>
          <!--end::Tables Widget 9-->
        </div>
      </div>

    </div>

@endsection
