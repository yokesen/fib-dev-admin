<form  method="POST" action="{{route('submitDepositCredit')}}" class="form">
    <!--begin::Card body-->
    <div class="card-body p-9">
      <h4 class="text-dark text-muted mb-6 ">Detail Penerima (Pastikan Akunnya benar)</h4>
      <!--begin::Input group-->
      <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama Bank </label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-8 fv-row">
          <input class="form-control form-control-lg form-control-solid input-valid" name="bank_name" value="{{$bank->namaBank}}" readonly/>
          @if ($errors->has('bank_name'))
            <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('bank_name') }}</h4>
          @endif
        </div>
        <!--end::Col-->
      </div>
      <!--end::Input group-->
      <!--begin::Input group-->
      <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-4 col-form-label required fw-bold fs-6">Nomor Rekening </label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-8 fv-row">
          <input class="form-control form-control-lg form-control-solid input-valid" name="account_number" value="{{$bank->nomorRekening}}" readonly/>
          @if ($errors->has('account_number'))
            <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('account_number') }}</h4>
          @endif
        </div>
        <!--end::Col-->
      </div>
      <!--end::Input group-->
      <!--begin::Input group-->
      <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama Rekening </label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-8 fv-row">
          <input class="form-control form-control-lg form-control-solid input-valid" name="account_name" value="{{$bank->namaRekening}}" readonly/>
          @if ($errors->has('account_name'))
            <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('account_name') }}</h4>
          @endif
        </div>
        <!--end::Col-->
      </div>
      <!--end::Input group-->

      <hr>
      <!--begin::Input group-->
      <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-4 col-form-label required fw-bold fs-6">Nilai CREDIT Withdrawal </label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-8 fv-row">
          <div class="input-group">
            <span class="input-group-text">
              IDR
            </span>
            <input class="form-control form-control-lg form-control-solid" name="amount" id="paternRupiah"  value="" min="10000" style="font-size:1.5em!important" required/>

          </div>

          @if ($errors->has('ammount'))
            <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('ammount') }}</h4>
          @endif
        </div>
        <!--end::Col-->
      </div>
      <!--end::Input group-->


      <!--begin::Actions-->
      <div class="text-center">

        @csrf
        <!--begin::Actions-->

        <input type="hidden" name="tipe_deposit" value="withdrawal"/>
        <input type="hidden" name="uuid" value="{{$user->uuid}}"/>
        <div class="card-footer d-flex justify-content-end py-6 px-9">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </div>
      <!--end::Actions-->
    </form>
    <!--end::Form-->
  </div>