@extends('template.master')

@section('title','Partner')
@section('bc-1','Partner')
@section('bc-2','Balance Statement')

@section('container')

  <div id="kt_content_container" class="container">
    @include('platform.partner.partial-partner._user-card')
    @include('platform.partner.partial-partner._partner-finance')
    @include('platform.partner.partial-partner._modal-deposit')
    @include('platform.partner.partial-partner._modal-withdraw')
  </div>
@endsection

@section('jsinline')

@endsection
