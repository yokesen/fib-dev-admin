<div class="card mb-5 mb-xl-8">
  <!--begin::Card body-->
  <div class="card-body pt-15">
    <!--begin::Summary-->
    <div class="d-flex flex-center flex-column mb-5">
      <!--begin::Avatar-->
      <div class="symbol symbol-100px symbol-circle mb-7">
        <img src="{{$user->photo}}" alt="image" />
      </div>
      <!--end::Avatar-->
      <!--begin::Name-->
      <a href="#" class="fs-3 text-gray-800 text-hover-primary fw-bolder mb-1">{{$user->username ? $user->username : $user->name}}</a>
      <!--end::Name-->
      <!--begin::Position-->
      <form class="form-control" action="{{route('updateUserLevel',$user->uuid)}}" method="post">
        <div class="row">
          <div class="col-lg-12 fv-row">
            <select class="form-select form-select-solid" name="level" required>
              <option value="">--Type--</option>
              <option value="17" {{$user->id_cms_privileges == '17' ? "selected" : ""}} >IB</option>
              <option value="16" {{$user->id_cms_privileges == '16' ? "selected" : ""}} >Senior IB</option>
              <option value="15" {{$user->id_cms_privileges == '15' ? "selected" : ""}} >Master IB</option>
            </select>
          </div>
        </div>
        <div class="row mt-6">
          <div class="col-lg-12 text-center">
            @csrf
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </form>
      <!--end::Position-->
    </div>
    <!--end::Summary-->

      <div class="separator separator-dashed my-3"></div>
      <!--begin::Details content-->
      <div id="kt_customer_view_details" class="collapse show">
        <div class="py-5 fs-6">
          @if ($user->id_cms_privileges == 10)
            <!--begin::Badge-->
            <div class="badge badge-light-success d-inline">Premium user</div>
            <!--begin::Badge-->
          @endif
          <!--begin::Details item-->
          <div class="fw-bolder mt-5">Account ID</div>
          <div class="text-gray-600">ID-{{$user->id}}</div>
          <!--begin::Details item-->
          <!--begin::Details item-->
          <div class="fw-bolder mt-5">Name</div>
          <div class="text-gray-600">{{$user->name}}</div>
          <!--begin::Details item-->
          <!--begin::Details item-->
          <div class="fw-bolder mt-5">Email</div>
          @if ($user->email && $user->email_verification == 'verified')
            <div class="text-gray-600"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> {{$user->email}}</div>
          @elseif ($user->email && $user->email_verification != 'verified')
            <div class="text-gray-600"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5"></i> {{$user->email}}</div>
          @else
            <div class="text-gray-600"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5"></i> </div>
          @endif
          <!--begin::Details item-->
          <!--begin::Details item-->
          <div class="fw-bolder mt-5">Phone</div>
          <div class="text-gray-600">{{$user->phone}}</div>
          <!--begin::Details item-->
          <!--begin::Details item-->
          <div class="fw-bolder mt-5">Mobile</div>
          <div class="text-gray-600">{{$user->mobile ? $user->mobile : '-'}}</div>
          <!--begin::Details item-->
          <!--begin::Details item-->
          <div class="fw-bolder mt-5">Whatsapp</div>
          @if (!empty($user->whatsapp))
            <span class="text-gray-600"><i class="fas fa-check-circle fs-7 text-success ls-5"></i> {{$user->whatsapp}}</span>
          @else
            <span class="text-gray-600"><i class="fas fa-exclamation-triangle fs-7 text-warning ls-5"></i></span>
          @endif
          <!--begin::Details item-->
          <!--begin::Details item-->
          <div class="fw-bolder mt-5">Address</div>
          <div class="text-gray-600">{{$user->address}}</div>
          <!--begin::Details item-->
          <!--begin::Details item-->
          <div class="fw-bolder mt-5">Date of Birth</div>
          <div class="text-gray-600">{{date('d-m-Y',strtotime($user->dob))}}</div>
          <!--begin::Details item-->

          <!--begin::Details item-->
          <div class="fw-bolder mt-5">Parent</div>
          @if ($parent)
            <a href="{{route('showUser',$parent->uuid)}}" class="text-gray-600">{{$parent->username}}</a>
          @else
            <div class="text-gray-600">-</div>
          @endif


          <!--begin::Details item-->
        </div>
      </div>
      <!--end::Details content-->
    </div>
    <!--end::Card body-->
  </div>
