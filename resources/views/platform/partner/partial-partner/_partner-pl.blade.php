<div class="card pt-4 mb-6 mb-xl-9">
  <!--begin::Card header-->
  <div class="card-header border-0">
    <!--begin::Card title-->
    <div class="card-title">
      <h2>Profit-Loss Statement</h2>
    </div>
    <!--end::Card title-->
    <!--begin::Card toolbar-->
    <div class="card-toolbar">

    </div>
    <!--end::Card toolbar-->
  </div>
  <!--end::Card header-->
  <!--begin::Card body-->
  <div class="card-body pt-0 pb-5">

    <!--begin::Table-->
    <table class="table align-middle table-row-dashed gy-5" id="kt_table_customers_payment">
      <!--begin::Table head-->
      <thead class="border-bottom border-gray-200 fs-7 fw-bolder">
        <!--begin::Table row-->
        <tr class="text-start text-gray-400 text-uppercase gs-0">
          <th class="min-w-100px">Week.</th>
          <th>Periode</th>
          <th>Closed</th>
          <th>Floating</th>
          <th>Total</th>
          <th>Overview</th>
        </tr>
        <!--end::Table row-->
      </thead>
      <!--end::Table head-->
      <!--begin::Table body-->
      <tbody class="fs-6 fw-bold text-gray-600">
        @foreach ($weeklyClosedStatements as $w)
          @php
            if ($w->profit < 0) {
              $warna = 'success';
              $icon = 'fa-check-circle';
            }elseif($w->profit > 0){
              $warna = 'danger';
              $icon = 'fa-times-circle';
            }else{
              $warna = 'primary';
              $icon = 'fa-clock';
            }

            $week = $w->weekoty;
            $year = $w->year;

            $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
            $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
            $date_for_monday = date( 'd-m-Y', $timestamp_for_monday );
            $date_for_friday = date( 'd-m-Y', strtotime('friday ',$timestamp_for_monday) );
          @endphp
          <!--begin::Table row-->
          <tr>
            <!--begin::Invoice=-->
            <td>
              Week-{{$w->weekoty}}
            </td>
            <!--end::Invoice=-->
            <td>{{$date_for_monday}} to {{$date_for_friday}}</td>
            <!--begin::Status=-->
            <td class="text-{{$warna}}">
              {{$w->profit != 0 ? number_format($w->profit,'2','.',',') : $w->profit}}
            </td>
            <!--end::Status=-->
            <!--begin::Amount=-->
            <td class="text-primary">
              {{$w->floating != 0 ? number_format($w->floating,'2','.',',') : $w->floating}}
            </td>
            <!--end::Amount=-->
            <!--begin::Date=-->

            <!--end::Date=-->
            <!--begin::Action=-->
            <td>{{number_format($w->profit + $w->floating,'2','.',',')}}</td>
            <td>
              <a href="{{route('viewTradingStatementClient',[$user->uuid,$w->weekoty,$w->year])}}" class="btn btn-primary"> <i class="far fa-eye text-white"></i> Check</a>
            </td>

            <!--end::Action=-->
          </tr>
          <!--end::Table row-->
        @endforeach
      </tbody>
      <!--end::Table body-->
    </table>
    <!--end::Table-->

  </div>
  <!--end::Card body-->
</div>
