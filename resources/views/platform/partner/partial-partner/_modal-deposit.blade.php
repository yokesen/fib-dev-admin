<div class="modal fade" id="kt_modal_deposit" tabindex="-1" aria-hidden="true">
  <!--begin::Modal dialog-->
  <div class="modal-dialog mw-650px">
    <!--begin::Modal content-->
    <div class="modal-content">
      <!--begin::Modal header-->
      <div class="modal-header">
        <!--begin::Modal title-->
        <h2 class="fw-bolder">Add Deposit for {{$user->username ? $user->username : $user->name}}</h2>
        <!--end::Modal title-->
      </div>
      <!--end::Modal header-->
      <!--begin::Modal body-->
      <div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
        <!--begin::Form-->
        <form  method="POST" action="{{route('submitDepositCredit')}}" class="form">
          <!--begin::Card body-->
          <div class="card-body p-9">
            <h4 class="text-dark text-muted mb-6 ">Detail Pengirim (Pastikan Akunnya benar)</h4>
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama Bank </label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid input-valid" name="bank_name" value="{{$bank->bank_name}}" required/>
                @if ($errors->has('bank_name'))
                  <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('bank_name') }}</h4>
                @endif
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nomor Rekening </label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid input-valid" name="account_number" value="{{$bank->account_number}}" required/>
                @if ($errors->has('account_number'))
                  <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('account_number') }}</h4>
                @endif
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama Rekening </label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <input class="form-control form-control-lg form-control-solid input-valid" name="account_name" value="{{$bank->account_name}}" required/>
                @if ($errors->has('account_name'))
                  <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('account_name') }}</h4>
                @endif
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->

            <hr>

            <!--begin::Input group-->
            <div class="row mb-6">
              <!--begin::Label-->
              <label class="col-lg-4 col-form-label required fw-bold fs-6">Nilai Transfer </label>
              <!--end::Label-->
              <!--begin::Col-->
              <div class="col-lg-8 fv-row">
                <div class="input-group">
                  <span class="input-group-text">
                    IDR
                  </span>
                  <input class="form-control form-control-lg form-control-solid" name="amount" id="paternRupiah"  value="" min="10000" style="font-size:1.5em!important" required/>

                </div>

                @if ($errors->has('ammount'))
                  <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('ammount') }}</h4>
                @endif
              </div>
              <!--end::Col-->
            </div>
            <!--end::Input group-->
            <div class="mb-10 fv-row">
              <!--begin::Label-->
              <label class="d-flex align-items-center required form-label mb-3">Pilihan Bank Tujuan Transfer</label>
              <!--end::Label-->
              <!--begin::Row-->
              <div class="row mb-2" data-kt-buttons="true">
                <!--begin::Col-->
                <div class="col">
                  <!--begin::Option-->
                  <label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5 active">
                    <input type="radio" class="btn-check" name="transferTo" checked="checked" id="transferTo" value="BCA" />
                    <span class="fw-bolder fs-3">BCA</span>
                  </label>
                  <!--end::Option-->
                </div>
                <!--end::Col-->
                <!--begin::Col-->
                <div class="col">
                  <!--begin::Option-->
                  <label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5">
                    <input type="radio" class="btn-check" name="transferTo" id="transferTo" value="BRI" />
                    <span class="fw-bolder fs-3">BRI</span>
                  </label>
                  <!--end::Option-->
                </div>
                <!--end::Col-->
                <!--begin::Col-->
                <div class="col">
                  <!--begin::Option-->
                  <label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5">
                    <input type="radio" class="btn-check" name="transferTo" id="transferTo" value="BNI" />
                    <span class="fw-bolder fs-3">BNI</span>
                  </label>
                  <!--end::Option-->
                </div>
                <!--end::Col-->
                <!--begin::Col-->
                <div class="col">
                  <!--begin::Option-->
                  <label class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5">
                    <input type="radio" class="btn-check" name="transferTo" id="transferTo" value="Mandiri" />
                    <span class="fw-bolder fs-3">Mandiri</span>
                  </label>
                  <!--end::Option-->
                </div>
                <!--end::Col-->
              </div>
              <!--end::Row-->
              <!--begin::Hint-->
              <div class="form-text">Pilih salah satu untuk menampilkan detil keterangan transfer</div>
              <!--end::Hint-->
            </div>

            <!--begin::Actions-->
            <div class="text-center">

              @csrf
              <!--begin::Actions-->
              <input type="hidden" name="tipe_deposit" value="deposit"/>
              <input type="hidden" name="uuid" value="{{$user->uuid}}"/>
              <div class="card-footer d-flex justify-content-end py-6 px-9">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
            <!--end::Actions-->
          </form>
          <!--end::Form-->
        </div>
        <!--end::Modal body-->
      </div>
      <!--end::Modal content-->
    </div>
    <!--end::Modal dialog-->
  </div>
</div>
