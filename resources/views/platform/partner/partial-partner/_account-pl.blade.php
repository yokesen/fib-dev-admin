<div class="card pt-4 mb-6 mb-xl-9">
  <!--begin::Card header-->
  <div class="card-header border-0">
    <!--begin::Card title-->
    <div class="card-title">
      <h2>Profit-Loss Statement</h2>
    </div>
    <!--end::Card title-->
    <!--begin::Card toolbar-->
    <div class="card-toolbar">

    </div>
    <!--end::Card toolbar-->
  </div>
  <!--end::Card header-->
  <!--begin::Card body-->
  <div class="card-body pt-0 pb-5">

    <!--begin::Table-->
    <table class="table align-middle table-row-dashed gy-5" id="kt_table_customers_payment">
      <!--begin::Table head-->
      <thead class="border-bottom border-gray-200 fs-7 fw-bolder">
        <!--begin::Table row-->
        <tr class="text-start text-gray-400 text-uppercase gs-0">
          <th class="min-w-100px">Account Trading</th>
          <th>Time</th>
          <th>Ticket</th>
          <th>Symbol</th>
          <th>Type</th>
          <th>Price</th>
          <th>Closed</th>
          <th>Floating</th>
          <th>Total</th>

        </tr>
        <!--end::Table row-->
      </thead>
      <!--end::Table head-->
      <!--begin::Table body-->
      <tbody class="fs-6 fw-bold text-gray-600">
        @foreach ($weeklyClosedStatements as $w)
          @php
            if ($w->Profit < 0) {
              $warna = 'success';
              $icon = 'fa-check-circle';
            }elseif($w->Profit > 0){
              $warna = 'danger';
              $icon = 'fa-times-circle';
            }else{
              $warna = 'primary';
              $icon = 'fa-clock';
            }

          @endphp
          <!--begin::Table row-->
          <tr>
            <!--begin::Invoice=-->
            <td>
              {{$w->Login}}
            </td>
            <td>
              {{$w->humanTime}}
            </td>
            <td>
              {{$w->PositionID}}
            </td>
            <td>
              {{$w->Symbol}}
            </td>
            <td>
              {{$w->Action == 0 ? 'BUY' : 'SELL'}}
            </td>
            <td>
              {{$w->Price}}
            </td>
            <!--end::Invoice=-->
            <!--begin::Status=-->
            <td class="text-{{$warna}}">
              {{$w->Profit != 0 ? number_format($w->Profit,'2','.',',') : $w->Profit}}
            </td>
            <!--end::Status=-->
            <!--begin::Amount=-->
            <td class="text-primary">

            </td>
            <!--end::Amount=-->
            <!--begin::Date=-->

            <!--end::Date=-->
            <!--begin::Action=-->
            <td>{{number_format($w->Profit,'2','.',',')}}</td>


            <!--end::Action=-->
          </tr>
          <!--end::Table row-->
        @endforeach
        @foreach ($weeklyFloatingStatements as $w)
          @php
            if ($w->Profit < 0) {
              $warna = 'success';
              $icon = 'fa-check-circle';
            }elseif($w->Profit > 0){
              $warna = 'danger';
              $icon = 'fa-times-circle';
            }else{
              $warna = 'primary';
              $icon = 'fa-clock';
            }

          @endphp
          <!--begin::Table row-->
          <tr>
            <!--begin::Invoice=-->
            <td>
              {{$w->Login}}
            </td>
            <td>
              {{$w->humanTime}}
            </td>
            <td>
              {{$w->Position}}
            </td>
            <td>
              {{$w->Symbol}}
            </td>
            <td>
              {{$w->Action == 0 ? 'BUY' : 'SELL'}}
            </td>
            <td>
              {{$w->PriceOpen}}
            </td>
            <!--end::Invoice=-->
            <!--begin::Status=-->
            <td>

            </td>
            <!--end::Status=-->
            <!--begin::Amount=-->
            <td class="text-{{$warna}}">
              {{$w->Profit != 0 ? number_format($w->Profit,'2','.',',') : $w->Profit}}
            </td>
            <!--end::Amount=-->
            <!--begin::Date=-->

            <!--end::Date=-->
            <!--begin::Action=-->
            <td>{{number_format($w->Profit,'2','.',',')}}</td>


            <!--end::Action=-->
          </tr>
          <!--end::Table row-->
        @endforeach
      </tbody>
      <!--end::Table body-->
    </table>
    <!--end::Table-->

  </div>
  <!--end::Card body-->
</div>
