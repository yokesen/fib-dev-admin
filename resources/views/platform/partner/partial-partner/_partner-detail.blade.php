<div class="row">
  <form method="POST" action="{{route('editNewPartner')}}" class="form" enctype="multipart/form-data">
    <div class="col-md-12">
      <!--begin::Basic info-->
      <div class="card mb-5 mb-xl-10">
        <!--begin::Card header-->
        <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
          <!--begin::Card title-->
          <div class="card-title m-0">
            <h3 class="fw-bolder m-0">Personal Profile </h3>
          </div>
          <!--end::Card title-->
        </div>
        <!--begin::Card header-->
        <!--begin::Content-->
        <div class="card-body border-top p-9">
          <div class="row">
            <div class="col-md-6">

              <!--begin::Input group-->
              <div class="row mb-6">
                <!--begin::Label-->
                <label class="col-lg-4 col-form-label required fw-bold fs-6">Nama</label>
                <!--end::Label-->
                <!--begin::Col-->
                <div class="col-lg-8 fv-row">
                  <input class="form-control form-control-lg form-control-solid {{ old('name') && !$errors->has('name') ? 'input-valid' : '' }} {{$errors->has('name') ? 'input-error' : ''}}" type="text" placeholder="" name="name" autocomplete="off" value="{{ $user->name }}" required {{$errors->has('name') ? 'autofocus' : ''}}/>
                  @if ($errors->has('name'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('name') }}</h4>
                  @endif
                </div>
                <!--end::Col-->
              </div>
              <!--end::Input group-->
              <!--begin::Input group-->
              <div class="row mb-6">
                <!--begin::Label-->
                <label class="col-lg-4 col-form-label required fw-bold fs-6">Email</label>
                <!--end::Label-->
                <!--begin::Col-->
                <div class="col-lg-8 fv-row">

                    <input class="form-control form-control-lg form-control-solid {{ old('email') && !$errors->has('email') ? 'input-valid' : '' }} {{$errors->has('email') ? 'input-error' : ''}}" type="email" name="email"  autocomplete="off" value="{{ $user->email }}" required {{$errors->has('email') ? 'autofocus' : ''}}/>
                    @if ($errors->has('email'))
                        <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('email') }}</h4>
                    @endif


                </div>
                <!--end::Col-->
              </div>
              <!--end::Input group-->
              <!--begin::Input group-->
              <div class="row mb-6">
                <!--begin::Label-->
                <label class="col-lg-4 col-form-label required fw-bold fs-6">Whatsapp</label>
                <!--end::Label-->
                <!--begin::Col-->
                <div class="col-lg-8 fv-row">
                  <input class="form-control form-control-lg form-control-solid {{ old('whatsapp') && !$errors->has('whatsapp') ? 'input-valid' : '' }} {{$errors->has('whatsapp') ? 'input-error' : ''}}" type="text" name="whatsapp"  autocomplete="off" value="{{ $user->whatsapp }}" required {{$errors->has('whatsapp') ? 'autofocus' : ''}} required/>
                  @if ($errors->has('whatsapp'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('whatsapp') }}</h4>
                  @endif
                </div>
                <!--end::Col-->
              </div>
              <!--end::Input group-->

            </div>
            <div class="col-md-6">
              <!--begin::Input group-->
              <div class="mb-10 fv-row">
                  <!--begin::Label-->
                  <label class="d-flex align-items-center required form-label mb-3">Partnership Type</label>
                  <!--end::Label-->
                  <!--begin::Row-->
                  <div class="row mb-2" data-kt-buttons="true">
                      <!--begin::Col-->
                      <div class="col">
                          <!--begin::Option-->
                          <label
                              class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5 {{ $user->freeMarginStatus == "no" ? 'active' : '' }}">
                              <input type="radio" class="btn-check" name="freeMarginStatus"
                                    value="no" {{ $user->freeMarginStatus == "no" ? 'checked' : '' }}/>
                              <span class="fw-bolder fs-3">Deposit Credit</span>
                          </label>
                          <!--end::Option-->
                      </div>
                      <!--end::Col-->
                      <!--begin::Col-->
                      <div class="col">
                          <!--begin::Option-->
                          <label
                              class="btn btn-outline btn-outline-dashed btn-outline-default w-100 p-4 mb-5 {{ $user->freeMarginStatus == "yes" ? 'active' : '' }}">
                              <input type="radio" class="btn-check" name="freeMarginStatus"
                                  value="yes" {{ $user->freeMarginStatus == "yes" ? 'checked' : '' }}/>
                              <span class="fw-bolder fs-3">Free Margin</span>
                          </label>
                          <!--end::Option-->
                      </div>
                      <!--end::Col-->
                  </div>
                  <!--end::Row-->
              </div>
              <!--end::Input group-->
              <!--begin::Input group-->
              <div class="row mb-6">
                <!--begin::Label-->
                <label class="col-lg-4 col-form-label required fw-bold fs-6">Deposit Credit</label>
                <!--end::Label-->
                <!--begin::Col-->
                <div class="col-lg-8 fv-row">

                  <div class="input-group">
                    <span class="input-group-text" style="font-size:1.5em!important;border:0px!important">
                        US$
                    </span>
                      <input class="form-control form-control-lg form-control-solid" type="number"
                          name="depositGuarantee" min="{{ $user->freeMarginStatus == "no" ? '2500' : '0' }}" step="250"
                          style="font-size:1.5em!important;text-align:right;" value="{{ $user->depositGuarantee }}" required />

                  </div>
                  @if ($errors->has('depositGuarantee'))
                      <h4 class="text-danger mt-6"> <i
                              class="bi bi-exclamation-triangle text-danger fs-2 blink"></i>
                          {{ $errors->first('depositGuarantee') }}</h4>
                  @endif
                </div>
                <!--end::Col-->
              </div>
              <!--end::Input group-->
              <!--begin::Input group-->
              <div class="row mb-6">
                <!--begin::Label-->
                <label class="col-lg-4 col-form-label required fw-bold fs-6">Credit</label>
                <!--end::Label-->
                <!--begin::Col-->
                <div class="col-lg-8 fv-row">

                  <div class="input-group">
                    <span class="input-group-text" style="font-size:1.5em!important;border:0px!important">
                        US$
                    </span>
                      <input class="form-control form-control-lg form-control-solid" type="number"
                          name="creditGiven" min="5000" step="1000"
                          style="font-size:1.5em!important;text-align:right;" value="{{ $user->creditGiven }}" required />

                  </div>
                  @if ($errors->has('creditGiven'))
                      <h4 class="text-danger mt-6"> <i
                              class="bi bi-exclamation-triangle text-danger fs-2 blink"></i>
                          {{ $errors->first('creditGiven') }}</h4>
                  @endif
                </div>
                <!--end::Col-->
              </div>
              <!--end::Input group-->

              <!--begin::Input group-->
              <div class="row mb-6">
                <!--begin::Label-->
                <label class="col-lg-4 col-form-label required fw-bold fs-6">Taking Position</label>
                <!--end::Label-->
                <!--begin::Col-->
                <div class="col-lg-8 fv-row">

                  <div class="input-group">

                      <input class="form-control form-control-lg form-control-solid" type="number"
                          name="takingPositionPercentage" min="20" step="5" max="95"
                          style="font-size:1.5em!important;text-align:right;" required value="{{ $user->takingPositionPercentage }}"/>
                      <span class="input-group-text" style="font-size:1.5em!important;border:0px!important">
                          %
                      </span>
                  </div>
                  @if ($errors->has('takingPositionPercentage'))
                      <h4 class="text-danger mt-6"> <i
                              class="bi bi-exclamation-triangle text-danger fs-2 blink"></i>
                          {{ $errors->first('takingPositionPercentage') }}</h4>
                  @endif
                </div>
                <!--end::Col-->
              </div>
              <!--end::Input group-->
              <!--begin::Input group-->
              <div class="row mb-6">
                <!--begin::Label-->
                <label class="col-lg-4 col-form-label required fw-bold fs-6">Daily Settlement Limit</label>
                <!--end::Label-->
                <!--begin::Col-->
                <div class="col-lg-8 fv-row">

                  <div class="input-group">
                      <span class="input-group-text" style="font-size:1.5em!important;border:0px!important">
                          US$
                      </span>
                      <input class="form-control form-control-lg form-control-solid" type="number"
                          name="dailySettlementLimit" min="1000" step="1000" value="{{ $user->dailySettlementLimit }}"
                          style="font-size:1.5em!important;text-align:right;" required />
                  </div>
                  @if ($errors->has('ammount'))
                      <h4 class="text-danger mt-6"> <i
                              class="bi bi-exclamation-triangle text-danger fs-2 blink"></i>
                          {{ $errors->first('ammount') }}</h4>
                  @endif
                </div>
                <!--end::Col-->
              </div>
              <!--end::Input group-->
            </div>

          </div>
          <hr>
          <div class="row">
            <div class="col-md-6">
              <!--begin::Input group-->
              <div class="row mb-6">
                <!--begin::Label-->
                <label class="col-lg-4 col-form-label required fw-bold fs-6">Username</label>
                <!--end::Label-->
                <!--begin::Col-->
                <div class="col-lg-8 fv-row">
                  <input class="form-control form-control-lg form-control-solid {{ old('username') && !$errors->has('username') ? 'input-valid' : '' }} {{$errors->has('username') ? 'input-error' : ''}}" type="text" minlength="4" maxlength="20" placeholder="" name="username" autocomplete="off" value="{{ $user->username }}" required {{$errors->has('username') ? 'autofocus' : ''}} readonly/>
                  @if ($errors->has('username'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('username') }}</h4>
                  @endif
                </div>
                <!--end::Col-->
              </div>
              <!--end::Input group-->
              <!--begin::Input group-->
              <div class="row mb-6">
                <!--begin::Label-->
                <label class="col-lg-4 col-form-label required fw-bold fs-6">Group Prefix MT5</label>
                <!--end::Label-->
                <!--begin::Col-->
                <div class="col-lg-8 fv-row">
                  <input class="form-control form-control-lg form-control-solid {{ old('prefix') && !$errors->has('prefix') ? 'input-valid' : '' }} {{$errors->has('prefix') ? 'input-error' : ''}}" type="text" minlength="2" maxlength="3" placeholder="" name="prefix" autocomplete="off" value="{{ $user->groupMT5 }}" required {{$errors->has('prefix') ? 'autofocus' : ''}} required/>
                  @if ($errors->has('prefix'))
                      <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('prefix') }}</h4>
                  @endif
                </div>
                <!--end::Col-->
              </div>
              <!--end::Input group-->
            </div>
            <div class="col-md-6">
              <h4>Account Type : </h4>
              @foreach ($accountType as $type)
                @php
                  $accGroup = $user->groupMT5.'-'.$type->masterRate.'-'.$type->masterGroup;
                @endphp
                <!--begin::Input group-->
                <div class="row mb-0">
                  <!--begin::Label-->
                  <label class="col-lg-8 col-form-label fw-bold fs-6">{{$type->masterName}} {{$type->masterRate}} ({{ $user->groupMT5 }}-{{$type->masterRate}}-{{$type->masterGroup}})</label>
                  <!--begin::Label-->
                  <!--begin::Label-->
                  <div class="col-lg-4 d-flex align-items-center">
                    <div class="form-check form-check-solid form-switch fv-row">
                      <input class="form-check-input w-45px h-30px" type="checkbox" name="group[]" value="{{$type->id}}" {{$accountYes->contains('accountGroup', $accGroup) ? 'checked' : ''}}/>
                    </div>
                  </div>
                  <!--begin::Label-->
                </div>
                <!--end::Input group-->
              @endforeach
              @if ($errors->has('group'))
                  <h4 class="text-danger mt-6"> <i class="bi bi-exclamation-triangle text-danger fs-2 blink"></i> {{ $errors->first('group') }}</h4>
              @endif
            </div>
          </div>

          @csrf
          <!--begin::Actions-->
          <div class="card-footer d-flex justify-content-end py-6 px-9">

            <button type="submit" class="btn btn-primary" id="ot_button">Submit</button>
          </div>
          <!--end::Actions-->
        </div>
      </div>
    </div>
  </form>
</div>
