<!--begin::Navbar-->
<div class="card mb-5 rounded mb-xl-10">
  <div class="card-body pt-9 pb-0">
    <!--begin::Details-->
    <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
      <!--begin: Pic-->
      <div class="d-inline-flex me-7 mb-4">
        <div class="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative">
          <img src="{{$user->photo}}" />
          <div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-20px w-20px"></div>
        </div>

        <!--begin::User-->
        <div class="d-flex ms-6 flex-column">
          <!--begin::Name-->
          <div class="d-flex align-items-center mb-2">
            <a href="#" class="text-gray-800 text-hover-primary fs-2 fw-bolder me-1">{{$user->username ? $user->username : $user->name}}</a>

          </div>
          <!--end::Name-->
          <!--begin::Info-->
          <div class="d-flex flex-wrap fw-bold fs-6 mb-4 pe-2">
            <a href="#" class="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
              <!--begin::Svg Icon | path: icons/duotone/General/User.svg-->
              <span class="svg-icon svg-icon-4 me-1">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                  </g>
                </svg>
              </span>
              <!--end::Svg Icon-->ID-{{$user->id}}</a>
            </div>
            <!--end::Info-->
          </div>
          <!--end::User-->

        </div>
        <!--end::Pic-->
        <!--begin::Info-->
        <div class="flex-grow-1">
          <!--begin::Title-->
          <div class="d-flex justify-content-between align-items-start flex-wrap mb-2">

            <div class="d-inline-flex flex-column">

            </div>
            <!--begin::Actions-->
            <div class="d-inline-flex my-4">

              <form class="form-control" action="{{route('updateUserLevel',$user->uuid)}}" method="post">
                <div class="row">
                  <div class="col-lg-12 fv-row">
                    <select class="form-select form-select-solid" name="level" required>
                      <option value="">--Type--</option>
                      <option value="17" {{$user->id_cms_privileges == '17' ? "selected" : ""}} >IB</option>
                      <option value="16" {{$user->id_cms_privileges == '16' ? "selected" : ""}} >Senior IB</option>
                      <option value="15" {{$user->id_cms_privileges == '15' ? "selected" : ""}} >Master IB</option>
                    </select>
                  </div>
                </div>
                <div class="row mt-6">
                  <div class="col-lg-12 text-center">
                    @csrf
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </form>
            </div>
            <!--end::Actions-->

          </div>
          <!--end::Title-->

        </div>
        <!--end::Info-->
      </div>
      <!--end::Details-->
      <!--begin::Navs-->
      <div class="d-flex overflow-auto h-55px">
        <ul class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap">
          <!--begin::Nav item-->
          <li class="nav-item">
            <a class="nav-link text-active-primary me-6 {{isset($usermenu) && $usermenu == 'profile' ? 'active' : ''}}" href="{{route('viewShowPartner',$user->uuid)}}">Profile</a>
          </li>
          <!--end::Nav item-->

          <!--begin::Nav item-->
          <li class="nav-item">
            <a class="nav-link text-active-primary me-6 {{isset($usermenu) && $usermenu == 'deposit' ? 'active' : ''}}" href="{{route('viewDepositPartner',$user->uuid)}}">Deposit</a>
          </li>
          <!--end::Nav item-->

          <!--begin::Nav item-->
          <li class="nav-item">
            <a class="nav-link text-active-primary me-6 {{isset($usermenu) && $usermenu == 'withdrawal' ? 'active' : ''}}" href="{{route('viewWithdrawalPartner',$user->uuid)}}">Withdrawal</a>
          </li>
          <!--end::Nav item-->

          <!--begin::Nav item-->
          {{-- <li class="nav-item">
            <a class="nav-link text-active-primary me-6 {{isset($usermenu) && $usermenu == 'showBalanceStatement' ? 'active' : ''}}" href="{{route('viewBalanceStatement',$user->uuid)}}">Balance Statement</a>
          </li> --}}
          <!--end::Nav item-->
          {{--
          <!--begin::Nav item-->
          <li class="nav-item">
          <a class="nav-link text-active-primary me-6 {{isset($usermenu) && $usermenu == 'account-mt5' ? 'active' : ''}}" href="{{route('viewAccountmt5')}}">MT5</a>
        </li>
        <!--end::Nav item-->
        --}}
        <!--begin::Nav item-->
        {{-- <li class="nav-item">
          <a class="nav-link text-active-primary me-6 {{isset($usermenu) && $usermenu == 'showTradingStatement' ? 'active' : ''}}" href="{{route('viewTradingStatement',$user->uuid)}}">Trading Statement</a>
        </li> --}}
        <!--end::Nav item-->
        <!--begin::Nav item-->
        {{-- <li class="nav-item">
          <a class="nav-link text-active-primary me-6 {{isset($usermenu) && $usermenu == 'showMarginInOut' ? 'active' : ''}}" href="{{route('viewMarginInOut',$user->uuid)}}">Margin In-Out</a>
        </li> --}}
        <!--end::Nav item-->
      </ul>
    </div>
    <!--begin::Navs-->
  </div>
</div>
<!--end::Navbar-->
