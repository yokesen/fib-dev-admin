@extends('template.master')

@section('title','Promotion')
@section('bc-1','Promotion')
@section('bc-2','Redeem Poin')

@section('container')
  <div class="d-flex flex-column flex-root">
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <!--begin::Engage Widget 1-->
        <div class="card rounded mb-5 mb-xl-10">
          <!--begin::Body-->
          <div class="card-body pb-0">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column justify-content-between h-100">
              <!--begin::Section-->
              <div class="pt-15 mb-10">
                <!--begin::Title-->
                <h3 class="text-grey-800 text-center fs-1 fw-bolder lh-lg">Coming Soon!</h3>
                  <p class="text-grey-800 text-center">Nantinya Poin dari Trading Academy, Deposit, dan Lot yang Kamu tradingkan <br />bisa diredeem hadiah.</p>
                  <!--end::Title-->


                    <div class="text-center py-7">
                      <a href="{{route('viewDashboard')}}" class="btn btn-primary fs-6 px-6">Klik untuk kembali ke Dashboard</a>
                    </div>




                  <!--begin::Image-->
                  <div class="flex-grow-1 bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-bottom card-rounded-bottom h-200px" style="background-image:url('/assets/media/illustrations/coming-soon.png')"></div>
                  <!--end::Image-->
                </div>
                <!--end::Wrapper-->
              </div>
              <!--end::Body-->
            </div>
            <!--end::Engage Widget 1-->
      </div>
    </div>
  </div>
@endsection
