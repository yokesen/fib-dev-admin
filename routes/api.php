<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WhatsappInboundController;
use App\Http\Controllers\LabController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v3')->group(function () {
  Route::post('/lab/wa/inbound',[WhatsappInboundController::class,'inbound'])->name('labWaInbound');
});

Route::prefix('v1')->group(function () {
  Route::post('/check/password',[LabController::class,'checkPassword']);
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
