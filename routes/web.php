<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\EconomicCalendar;
use App\Http\Controllers\LoginSocialite;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GettingStartedController;
use App\Http\Controllers\MailVerificationController;
use App\Http\Controllers\ApiMySessionController;
use App\Http\Controllers\AuthRegisterController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\WhatsappInboundController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ImageUploaderController;
use App\Http\Controllers\LabController;
use App\Http\Controllers\AjaxSearchController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\DownloadMt5Controller;
use App\Http\Controllers\DepositController;
use App\Http\Controllers\WithdrawalController;
use App\Http\Controllers\PasswordController;
use App\Http\Controllers\InternalTransferController;
use App\Http\Controllers\SignalController;
use App\Http\Controllers\AcademyController;
use App\Http\Controllers\RedeemController;
use App\Http\Controllers\ReferralController;
use App\Http\Controllers\InfoController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\TrafficController;
use App\Http\Controllers\LeadController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PotentialController;
use App\Http\Controllers\WinningController;
use App\Http\Controllers\LosingController;
use App\Http\Controllers\TradingAccountController;
use App\Http\Controllers\StatementController;
use App\Http\Controllers\CommissionController;
use App\Http\Controllers\BalanceSheetController;
use App\Http\Controllers\ProfitLossController;
use App\Http\Controllers\MarginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PartnerListController;
use App\Http\Controllers\AccountTypeController;
use App\Http\Controllers\PartnerController;
use App\Http\Controllers\groupController;
use App\Http\Controllers\reconsileController;
use App\Http\Controllers\depositPartnerController;
/*
|--------------------------------------------------------------------------
| VIEW ROUTES
|--------------------------------------------------------------------------
|
*/
Route::get('/', [PageController::class,'viewLogin'])->name('viewHomepage');


  Route::get('/register', [PageController::class,'viewLogin'])->name('viewRegister');
  Route::get('/login', [PageController::class,'viewLogin'])->name('viewLogin');
  Route::get('/lost-password', [PageController::class,'viewLostPassword'])->name('viewLostPassword');
  Route::get('/new-password/{email}/verify/{code}', [PageController::class,'viewNewPassword'])->name('viewNewPassword');
  Route::get('/onboarding/mail/{email}/verify/{code}', [MailVerificationController::class,'verifyActionMail'])->name('verifyActionMail');


Route::get('/privacy-policy',function () {return view('desktop.privacy-policy');})->name('privacy-policy');
/*
|--------------------------------------------------------------------------
| VIEW ROUTES WITH AUTH
|--------------------------------------------------------------------------
|
*/
Route::group(['middleware' => 'auth'], function() {
    Route::get('/dashboard', [DashboardController::class,'viewDashboard'])->name('viewDashboard');

    /*ONBOARDING*/
    Route::get('/verify/whatsapp', [GettingStartedController::class,'verifyWhatsapp'])->name('verifyWhatsapp');
    Route::get('/verify/email', [GettingStartedController::class,'verifyEmail'])->name('verifyEmail');
    Route::get('/getting-started', [GettingStartedController::class,'viewGettingStarted'])->name('viewGettingStarted');
    Route::get('/onboarding/mail/send-code', [MailVerificationController::class,'askToVerifyMail'])->name('askToVerifyMail');
    Route::get('/app/do/onboarding', [GettingStartedController::class,'appDoOnboard'])->name('viewAppDoOnboard');

    /*INFO*/
    Route::get('/info/new-register',[InfoController::class,'level7NewRegister'])->name('level7info');
    Route::get('/info/verified-user',[InfoController::class,'level8NewRegister'])->name('level8info');
    Route::get('/info/trader',[InfoController::class,'level9NewRegister'])->name('level9info');
    Route::get('/info/pro-trader',[InfoController::class,'level10NewRegister'])->name('level10info');
    Route::get('/info/forbiden',[InfoController::class,'levelall'])->name('levelall');

    /*VIEW ONLY*/
    Route::get('/apps/economic-calendar', [EconomicCalendar::class,'calendar'])->name('viewEconomicCalendar');

    /*SESSION*/
    Route::get('/client/m/session',[ApiMySessionController::class,'mySession']);
    Route::get('/client/m/logout',[LogoutController::class,'destroy'])->name('logout');

    /*LAB*/
    Route::get('/lab/image/uploader',[LabController::class,'imageUploader']);

    /*PROFILE*/
    Route::get('/profile', [ProfileController::class,'profile'])->name('viewProfile');
    Route::get('/app/do/profile/edit', [GettingStartedController::class,'editProfile'])->name('viewAppDoEditProfile');
    Route::get('/app/do/bank/edit', [GettingStartedController::class,'editBank'])->name('viewAppDoEditBank');
    Route::get('/document/upload', [GettingStartedController::class,'uploadDocument'])->name('viewUploadDocument');

    /*REFERRAL SYSTEM*/
    Route::get('/business/referral',[ReferralController::class,'explore'])->name('referralSystem');
    Route::get('/ref/user/{id}',[ReferralController::class,'showUser'])->name('showUser');

    /*-----------------------------------------------LEVEL-LEVELAN--------------------------------------------------*/

    //Route::group(['middleware' => 'level7'], function() {
      /*FORM (NEED CAFFEINE)*/
      Route::get('/account/create', [AccountController::class,'viewtypeAccount'])->name('viewAccount');
      Route::get('/account-mt5/create', [AccountController::class,'viewtypeAccountmt5'])->name('viewAccountmt5');
      Route::get('/account-mt4/create', [AccountController::class,'viewtypeAccountmt4'])->name('viewAccountmt4');
  //  });

  //  Route::group(['middleware' => 'level8'], function() {
      Route::get('/deposit/create', [DepositController::class,'viewCreateDeposit'])->name('viewDeposit');
      Route::get('/deposit/do/{account}/create', [DepositController::class,'viewCreateDepositfromMt5'])->name('viewDepositfromMt5');
      Route::get('/withdrawal/create', [WithdrawalController::class,'viewCreateWithdrawal'])->name('viewWithdrawal');
      Route::get('/withdrawal/do/{account}/create', [WithdrawalController::class,'viewCreateWithdrawalfromMt5'])->name('viewCreateWithdrawalfromMt5');
      Route::get('/internal-transfer/create', [InternalTransferController::class,'create'])->name('createInternalTransfer');

      Route::get('/account/list', [AccountController::class,'viewListAccount'])->name('view-list-account');
      Route::get('/mt4/list', [AccountController::class,'viewListAccountmt4'])->name('view-list-account-mt4');
      Route::get('/mt5/list', [AccountController::class,'viewListAccountmt5'])->name('view-list-account-mt5');
      Route::get('/account/download-mt5', [DownloadMt5Controller::class,'downloadPage'])->name('viewDownloadMt5');
      Route::get('/account/webtrader', [DownloadMt5Controller::class,'demoWebTrader'])->name('viewDemoWebTrader');
      Route::get('/deposit/list', [DepositController::class,'depositList'])->name('viewDepositList');
      Route::get('/deposit/{id}/{account}/{uuid}', [DepositController::class,'depositInvoice'])->name('viewDepositInvoice');
      Route::get('/withdrawal/list', [WithdrawalController::class,'withdrawalList'])->name('viewWithdrawalList');
      Route::get('/withdrawal/{id}/{account}/{uuid}', [WithdrawalController::class,'withdrawalInvoice'])->name('viewWithdrawalInvoice');
      Route::get('/internal-transfer/list', [InternalTransferController::class,'internalTransferList'])->name('viewInternalTransferList');
      Route::get('/internal-transfer/{id}/{account}/{uuid}', [InternalTransferController::class,'internalTransferInvoice'])->name('viewInternalTransferInvoice');

      /*ACADEMY*/
      Route::get('/academy/explore',[AcademyController::class,'viewAcademyExplore'])->name('viewAcademyExplore');
      Route::get('/academy/video/{uavid}',[AcademyController::class,'showAcademy'])->name('showAcademy');
      Route::get('/academy/forexflix',[AcademyController::class,'viewForexFlix'])->name('viewForexFlix');
  //  });

  //  Route::group(['middleware' => 'level9'], function() {
      /*SIGNAL VIEW*/
      Route::get('/signal/{id}',[SignalController::class,'viewSignalDynamic'])->name('viewSignalDynamic');
      Route::get('/signal/wingman',[SignalController::class,'viewSignalWingman'])->name('viewSignalWingman');
      Route::get('/signal/money-monster',[SignalController::class,'viewSignalMoneyMonster'])->name('viewSignalMoneyMonster');
      Route::get('/signal/market-radar',[SignalController::class,'viewSignalMarketRadar'])->name('viewSignalMarketRadar');
      Route::get('/signal/video-analysis',[SignalController::class,'viewSignalVideoAnalysis'])->name('viewSignalVideoAnalysis');


      /*REDEEM*/
      Route::get('/promotion/gift/list',[RedeemController::class,'viewgiftList'])->name('viewgiftList');
//    });

    Route::group(['prefix' => 'platform'], function() {

      Route::get('/my-client',[ClientController::class,'viewMyClient'])->name('viewMyClient');
      Route::get('/traffic',[TrafficController::class,'viewTraffic'])->name('viewTraffic');
      Route::get('/lead',[LeadController::class,'viewLead'])->name('viewLead');
      Route::get('/contact',[ContactController::class,'viewContact'])->name('viewContact');
      Route::get('/potential',[PotentialController::class,'viewPotential'])->name('viewPotential');
      Route::get('/winning',[WinningController::class,'viewWinning'])->name('viewWinning');
      Route::get('/losing',[LosingController::class,'viewLosing'])->name('viewLosing');
      Route::get('/trading-account',[TradingAccountController::class,'viewTradingAccount'])->name('viewTradingAccount');
      Route::get('/statement',[StatementController::class,'viewStatement'])->name('viewStatement');
      Route::get('/commission',[CommissionController::class,'viewCommission'])->name('viewCommission');
      Route::get('/balance-sheet',[BalanceSheetController::class,'viewBalance'])->name('viewBalance');
      Route::get('/profit-loss',[ProfitLossController::class,'viewProfitLoss'])->name('viewProfitLoss');
      Route::get('/margin-in',[MarginController::class,'viewMarginIn'])->name('viewMarginIn');
      Route::get('/margin-out',[MarginController::class,'viewMarginOut'])->name('viewMarginOut');
      Route::get('/margin-internal',[MarginController::class,'viewInternalTransfer'])->name('viewInternalTransfer');

      Route::get('/partner/add-new',[PartnerListController::class,'addNew'])->name('viewPartnerAddNew');
      Route::get('/partner/register-from-web',[PartnerListController::class,'registerFromWeb'])->name('viewPartnerRegisterFromWeb');
      Route::get('/partner/partner-list',[PartnerListController::class,'partnerList'])->name('viewPartnerList');
      Route::get('/partner/active-partner',[PartnerListController::class,'activePartner'])->name('viewPartnerActive');
      Route::get('/partner/suspended-partner',[PartnerListController::class,'suspendedPartner'])->name('viewPartnerSuspended');
      Route::get('/partner/default-setting',[PartnerListController::class,'defaultSetting'])->name('viewPartnerdefaultSetting');
      Route::get('/partner/dailySettlement',[PartnerListController::class,'dailySettlement'])->name('viewDailySettlement');
      Route::get('/partner/action/{uuid}',[PartnerListController::class,'showPartner'])->name('viewShowPartner'); 
      Route::get('/partner/balance-statement/{uuid}',[PartnerListController::class,'showBalanceStatement'])->name('viewBalanceStatement');
      Route::get('/partner/trading-statement/{uuid}',[PartnerListController::class,'showTradingStatement'])->name('viewTradingStatement');
      Route::get('/partner/trading-statement-per-client/{uuid}/{week}/{year}',[PartnerListController::class,'showTradingStatementClient'])->name('viewTradingStatementClient');
      Route::get('/partner/trading-statement-per-account/{uuidp}/{uuid}/{week}/{year}',[PartnerListController::class,'showTradingStatementAccount'])->name('viewTradingStatementAccount');
      Route::get('/partner/margin-in-out/{uuid}',[PartnerListController::class,'showMarginInOut'])->name('viewMarginInOut');

      Route::get('/partner/deposit/{uuid}',[depositPartnerController::class,'viewDepositPartner'])->name('viewDepositPartner');
      Route::get('/partner/withdrawal/{uuid}',[depositPartnerController::class,'viewWithdrawalPartner'])->name('viewWithdrawalPartner');


      Route::get('/account/type',[AccountTypeController::class,'typeList'])->name('viewAccountTypeList');
      Route::get('/account/add-new',[AccountTypeController::class,'addNew'])->name('viewAccountAddNew');

    });

    Route::get('/user/list',[UserController::class,'index'])->name('indexUser');
    Route::get('/user/create',[UserController::class,'create'])->name('createUser');
    Route::get('/user/show/{id}',[UserController::class,'show'])->name('showUser');
    Route::get('/user/edit/{id}',[UserController::class,'edit'])->name('editUser');

    /* DEALING */
    /*----------------------------GET---------------------------------*/
    Route::get('/dashboard', [DashboardController::class,'viewDashboard'])->name('viewDashboard');
    Route::get('/client/m/logout',[LogoutController::class,'destroy'])->name('logout');
    Route::group(['prefix' => 'platform'], function() {
      Route::get('/group/request',[groupController::class,'listRequest'])->name('groupRequest');
      Route::get('/group/approval/{id}',[groupController::class,'viewApproval'])->name('groupApproval');
      Route::get('/group/activation',[groupController::class,'listActivation'])->name('groupActivation');
      Route::get('/group/activation/{id}',[groupController::class,'processActivate'])->name('groupProcessActivation');
    });


    /*FINANCE*/
    Route::group(['prefix' => 'platform'], function() {
    Route::get('/balance-sheet/list', [reconsileController::class,'list'])->name('viewReconsileList');
    Route::get('/balance-sheet/action/{id}', [reconsileController::class,'action'])->name('viewReconsileAction');

    Route::get('/reconsile/list',[reconsileController::class,'cocokan'])->name('cocokan');
    Route::get('/reconsile/detail/{parent}/{week}/{year}',[reconsileController::class,'invoice'])->name('invoiceReconsile');
    Route::get('/reconsile/{hash}',[reconsileController::class,'detailPerAccount'])->name('detailPerAccount');
    Route::get('/trade-statement/{hash}',[reconsileController::class,'statementPerAccount'])->name('statementPerAccount');

    Route::get('/deposit/list', [depositPartnerController::class,'list'])->name('viewDepositList');
    Route::get('/deposit/action/{id}', [depositPartnerController::class,'action'])->name('viewDepositAction');

    route::get('/trading-statement/{week}/{year}/{parent}',[reconsileController::class,'statement'])->name('trading-statement');
    route::get('/last-statement/{week}/{year}',[reconsileController::class,'lastStatement'])->name('last-statement');

    Route::get('/upload/summary', [UploadController::class,'viewUploadSummary'])->name('viewUploadSummary');
    Route::get('/upload/equity', [UploadController::class,'viewUploadEquity'])->name('viewUploadEquity');
    Route::get('/upload/deposit-withdraw', [UploadController::class,'viewUploadDepositWd'])->name('viewUploadDepositWd');
    Route::get('/upload/daily-report', [UploadController::class,'viewUploadDailyReport'])->name('viewUploadDailyReport');

  });
});

/*
|--------------------------------------------------------------------------
| PROCESS ROUTES
|--------------------------------------------------------------------------
|
*/

Route::get('login/oAuth/process/{provider}', [LoginSocialite::class,'redirectToSocialite'])->name('process-login-oAuth');
Route::get('login/oAuth/callback/{provider}', [LoginSocialite::class,'handleSocialiteCallback'])->name('process-callback-oAuth');
Route::get('login/oAuth/failed/{provider}', [LoginSocialite::class,'handleSocialiteFailed'])->name('process-failed-oAuth');
Route::get('login/oAuth/unsubscribe/{provider}', [LoginSocialite::class,'handleSocialiteUnsubscribe'])->name('process-unsubscribe-oAuth');
/*LOGIN REGISTER*/
Route::post('/auth/register',[AuthRegisterController::class,'submitRegister'])->name('process-Register');
Route::post('/auth/login',[LoginController::class,'submitLogin'])->name('process-Login');
/*LOST PASSWORD*/
Route::post('/checking/lost-password/process',[PasswordController::class,'checkingprocess'])->name('passwordCheckingProcess');
Route::post('/change/lost-password/process',[PasswordController::class,'changeprocess'])->name('changePassword');

Route::group(['middleware' => 'auth'], function() {
  /*ONBOARDING PROCESS*/
  Route::post('/p/v3/getting-started',[GettingStartedController::class,'SubmitGettingStarted']);
  Route::post('/p/v3/verify-mail',[GettingStartedController::class,'sendVerifyEmail']);
  Route::post('/p/v3/verifying-mail',[GettingStartedController::class,'verifiyingEmail']);

  /*PROFILE PROCESS*/
  Route::post('/profile/edit',[ProfileController::class,'editProfilePost'])->name('process-Edit-Profile');
  Route::post('/bank/edit',[ProfileController::class,'editBankPost'])->name('process-Edit-Bank');
  Route::post('/upload/docs',[ProfileController::class,'submitBukti'])->name('process-submit-bukti');

  /*ACCOUNT MT5 PROCESS*/
  Route::post('/creating/account',[AccountController::class,'createAccount'])->name('process-create-account');
  Route::post('/creating/mt5',[AccountController::class,'createAccountMt5'])->name('process-create-mt5');
  Route::post('/creating/mt4',[AccountController::class,'createAccountMt4'])->name('process-create-mt4');

  /*DEPOSIT PROCESS*/
  Route::post('/deposit/submit',[DepositController::class,'submitDeposit'])->name('process-Submit-Deposit');
  /*WITHDRAWAL PROCESS*/
  Route::post('/withdrawal/submit',[WithdrawalController::class,'submitWithdrawal'])->name('process-Submit-Withdrawal');
  /*INTERNAL TRANSFER PROCESS*/
  Route::post('/internal-transfer/submit', [InternalTransferController::class,'submitInternal'])->name('process-Submit-InternalTransfer');

  /*UPLOAD IMAGE PROCESS*/
  Route::post('/upload/image',[ImageUploaderController::class,'uploadImage'])->name('process-Upload-Image');
  Route::post('/upload/bukuTabungan',[ProfileController::class,'uploadBukuTabungan'])->name('process-Upload-Tabungan');
  Route::post('/upload/Ktp',[ProfileController::class,'uploadKtp'])->name('process-Upload-Ktp');
  Route::post('/upload/deposit',[DepositController::class,'uploadDeposit'])->name('process-Upload-Deposit');

  /*AJAX SEARCH*/
  Route::post('/search/username',[AjaxSearchController::class,'ajaxSearchUsername'])->name('ajaxSearchUsername');
  Route::post('/search/whatsapp',[AjaxSearchController::class,'ajaxSearchWhatsapp'])->name('ajaxSearchWhatsapp');
  Route::post('/search/bank-custodian',[AjaxSearchController::class,'ajaxSearchBankCustodian'])->name('ajaxSearchBankCustodian');
  Route::post('/search/account-mt4',[AjaxSearchController::class,'ajaxSearchAccountMt4'])->name('ajaxSearchAccountMt4');

  /*PROCESS ACADEMY*/
  Route::post('/academy/quiz',[AcademyController::class,'submitQuiz'])->name('academySubmitQuiz');


  Route::post('/user/store',[UserController::class,'store'])->name('storeUser');
  Route::post('/user/update/{id}',[UserController::class,'update'])->name('updateUser');
  Route::post('/user/update-bank/{id}',[UserController::class,'editBank'])->name('updateUserBank');
  Route::post('/user/update-level/{id}',[UserController::class,'editLevel'])->name('updateUserLevel');
  Route::post('/user/update-ib/{id}',[UserController::class,'editParent'])->name('updateParent');
  Route::post('/user/upload-rekening/{id}',[UserController::class,'uploadRekening'])->name('updateUseruploadRekening');
  Route::post('/user/upload-ktp/{id}',[UserController::class,'uploadKTP'])->name('updateUseruploadKTP');

  Route::post('/deposit/by-admin',[DepositByAdminController::class,'submitDeposit'])->name('submitDepositbyAdmin');
  Route::post('/deposit/search/account',[DepositByAdminController::class,'ajaxSearchAccountMt4'])->name('ajaxSearchAccountMt4');
  Route::post('/withdrawal/by-admin',[DepositByAdminController::class,'submitWithdrawal'])->name('submitWithdrawalbyAdmin');
  Route::post('/metatrader-4/create/by-admin',[AccountController::class,'createAccountMt4'])->name('createAccountMt4byAdmin');

  Route::group(['prefix' => 'platform'], function() {
    Route::post('/partner/store',[PartnerListController::class,'store'])->name('storeNewPartner');
    Route::post('/account/store',[AccountTypeController::class,'store'])->name('storeNewAccountType');
    Route::post('/partner/edit',[PartnerListController::class,'edit'])->name('editNewPartner');
    Route::post('/deposit/submit',[PartnerController::class,'submitDepositCredit'])->name('submitDepositCredit');
  });

  /*DEALING*/
  /*----------------------------POST---------------------------------*/
  Route::post('/group/process/approve',[groupController::class,'processApprove'])->name('groupProcessApproval');

  /*FINANCE*/
  /*----------------------------POST---------------------------------*/
  Route::post('/reconsile/process/action',[reconsileController::class,'submitAction'])->name('processReconsileAction');
  Route::post('/deposit/process', [depositPartnerController::class,'process'])->name('depositProcess');
  Route::post('/deposit/reject', [depositPartnerController::class,'reject'])->name('depositReject');

  Route::post('/uploading/summary', [UploadController::class,'processUploadSummary'])->name('processUploadSummary');
  Route::post('/uploading/equity', [UploadController::class,'processUploadEquity'])->name('processUploadEquity');
  Route::post('/uploading/deposit-withdraw', [UploadController::class,'processUploadDepositWd'])->name('processUploadDepositWd');
  Route::post('/uploading/daily-report', [UploadController::class,'processUploadDailyReport'])->name('processUploadDailyReport');
});
/*
|--------------------------------------------------------------------------
| LAB ROUTES
|--------------------------------------------------------------------------
|
*/

Route::get('/lab/user-status',[LabController::class,'userStatus']);
Route::get('/lab/user-winning',[LabController::class,'userWinning']);
Route::get('/lab/parent-deposit',[LabController::class,'parentDeposit']);
Route::get('/lab/parent-get',[LabController::class,'bikinParent']);

Route::get('/struktur-ib',[LabController::class,'strukturib']);
