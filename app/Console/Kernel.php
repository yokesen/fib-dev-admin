<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->call('App\Http\Controllers\LabController@parentDeposit')->everyMinute();
        $schedule->call('App\Http\Controllers\LabController@parentWithdrawal')->everyMinute();
        $schedule->call('App\Http\Controllers\LabController@parentMetatrader')->everyMinute();

        $schedule->call('App\Http\Controllers\LabController@userStatus')->everyMinute();
        $schedule->call('App\Http\Controllers\LabController@userWinning')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
