<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class PotentialController extends Controller
{
  public function viewPotential(){
    $menu = 'viewPotential';
    /*-------------------*/
    $users = DB::table('users_cabinet')->orderby('id','desc')->where('parent',profile()->id)->where('status','potential')->paginate(20);
    return view('platform.potential',compact('menu','users'));
  }
}
