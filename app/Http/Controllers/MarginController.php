<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class MarginController extends Controller
{
  public function viewMarginIn(){
    $menu = 'viewMarginIn';
    /*-------------------*/
    $margin = DB::table('deposits')
                  ->join('users_cabinet','users_cabinet.uuid','deposits.uuid')
                  ->where('deposits.parent',profile()->id)
                  ->where('deposits.from_bank','!=','internal transfer')
                  ->orderby('deposits.created_at','desc')
                  ->select(
                    'users_cabinet.username',
                    'deposits.created_at',
                    'deposits.metatrader',
                    'deposits.amount',
                    'deposits.status',
                    'deposits.reason',
                    )
                  ->paginate(20);
    return view('platform.margin-in',compact('menu','margin'));
  }

  public function viewMarginOut(){
    $menu = 'viewMarginOut';
    /*-------------------*/
    $margin = DB::table('withdrawals')
                  ->join('users_cabinet','users_cabinet.uuid','withdrawals.uuid')
                  ->where('withdrawals.parent',profile()->id)
                  ->whereNull('withdrawals.internal')
                  ->orderby('withdrawals.created_at','desc')
                  ->select(
                    'users_cabinet.username',
                    'withdrawals.created_at',
                    'withdrawals.metatrader',
                    'withdrawals.approved_amount',
                    'withdrawals.status',
                    'withdrawals.reason',
                    )
                  ->paginate(20);
    return view('platform.margin-out',compact('menu','margin'));
  }

  public function viewInternalTransfer(){
    $menu = 'viewInternalTransfer';
    /*-------------------*/
    $margin = DB::table('withdrawals')
                  ->join('users_cabinet','users_cabinet.uuid','withdrawals.uuid')
                  ->where('withdrawals.parent',profile()->id)
                  ->where('withdrawals.internal',1)
                  ->orderby('withdrawals.created_at','desc')
                  ->select(
                    'users_cabinet.username',
                    'withdrawals.created_at',
                    'withdrawals.metatrader',
                    'withdrawals.internalToAccountMeta',
                    'withdrawals.approved_amount',
                    'withdrawals.status',
                    'withdrawals.reason',
                    )
                  ->paginate(20);
    return view('platform.margin-internal',compact('menu','margin'));
  }
}
