<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;
use Validator;
use Alert;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Hash;

class PartnerListController extends Controller
{
    public function registerFromWeb(){
      $menu = 'registerFromWeb';
      /*-------------------*/
      $users = DB::table('users_ib')->orderby('id','desc')->where('status','pending')->where('first_landing','website')->paginate(20);
      return view('platform.partner.register-from-web',compact('menu','users'));
    }

    public function partnerList(){
      $menu = 'partnerList';
      /*-------------------*/
      $users = DB::table('users_ib')->orderby('id','desc')->paginate(20);
      return view('platform.partner.partner-list',compact('menu','users'));
    }

    public function activePartner(){
      $menu = 'activePartner';
      /*-------------------*/
      $users = DB::table('users_ib')->orderby('id','desc')->where('status','active')->paginate(20);
      return view('platform.partner.active-partner',compact('menu','users'));
    }

    public function suspendedPartner(){
      $menu = 'suspendedPartner';
      /*-------------------*/
      $users = DB::table('users_ib')->orderby('id','desc')->where('status','suspended')->paginate(20);
      return view('platform.partner.suspended-partner',compact('menu','users'));
    }

    public function defaultSetting(){
      $menu = 'defaultSetting';
      /*-------------------*/

      return view('platform.partner.default-setting',compact('menu'));
    }

    public function addNew(){
      $menu = 'addNew';
      /*-------------------*/
      $setting_sdtp = DB::table('setting_all')->where('type','senior_default_taking_position')->first();
      $senior_default_taking_position = $setting_sdtp->value;

      $setting_sntp = DB::table('setting_all')->where('type','senior_minimum_taking_position')->first();
      $senior_minimum_taking_position = $setting_sntp->value;

      $setting_smtp = DB::table('setting_all')->where('type','senior_maximum_taking_position')->first();
      $senior_maximum_taking_position = $setting_smtp->value;

      $accountType = DB::table('typeMT4MasterTemplate')->where('masterStatus','active')->get();
      return view('platform.partner.create',compact('menu','accountType','senior_default_taking_position','senior_minimum_taking_position','senior_maximum_taking_position'));
    }

    public function dailySettlement(){
      $menu = 'dailySettlement';
      /*-------------------*/
      $users = DB::table('users_ib')->orderby('id','desc')->where('dailySettlementLimit','>',0)->paginate(20);
      return view('platform.partner.daily-settlement',compact('menu','users'));
    }

    public function store(Request $request){
      $rules = [
        'name' => 'required|string|min:3|max:50',
        'email' => 'required|string|min:6|max:100',
        'password' => 'required|string|min:6|max:100',
        'freeMarginStatus' => 'required|string',
        'depositGuarantee' => 'required|numeric',
        'creditGiven' => 'required|numeric|min:5000',
        'takingPositionPercentage' => 'required|numeric|min:20|max:95',
        'dailySettlementLimit' => 'required|numeric|min:1000',
        'username' => 'required|string|min:3',
        'prefix' => 'required|string|min:3|max:6',
        'group' => 'required|array|min:1'
      ];

      $validator = Validator::make($request->all(), $rules);

      if($validator->fails()){
          return redirect()->back()->withErrors($validator)->withInput($request->all());
      }

      $timesufix = substr(strval(time()),-2);

      $dbUsername = DB::table('cities')->where('used','0')->first();
      $sufix = preg_replace("/[^A-Za-z0-9 ]/", '', $dbUsername->name);
      $sufix = strtoupper($sufix);
      $sufix = substr($sufix,-2);
      $sufix .= $timesufix;

      $name = $request->name;

      $prefixs = explode(' ',$name);
      $prefix = '';
      foreach ($prefixs as $pre) {
        $pref = substr($pre,0,1);
        $prefix .= strtoupper($pref);
      }
      $username = $prefix.$sufix;
      $group = $prefix.$timesufix;
      // /dd($username,$group);
      $updateDbUsername =  DB::table('cities')->where('id',$dbUsername->id)->update([
        'used' => '1'
      ]);

      //dd($request->all());
      $uuid = sha1($request->email).time();

      $saveUserIb = DB::table('users_ib')->insertGetId([
        'uuid' => $uuid,
        'name' => $request->name,
        'username' => $username,
        'groupMT5' => $group,
        'email' => $request->email,
        'email_validation' => 'new',
        'email_verification' => 'new',
        'password' => Hash::make($request->password),
        'phone' => $request->whatsapp,
        'whatsapp' => $request->whatsapp,
        'providerId' => 'admin',
        'providerOrigin' => 'manual',
        'id_cms_privileges' => '50',
        'parent' => '0',
        'freeMarginStatus' => $request->freeMarginStatus,
        'depositGuarantee' => $request->depositGuarantee,
        'creditGiven' => $request->creditGiven,
        'takingPositionPercentage' => $request->takingPositionPercentage,
        'dailySettlementLimit' => $request->dailySettlementLimit,
        'status' => 'pending'
      ]);

      // $saveTypeCategory = DB::table('typeMT4Category')->insert([
      //   'id' => $saveUserIb,
      //   'namaCategory' => $username,
      //   'status' => 'active'
      // ]);
      //
       foreach ($request->group as $v) {
         $grup = DB::table('typeMT4MasterTemplate')->where('id',$v)->first();
      
         $group = $group."-".$grup->masterRate."-".$grup->masterGroup;
      
         $savetypeMT4Accounts = DB::table('typeMT4_accounts_choose_level')->insert([
          'categoryAccount' => $saveUserIb,
          'namaAccount' => $grup->masterName,
          'accountGroup' => $group,
          'bestChoice' => "yes",
          'status' => "active",
        ]);
      //
      //   $savetypeMT4Accounts = DB::table('typeMT4GroupList')->insert([
      //     'typeAccount' => $grup->masterName,
      //     'groupName' => $grup->masterNumber,
      //     'status' => "active",
      //   ]);
       }

      return redirect()->route('viewPartnerList');
    }

    public function edit(Request $request){

      $rules = [
        'name' => 'required|string|min:3|max:50',
        'email' => 'required|string|min:6|max:100',
        'freeMarginStatus' => 'required|string',
        'depositGuarantee' => 'required|numeric',
        'creditGiven' => 'required|numeric|min:5000',
        'takingPositionPercentage' => 'required|numeric|min:20|max:95',
        'dailySettlementLimit' => 'required|numeric|min:1000',
        'username' => 'required|string|min:3',
        'prefix' => 'required|string|min:3|max:6',
        'group' => 'required|array|min:1'
      ];

      $validator = Validator::make($request->all(), $rules);

      if($validator->fails()){

          return redirect()->back()->withErrors($validator)->withInput($request->all());
      }

      $uuid = sha1($request->email).time();
      $ib = DB::table('users_ib')->where('email',$request->email)->first();
      $saveUserIb = $ib->id;

      $update = DB::table('users_ib')->where('uuid',$ib->uuid)->update([
        'name' => $request->name,
        'username' => $request->username,
        'groupMT5' => $request->prefix,
        'email' => $request->email,
        'phone' => $request->whatsapp,
        'whatsapp' => $request->whatsapp,
        'freeMarginStatus' => $request->freeMarginStatus,
        'depositGuarantee' => $request->depositGuarantee,
        'creditGiven' => $request->creditGiven,
        'takingPositionPercentage' => $request->takingPositionPercentage,
        'dailySettlementLimit' => $request->dailySettlementLimit
      ]);

      $master = DB::table('typeMT4MasterTemplate')->get();

      foreach ($master as $mtr) {
        $group = $request->prefix."-".$mtr->masterRate."-".$mtr->masterGroup;
        //echo $group."<br>";
        if (in_array($mtr->id, $request->group)) {
          echo $mtr->id." is in array <br>";

          $checktypechooseaccount = DB::table('typeMT4_accounts_choose_level')->where('categoryAccount',$saveUserIb)->where('accountGroup',$group)->count();
          if ($checktypechooseaccount > 0) {
            $checkStatustypeMT4Accounts = DB::table('typeMT4_accounts_choose_level')->where('categoryAccount',$saveUserIb)->where('accountGroup',$group)->first();
            // if ($checkStatustypeMT4Accounts->status == 'inactive') {
              $updateTypeMT4Accounts = DB::table('typeMT4_accounts_choose_level')->where('categoryAccount',$saveUserIb)->where('accountGroup',$group)->update([
                'status' => 'active'
              ]);
            // }
          }else{
            $savetypeMT4Accounts = DB::table('typeMT4_accounts_choose_level')->insert([
              'categoryAccount' => $saveUserIb,
              'namaAccount' => $mtr->masterName,
              'accountGroup' => $group,
              'bestChoice' => "yes",
              'status' => "pending",
            ]);
          }


          $checktypeMT4Accounts = DB::table('typeMT4Accounts')->where('categoryAccount',$saveUserIb)->where('accountGroup',$group)->count();
          if ($checktypeMT4Accounts > 0) {
            $checkStatustypeMT4Accounts = DB::table('typeMT4Accounts')->where('categoryAccount',$saveUserIb)->where('accountGroup',$group)->first();
            // if ($checkStatustypeMT4Accounts->status == 'inactive') {
              $updateTypeMT4Accounts = DB::table('typeMT4Accounts')->where('categoryAccount',$saveUserIb)->where('accountGroup',$group)->update([
                'status' => 'active'
              ]);
            // }
          }else {
              $savetypeMT4Accounts = DB::table('typeMT4Accounts')->insert([
                'categoryAccount' => $saveUserIb,
                'namaAccount' => $mtr->masterName,
                'accountGroup' => $group,
                'bestChoice' => "yes",
                'status' => "pending",
              ]);
              $savetypeMT4Accounts = DB::table('typeMT4GroupList')->insert([
                  'typeAccount' => $mtr->masterName,
                  'groupName' => $mtr->masterNumber,
                  'status' => "active",
                ]);
          }
        }else {
          //echo $mtr->id." is NOT in array <br>";

          $checktypeMT4Accounts = DB::table('typeMT4Accounts')->where('categoryAccount',$saveUserIb)->where('accountGroup',$group)->count();
          if ($checktypeMT4Accounts > 0) {
            $updateTypeMT4Accounts = DB::table('typeMT4Accounts')->where('categoryAccount',$saveUserIb)->where('accountGroup',$group)->update([
              'status' => 'inactive'
            ]);
          }else {
              //echo "if not exit then SKIP<br>";
          }

          $checktypeMT4Accounts = DB::table('typeMT4_accounts_choose_level')->where('categoryAccount',$saveUserIb)->where('accountGroup',$group)->count();
          if ($checktypeMT4Accounts > 0) {
            $updateTypeMT4Accounts = DB::table('typeMT4_accounts_choose_level')->where('categoryAccount',$saveUserIb)->where('accountGroup',$group)->update([
              'status' => 'inactive'
            ]);
          }else {
              //echo "if not exit then SKIP<br>";
          }
        }
      }


      return redirect()->back();
    }

    public function showPartner($uuid){
      $menu = 'partnerList';
      $usermenu = 'profile';
      $user = DB::table('users_ib')->where('uuid',$uuid)->first();
      $parent = DB::table('users_ib')->where('parent',$user->parent)->first();
      $accountType = DB::table('typeMT4MasterTemplate')->where('masterStatus','active')->get();
      $accountYes = DB::table('typeMT4Accounts')->where('categoryAccount',$user->id)->where('status','!=','inactive')->get(); 
      $deposits = DB::table('partner_deposits')->where('uuid',$uuid)->orderby('id','desc')->paginate(7);
      $bank = DB::table('banks')->where('uuid',$uuid)->orderby('id','desc')->first();
      return view('platform.partner.user-card',compact('usermenu','menu','user','parent','accountType','accountYes','deposits','bank'));
    }

    public function showBalanceStatement($uuid){
      $menu = 'partnerList';
      $usermenu = 'showBalanceStatement';
      $user = DB::table('users_ib')->where('uuid',$uuid)->first();
      $statements = DB::table('statement_balance_sheet')->where('parent',$user->id)->orderby('id','asc')->get();
      $accountType = DB::table('typeMT4MasterTemplate')->where('masterStatus','active')->get();
      $accountYes = DB::table('typeMT4Accounts')->where('categoryAccount',$user->id)->where('status','!=','inactive')->get();
      $deposits = DB::table('partner_deposits')->where('uuid',$uuid)->orderby('id','desc')->paginate(7);
      $bank = DB::table('banks')->where('uuid',$uuid)->orderby('id','desc')->first();
      //dd($uuid,$bank);
      return view('platform.partner.balance-statement',compact('usermenu','menu','user','statements','accountType','accountYes','deposits','bank'));
    }

    public function showTradingStatement($uuid){
      $menu = 'partnerList';
      $usermenu = 'showTradingStatement';
      $user = DB::table('users_ib')->where('uuid',$uuid)->first();
      $weeklyClosedStatements = DB::table('statement_partner_weekly')->where('parent',$user->id)->whereNotNull('weekoty')->orderby('id','desc')->get();
      return view('platform.partner.trading-statement',compact('usermenu','menu','user','weeklyClosedStatements'));
    }

    public function showTradingStatementClient($uuid,$week,$year){
      $menu = 'partnerList';
      $usermenu = 'showTradingStatement';
      $user = DB::table('users_ib')->where('uuid',$uuid)->first();
      $weeklyClosedStatements = DB::table('statement_client_weekly')->join('users_cabinet','users_cabinet.uuid','statement_client_weekly.uuid')->where('statement_client_weekly.parent',$user->id)->where('statement_client_weekly.weekoty',$week)->where('statement_client_weekly.year',$year)->orderby('statement_client_weekly.id','desc')->get();
      //dd($weeklyClosedStatements);
      return view('platform.partner.trading-statement-client',compact('usermenu','menu','user','weeklyClosedStatements'));
    }

    public function showTradingStatementAccount($uuidp,$uuid,$week,$year){
      $menu = 'partnerList';
      $usermenu = 'showTradingStatement';
      $user = DB::table('users_ib')->where('uuid',$uuidp)->first();
      $weeklyClosedStatements = DB::table('raw_data_deal')->where('uuid',$uuid)->where('lotweek',$week)->where('lotyear',$year)->where('Entry','1')->where('Action','<','2')->orderby('Time','desc')->get();
      $distinct = DB::table('raw_data_position')->where('uuid',$uuid)->where('lotweek',$week)->where('lotyear',$year)->distinct()->select('Position')->get();
      $weeklyFloatingStatements = [];
      foreach ($distinct as $v) {
        $arr = DB::table('raw_data_position')->where('Position',$v->Position)->orderby('humanTime','desc')->first();
        $weeklyFloatingStatements[] = $arr;
      }
      //dd($weeklyFloatingStatements);
      return view('platform.partner.trading-statement-account',compact('usermenu','menu','user','weeklyClosedStatements','weeklyFloatingStatements'));
    }

    public function showMarginInOut($uuid){
      $menu = 'partnerList';
      $usermenu = 'showMarginInOut';
      $user = DB::table('users_ib')->where('uuid',$uuid)->first();
      $parent = DB::table('users_ib')->where('parent',$user->parent)->first();
      $accountType = DB::table('typeMT4MasterTemplate')->where('masterStatus','active')->get();
      $accountYes = DB::table('typeMT4Accounts')->where('categoryAccount',$user->id)->where('status','!=','inactive')->get();
      $deposits = DB::table('partner_deposits')->where('uuid',$uuid)->orderby('id','desc')->paginate(7);
      $bank = DB::table('banks')->where('uuid',$uuid)->orderby('id','desc')->first();
      return view('platform.partner.balance-statement',compact('usermenu','menu','user','parent','accountType','accountYes','deposits','bank'));
    }

}
