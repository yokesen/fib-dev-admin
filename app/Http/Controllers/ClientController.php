<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class ClientController extends Controller
{
    public function viewMyClient(){
      $menu = 'viewMyClient';
      /*-------------------*/
      $users = DB::table('users_cabinet')->orderby('id','desc')->where('parent',profile()->id)->where('status','winning')->paginate(20);
      return view('platform.my-client',compact('menu','users'));
    }
}
