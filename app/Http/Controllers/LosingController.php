<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class LosingController extends Controller
{
  public function viewLosing(){
    $menu = 'viewLosing';
    /*-------------------*/
    $users = DB::table('users_cabinet')->orderby('id','desc')->where('parent',profile()->id)->where('status','losing')->paginate(20);
    return view('platform.losing',compact('menu','users'));
  }
}
