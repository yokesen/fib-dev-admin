<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class ProfitLossController extends Controller
{
  public function viewProfitLoss(){
    $menu = 'viewProfitLoss';
    /*-------------------*/
    $lastmonth = date('m',strtotime('- 1 month'));
    $profitLastmonth = db::table('lot_raw_data')->where('parent',profile()->id)->where('lotmonth',$lastmonth)->sum('totalProfit');
    $depositLastmonth = DB::table('deposits')->where('parent',profile()->id)->whereMonth('created_at',$lastmonth)->where('from_bank','!=','internal transfer')->where('status','approved')->sum('amount');
    $withdrawalLastmonth = DB::table('withdrawals')->where('parent',profile()->id)->whereMonth('created_at',$lastmonth)->whereNull('withdrawals.internal')->where('status','approved')->sum('approved_amount');
    $netmarginLastmonth = $depositLastmonth - $withdrawalLastmonth;
    $profit = $profitLastmonth * -10000;

    if ($netmarginLastmonth < 1000000000) {
      $share = 0.2;
    }elseif ($netmarginLastmonth >= 1000000000 && $netmarginLastmonth < 2000000000) {
      $share = 0.3;
    }elseif ($netmarginLastmonth >= 2000000000 && $netmarginLastmonth < 3000000000) {
      $share = 0.4;
    }elseif ($netmarginLastmonth >= 3000000000 && $netmarginLastmonth < 4000000000) {
      $share = 0.5;
    }elseif ($netmarginLastmonth >= 4000000000) {
      $share = 0.6;
    }

    $commission = db::table('lot_raw_data')->where('parent',profile()->id)->where('lotmonth',$lastmonth)->sum('dataCommission');
    $comm = $commission * -10000;
    return view('platform.profit-loss',compact('menu','lastmonth','profit','depositLastmonth','withdrawalLastmonth','netmarginLastmonth','share','comm'));
  }
}
