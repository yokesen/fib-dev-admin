<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\SummaryImport;
use App\Imports\EquityImport;
use App\Imports\DepoWdImport;
use App\Imports\FloatingImport;
use Alert;
use DB;

class UploadController extends Controller
{
  public function viewUploadSummary(){
    $menu = 'uploadSummary';
    return view('platform.uploads.summary',compact('menu'));
  }

  public function viewUploadEquity(){
    $menu = 'uploadEquity';
    return view('platform.uploads.equity',compact('menu'));
  }

  public function viewUploadDepositWd(){
    $menu = 'uploadDepoWd';
    return view('platform.uploads.deposit-wd',compact('menu'));
  }

  public function viewUploadDailyReport(){
    $menu = 'uploadDailyReport';
    return view('platform.uploads.daily-report',compact('menu'));
  }

  public function processUploadSummary(Request $request){
    $file = $request->file('file');
    if ($file) {
      $filename = $file->getClientOriginalName();
      $extension = $file->getClientOriginalExtension();
      $fileSize = $file->getSize();
      if (strtolower($extension) == 'xlsx') {
        Excel::import(new SummaryImport, $request->file('file'));
      }else{
        Alert::warning('GAGAL','FILE SALAH. MOHON UPLOAD FILE XLSX');
      }

    }
    return redirect()->back();
  }

  public function processUploadEquity(Request $request){
    $file = $request->file('file');
    if ($file) {
      $filename = $file->getClientOriginalName();
      $extension = $file->getClientOriginalExtension();
      $fileSize = $file->getSize();
      if (strtolower($extension) == 'xlsx') {
        Excel::import(new EquityImport, $request->file('file'));
      }else{
        Alert::warning('GAGAL','FILE SALAH. MOHON UPLOAD FILE XLSX');
      }

    }
    return redirect()->back();
  }

  public function processUploadDepositWd(Request $request){
    $file = $request->file('file');
    if ($file) {
      $filename = $file->getClientOriginalName();
      $extension = $file->getClientOriginalExtension();
      $fileSize = $file->getSize();
      if (strtolower($extension) == 'xlsx') {
        Excel::import(new DepoWdImport, $request->file('file'));
      }else{
        Alert::warning('GAGAL','FILE SALAH. MOHON UPLOAD FILE XLSX');
      }

    }
    return redirect()->back();
  }

  public function processUploadDailyReport(Request $request){
    $file = $request->file('file');
    if ($file) {
      $filename = $file->getClientOriginalName();
      $extension = $file->getClientOriginalExtension();
      $fileSize = $file->getSize();
      if (strtolower($extension) == 'xlsx') {
        Excel::import(new FloatingImport, $request->file('file'));
      }else{
        Alert::warning('GAGAL','FILE SALAH. MOHON UPLOAD FILE XLSX');
      }

    }
    return redirect()->back();
  }


}
