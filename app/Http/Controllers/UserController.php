<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Image;
use Storage;
use Session;
use Alert;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu = 'Master User';
        /*-----------------------*/
        $users = DB::table('users_cabinet')->orderby('id','desc')->paginate(20);

        return view('user.list',compact('menu','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu = 'Master User';
        /*-----------------------*/

        return view('user.create',compact('menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $menu = 'Master User';
      /*-----------------------*/
      $user = DB::table('users_cabinet')->where('uuid',$id)->first();
      $role = DB::table('cms_privileges')->where('id',$user->id_cms_privileges)->first();
      $roles = DB::table('cms_privileges')->where('id','>','6')->get();
      $parent = DB::table('users_cabinet')->where('id',$user->parent)->select('username','uuid')->first();
      $deposits = DB::table('deposits')->where('uuid',$id)->orderby('id','desc')->paginate(7);
      $bank = DB::table('banks')->where('uuid',$id)->orderby('id','desc')->first();
      $withdrawals = DB::table('withdrawals')->where('uuid',$id)->orderby('id','desc')->paginate(7);
      $metatrader4 = DB::table('typeMT4AvailabeAccount')->where('uuid',$id)->orderby('id','desc')->paginate(7);
      $mt4 = DB::table('typeMT4AvailabeAccount')->where('uuid',$id)->where('status','approved')->orderby('id','desc')->get();
      $referral = DB::table('users_cabinet')->where('parent',$user->id)->orderby('id','desc')->paginate(7);
      $listAllow = DB::table('bank_lists')->get();
      $listCategories = DB::table('typeMT4Category')->where('status','active')->orderby('id','asc')->get();
      $listAccounts = DB::table('typeMT4Accounts')
        ->leftjoin('typeMT4Category','typeMT4Category.id','typeMT4Accounts.categoryAccount')
        ->where('typeMT4Accounts.status','active')
        ->where('typeMT4Category.status','active')
        ->orderby('typeMT4Accounts.minimumDepo','asc')
        ->select('typeMT4Category.namaCategory','typeMT4Accounts.id','typeMT4Accounts.namaAccount','typeMT4Accounts.deskripsiAccount','typeMT4Accounts.minimumDepo','typeMT4Accounts.bestChoice')
        ->get();
      if(empty($bank)){
        $insert = DB::table('banks')->insert([
          'uuid' => $id
        ]);
        $bank = DB::table('banks')->where('uuid',$id)->orderby('id','desc')->first();
      }
      return view('user.show',compact(
        'menu',
        'user',
        'role',
        'parent',
        'deposits',
        'bank',
        'withdrawals',
        'metatrader4',
        'referral',
        'listAllow',
        'roles',
        'mt4',
        'listCategories',
        'listAccounts'
      ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = 'Master User';
        /*-----------------------*/

        return view('user.edit',compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $uuid = $id;
      $user = DB::table('users_cabinet')->where('uuid',$uuid)->first();

      if(empty($user->username)){
        $rules = [
          'photo' => 'max:10000',
          'username' => 'required|min:4|max:30|unique:users_cabinet',
          'name' => 'required|min:2|max:70',
          'dob' => 'required|date'
        ];

        $messages = [
          'photo.max' => 'photo yg diupload barusan terlalu besar, ada yang kecilan sedikit ga?',
          'username.required' => 'usernamenya kosong',
          'username.min' => 'username terlalu pendek, minimal 4 huruf ya',
          'username.max' => 'username terlalu panjang, maximal 30 huruf ya',
          'username.unique' => 'username sudah dipakai, ayo pilih username yang lain',
          'name.required' => 'namenya kosong',
          'name.min' => 'nama terlalu pendek, minimal 2 huruf ya',
          'name.max' => 'nama terlalu panjang, maximal 70 huruf ya',
          'dob.required' => 'tanggal lahirmu brp?',
          'dob.date' => 'bukan tanggal nih'
        ];
      }else{
        $rules = [
          'photo' => 'max:10000',
          'name' => 'required|min:2|max:70',
          'dob' => 'required|date'
        ];

        $messages = [
          'photo.max' => 'photo yg diupload barusan terlalu besar, ada yang kecilan sedikit ga?',
          'name.required' => 'namenya kosong',
          'name.min' => 'nama terlalu pendek, minimal 2 huruf ya',
          'name.max' => 'nama terlalu panjang, maximal 70 huruf ya',
          'dob.required' => 'tanggal lahirmu brp?',
          'dob.date' => 'bukan tanggal nih'
        ];
      }


      $validator = Validator::make($request->all(), $rules, $messages);

      if($validator->fails()){
        return redirect()->back()->withErrors($validator)->withInput($request->all());
      }

      if($request->file('photo'))
      {
        $image = $request->file('photo');
        $time = time();
        /*image name*/
        $extention = $image->extension();
        $image_name = $uuid.$time.".".$extention;
        $thumb_name = "thumb-".$uuid.$time.".".$extention;
        /*image name*/
        /*THUMB harus diatas supaya ga ke rotate*/
        $image_thumb = Image::make($image)->fit(200, 200);
        $image_thumb = $image_thumb->stream();
        Storage::disk('upcloud')->put('images/orbitrade/user/'.$thumb_name, $image_thumb->__toString());
        /*THUMB harus diatas supaya ga ke rotate*/
        $update = DB::table('users_cabinet')->where('uuid',$uuid)->update([
          'username' => $request->username,
          'name' => $request->name,
          'photo' => env('IMG_USER').$thumb_name,
          'email' => $request->email,
          'whatsapp' => $request->whatsapp,
          'address' => $request->address,
          'city' => $request->city,
          'dob' => $request->dob,
          'consent_wa' => $request->consent_wa,
          'consent_email' => $request->consent_email,
          'consent_marketing' => $request->consent_marketing
        ]);
      }else{
        $update = DB::table('users_cabinet')->where('uuid',$uuid)->update([
          'username' => $request->username,
          'name' => $request->name,
          'dob' => $request->dob,
          'email' => $request->email,
          'whatsapp' => $request->whatsapp,
          'address' => $request->address,
          'city' => $request->city,
          'consent_wa' => $request->consent_wa,
          'consent_email' => $request->consent_email,
          'consent_marketing' => $request->consent_marketing
        ]);

      }

      return redirect()->back();
    }

    public function editBank(Request $request, $id)
    {
      $uuid = $id;
      $user = DB::table('users_cabinet')->where('uuid',$uuid)->first();
      $update = DB::table('banks')->where('uuid',$uuid)->update([
        'bank_name' => $request->bank_name,
        'account_name' => $request->account_name,
        'account_number' => $request->account_number
      ]);
      return redirect()->back();
    }

    public function editLevel(Request $request, $id)
    {
      $uuid = $id;
      $update = DB::table('users_cabinet')->where('uuid',$uuid)->update([
        'status' => $request->level,
      ]);
      return redirect()->back();
    }

    public function editParent(Request $request, $id)
    {
      $uuid = $id;
      $update = DB::table('users_cabinet')->where('uuid',$uuid)->update([
        'parent' => $request->parent,
      ]);
      return redirect()->back();
    }

    public function uploadRekening(Request $request, $id)
    {
      $uuid = $id;
      if($request->file('gambar'))
      {
        $image = $request->file('gambar');
        $time = time();
        /*image name*/
        $extention = $image->extension();
        $image_name = $uuid.$time.".".$extention;
        $thumb_name = "thumb-".$uuid.$time.".".$extention;
        /*image name*/
        /*THUMB harus diatas supaya ga ke rotate*/
        $image_thumb = Image::make($image)->fit(200, 200);
        $image_thumb = $image_thumb->stream();
        Storage::disk('upcloud')->put('images/orbitrade/user/'.$thumb_name, $image_thumb->__toString());
        /*THUMB harus diatas supaya ga ke rotate*/
        $dimension = getimagesize($image);
        $width = $dimension[0];
        $height = $dimension[1];
        if ($height > $width) {
          // create Image from file
          $image_normal = Image::make($image)->heighten(600, function ($constraint)
          {
            $constraint->upsize();
          });
        }else{
          $image_normal = Image::make($image)->widen(600, function ($constraint)
          {
            $constraint->upsize();
          });
        }
        $image_normal = $image_normal->stream();

        Storage::disk('upcloud')->put('images/orbitrade/user/'.$image_name, $image_normal->__toString());

        $update = DB::table('users_cabinet')->where('uuid',$uuid)->update([
          'photoTabungan' => $image_name
        ]);
      }

      return $image_name;
    }

    public function uploadKTP(Request $request, $id)
    {
      $uuid = $id;
      if($request->file('gambar'))
      {
        $image = $request->file('gambar');
        $time = time();
        /*image name*/
        $extention = $image->extension();
        $image_name = $uuid.$time.".".$extention;
        $thumb_name = "thumb-".$uuid.$time.".".$extention;
        /*image name*/
        /*THUMB harus diatas supaya ga ke rotate*/
        $image_thumb = Image::make($image)->fit(200, 200);
        $image_thumb = $image_thumb->stream();
        Storage::disk('upcloud')->put('images/orbitrade/user/'.$thumb_name, $image_thumb->__toString());
        /*THUMB harus diatas supaya ga ke rotate*/
        $dimension = getimagesize($image);
        $width = $dimension[0];
        $height = $dimension[1];
        if ($height > $width) {
          // create Image from file
          $image_normal = Image::make($image)->heighten(600, function ($constraint)
          {
            $constraint->upsize();
          });
        }else{
          $image_normal = Image::make($image)->widen(600, function ($constraint)
          {
            $constraint->upsize();
          });
        }
        $image_normal = $image_normal->stream();

        Storage::disk('upcloud')->put('images/orbitrade/user/'.$image_name, $image_normal->__toString());

        $update = DB::table('users_cabinet')->where('uuid',$uuid)->update([
          'photoKTP' => $image_name
        ]);
      }

      return $image_name;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
