<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;
Use Alert;

class GettingStartedController extends Controller
{
      public function viewGettingStarted(){
        $menu = 'Getting Started';
        /*-----------------*/
        $user = profile();

        if (empty($user->whatsapp)) {
          return view('app.ims-verifikasi-whatsapp',compact('menu','user'));
        }

        return view('app.ims-action',compact('menu','user'));
      }

      public function verifyEmail(){
        $menu = 'Getting Started';
        /*-----------------*/
        $user = profile();

        return view('app.ims-verifikasi-email',compact('menu','user'));
      }

      public function verifyWhatsapp(){
        $menu = 'Getting Started';
        /*-----------------*/
        $user = profile();

        return view('app.ims-verifikasi-whatsapp',compact('menu','user'));
      }

      public function editProfile(){
        $menu = 'edit profile';
        /*-----------------*/
        $user = profile();
        return view('app.ims-profile',compact('menu','user'));
      }

      public function editBank(){
        $menu = 'edit Bank';
        /*-----------------*/
        $user = profile();
        $bank = bank();
        $listAllow = DB::table('bank_lists')->get();
        return view('app.ims-bank',compact('menu','user','bank','listAllow'));
      }

      public function uploadDocument(){
        $menu = 'edit Bank';
        /*-----------------*/
        $user = profile();
        $bank = bank();

        return view('app.ims-upload-bukti',compact('menu','user','bank'));
      }
}
