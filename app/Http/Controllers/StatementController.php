<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class StatementController extends Controller
{
  public function viewStatement(){
    $menu = 'viewStatement';
    /*-------------------*/
    $statements = DB::table('lot_raw_data')
                  ->join('users_cabinet','users_cabinet.uuid','lot_raw_data.uuid')
                  ->where('lot_raw_data.parent',profile()->id)
                  ->orderby('lot_raw_data.dataTime','desc')
                  ->select(
                    'users_cabinet.username',
                    'lot_raw_data.accountId',
                    'lot_raw_data.dataOrder',
                    'lot_raw_data.dataTime',
                    'lot_raw_data.dataVolumeClosed',
                    'lot_raw_data.dataSymbol',
                    'lot_raw_data.dataProfit',
                    'lot_raw_data.dataCommission',
                    'lot_raw_data.dataStorage',
                    'lot_raw_data.totalProfit',
                    )
                  ->paginate(20);
    return view('platform.statement',compact('menu','statements'));
  }
}
