<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class ContactController extends Controller
{
  public function viewContact(){
    $menu = 'viewContact';
    /*-------------------*/
    $users = DB::table('users_cabinet')->orderby('id','desc')->where('parent',profile()->id)->where('status','contact')->paginate(20);
    return view('platform.contact',compact('menu','users'));
  }
}
