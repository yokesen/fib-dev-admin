<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AjaxSearchController extends Controller
{
/*class begin*/
    public function ajaxSearchUsername(Request $request){
      $check = DB::table('users_cabinet')->where('username',$request->username)->count();
      if($request->username == profile()->username){
        $check = 0;
      }
      if($check > 0){
        $suggests = [];
        $username1 = $request->username.date('y');
        $check1 = DB::table('users_cabinet')->where('username',$username1)->count();
        if($check1 == 0){
          array_push($suggests,$username1);
        }
        $username2 = $request->username.date('m');
        $check2 = DB::table('users_cabinet')->where('username',$username2)->count();
        if($check2 == 0){
          array_push($suggests,$username2);
        }
        $username3 = explode('@',profile()->email)[0];
        $check3 = DB::table('users_cabinet')->where('username',$username3)->count();
        if($check3 == 0){
          array_push($suggests,$username3);
        }
        $view = view('app.partials.ajax.username',compact('suggests'))->render();
        return response()->json(['html'=>$view]);
      }else{
        return $check;
      }
    }

    public function ajaxSearchAccountCategory(Request $request){
      $check = DB::table('typeMT5Accounts')->where('categoryAccount',$request->category)->orderby('minimumDepo','asc')->get();
    }

    public function ajaxSearchWhatsapp(Request $request){
      $whatsapp = profile()->whatsapp;
      return $whatsapp;
    }

    public function ajaxSearchBankCustodian(Request $request){
      $banklists = DB::table('bank_segre')->where('namaBank',$request->namaBank)->get();

      return $banklists;
    }

    public function ajaxSearchAccountMt4(Request $request){

      $account = account4($request->mt4);
      $type = $account->tipe_deposit;
      if ($type == 0) {
        $view = view('app.partials.ajax.deposit-unknown')->render();
        return response()->json(['html'=>$view]);
      }else{
        $td = currencyRate($type);
        $view = view('app.partials.ajax.deposit-known',compact('td'))->render();
        return response()->json(['html'=>$view]);
      }
    }

/*class end*/
}
