<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Jenssegers\Agent\Agent;
use Cookie;
use Session;
use Illuminate\Support\Facades\Hash;
use Alert;
use Illuminate\Support\Str;

class PasswordController extends Controller
{
    public function checkingprocess(Request $request){
      $login = DB::table('users_cabinet')->where('email',$request->email)->first();
      if(!$login){
        Alert::warning( 'Wah emailnya ga ketemu','coba ingat-ingat lagi email Kamu apa? Atau hubungi customer service untuk bantuan.')->showConfirmButton('Baik!', '#DB1430');
        return redirect()->back();
      }
      $rand = Str::random(8);
      $uuid = $login->uuid;
      $create_code = DB::table('lost_password')->insert([
        'email' => $request->email,
        'code' => $rand,
        'created_at' => date('Y-m-d H:i:s'),
        'expired_at' => date('Y-m-d H:i:s',strtotime('+24hours'))
      ]);

      $params = [
        'linkpassword' => ENV('APP_URL').'/new-password/'.$request->email.'/verify/'.$uuid,
        'code' => $rand,
        'time' => date('d-m-Y H:i:s')
      ];

      $mail = DB::table('daftar_queue_email')->insert([
        'type' => 'transactional',
        'content' => 'password',
        'uuid' => $uuid,
        'params' => serialize($params)
      ]);

      Alert::success( 'Check Email!!','Barusan saja kami sudah kirim email untuk link reset password ke '.$request->email)->showConfirmButton('Baik!', '#DB1430');
      return redirect()->back();
    }

    public function changeprocess(Request $request){
      $login = DB::table('users_cabinet')->where('email',$request->email)->first();
      if(!$login){
        Alert::warning( 'Wah emailnya ga ketemu','coba ingat-ingat lagi email Kamu apa? Atau hubungi customer service untuk bantuan.')->showConfirmButton('Baik!', '#DB1430');
        return redirect()->back();
      }

      if($login->uuid != $request->uuid){
        Alert::warning('Wah email kamu kok ga bisa terverifikasi ya?','Coba periksa kembali email kamu atau hubungi customer service untuk bantuan')->showConfirmButton('Okay!', '#DB1430');
        return redirect()->back();
      }

      $rules = [

          'password' => 'required|string|min:8|max:20|confirmed'
      ];

      $messages = [

          'password.required' => 'Kesalahan : Password-mu masih kosong nih!',
          'password.min' => 'Kesalahan : Password-mu minimal harus 8 huruf/angka!',
          'password.max' => 'Kesalahan : Password-mu terlalu panjang!',
          'password.confirmed' => 'Kesalahan : Password dan Password Konfirmasinya salah!',

      ];

      $validator = Validator::make($request->all(), $rules, $messages);

      if($validator->fails()){
          return redirect()->back()->withErrors($validator)->withInput($request->all());
      }

      $update = DB::table('users_cabinet')->where('email',$request->email)->update([
        'password' => Hash::make($request->password)
      ]);
      Alert::success( 'YEAY Berhasil!!','Yuk login lagi dengan password baru Kamu')->showConfirmButton('Baik!', '#DB1430');
      return redirect()->route('viewLogin');
    }

}
