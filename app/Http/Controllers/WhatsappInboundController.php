<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class WhatsappInboundController extends Controller
{
    public function inbound(Request $request){
      $data = json_decode($request->getContent());
      $appPackageName = $data->appPackageName;
      // package name of messenger to detect which messenger the message comes from
    	$messengerPackageName = $data->messengerPackageName;
    	// name/number of the message sender (like shown in the Android notification)
    	$sender = $data->query->sender;
    	// text of the incoming message
    	$message = $data->query->message;
    	// is the sender a group? true or false
    	$isGroup = $data->query->isGroup;
    	// id of the AutoResponder rule which has sent the web server request
    	$ruleId = $data->query->ruleId;

      $verifyID = explode('verify ',$message);
      $verify = $verifyID[1];

      $insert = DB::table('whatsapp')->insert([
        'content' => $appPackageName.'|'.$messengerPackageName.'|'.$sender.'|'.$message.'|'.$isGroup.'|'.$ruleId,
        'phone' => $sender,
        'message' => $message
      ]);

      $user = DB::table('users_cabinet')->where('uuid',$verify)->first();

      if(!empty($user)){
        $update = DB::table('users_cabinet')->where('uuid',$verify)->update([
          'whatsapp' => $sender
        ]);

        $update = DB::table('users_cabinet')->where('uuid',$verify)->update([
            'id_cms_privileges' => 9
        ]);

        $priv = DB::table("cms_privileges")->where("id", 9)->first();
        $roles = DB::table('cms_privileges_roles')->where('id_cms_privileges', 9)->join('cms_moduls', 'cms_moduls.id', '=', 'id_cms_moduls')->select('cms_moduls.name', 'cms_moduls.path', 'is_visible', 'is_create', 'is_read', 'is_edit', 'is_delete')->get();

        Session::put('priv', $priv);
        Session::put('modul', $roles);

        return json_encode(array("replies" => array(
          array("message" => "Halo $user->name, terima kasih atas verifikasi whatsapp $sender di ".env('APP_NAME')),
          array("message" => "Success ✅ kode ".time()." Sekarang silahkan kembali ke Platfrom ".env('APP_NAME').". Terima kasih!")
        )));

        return redirect()->route('viewProfile');
      }else{
        return json_encode(array("replies" => array(
          array("message" => "Sepertinya kode verifikasi ini ga valid, coba klik lagi dari website, jangan ada yang diubah ya")
        )));
      }

    }
}
