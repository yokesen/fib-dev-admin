<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Image;
use Storage;
use Session;
use Alert;
use Validator;

class ProfileController extends Controller
{
    public function profile(){
      $menu = 'Dashboard';
      $usermenu = 'profile';
      /*-----------------*/
      $user = profile();

      return view('app.ims-action',compact('menu','user','usermenu'));
    }

    public function editProfilePost(Request $request){
      $uuid = uuid();
      $user = profile();
      if(empty($user->username)){
        $rules = [
          'photo' => 'max:10000',
            'username' => 'required|min:4|max:30|unique:users_cabinet',
            'name' => 'required|min:2|max:70',
            'dob' => 'required|date'
        ];

        $messages = [
          'photo.max' => 'photo yg diupload barusan terlalu besar, ada yang kecilan sedikit ga?',
          'username.required' => 'usernamenya kosong',
          'username.min' => 'username terlalu pendek, minimal 4 huruf ya',
          'username.max' => 'username terlalu panjang, maximal 30 huruf ya',
          'username.unique' => 'username sudah dipakai, ayo pilih username yang lain',
          'name.required' => 'namenya kosong',
          'name.min' => 'nama terlalu pendek, minimal 2 huruf ya',
          'name.max' => 'nama terlalu panjang, maximal 70 huruf ya',
          'dob.required' => 'tanggal lahirmu brp?',
          'dob.date' => 'bukan tanggal nih'
        ];
      }else{
        $rules = [
            'photo' => 'max:10000',
            'name' => 'required|min:2|max:70',
            'dob' => 'required|date'
        ];

        $messages = [
          'photo.max' => 'photo yg diupload barusan terlalu besar, ada yang kecilan sedikit ga?',
          'name.required' => 'namenya kosong',
          'name.min' => 'nama terlalu pendek, minimal 2 huruf ya',
          'name.max' => 'nama terlalu panjang, maximal 70 huruf ya',
          'dob.required' => 'tanggal lahirmu brp?',
          'dob.date' => 'bukan tanggal nih'
        ];
      }


      $validator = Validator::make($request->all(), $rules, $messages);

      if($validator->fails()){
          return redirect()->back()->withErrors($validator)->withInput($request->all());
      }

      if($request->file('photo'))
      {
        $image = $request->file('photo');
        $time = time();
        /*image name*/
        $extention = $image->extension();
        $image_name = $uuid.$time.".".$extention;
        $thumb_name = "thumb-".$uuid.$time.".".$extention;
        /*image name*/
        /*THUMB harus diatas supaya ga ke rotate*/
        $image_thumb = Image::make($image)->fit(200, 200);
        $image_thumb = $image_thumb->stream();
        Storage::disk('upcloud')->put('images/orbitrade/user/'.$thumb_name, $image_thumb->__toString());
        /*THUMB harus diatas supaya ga ke rotate*/
        $update = DB::table('users_cabinet')->where('uuid',$uuid)->update([
            'username' => $request->username,
            'name' => $request->name,
            'photo' => env('IMG_USER').$thumb_name,
            'email' => $request->email,
            'dob' => $request->dob,
            'consent_wa' => $request->consent_wa,
            'consent_email' => $request->consent_email,
            'consent_marketing' => $request->consent_marketing
        ]);
      }else{
        $update = DB::table('users_cabinet')->where('uuid',$uuid)->update([
            'username' => $request->username,
            'name' => $request->name,
            'dob' => $request->dob,
            'email' => $request->email,
            'consent_wa' => $request->consent_wa,
            'consent_email' => $request->consent_email,
            'consent_marketing' => $request->consent_marketing
        ]);

      }

      if ($user->id_cms_privileges == 7 && profileCompletion() > 99) {
        $update = DB::table('users_cabinet')->where('uuid',$uuid)->update([
            'id_cms_privileges' => 8
        ]);
        Alert::success( 'Selamat profile Kamu terverifikasi!!','Statusmu sekarang Verified User!')->showConfirmButton('Lanjutkan', '#DB1430');
        return redirect()->route('viewAccountmt4');
      }
      newSession();
      return redirect()->route('viewProfile');
    }

    public function uploadBukuTabungan(Request $request){
      $uuid = uuid();
      if($request->file('gambar'))
      {
        $image = $request->file('gambar');
        $time = time();
        /*image name*/
        $extention = $image->extension();
        $image_name = $uuid.$time.".".$extention;
        $thumb_name = "thumb-".$uuid.$time.".".$extention;
        /*image name*/
        /*THUMB harus diatas supaya ga ke rotate*/
        $image_thumb = Image::make($image)->fit(200, 200);
        $image_thumb = $image_thumb->stream();
        Storage::disk('upcloud')->put('images/orbitrade/user/'.$thumb_name, $image_thumb->__toString());
        /*THUMB harus diatas supaya ga ke rotate*/
        $dimension = getimagesize($image);
        $width = $dimension[0];
        $height = $dimension[1];
        if ($height > $width) {
          // create Image from file
          $image_normal = Image::make($image)->heighten(600, function ($constraint)
          {
            $constraint->upsize();
          });
        }else{
          $image_normal = Image::make($image)->widen(600, function ($constraint)
          {
            $constraint->upsize();
          });
        }
        $image_normal = $image_normal->stream();

        Storage::disk('upcloud')->put('images/orbitrade/user/'.$image_name, $image_normal->__toString());

        $update = DB::table('users_cabinet')->where('uuid',$uuid)->update([
          'photoTabungan' => $image_name
        ]);
      }

      return $image_name;
    }

    public function uploadKtp(Request $request){
      $uuid = uuid();
      if($request->file('gambar'))
      {
        $image = $request->file('gambar');
        $time = time();
        /*image name*/
        $extention = $image->extension();
        $image_name = $uuid.$time.".".$extention;
        $thumb_name = "thumb-".$uuid.$time.".".$extention;
        /*image name*/
        /*THUMB harus diatas supaya ga ke rotate*/
        $image_thumb = Image::make($image)->fit(200, 200);
        $image_thumb = $image_thumb->stream();
        Storage::disk('upcloud')->put('images/orbitrade/user/'.$thumb_name, $image_thumb->__toString());
        /*THUMB harus diatas supaya ga ke rotate*/
        $dimension = getimagesize($image);
        $width = $dimension[0];
        $height = $dimension[1];
        if ($height > $width) {
          // create Image from file
          $image_normal = Image::make($image)->heighten(600, function ($constraint)
          {
            $constraint->upsize();
          });
        }else{
          $image_normal = Image::make($image)->widen(600, function ($constraint)
          {
            $constraint->upsize();
          });
        }
        $image_normal = $image_normal->stream();

        Storage::disk('upcloud')->put('images/orbitrade/user/'.$image_name, $image_normal->__toString());

        $update = DB::table('users_cabinet')->where('uuid',$uuid)->update([
          'photoKTP' => $image_name
        ]);
      }

      return $image_name;
    }

    public function editBankPost(Request $request){
      $uuid = uuid();
      $user = profile();
      $update = DB::table('banks')->where('uuid',Session::get('user')->uuid)->update([
        'bank_name' => $request->bank_name,
        'account_name' => $request->account_name,
        'account_number' => $request->account_number
      ]);

      if ($user->id_cms_privileges == 7 && profileCompletion() > 99) {
        $update = DB::table('users_cabinet')->where('uuid',$uuid)->update([
            'id_cms_privileges' => 8
        ]);
        newSession();
        Alert::success( 'Selamat profile Kamu terverifikasi!!','Statusmu sekarang Verified User!')->showConfirmButton('Lanjutkan', '#DB1430');
        return redirect()->route('viewAccountmt4');
      }
      return redirect()->route('viewProfile');
    }

    public function submitBukti(Request $request){

      $user = profile();
      if ($user->id_cms_privileges == 7 && profileCompletion() > 99) {
        $update = DB::table('users_cabinet')->where('uuid',uuid())->update([
            'id_cms_privileges' => 8
        ]);
        newSession();
        Alert::success( 'Selamat profile Kamu terverifikasi!!','Statusmu sekarang Verified User!')->showConfirmButton('Lanjutkan', '#DB1430');
        return redirect()->route('viewAccountmt4');
      }
      return redirect()->route('viewProfile');
    }
}
