<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class TradingAccountController extends Controller
{
  public function viewTradingAccount(){
    $menu = 'viewTradingAccount';
    /*-------------------*/
    $accounts = DB::table('typeMT4AvailabeAccount')
                  ->join('users_cabinet','users_cabinet.uuid','typeMT4AvailabeAccount.uuid')
                  ->where('typeMT4AvailabeAccount.parent',profile()->id)
                  ->orderby('typeMT4AvailabeAccount.process_at','desc')
                  ->select(
                    'users_cabinet.username',
                    'typeMT4AvailabeAccount.typeAccount',
                    'typeMT4AvailabeAccount.mt4_id'
                    )
                  ->paginate(20);
    return view('platform.trading-account',compact('menu','accounts'));
  }
}
