<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class reconsileController extends Controller
{
    public function list(){
      $menu = 'balancesheet';
      $thisweek = date('W');
      $thisyear = date('Y');

      $balanceWeek = $thisweek-1;
      //dd($thisweek-1,$thisyear);

      $list = DB::table('statement_balance_sheet')->where('tipe_mutasi','LIKE','%company%')->where('weekoty',$balanceWeek)->where('year',$thisyear)
              ->orderBy('statement_balance_sheet.id','asc')->get();

      if ($list) {
        $getweek = DB::table('statement_balance_sheet')->where('weekoty','<',$balanceWeek)->orderby('id','desc')->first();
        if ($getweek) {
          $last =  DB::table('statement_balance_sheet')->where('weekoty',$getweek->weekoty)->orderby('id','desc')->first();
          $lastWeek = $getweek->weekoty;

        }else{
          $last = [];
          $lastWeek = [];
        }
      }else{
        $last = DB::table('statement_balance_sheet')->where('id','1')->orderby('id','desc')->first();
      }


      $getnextweek = DB::table('statement_balance_sheet')->where('weekoty','>',$balanceWeek)->orderby('id','asc')->first();
      if ($getnextweek) {
        $nextweek = $getnextweek->weekoty;
      }else{
        $nextweek = 0;
      }

      return view('platform.reconsile.list',compact('menu','list','balanceWeek','last','lastWeek','nextweek'));
    }

    public function action($id){
      $menu = 'balancesheet';

      $reconsile = DB::table('reconsile_finance')
                  ->where('reconsile_finance.id',$id)
                  ->join('users_ib','users_ib.uuid','reconsile_finance.uuid')
                  ->select(
                    'reconsile_finance.id',
                    'reconsile_finance.dateReconsile',
                    'reconsile_finance.typeReconsile',
                    'reconsile_finance.amount',
                    'reconsile_finance.statusReconsile',
                    'reconsile_finance.created_at',
                    'users_ib.name',
                    'users_ib.username',
                    'users_ib.takingPositionPercentage',
                    'users_ib.lastKnownDeposit',
                    )
                  ->first();

      return view('platform.reconsile.action', compact('menu','reconsile'));
    }

    public function submitAction(Request $request){
      $update = $reconsile = DB::table('reconsile_finance')->where('id',$request->id)->update([
        'statusReconsile' => $request->statusReconsile
      ]);

      return redirect()->route('viewReconsileList');
    }

    public function statement($week,$year,$parent){
      $menu = 'balancesheet';

      $weeklyClosedStatements = DB::table('raw_data_deal')->where('parent',$parent)->where('lotweek',$week)->where('lotyear',$year)->where('Entry','1')->where('Action','<','2')->orderby('Time','desc')->get();
      $weeklyBalanceStatements = DB::table('raw_data_deal')->where('parent',$parent)->where('lotweek',$week)->where('lotyear',$year)->where('Action','2')->orderby('Time','desc')->get();
      // $distinct = DB::table('raw_data_position')->where('parent',$parent)->where('lotweek',$week)->where('lotyear',$year)->distinct()->select('Position')->get();
      // $weeklyFloatingStatements = [];
      // foreach ($distinct as $v) {
      //   $arr = DB::table('raw_data_position')->where('Position',$v->Position)->orderby('humanTime','desc')->first();
      //   $weeklyFloatingStatements[] = $arr;
      // }
      $commission = DB::table('lot_week_by_parent')->where('parent',$parent)->where('week',$week)->where('year',$year)->first();
      $getlotdate = DB::table('raw_data_position')->where('parent',$parent)->where('lotweek',$week)->where('lotyear',$year)->distinct()->select('lotdate')->get();
      //dd($getlotdate);
      foreach ($getlotdate as $key => $value) {
        $date = date('w',strtotime('1 Jan 2022 + '.$value->lotdate.' days'));
        if ($date == "6") {
          $lotdate = $value->lotdate;
        }
      }
      //dd($lotdate);
      $weeklyFloatingStatements = DB::table('raw_data_position')->where('parent',$parent)->where('lotweek',$week)->where('lotyear',$year)->where('lotdate',$lotdate)->get();
      $lastweekFloatingPL = DB::table('statement_partner_weekly')->where('parent',$parent)->where('weekoty','<',$week)->where('year',$year)->orderby('id','desc')->first();
      $adjustPL = DB::table('raw_data_deal')->where('parent',$parent)->where('lotweek',$week)->where('lotyear',$year)->where('Action','2')->where('flag_statement','1')->get();
      $adjustRollover = DB::table('raw_data_deal')->where('parent',$parent)->where('lotweek',$week)->where('lotyear',$year)->where('Action','2')->where('flag_swap','1')->get();
      //dd($adjustPL,$adjustRollover);
      return view('platform.reconsile.detil',compact('menu','weeklyClosedStatements','weeklyFloatingStatements','weeklyBalanceStatements','commission','lastweekFloatingPL','adjustPL','adjustRollover'));
    }

    public function lastStatement($week,$year){
      $menu = 'balancesheet';
      $balanceWeek = $week;
      $list = DB::table('statement_balance_sheet')->where('weekoty',$week)->where('year',$year)
              ->orderBy('statement_balance_sheet.id','asc')->get();
      if ($list) {
        $getweek = DB::table('statement_balance_sheet')->where('weekoty','<',$week)->orderby('id','desc')->first();
        $last =  DB::table('statement_balance_sheet')->where('weekoty',$getweek->weekoty)->orderby('id','desc')->first();
      }else{
        $last = DB::table('statement_balance_sheet')->where('id','1')->orderby('id','desc')->first();
      }

      $lastWeek = $getweek->weekoty;

      $getnextweek = DB::table('statement_balance_sheet')->where('weekoty','>',$week)->orderby('id','asc')->first();
      if ($getnextweek) {
        $nextweek = $getnextweek->weekoty;
      }else{
        $nextweek = 0;
      }

      return view('platform.reconsile.list',compact('menu','list','balanceWeek','last','lastWeek','nextweek'));
    }

    public function cocokan(){
      $menu = 'listReconsile';
      $getCocokan = DB::table('calculate_sib_weekly')->join('users_ib','calculate_sib_weekly.sib','users_ib.id')->where('calculate_sib_weekly.payment_comp','0')->orderby('calculate_sib_weekly.id','asc')->get();
      //dd($getCocokan);
      return view('platform.cocokan.list',compact('getCocokan','menu'));
    }

    public function invoice($parent,$week,$year){
      $menu = 'listReconsile';
      $invoice = DB::table('statement_reconsile')->join('users_ib','statement_reconsile.parent','users_ib.id')->where('statement_reconsile.parent',$parent)->where('statement_reconsile.weekoty',$week)->where('statement_reconsile.year',$year)->first();
      $balance = DB::table('statement_balance_sheet')->join('users_ib','statement_balance_sheet.parent','users_ib.id')->where('statement_balance_sheet.parent',$parent)->where('statement_balance_sheet.weekoty',$week)->where('statement_balance_sheet.year',$year)->get();
      //dd($invoice);
      return view('platform.cocokan.invoice',compact('invoice','balance','menu'));
    }

    public function detailPerAccount($hash){
      $menu = 'listReconsile';
      $dataHash = DB::table('calculate_sib_weekly')->where('hashCalc',$hash)->first();
      $dataDetail = DB::table('calculate_account_weekly')->where('sib',$dataHash->sib)->where('weekoty',$dataHash->weekoty)->where('year',$dataHash->year)->get();
      return view('platform.reconsile.detail-per-account',compact('dataDetail','menu'));
    }

    public function statementPerAccount($hash){
      $menu = 'listReconsile';
      $dataHash = DB::table('calculate_account_weekly')->where('hashCalc',$hash)->first();
      $weeklyClosedStatements = DB::table('raw_data_deal')->where('Login',$dataHash->accountId)->where('lotweek',$dataHash->weekoty)->where('lotyear',$dataHash->year)->where('Entry','1')->where('Action','<','2')->orderby('Time','desc')->get();
      $weeklyBalanceStatements = DB::table('raw_data_deal')->where('Login',$dataHash->accountId)->where('lotweek',$dataHash->weekoty)->where('lotyear',$dataHash->year)->where('Action','2')->orderby('Time','desc')->get();

      $commission = DB::table('raw_data_deal')->where('Login',$dataHash->accountId)->where('lotweek',$dataHash->weekoty)->where('lotyear',$dataHash->year)->where('Commission','<',0)->sum('Profit');
      $getlotdate = DB::table('raw_data_position')->where('Login',$dataHash->accountId)->where('lotweek',$dataHash->weekoty)->where('lotyear',$dataHash->year)->distinct()->select('lotdate')->get();

      foreach ($getlotdate as $key => $value) {
        $date = date('w',strtotime('1 Jan 2022 + '.$value->lotdate.' days'));
        if ($date == "6") {
          $lotdate = $value->lotdate;
        }
      }

      $weeklyFloatingStatements = DB::table('raw_data_position')->where('Login',$dataHash->accountId)->where('lotweek',$dataHash->weekoty)->where('lotyear',$dataHash->year)->where('lotdate',$lotdate)->get();
      $lastweekFloatingPL = [];
      $adjustPL = DB::table('raw_data_deal')->where('Login',$dataHash->accountId)->where('lotweek',$dataHash->weekoty)->where('lotyear',$dataHash->year)->where('Action','2')->where('flag_statement','1')->get();
      $adjustRollover = DB::table('raw_data_deal')->where('Login',$dataHash->accountId)->where('lotweek',$dataHash->weekoty)->where('lotyear',$dataHash->year)->where('Action','2')->where('flag_swap','1')->get();
      //dd($adjustPL,$adjustRollover);
      return view('platform.reconsile.detil',compact('menu','weeklyClosedStatements','weeklyFloatingStatements','weeklyBalanceStatements','commission','lastweekFloatingPL','adjustPL','adjustRollover'));
      //return view('platform.reconsile.detil',compact('dataDetail','menu'));
    }

}
