<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InfoController extends Controller
{
    public function level7NewRegister(){
      $menu = '';
      $usermenu = '';
      /*---------------*/

      return view('forbiden.level-7',compact('menu','usermenu'));
    }

    public function level8NewRegister(){
      $menu = '';
      $usermenu = '';
      /*---------------*/

      return view('forbiden.level-8',compact('menu','usermenu'));
    }

    public function level9NewRegister(){
      $menu = '';
      $usermenu = '';
      /*---------------*/

      return view('forbiden.level-9',compact('menu','usermenu'));
    }

    public function level10NewRegister(){
      $menu = '';
      $usermenu = '';
      /*---------------*/

      return view('forbiden.level-10',compact('menu','usermenu'));
    }

    public function levelall(){
      $menu = '';
      $usermenu = '';
      /*---------------*/

      if (profile()->id_cms_privileges ==7) {
        return view('forbiden.level-7',compact('menu','usermenu'));
      }
      if (profile()->id_cms_privileges ==8) {
        return view('forbiden.level-8',compact('menu','usermenu'));
      }
      if (profile()->id_cms_privileges ==9) {
        return view('forbiden.level-9',compact('menu','usermenu'));
      }
      if (profile()->id_cms_privileges ==10) {
        return view('forbiden.level-10',compact('menu','usermenu'));
      }
    }
}
