<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;
use Validator;
use Alert;
use Illuminate\Support\Facades\Http;

class groupController extends Controller
{
    public function listRequest(){
      $menu = 'listRequest';
      $list = DB::table('typeMT4Accounts')
              ->join('users_ib','users_ib.id','typeMT4Accounts.categoryAccount')
              ->where('typeMT4Accounts.status','pending')
              ->where('typeMT4Accounts.created_at','>','2022-01-01')
              ->where('users_ib.id_cms_privileges','70')
              ->select(
                'users_ib.name',
                'users_ib.username',
                'typeMT4Accounts.namaAccount',
                'typeMT4Accounts.accountGroup',
                'typeMT4Accounts.status',
                'typeMT4Accounts.created_at',
                'typeMT4Accounts.id',
                )
              ->paginate(20);

      return view('platform.groups.request',compact('list','menu'));
    }

    public function listActivation(){
      $menu = 'listActivation';

      $list = DB::table('typeMT4Accounts')
              ->join('users_ib','users_ib.id','typeMT4Accounts.categoryAccount')
              ->where('typeMT4Accounts.status','waiting')
              ->select(
                'users_ib.name',
                'users_ib.username',
                'typeMT4Accounts.namaAccount',
                'typeMT4Accounts.accountGroup',
                'typeMT4Accounts.status',
                'typeMT4Accounts.created_at',
                'typeMT4Accounts.id',
                )
              ->paginate(20);

      return view('platform.groups.activation',compact('list','menu'));
    }

    public function viewApproval($gn){
      $menu = 'listRequest';

      $grup = DB::table('typeMT4Accounts')
            ->join('users_ib','users_ib.id','typeMT4Accounts.categoryAccount')
            ->select(
              'users_ib.name',
              'users_ib.username',
              'typeMT4Accounts.namaAccount',
              'typeMT4Accounts.accountGroup',
              'typeMT4Accounts.status',
              'typeMT4Accounts.created_at',
              'typeMT4Accounts.id',
              'typeMT4Accounts.startAccNumber',
              'typeMT4Accounts.categoryAccount',
              )
            ->where('typeMT4Accounts.accountGroup',$gn)->first();
       //dd($grup);

      return view('platform.groups.approval',compact('grup','menu'));
    }

    public function processApprove(Request $request){

      $user = DB::table('users_ib')->where('id',$request->user)->first();
      $sib = DB::table('users_ib')->where('id',$user->seniorib)->first();
      $mib = DB::table('users_ib')->where('id',$user->masterib)->first();

      $test = [
        'sib' => $sib->groupMT5,
        'mib' => $mib->groupMT5,
        'folder' => $user->groupMT5,
        'group' => $request->accountGroup,
        'email' => $user->email
      ];
      //dd($test);
      $explode = explode('-',$request->accountGroup);
      $groupN = $explode[0]."-N-".$explode[1]."-".$explode[2];

      $response = Http::post(env('ACCOUNT_API').'go/create-group',[
          'sib' => $sib->groupMT5,
          'mib' => $mib->groupMT5,
          'folder' => $user->groupMT5,
          'group' => $request->accountGroup,
          'email' => $user->email
        ]);

      $responseN = Http::post(env('ACCOUNT_API').'go/create-group',[
        'sib' => $sib->groupMT5,
        'mib' => $mib->groupMT5,
        'folder' => $user->groupMT5,
        'group' => $groupN,
        'email' => $user->email
      ]);

      $update = DB::table('typeMT4Accounts')->where('accountGroup',$request->accountGroup)->update([
        'accountLeverage' => $request->accountLeverage,
        'accountMinLot' => $request->accountMinLot,
        'accountMinStep' => $request->accountMinStep,
        'accountSpread' => $request->accountSpread,
        'accountCom' => $request->accountCom,
        'status' => 'waiting',
        'approved_at' => date('Y-m-d H:i:s')
      ]);

      Alert::success( 'Berhasil!','Group '.$request->accountGroup.'berhasil dibuat untuk '.$request->name)->showConfirmButton('Okei dokei!', '#DB1430');
      return redirect()->route('groupRequest');
    }

    public function processActivate($id){

      $update = DB::table('typeMT4Accounts')->where('id',$id)->update([
        'status' => 'active',
      ]);

      Alert::success( 'Berhasil!','Group berhasil diaktivasi')->showConfirmButton('Okei dokei!', '#DB1430');
      return redirect()->route('groupActivation');
    }
}
