<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ReferralController extends Controller
{
    public function explore(){
      $menu = 'Referral';
      $usermenu = '';

      /*-----------------*/

      $counters = [];
      $users = DB::table('users_cabinet')->where('parent',profile()->id)->orderby('id','desc')->paginate(5);

      if (profile()->id_cms_privileges < 11) {
        return view('forbiden.level-11',compact('menu','users','counters'));
      }

      return view('affiliate.share-link',compact('menu','users','counters'));
    }

    public function showUser($id){
      return redirect()->back();
    }
}
