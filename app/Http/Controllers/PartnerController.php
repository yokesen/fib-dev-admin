<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;
use Validator;
use Alert;
use Illuminate\Support\Facades\Http;

class PartnerController extends Controller
{
    public function submitDepositCredit(Request $request){
      $rules = [
        'bank_name' => 'required|string|min:3|max:50',
        'account_number' => 'required|string|min:2|max:100',
        'account_name' => 'required|string|min:2|max:100',
        'amount' => 'required|numeric',
        'uuid' => 'required|string|min:32',
      ];

      $validator = Validator::make($request->all(), $rules);

      if($validator->fails()){
          Alert::error( 'GAGAL','Deposit! IDR '.$request->amount)->showConfirmButton('OK', '#DB1430');
          return redirect()->back()->withErrors($validator)->withInput($request->all());
      }

      $user = DB::table('users_ib')->where('uuid',$request->uuid)->first();


      if ($request->tipe_deposit == "deposit") {
        $amount = $request->amount;
        $transferTo = $request->transferTo;
      }elseif($request->tipe_deposit == "withdrawal"){
        $amount = $request->amount * -1 * (100 - $user->takingPositionPercentage) / 100;
        $credit = $request->amount *-1;
        $transferTo = 'bca';
      }

      $bankDetail = DB::table('bank_segre')->where('parent',$user->parent)->where('statusRekening','active')->first();

      $data = [
        'uuid' => $request->uuid,
        'amount' => $amount,
        'tipe_deposit' => $request->tipe_deposit,
        'currency' => 'IDR',
        'status' => 'pending',
        'credit' => $credit,
        'reason' => $request->tipe_deposit." by ".$user->username,
        'from_bank' => $bankDetail->namaBank,
        'from_name' => $bankDetail->namaRekening,
        'from_rekening' => $bankDetail->nomorRekening,
        'to_bank' => $request->bank_name,
        'to_name' => $request->account_name,
        'to_rekening' => $request->account_number
      ];

      // dd($data);

      $insert = DB::table('partner_deposits')->insert($data);
      Alert::success( 'Success','Deposit! IDR '.$request->amount)->showConfirmButton('OK', '#DB1430');
      return redirect()->back();
    }
}
