<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class DashboardController extends Controller
{
  public function viewDashboard(){
    $menu = 'dashboard';
    $usermenu = 'profile';
    $user = profile();
    /*-----------------*/
    $lastmonth = date('m',strtotime('- 1 month'));
    $profitLastmonth = db::table('lot_raw_data')->where('parent',profile()->id)->where('lotmonth',$lastmonth)->sum('totalProfit');
    $depositLastmonth = DB::table('deposits')->where('parent',profile()->id)->whereMonth('created_at',$lastmonth)->where('from_bank','!=','internal transfer')->where('status','approved')->sum('amount');
    $withdrawalLastmonth = DB::table('withdrawals')->where('parent',profile()->id)->whereMonth('created_at',$lastmonth)->whereNull('withdrawals.internal')->where('status','approved')->sum('approved_amount');
    $netmarginLastmonth = $depositLastmonth - $withdrawalLastmonth;
    $profitLastmonth = $profitLastmonth * -10000;

    if ($netmarginLastmonth < 1000000000) {
      $shareLast = 0.2;
    }elseif ($netmarginLastmonth >= 1000000000 && $netmarginLastmonth < 2000000000) {
      $shareLast = 0.3;
    }elseif ($netmarginLastmonth >= 2000000000 && $netmarginLastmonth < 3000000000) {
      $shareLast = 0.4;
    }elseif ($netmarginLastmonth >= 3000000000 && $netmarginLastmonth < 4000000000) {
      $shareLast = 0.5;
    }elseif ($netmarginLastmonth >= 4000000000) {
      $shareLast = 0.6;
    }

    $commLast = db::table('lot_raw_data')->where('parent',profile()->id)->where('lotmonth',$lastmonth)->sum('dataCommission');
    $comLastmonth = $commLast * -10000;

    $thismonth = date('m');
    $profitThismonth = db::table('lot_raw_data')->where('parent',profile()->id)->where('lotmonth',$thismonth)->sum('totalProfit');
    $depositThismonth = DB::table('deposits')->where('parent',profile()->id)->whereMonth('created_at',$thismonth)->where('from_bank','!=','internal transfer')->where('status','approved')->sum('amount');
    $withdrawalThismonth = DB::table('withdrawals')->where('parent',profile()->id)->whereMonth('created_at',$thismonth)->whereNull('withdrawals.internal')->where('status','approved')->sum('approved_amount');
    $netmarginThismonth = $depositThismonth - $withdrawalThismonth;
    $profitThismonth = $profitThismonth * -10000;

    if ($netmarginThismonth < 1000000000) {
      $toAchieve = 500000000;
      $shareThisMonth = 0.2;
    }elseif ($netmarginThismonth >= 1000000000 && $netmarginThismonth < 2000000000) {
      $toAchieve = 1000000000;
      $shareThisMonth = 0.3;
    }elseif ($netmarginThismonth >= 2000000000 && $netmarginThismonth < 3000000000) {
      $toAchieve = 2000000000;
      $shareThisMonth = 0.4;
    }elseif ($netmarginThismonth >= 3000000000 && $netmarginThismonth < 4000000000) {
      $toAchieve = 3000000000;
      $shareThisMonth = 0.5;
    }elseif ($netmarginThismonth >= 4000000000) {
      $toAchieve = 4000000000;
      $shareThisMonth = 0.6;
    }

    $commThis = db::table('lot_raw_data')->where('parent',profile()->id)->where('lotmonth',$thismonth)->sum('dataCommission');
    $comThisMonth = $commThis * -10000;
    $dayStart = date('z',strtotime(date('Y-m-1').'-1 day'));
    $today = date('z');
    $month = date('m');
    $counters = DB::table('lot_month_by_parent')->where('parent',profile()->id)->where('month',$month)->first();
    $vtoday = DB::table('lot_day_by_parent')->where('parent',profile()->id)->where('day',date('z'))->first();
    $daily = DB::table('lot_day_by_parent')->where('parent',profile()->id)->where('day','>',$dayStart)->where('day','<',$today+1)->get();
    $totalD = 0;
    foreach ($daily as $vd) {
      $totalD += $vd->volume;
    }
    if (count($daily)) {
      $avgDaily = $totalD/count($daily);
    }else{
      $avgDaily = 0;
    }

    return view('platform.dashboard',compact('menu',
        'lastmonth',
        'profitLastmonth',
        'depositLastmonth',
        'withdrawalLastmonth',
        'netmarginLastmonth',
        'shareLast',
        'comLastmonth',
        'thismonth',
        'profitThismonth',
        'depositThismonth',
        'withdrawalThismonth',
        'netmarginThismonth',
        'shareThisMonth',
        'comThisMonth',
        'toAchieve',
        'counters',
        'vtoday',
        'avgDaily',
      ));

  }
}
