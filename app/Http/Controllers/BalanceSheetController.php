<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class BalanceSheetController extends Controller
{
  public function viewBalance(){
    $menu = 'viewBalance';
    /*-------------------*/
    return view('platform.balance-sheet',compact('menu'));
  }
}
