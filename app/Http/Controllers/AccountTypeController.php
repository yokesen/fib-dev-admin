<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;
use Validator;
use Alert;

class AccountTypeController extends Controller
{
    public function typeList(){
      $menu = 'typeList';
      /*-------------------*/
      $masterType = DB::table('typeMT4MasterTemplate')->orderby('id','desc')->where('masterStatus','active')->paginate(20);

      return view('platform.account.type-list',compact('menu','masterType'));
    }

    public function addNew(){
      $menu = 'addNewAccount';
      /*-------------------*/
      $max = DB::table('typeMT4MasterTemplate')->max('masterNumber');

      if ($max) {
        $next = $max;
      }else{
        $next = 18;
      }
      return view('platform.account.add-type',compact('menu','next'));
    }

    public function store(Request $request){

      $rules = [
        'masterName' => 'required|string|min:3|max:50',
        'masterRate' => 'required|string|min:2|max:4',
        'masterGroup' => 'required|string|min:2|max:5',
        'masterNumber' => 'required|numeric|min:19|max:999'
      ];

      $validator = Validator::make($request->all(), $rules);

      if($validator->fails()){
          return redirect()->back()->withErrors($validator)->withInput($request->all());
      }

      $save = DB::table('typeMT4MasterTemplate')->insert([
        'masterName' => $request->masterName,
        'masterGroup' => $request->masterGroup,
        'masterRate' => $request->masterRate,
        'masterNumber' => $request->masterNumber,
        'masterStatus' => 'active'
      ]);

      return redirect()->route('viewAccountTypeList');
    }
}
