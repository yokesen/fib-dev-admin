<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class WinningController extends Controller
{
  public function viewWinning(){
    $menu = 'viewWinning';
    /*-------------------*/
    $users = DB::table('users_cabinet')->orderby('id','desc')->where('parent',profile()->id)->where('status','winning')->paginate(20);
    return view('platform.winning',compact('menu','users'));
  }
}
