<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class depositPartnerController extends Controller
{
    public function list(){
      $menu = 'partnerDeposit';

      $requests = DB::table('partner_deposits')
                  ->join('users_ib','users_ib.uuid','partner_deposits.uuid')
                  ->orderby('partner_deposits.id','desc')
                  ->select(
                    'partner_deposits.id',
                    'partner_deposits.uuid',
                    'partner_deposits.created_at',
                    'partner_deposits.metatrader',
                    'partner_deposits.from_rekening',
                    'partner_deposits.from_bank',
                    'partner_deposits.from_name',
                    'partner_deposits.to_bank',
                    'partner_deposits.to_rekening',
                    'partner_deposits.to_name',
                    'partner_deposits.approved_amount',
                    'partner_deposits.amount',
                    'partner_deposits.currency',
                    'partner_deposits.currency_rate',
                    'partner_deposits.status',
                    'partner_deposits.tipe_deposit',
                    'partner_deposits.photo as bukti',
                    'users_ib.photo as photo',
                    'users_ib.username'
                    )
                  ->paginate(50);
      $count = DB::table('partner_deposits')->where('status','pending')->count();


      return view('platform.balance-statement.deposit',compact('menu','requests','count'));
    }

    public function action($id){
      $menu = 'partnerDeposit';

      $deposit = DB::table('partner_deposits')
                  ->where('partner_deposits.id',$id)
                  ->join('users_ib','users_ib.uuid','partner_deposits.uuid')
                  ->orderby('partner_deposits.id','asc')
                  ->where('partner_deposits.status','pending')
                  ->select(
                    'partner_deposits.id',
                    'partner_deposits.uuid',
                    'partner_deposits.created_at',
                    'partner_deposits.metatrader',
                    'partner_deposits.from_rekening',
                    'partner_deposits.from_bank',
                    'partner_deposits.from_name',
                    'partner_deposits.to_bank',
                    'partner_deposits.to_rekening',
                    'partner_deposits.to_name',
                    'partner_deposits.approved_amount',
                    'partner_deposits.amount',
                    'partner_deposits.currency',
                    'partner_deposits.currency_rate',
                    'partner_deposits.tipe_deposit',
                    'partner_deposits.status',
                    'partner_deposits.photo as bukti',
                    'users_ib.photo as photo',
                    'users_ib.username'
                    )
                  ->first();

                  $client = DB::table('users_ib')->where('uuid',$deposit->uuid)->first();
                  $bank = DB::table('banks')->where('uuid',$deposit->uuid)->first();

      return view('platform.balance-statement.edit',compact('menu','deposit','client','bank'));
    }

  public function process(Request $request){

    $deposit = DB::table('partner_deposits')
                ->where('partner_deposits.id',$request->id)
                ->join('users_ib','users_ib.uuid','partner_deposits.uuid')
                ->orderby('partner_deposits.id','asc')
                ->where('partner_deposits.status','pending')
                ->select(
                  'partner_deposits.id',
                  'partner_deposits.uuid',
                  'partner_deposits.created_at',
                  'partner_deposits.metatrader',
                  'partner_deposits.from_rekening',
                  'partner_deposits.from_bank',
                  'partner_deposits.from_name',
                  'partner_deposits.to_bank',
                  'partner_deposits.to_rekening',
                  'partner_deposits.to_name',
                  'partner_deposits.approved_amount',
                  'partner_deposits.amount',
                  'partner_deposits.currency',
                  'partner_deposits.currency_rate',
                  'partner_deposits.tipe_deposit',
                  'partner_deposits.status',
                  'partner_deposits.photo as bukti',
                  'users_ib.photo as photo',
                  'users_ib.username'
                  )
                ->first();
                //dd($deposit);
    if ($deposit->tipe_deposit == "deposit") {
      $tipe_mutasi = "In";
      $state = "setoran dari ";
    }elseif($deposit->tipe_deposit == "withdrawal"){
      $tipe_mutasi = "Out";
      $state = "transfer ke ";
    }
    $client = DB::table('users_ib')->where('uuid',$deposit->uuid)->first();
    $saldoGlobal = DB::table('statement_balance_sheet')->orderby('id','desc')->first();
    $global = $saldoGlobal->saldo_global;
    $saldoGet = DB::table('statement_balance_sheet')->where('parent',$client->id)->orderby('id','desc')->first();
    if (!$saldoGet) {
      $saldo = 0;
    }else{
      $saldo = $saldoGet->saldo;
    }
     //dd($global,$saldo);
    $update = DB::table('partner_deposits')->where('id',$request->id)->update([
      'approved_amount' => $deposit->amount,
      'currency_rate' => 1,
      'status' => 'approved',
      'updated_at' => date('Y-m-d H:i:s'),
      'process_by' => profile()->id
    ]);



    $lotdate = date('z',strtotime($deposit->created_at));
    $lotweek = date('W',strtotime($deposit->created_at));
    $lotmonth = date('m',strtotime($deposit->created_at));
    $lotyear = date('Y',strtotime($deposit->created_at));
    $humanTime = date('Y-m-d H:i:s');

    $insert_balance_statement = DB::table('statement_balance_sheet')->insert([
      'parent' => $client->id,
      'tanggal' => $humanTime,
      'sumber' => "BR-".$deposit->id,
      'tipe_transaksi' => "settlement",
      'keterangan' => $state.$deposit->username,
      'mutasi' => $deposit->amount,
      'tipe_mutasi' => $tipe_mutasi,
      'saldo' => $saldo+$deposit->amount,
      'saldo_global' => $global+$deposit->amount,
      'dayoty' => $lotdate,
      'weekoty' => $lotweek,
      'monthoty' => $lotmonth,
      'year' => $lotyear
    ]);
    return redirect()->route('viewDepositList');
  }

  public function reject(Request $request){

    $update = DB::table('partner_deposits')->where('id',$request->id)->update([
      'status' => 'rejected',
      'updated_at' => date('Y-m-d H:i:s'),
      'process_by' => profile()->id,
      'reason' => $request->reason
    ]);
    return redirect()->route('viewDepositList');
  }

  public function viewWithdrawalPartner($uuid){
    $user = DB::table('users_ib')->where('uuid',$uuid)->first();
    $bank = DB::table('bank_segre')->where('parent',$user->id)->first();
    // dd($user,$bank);
    return view('platform.partner.partner-withdrawal',compact('bank','user'));
  }
}
