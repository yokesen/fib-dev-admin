<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Jenssegers\Agent\Agent;
use DB;
use Cookie;
use Session;

class LoginSocialite extends Controller
{
  public function redirectToSocialite($provider)
  {

      return Socialite::driver($provider)->redirect();
  }

  public function handleSocialiteCallback($provider)
  {
      $agent = new Agent();
      if($agent->isPhone()){
        $screen = "Phone";
      }elseif($agent->isTablet()){
        $screen = "Tablet";
      }elseif($agent->isDesktop()){
        $screen = "Desktop";
      }
      $platform = $agent->platform();
      $version_platform = $agent->version($platform);
      $device = $agent->device();
      $browser = $agent->browser();
      $version_browser = $agent->version($browser);
      $languages = serialize($agent->languages());
      $robot = $agent->robot();
      $ip = request()->ip();

      $users = Socialite::driver($provider)->user();

      $uuid = sha1($users->email).time();

      $exist = DB::table('users_ib')->where('email',$users->email)->count();
      //dd($ip,$users,$users->id,$users->email,$uuid,$exist,Cookie::get(),Cookie::get('utm_source'),$platform,$version_platform,$device,$browser,$version_browser,$languages,$robot);
      if($exist < 1){
        $insert = DB::table('users_ib')->insertGetId([
          'name' => $users->name,
          'email' => $users->email,
          'email_validation' => 'true',
          'email_verification' => 'verified',
          'photo' => $users->avatar,
          'providerId' => $users->id,
          'providerOrigin' => $provider,
          'id_cms_privileges' => '7',
          'parent' => Cookie::get('ref'),
          'utm_source' => Cookie::get('utm_source'),
          'utm_medium' => Cookie::get('utm_medium'),
          'utm_campaign' => Cookie::get('utm_campaign'),
          'utm_term' => Cookie::get('utm_term'),
          'utm_content' => Cookie::get('utm_content'),
          'uuid' => $uuid,
          'ipaddress' => $ip,
          'screen' => $screen,
          'platform' => $platform,
          'platformVersion' => $version_platform,
          'device' => $device,
          'browser' => $browser,
          'browserVersion' => $version_browser,
          'language' => $languages,
          'robot' => $robot
        ]);

        $params = ['linkcode' => ENV('APP_URL').'/onboarding/mail/'.$users->email.'/verify/'.$uuid];

        $mail = DB::table('daftar_queue_email')->insert([
          'type' => 'transactional',
          'content' => 'register',
          'uuid' => $uuid,
          'params' => serialize($params)
        ]);
      }else{
        $update = DB::table('users_ib')->where('email',$users->email)->update([
          'providerId' => $users->id,
          'providerOrigin' => $provider,
          'email_validation' => 'true',
          'email_verification' => 'verified',
        ]);
      }

      $login = DB::table('users_ib')->where('email',$users->email)->first();
      $priv = DB::table("cms_privileges")->where("id", $login->id_cms_privileges)->first();
      $roles = DB::table('cms_privileges_roles')->where('id_cms_privileges', $login->id_cms_privileges)->join('cms_moduls', 'cms_moduls.id', '=', 'id_cms_moduls')->select('cms_moduls.name', 'cms_moduls.path', 'is_visible', 'is_create', 'is_read', 'is_edit', 'is_delete')->get();
      $menus = DB::table('cms_menus_privileges')->where('cms_menus_privileges.id_cms_privileges', $login->id_cms_privileges)->join('cms_menus', 'cms_menus.id', 'cms_menus_privileges.id_cms_menus')->get();
      $remember_token = time().$uuid;
      $remember = DB::table('users_ib')->where('email',$users->email)->update([
        'remember_token' => $remember_token
      ]);
      Cookie::queue('remember_token', $remember_token, 31536000);
      Session::put('user', $login);
      Session::put('priv', $priv);
      Session::put('modul', $roles);
      Session::put('menu', $menus);

      return redirect()->route('viewDashboard');
  }

  public function handleSocialiteFailed($provider)
  {
    dd("failed");
  }

  public function handleSocialiteUnsubscribe($provider)
  {
    dd("unsubscribe");
  }
}
