<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Cookie;
use Session;
use Illuminate\Support\Facades\Hash;
use Alert;

class LabController extends Controller
{
  public function userStatus()
  {
    $get = DB::table('users_cabinet')->whereNULL('updated_at')->where('parent', '>', 0)->limit(100)->get();
    foreach ($get as $x) {
      $meta = DB::table('typeMT4AvailabeAccount')->where('uuid', $x->uuid)->count();
      if ($meta > 0) {
        $update = DB::table('users_cabinet')->where('id', $x->id)->update([
          'status' => 'potential',
          'updated_at' => date('Y-m-d H:i:s')
        ]);
      }
    }
  }

  public function userWinning()
  {
    $get = DB::table('users_cabinet')->where('cekAnalytic', '2')->where('parent', '>', 0)->limit(100)->get();
    foreach ($get as $x) {
      $meta = DB::table('deposits')->where('uuid', $x->uuid)->where('status', 'approved')->count();
      if ($meta > 0) {
        $update = DB::table('users_cabinet')->where('id', $x->id)->update([
          'status' => 'winning',
          'cekAnalytic' => '3'
        ]);
      }
    }
  }

  public function parentDeposit()
  {
    $get = DB::table('deposits')->where('flag_parent', 1)->limit(100)->get();
    foreach ($get as $key => $value) {
      $user = DB::table('users_cabinet')->where('uuid', $value->uuid)->first();
      $update = DB::table('deposits')->where('uuid', $value->uuid)->update([
        'parent' => $user->parent,
        'flag_parent' => 2
      ]);
      //echo $value->uuid."|".$user->parent."<br>";
    }
  }

  public function parentWithdrawal()
  {
    $get = DB::table('withdrawals')->where('flag_parent', 1)->limit(100)->get();
    foreach ($get as $key => $value) {
      $user = DB::table('users_cabinet')->where('uuid', $value->uuid)->first();
      $update = DB::table('withdrawals')->where('uuid', $value->uuid)->update([
        'parent' => $user->parent,
        'flag_parent' => 2
      ]);
      //echo $value->uuid."|".$user->parent."<br>";
    }
  }

  public function parentMetatrader()
  {
    $get = DB::table('typeMT4AvailabeAccount')->where('flag_parent', 1)->limit(100)->get();
    foreach ($get as $key => $value) {
      $user = DB::table('users_cabinet')->where('uuid', $value->uuid)->first();
      $update = DB::table('typeMT4AvailabeAccount')->where('uuid', $value->uuid)->update([
        'parent' => $user->parent,
        'flag_parent' => 2
      ]);
      //echo $value->uuid."|".$user->parent."<br>";
    }
  }

  public function bikinParent()
  {
    $pull = DB::table('users_cabinet')->distinct()->select('parent')->get();

    foreach ($pull as $key => $value) {
      if ($value->parent != '0' || !empty($value->parent)) {
        $client = DB::table('users_cabinet')->where('id', $value->parent)->first();

        if ($client) {
          $insert = DB::table('users_ib')->insert([
            'id' => $client->id,
            'uuid' => $client->uuid,
            'username' => $client->username,
            'name' => $client->name,
            'photo' => $client->photo,
            'email' => $client->email,
            'phone' => $client->phone,
            'whatsapp' => $client->whatsapp,
            'percentShare' => 0,
            'allowOperate' => 'no',
            'pointMarkupSpread' => 0,
            'depositGuarantee' => 0,
            'mobile' => $client->mobile,
            'dob' => $client->dob,
            'address' => $client->address,
            'city' => $client->city,
            'photoKTP' => $client->photoKTP,
            'photoTabungan' => $client->photoTabungan,
            'disclaimer' => $client->disclaimer,
            'consent_wa' => $client->consent_wa,
            'consent_email' => $client->consent_email,
            'consent_marketing' => $client->consent_marketing,
            'password' => $client->password,
            'remember_token' => $client->remember_token,
            'id_cms_privileges' => $client->id_cms_privileges,
            'email_validation' => $client->email_validation,
            'email_verification' => $client->email_verification,
            'providerId' => $client->providerId,
            'providerOrigin' => $client->providerOrigin,
            'parent' => $client->parent,
            'first_landing' => $client->first_landing,
            'utm_campaign' => $client->utm_campaign,
            'utm_source' => $client->utm_source,
            'utm_medium' => $client->utm_medium,
            'utm_content' => $client->utm_content,
            'utm_term' => $client->utm_term,
            'ipaddress' => $client->ipaddress,
            'device' => $client->device,
            'screen' => $client->screen,
            'platform' => $client->platform,
            'platformVersion' => $client->platformVersion,
            'browser' => $client->browser,
            'browserVersion' => $client->browserVersion,
            'language' => $client->language,
            'robot' => $client->robot,
            'cekAnalytic' => $client->cekAnalytic,
            'onboardingStep' => $client->onboardingStep,
            'status' => $client->status,
            'created_at' => $client->created_at,
            'updated_at' => $client->updated_at,
            'deleted_at' => $client->deleted_at
          ]);
        }
      }


    }
  }

  public function strukturib()
  {
    $fetchSIB = DB::table('users_ib')->where('parent', 0)->get();
    foreach ($fetchSIB as $sib) {
      $walletnya = DB::table('wallets')->where('uuid', $sib->uuid)->select('amount')->first();
      if ($walletnya != null) {
        echo $sib->id . " -> " . $sib->name . " (" . $sib->username . ") ".$sib->takingPositionPercentage." % => sisa saldo [" . number_format($walletnya->amount,0,'.',',') . "]<br>";

      } else {

        echo $sib->id . " -> " . $sib->name . " (" . $sib->username . ") ".$sib->takingPositionPercentage." % => sisa saldo [" . 0 . "]<br>";
      }
      $partnerDeposit = DB::table('partner_deposits')->where('uuid',$sib->uuid)->where('status','approved')->get();
          foreach ($partnerDeposit as $pdp) {
            echo ".........................................................-----> deposit warranty: [<b>".number_format($pdp->amount,0,'.',',')."</b>] ----> utk CREDIT: [".number_format($pdp->credit,0,'.',',')."]<br>";
          }
          $depositTerpakai = DB::table('wallet_book')->where('wallet', $sib->uuid)->orderby('id','asc')->get();
          foreach ($depositTerpakai as $dtp) {
            echo ".........................................................-----> wallet_book: [".number_format($dtp->amount,0,'.',',')."] [".number_format($dtp->saldo_awal,0,'.',',')."] [".number_format($dtp->saldo_akhir,0,'.',',')."] ----> utk $dtp->type <br>";
          }
      $fetchMIB = DB::table('users_ib')->where('parent', $sib->id)->get();
      foreach ($fetchMIB as $mib) {
        $walletnya = DB::table('wallets')->where('uuid', $mib->uuid)->select('amount')->first();
        if ($walletnya != null) {
          echo "|________> " . $mib->id . " -> " . $mib->name . " (" . $mib->username . ") ".$mib->takingPositionPercentage." % => sisa saldo [" . number_format($walletnya->amount,0,'.',',') . "]<br>";

        } else {

          echo "|________> " . $mib->id . " -> " . $mib->name . " (" . $mib->username . ") ".$mib->takingPositionPercentage." % => sisa saldo [" . 0 . "]<br>";
        }
        $partnerDeposit = DB::table('partner_deposits')->where('uuid',$mib->uuid)->where('status','approved')->get();
          foreach ($partnerDeposit as $pdp) {
            echo ".........................................................-----> deposit warranty: [<b>".number_format($pdp->amount,0,'.',',')."</b>] ----> utk CREDIT: [".number_format($pdp->credit,0,'.',',')."]<br>";
          }
          $depositTerpakai = DB::table('wallet_book')->where('wallet', $mib->uuid)->orderby('id','asc')->get();
          foreach ($depositTerpakai as $dtp) {
            echo ".........................................................-----> wallet_book: [".number_format($dtp->amount,0,'.',',')."] [".number_format($dtp->saldo_awal,0,'.',',')."] [".number_format($dtp->saldo_akhir,0,'.',',')."] ----> utk $dtp->type <br>";
          }

        $fetchIB = DB::table('users_ib')->where('parent', $mib->id)->get();
        foreach ($fetchIB as $ib) {
          $walletnya = DB::table('wallets')->where('uuid', $ib->uuid)->select('amount')->first();
          if ($walletnya != null) {
            echo ".................|________> " . $ib->id . " -> " . $ib->name . " (" . $ib->username . ") ".$ib->takingPositionPercentage." % => sisa saldo [" . number_format($walletnya->amount,0,'.',',') . "]<br>";

          } else {

            echo ".................|________> " . $ib->id . " -> " . $ib->name . " (" . $ib->username . ") ".$ib->takingPositionPercentage." % => sisa saldo [" . 0 . "]<br>";
          }
          $partnerDeposit = DB::table('partner_deposits')->where('uuid',$ib->uuid)->where('status','approved')->get();
          foreach ($partnerDeposit as $pdp) {
            echo ".........................................................-----> deposit warranty: [<b>".number_format($pdp->amount,0,'.',',')."</b>] ----> utk CREDIT: [".number_format($pdp->credit,0,'.',',')."]<br>";
          }
          $depositTerpakai = DB::table('wallet_book')->where('wallet', $ib->uuid)->orderby('id','asc')->get();
          foreach ($depositTerpakai as $dtp) {
            echo ".........................................................-----> wallet_book: [".number_format($dtp->amount,0,'.',',')."] [".number_format($dtp->saldo_awal,0,'.',',')."] [".number_format($dtp->saldo_akhir,0,'.',',')."] ----> utk $dtp->type <br>";
          }
        }
      }
    }
  }
}
