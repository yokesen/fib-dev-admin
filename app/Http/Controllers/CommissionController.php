<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class CommissionController extends Controller
{
  public function viewCommission(){
    $menu = 'viewCommission';
    /*-------------------*/
    /*-----------------*/
      $totalRef = DB::table('users_cabinet')->where('parent',profile()->id)->count();
      $dayStart = date('z',strtotime(date('Y-m-1').'-1 day'));
      $today = date('z');
      $month = date('m',strtotime('- 1 month'));
      $counters = DB::table('lot_month_by_parent')->where('parent',profile()->id)->where('month',$month)->first();
      $vtoday = DB::table('lot_day_by_parent')->where('parent',profile()->id)->where('day',date('z'))->first();
      $active7 = DB::table('lot_month_by_client')
                              ->join('users_cabinet','users_cabinet.uuid','lot_month_by_client.uuid')
                              ->where('lot_month_by_client.parent',profile()->id)->where('lot_month_by_client.month',$month)
                              ->orderby('lot_month_by_client.volume','desc')
                              ->limit(7)
                              ->select('users_cabinet.name','users_cabinet.photo')->get();
      $countActive = DB::table('lot_month_by_client')->where('parent',profile()->id)->where('month',$month)->count();
      $restActive = $countActive - 7;
      $year = DB::table('lot_year_by_parent')->where('parent',profile()->id)->where('year',date('Y'))->first();

      $daily = DB::table('lot_day_by_parent')->where('parent',profile()->id)->where('day','>',$dayStart)->where('day','<',$today+1)->get();
      $totalD = 0;
      foreach ($daily as $vd) {
        $totalD += $vd->volume;
      }
      if (count($daily)) {
        $avgDaily = $totalD/count($daily);
      }else{
        $avgDaily = 0;
      }

      $levels = DB::table('users_cabinet')->where('parent',profile()->id)->distinct()->select('id_cms_privileges')->get();
      $bg = [
        "#B4F8C8",
        "#FFAEBC",
        "#36EEE0",
        "#A0E7E5",
        "#FBE7C6",
        "#4C5270",
        "#F652A0",
        "#BCECE0"
      ];
      foreach ($levels as $x => $level) {
        $getCount = DB::table('users_cabinet')->where('parent',profile()->id)->where('id_cms_privileges',$level->id_cms_privileges)->count();
        $role = DB::table('cms_privileges')->where('id',$level->id_cms_privileges)->first();
        $data[] = [
          'namaPriv' => $role->name,
          'count' => $getCount,
          'color' => $bg[$x]
        ];
      }

      $actives = DB::table('lot_month_by_client')
                              ->join('users_cabinet','users_cabinet.uuid','lot_month_by_client.uuid')
                              ->where('lot_month_by_client.parent',profile()->id)->where('lot_month_by_client.month',$month)
                              ->orderby('lot_month_by_client.volume','desc')
                              ->select(
                                'users_cabinet.name',
                                'users_cabinet.photo',
                                'users_cabinet.uuid',
                                'users_cabinet.email',
                                'users_cabinet.id_cms_privileges',
                                'lot_month_by_client.volume'
                                )->paginate(6);
    return view('platform.commission',compact('totalRef','actives','menu','counters','data','year','vtoday','active7','countActive','restActive','bg','avgDaily'));
  }
}
