<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class LeadController extends Controller
{
  public function viewLead(){
    $menu = 'viewLead';
    /*-------------------*/
    $users = DB::table('users_cabinet')->orderby('id','desc')->where('parent',profile()->id)->where('status','lead')->paginate(20);
    return view('platform.lead',compact('menu','users'));
  }
}
