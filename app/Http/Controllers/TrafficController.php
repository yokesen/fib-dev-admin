<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;

class TrafficController extends Controller
{
  public function viewTraffic(){
    $menu = 'viewTraffic';
    /*-------------------*/
    return view('platform.traffic',compact('menu'));
  }
}
