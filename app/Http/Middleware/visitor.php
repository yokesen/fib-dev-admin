<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use DB;
use Cookie;
use Session;
use Jenssegers\Agent\Agent;
use URL;

class visitor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        date_default_timezone_set("Asia/Jakarta");

        if($request->ref){
          Cookie::queue('ref', $request->ref, 31536000);
          $ref = $request->ref;
        }else{
          $ref = Cookie::get('ref');
        }

        if($request->utm_source){
          Cookie::queue('utm_source', $request->utm_source, 31536000);
          $utm_source = $request->utm_source;
        }else{
          $utm_source = Cookie::get('utm_source');
        }

        if($request->utm_medium){
          Cookie::queue('utm_medium', $request->utm_medium, 31536000);
          $utm_medium = $request->utm_medium;
        }else{
          $utm_medium = Cookie::get('utm_medium');
        }

        if($request->utm_campaign){
          Cookie::queue('utm_campaign', $request->utm_campaign, 31536000);
          $utm_campaign = $request->utm_campaign;
        }else{
          $utm_campaign = Cookie::get('utm_campaign');
        }

        if($request->utm_term){
          Cookie::queue('utm_term', $request->utm_term, 31536000);
          $utm_term = $request->utm_term;
        }else{
          $utm_term = Cookie::get('utm_term');
        }

        if($request->utm_content){
          Cookie::queue('utm_content', $request->utm_content, 31536000);
          $utm_content = $request->utm_content;
        }else{
          $utm_content = Cookie::get('utm_content');
        }

        $visitke = Cookie::get('visit');
        $visitke +=1;
        Cookie::queue('visit', $visitke , 31536000);

        return $next($request);
    }
}
