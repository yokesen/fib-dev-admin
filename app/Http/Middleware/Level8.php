<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Session;
use DB;

class Level8
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
      if(profile()->id_cms_privileges > 8){

        $menus = DB::table('cms_menus_privileges')->where('cms_menus.is_active', 1)->where('cms_menus_privileges.id_cms_privileges', profile()->id_cms_privileges)->where('cms_menus.parent_id', '0')->join('cms_menus', 'cms_menus.id', 'cms_menus_privileges.id_cms_menus')->orderby('cms_menus.sorting','asc')->get();
        Session::put('menu',$menus);
        return $next($request);
      }else{
        return redirect()->route('levelall');
      }
    }
}
